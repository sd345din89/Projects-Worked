package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.referral.AbstractCreateReferral;
import com.strobilanthes.forrestii.model.referral.Referral;
import com.strobilanthes.forrestii.model.skillset.UserSkillsSet;
import com.strobilanthes.forrestii.model.wallpost.RefferalPost;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;

//dint change anything
public class ReferSomeOneFragment extends BaseFragment<DBAccess> implements OnClickListener{

	@InjectView(R.id.refersomeone_list)						private ListView lv_Friends;
	@InjectView(R.id.contacts_txt_emptyview)				private TextView txt_emptyview;
	@Inject Referral 										referralModel;
	
	private Typeface mFontLight,mFontDark;
	
	private ArrayList<FriendsList>T2FriendsFilterAL = new ArrayList<FriendsList>();
	private CustomSearchAdpater adapter;
	private RefferalPost refferalPost = null;
	private int postId;
	private int mWallPosition = -1;
	private int postByUserId = 0 , skillSetId = 0;
	
	public static ReferSomeOneFragment newInstance(int postId, int postByUserId, int position, int skillSetId) {
		 ReferSomeOneFragment frag = new ReferSomeOneFragment();
         Bundle args = new Bundle();
         args.putInt("postId", postId);
         args.putInt("position", position);
         args.putInt("postByUserId", postByUserId);
         args.putInt("skillSetId", skillSetId);
         frag.setArguments(args);
         return frag;
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.act_refersomeone, null);

		referralModel.setContext(getActivity());
		
		super.initilizeContents();

		return view;
	}
	
 
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		postId = getArguments().getInt("postId",0);
		postByUserId = getArguments().getInt("postByUserId");
		mWallPosition = getArguments().getInt("position",0);
		skillSetId = getArguments().getInt("skillSetId", 0);
		ArrayList<FriendsList> T2FriendsList = getHelper().getDBT2Friends();
		if (T2FriendsList != null) {
			
			for (FriendsList friendsList : T2FriendsList) {
				if(friendsList.getFriendUserProfile().getUserId() != postByUserId){
					for(UserSkillsSet userSkillsSet : friendsList.getFriendUserProfile().getUserSkillSets()){
						if(userSkillsSet.getSkills().getSkillId() == skillSetId)
							T2FriendsFilterAL.add(friendsList);
					}
					
				}
			}
			adapter = new CustomSearchAdpater(getActivity(),
					T2FriendsFilterAL);
			
			txt_emptyview.setText("No one found with this skillset, please create a quick card for them.");
			lv_Friends.setEmptyView(txt_emptyview);
			lv_Friends.setAdapter(adapter);
		}
		
		//fontStyles();
	}
	

	/*private void SearchEditTextWatcher(String string) {
		TextWatcher filterTextWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {
				

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {
				

			}

			@Override
			public void afterTextChanged(Editable s) {
				
				if (searchEdtTxt.getText().toString().length() > 0) {
					ArrayList<FriendsList> contactsfilterAl = new ArrayList<FriendsList>();
					for (int i = 0; i < T2FriendsFilterAL
							.size(); i++) {
						if (T2FriendsFilterAL
								.get(i)
								.getFriendUserProfile().getFirstName()
								.toLowerCase()
								.contains(
										searchEdtTxt.getText()
												.toString().toLowerCase())) {

							contactsfilterAl.add(T2FriendsFilterAL.get(i));

						}
						adapter.updateContactAL(contactsfilterAl);

					}

				} else {
					adapter.updateContactAL(T2FriendsFilterAL);
				}
			}
		};
		searchEdtTxt.addTextChangedListener(filterTextWatcher);
	}*/
	
	
	@Override
	public void onResume(){
		super.onResume();
		referralModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		referralModel.removeChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("createFriendReferral")) {
			AbstractCreateReferral abstractCreateReferral = referralModel.abstractCreateReferral();
			if(abstractCreateReferral != null){
				if(abstractCreateReferral.getStatus() == 1){
					Intent intent = new Intent();
					Bundle bundle = new Bundle(); 
					if(refferalPost != null)
					bundle.putSerializable("refferalPost", refferalPost);
					bundle.putInt("pos", mWallPosition);
					intent.putExtras(bundle);
					getActivity().setResult(88, intent);
					getActivity().finish();
				}else{
					Util.showAlert(getActivity(), "Message",abstractCreateReferral.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}
		}
	}
	
	@Override
	public void onClick(View v) {
		Util.hideSoftKeyboard(getActivity(), v);

		switch (v.getId()) {
		
		case R.id.login_screen_forgetpassword_txt:
			
			break;
		case R.id.login_screen_btnLogin:
			
			break;
		default:
			break;
		}
	}

	class CustomSearchAdpater extends BaseAdapter {

		private LayoutInflater mLayoutInflater;
		private ImageLoadingListener animateFirstListener;
		ArrayList<FriendsList> abstractFriendsLists;
		String listHeader;
		private static final int TYPE_ITEM = 0;
		private static final int TYPE_SEPARATOR = 1;
		private static final int TYPE_MAX_COUNT = TYPE_SEPARATOR + 1;

		public CustomSearchAdpater(FragmentActivity activity,
				ArrayList<FriendsList> abstractFriendsLists) {

			this.abstractFriendsLists = abstractFriendsLists;
			mLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public void updateContactAL(ArrayList<FriendsList> abstractFriendsLists) {
			this.abstractFriendsLists = abstractFriendsLists;
			notifyDataSetChanged();
			
		}

		@Override
		public int getCount() {
			return abstractFriendsLists.size();
		}

		@Override
		public FriendsList getItem(int position) {
			return abstractFriendsLists.get(position);
		}

		@Override
		public int getItemViewType(int position) {
			if (position == 0 || position == 20) {
				return TYPE_SEPARATOR;
			} else {
				return TYPE_ITEM;
			}
		}

		@Override
		public int getViewTypeCount() {
			return TYPE_MAX_COUNT;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,ViewGroup parent) {
			View v = convertView;
			ViewHolder holder = null;
			int type = getItemViewType(position);
			if (v == null) {
				v = mLayoutInflater.inflate(R.layout.act_refersomeone_list_item, null);
				holder = new ViewHolder();
				holder.txt_userName = (TextView) v
						.findViewById(R.id.refersomeone_list_item_txt_name);
				holder.txt_jobrole = (TextView) v
						.findViewById(R.id.refersomeone_list_item_txt_skills);
				holder.txt_mobile = (TextView) v
						.findViewById(R.id.refersomeone_list_item_txt_mobilenum);
				holder.img_user = (RoundedImageView) v
						.findViewById(R.id.refersomeone_list_item_img_user);
				holder.img_referral = (ImageView) v
						.findViewById(R.id.refersomeone_list_item_btn_done);
				/*holder.rating_bar = (RatingBar) v
						.findViewById(R.id.refersomeone_subview_ratingBar);*/
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			holder.img_referral.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					
					new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT)
					.setMessage("Do you want to refer "+abstractFriendsLists.get(position).getFriendUserProfile().getFirstName()+"?")
					.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							JSONObject jobject = new JSONObject();
							
							refferalPost = new RefferalPost();
							refferalPost.setPick(false);
							refferalPost.setRating(null);
							refferalPost.setReferralFromUserProfile(getHelper().getDBUserProfile());
							refferalPost.setReferralId(0);
							refferalPost.setReferredQuickCard(null);
							
							refferalPost.setReferredUserProfile(abstractFriendsLists.get(position).getFriendUserProfile());
							
							try {
								
								jobject.put("referralFromUserProfile", new JSONObject().put("userId", getHelper().getUserId()));
								jobject.put("referredUserProfile", new JSONObject().put("userId",abstractFriendsLists.get(position).getFriendUserProfile().getUserId() ));
								jobject.put("posts", new JSONObject().put("postId", postId).put("postByUserProfile", new JSONObject().put("userId", postByUserId)));
								
								referralModel.createFriendReferral(Constant.CREATE_FRIEND_REFERRAL, jobject);
								
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}).setNegativeButton("Cancel",null).show();
				}
			});
			// holder.txt_headerName.setTypeface(tf);
			holder.txt_userName.setTypeface(mFontLight);
			holder.txt_jobrole.setTypeface(mFontLight);
			holder.txt_mobile.setTypeface(mFontLight);

			holder.txt_userName.setSelected(true);
			holder.txt_jobrole.setSelected(true);
			holder.txt_userName.setText(abstractFriendsLists.get(position).getFriendUserProfile().getFirstName());
			holder.txt_mobile.setText(abstractFriendsLists.get(position).getFriendUserProfile().getMobile());
			ArrayList<UserSkillsSet> userSkillsSetsArrayList = abstractFriendsLists.get(position).getFriendUserProfile().getUserSkillSets();
			holder.txt_jobrole.setText("");
			
			if(userSkillsSetsArrayList != null && userSkillsSetsArrayList.size() > 0){
				for (UserSkillsSet userSkillsSet : userSkillsSetsArrayList) {
					if(userSkillsSet.isPrimarySkill()){
						holder.txt_jobrole.setText(userSkillsSet.getSkills().getSkillName());
						break;
					}
				}
			}
				
			float a = abstractFriendsLists.get(position).getFriendUserProfile().getTotalRating();
			int d = (int) Math.ceil(a);
			
			//holder.rating_bar.setRating(d);
			ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + abstractFriendsLists.get(position).getFriendUserProfile().getPhotoID(), holder.img_user, options,animateFirstListener);

			return v;
		}

		class ViewHolder {
			private TextView txt_userName, txt_jobrole,txt_mobile;
			private ImageView img_referral;
			private RoundedImageView img_user;
			//private RatingBar rating_bar;
		}
	}

}

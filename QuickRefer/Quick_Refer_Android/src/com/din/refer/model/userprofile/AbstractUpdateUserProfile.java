package com.strobilanthes.forrestii.model.userprofile;

import java.util.ArrayList;

import com.strobilanthes.forrestii.model.settings.SettingsDetails;


public class AbstractUpdateUserProfile{

	private String error;
	private int status;
	private UpdateUserProfileResponse response;
	
	public UpdateUserProfileResponse getResponse() {
		return response;
	}
	public void setResponse(UpdateUserProfileResponse response) {
		this.response = response;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public class UpdateUserProfileResponse{
		private ArrayList<SettingsDetails> Settings;
		private AbstractUserProfile UserProfile;
		
		public ArrayList<SettingsDetails> getSettings() {
			return Settings;
		}
		public void setSettings(ArrayList<SettingsDetails> settings) {
			Settings = settings;
		}
		public AbstractUserProfile getUserProfile() {
			return UserProfile;
		}
		public void setUserProfile(AbstractUserProfile userProfile) {
			this.UserProfile = userProfile;
		}	
	}
}

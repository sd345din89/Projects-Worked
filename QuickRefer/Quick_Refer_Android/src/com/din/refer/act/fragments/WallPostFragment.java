package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionItemTarget;
import com.github.amlcurran.showcaseview.targets.ActionViewTarget;
import com.google.inject.Inject;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.fragments.WallPostCommentDialogFragment.UpdateWallpost;
import com.strobilanthes.forrestii.activities.AuthenticationActivity;
import com.strobilanthes.forrestii.activities.ForrestiiFriendActivity;
import com.strobilanthes.forrestii.activities.ReferSomeOneActivity;
import com.strobilanthes.forrestii.activities.WallPostCreateActivity;
import com.strobilanthes.forrestii.activities.WallPostExpandActivity;
import com.strobilanthes.forrestii.custom.views.CustomWallPostListview;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.notification.Notification;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.AbstractPaginateGetWallPost;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;
import com.strobilanthes.forrestii.model.wallpost.GetWallPostContents;
import com.strobilanthes.forrestii.model.wallpost.RefferalPost;
import com.strobilanthes.forrestii.model.wallpost.ReplyPost;
import com.strobilanthes.forrestii.model.wallpost.WallPost;
import com.strobilanthes.forrestii.pulltorefresh.widget.RefreshableListView.OnUpdateTask;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

@SuppressLint("SetJavaScriptEnabled")
public class WallPostFragment extends BaseFragment<DBAccess> implements OnClickListener,UpdateWallpost {

	@InjectView(R.id.listView1)								private CustomWallPostListview listView;
	//@InjectView(R.id.arc_menu_2) 							private ArcMenu arcMenu2;
	//@InjectView(R.id.wallpost_emptyView)					private TextView txt_emptyView;
	@InjectView(R.id.layout_slide)           				private LinearLayout layoutSlide;
	@InjectView(R.id.refer_txt_pick_contacts) 				private TextView referTxtPickContacts;
	@InjectView(R.id.refer_txt_quickcard)     				private TextView referTxtQuickcard;
	@InjectView(R.id.refer_txt_cancel)        			    private TextView referTxtCancel;
	@InjectView(R.id.lyt_card)                              private FrameLayout referLytCard;
	@InjectView(R.id.view_transparent)                      private View transparentView;
	@InjectView(R.id.contacts_txt_emptyview)				private TextView txt_emptyview;
	
	@Inject WallPost wallPostModel;
	@Inject Notification notificationModel;
	
	private ViewStub inflate_stub;
	private Typeface mFontLight,mFontDark;
	private static int REPLY_REQUEST_CODE = 11;
	private static int WALL_CREATE_REQUEST_CODE = 22;
	private static int DELETE_WALL_POST = 33;
	private static int REFERRAL_REQUEST_CODE = 88;
	private static int FORRESTII_REQUEST_CODE = 55;
	
	private static int FINISH_ALL = 1919;
	private AbstractUserProfile abstractUserProfile; 
	private CustomAdapter adpater;
	private ArrayList<GetWallPost> getWallPostArrayList;
	private static boolean isNormalRefresh = false;
	private Dialog commentsDialog;
	private ShowcaseView sv;
    private int counter = 0;
	private SharedPreferences storyPreference;
	
	//private static WallPostFragment frag = null;
	 
	public static WallPostFragment newInstance() {
		WallPostFragment frag = new WallPostFragment();
        return frag;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (container == null) {
			return null;
		}
		
		View view = inflater.inflate(R.layout.frg_wallpost, container, false);
		inflate_stub = (ViewStub) view.findViewById(R.id.inflate_stub);
        inflate_stub.inflate();
    	inflate_stub.setVisibility(View.GONE);
    	
		super.initilizeContents();
		
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		getActivity().getActionBar().setTitle("Wall");
		
		getActivity().getActionBar().setDisplayShowHomeEnabled(true);
		
		wallPostModel.setContext(getActivity());
		
		storyPreference = getActivity().getSharedPreferences("Forrestii", Context.MODE_PRIVATE);
		
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");

		abstractUserProfile = getHelper().getDBUserProfile();
		txt_emptyview.setText("No Wall post yet...");
		txt_emptyview.setTypeface(mFontDark);
		//makeViewTransparent(false);
		/*mPoppyViewHelper = new PoppyViewHelper(getActivity());
		View poppyView = mPoppyViewHelper.createPoppyViewOnListView(R.id.listView1, R.layout.wall_popup_view, new AbsListView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				//Log.d("ListViewActivity", "onScrollStateChanged");
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				//Log.d("ListViewActivity", "onScroll");
			}
		});*/
		
		/*TextView txt_PopyView = (TextView)poppyView.findViewById( R.id.poppy_username_txt );
		txt_PopyView.setTypeface(tf);
		txt_PopyView.setText(abstractUserProfile.getFirstName());
		ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.s[0], (ImageView)poppyView.findViewById( R.id.poppy_user_image ), options,
				animateFirstListener);
		//TextView wallPostUsername = (TextView)poppyView.findViewById( R.id.poppy_username_txt );
		
		poppyView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(getActivity(), "Click me!", Toast.LENGTH_SHORT).show();
			}
		});*/
		
		listView.setOnUpdateTask(new OnUpdateTask() {

			public void updateBackground() {
				
				JSONObject object = new JSONObject();
				 try { 
					 
					//isAddTop = true;
					isNormalRefresh = true;
					 
					object.put("postToUserProfile",new JSONObject().put("userId", getHelper().getUserId()));
					object.put("StartRow" , 0);
					object.put("MaximumRow" , 5);
					object.put("isNeedDashboard", true);
					wallPostModel.paginateWallPost(Constant.GET_PAGINATE_WALLPOST, object);
					
				 } catch (JSONException e) {
					e.printStackTrace();
				 }
			}

			public void updateUI() {
				// aa.notifyDataSetChanged();
			}

			public void onUpdateStart() {

			}

		});
		
		/*btn_share.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				String cardName = abstractUserProfile.getFirstName();
				
				ArrayList<Uri> uris = new ArrayList<Uri>();
				String image1Path = Constant.IMAGE_PATH + cardName + ".vcf";
				String image2Path = Constant.IMAGE_PATH + cardName + ".png";
				File vcfFile = new File(image1Path);
				File imgFile = new File(image2Path);
				if (vcfFile.exists() || imgFile.exists()) {
					String[] filePaths = new String[] { image1Path, image2Path };
					for (String file : filePaths) {
						File fileIn = new File(file);
						Uri u = Uri.fromFile(fileIn);
						uris.add(u);
					}
					
					Intent shareIntent = new Intent(Intent.ACTION_SEND);
					shareIntent.setAction(android.content.Intent.ACTION_SEND_MULTIPLE);
					shareIntent.setType("message/rfc822");
					shareIntent.setType("image/*");
					
					shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
					startActivity(Intent.createChooser(shareIntent, "Share card and vcf to..."));
	               }
			}
		});*/

		/*listView.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// Your code to refresh the list contents goes here

				// Make sure you call listView.onRefreshComplete()
				// when the loading is done. This can be done from here or any
				// other place, like on a broadcast receive from your loading
				// service or the onPostExecute of your AsyncTask.

				// For the sake of this sample, the code will pause here to
				// force a delay when invoking the refresh
				listView.postDelayed(new Runnable() {

					@Override
					public void run() {
						listView.onRefreshComplete();
					}
				}, 2000);
			}
		});*/
		
		LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		listView.setLoadingView(layoutInflater.inflate(R.layout.view_loading, null));
		
		
		getWallPostArrayList = Constant.mGetWallPostArrayList;
		adpater = new CustomAdapter(getActivity());
		listView.setOnScrollListener(adpater);
		listView.setAdapter(adpater);
		
		if(getWallPostArrayList != null){
			if(getWallPostArrayList.size() >= 5){
				adpater.notifyHasMore();
			}else{
				JSONObject object = new JSONObject();
				 try {
					
					isNormalRefresh = true;
					
					object.put("postToUserProfile",new JSONObject().put("userId", getHelper().getUserId()));
					object.put("StartRow" , 0);
					object.put("MaximumRow",5);
					object.put("isNeedDashboard", true);
					wallPostModel.paginateWallPost(Constant.GET_PAGINATE_WALLPOST, object);
					
				 } catch (JSONException e) {
					e.printStackTrace();
				 }
			}
		}
		
		/*listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Intent intent = new Intent(getActivity(), WallPostExpandActivity.class);
				intent.putExtra("position", (position-2));
				GetWallPost getWallPost = (GetWallPost) parent.getItemAtPosition(position);
				intent.putExtra("WallPostContent", getWallPost);
				startActivityForResult(intent,FORRESTII_REQUEST_CODE);
			}
		});*/
		if(Constant.mGetWallPostArrayList.size()==0){
			inflate_stub.setVisibility(View.VISIBLE);
		}else{
			inflate_stub.setVisibility(View.GONE);
		}
	}
		
	/*private void initArcMenu(ArcMenu menu, int[] itemDrawables) {                                               
		
		final int itemCount = itemDrawables.length;
		for (int i = 0; i < itemCount; i++) {
			
			ImageView item = new ImageView(getActivity());
			item.setImageResource(itemDrawables[i]);
			item.setId(i);
			item.setLayoutParams(new LinearLayout.LayoutParams(200, 200));  
			//final int position = i;
			menu.addItem(item, new OnClickListener() {

				@Override
				public void onClick(View v) {
					switch (v.getId()) {
					case 0:
						//startActivityForResult(new Intent(getActivity(), WallPostCreateActivity.class),WALL_CREATE_REQUEST_CODE);
						
						break;
					case 1:
						//startActivity(new Intent(getActivity(), NotificationActivity.class));
						
						break;
					case 2:
						//startActivityForResult(new Intent(getActivity(), SettingsActivity.class),FINISH_ALL);
						break;
					case R.id.img_logout:
						
						getHelper().logout();
						startActivity(new Intent(getActivity(), AuthenticationActivity.class));
						getActivity().finish();
						break;
					default:
						break;
					}
				}
			});
			
		}
		
	}*/
	
	
	@Override
	public void onPause() {
		super.onPause();
		notificationModel.removeChangeListener(this);
		wallPostModel.removeChangeListener(this);
	}

	
	@Override
	public boolean onBackPressed() {
		if(commentsDialog != null){
			if(commentsDialog.isShowing()){
				commentsDialog.dismiss();
				return false;
			}else{
				return super.onBackPressed();
			}
		}else{
			return super.onBackPressed();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		inflater.inflate(R.menu.wallpost_menu, menu);
		if(!storyPreference.getBoolean("storyActionbar", false)){
			
			ActionViewTarget target = new ActionViewTarget(getActivity(), ActionViewTarget.Type.HOME);
			sv = new ShowcaseView.Builder(getActivity())
			.setTarget(target)
			.setContentTitle("Slide Navigation Icon")
			.setContentText("This toggles the screen in and out.")
	        .setOnClickListener(storyClickLisnear)
	        .build();
			sv.setButtonText("Next");
			
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//View v = item.getActionView();
		if(item.getItemId() == R.id.create_post){
			startActivityForResult(new Intent(getActivity(), WallPostCreateActivity.class),WALL_CREATE_REQUEST_CODE);
		}else if(item.getItemId() == R.id.share){
			//Util.setTouchDelay(getActivity(), v);
			Intent intent = Intent.createChooser(getDefaultShareIntent(), "Share contacts through...");
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//intent.createChooser(intent, "Share contacts through...");
			startActivity(intent);
		}else if(item.getItemId() == R.id.forrestiifriend){
			startActivityForResult(new Intent(getActivity(), ForrestiiFriendActivity.class),FORRESTII_REQUEST_CODE);
		}
		return super.onOptionsItemSelected(item);
	}
	
	private OnClickListener storyClickLisnear = new OnClickListener() {
		
		@Override
		public void onClick(View v) {

	        switch (counter) {
	            case 0:
	                sv.setShowcase(new ActionItemTarget(getActivity(), R.id.share), true);
	                sv.setContentTitle("Share Your Card ");
	                sv.setContentText("Use this to share your business card and Vcard easily from Forrestii.");
	                break;

	            case 1:
	                sv.setShowcase(new ActionItemTarget(getActivity(), R.id.create_post), true);
	                sv.setContentTitle("Create Wall post");
	                sv.setContentText("Need trusted referrals? Touch this to post your need on your wall.");
	                break;

	            case 2:
	                sv.setShowcase(new ActionItemTarget(getActivity(), R.id.forrestiifriend), true);
	                sv.setContentTitle("Forrestii Friend Search");
	                sv.setContentText("Use this to search for experts where you want!");
	                sv.setButtonText("Close");
	                break;

	            case 3:
	            	sv.hide();
	            	storyPreference.edit().putBoolean("storyActionbar", true).commit();
	            	/*sv.setTarget(ViewTarget.NONE);
	                sv.setContentTitle("Check it out");
//	                sv.setContentText("You don't always need a target to showcase");
	                sv.setButtonText("Close");*/
	                //setAlpha(0.4f, new ActionItemTarget(getActivity(), R.id.create_post), new ActionItemTarget(getActivity(), R.id.share), new ActionItemTarget(getActivity(), R.id.forrestiifriend));
	                break;
	            case 4:
	            	 sv.hide();
	            	 storyPreference.edit().putBoolean("storyActionbar", true).commit();
		             // setAlpha(1.0f, textView1, textView2, textView3);
	            	break;
	        }
	        counter++;
		}
	};


	private Intent getDefaultShareIntent() {
		
		String cardName = abstractUserProfile.getFirstName();
		
		ArrayList<Uri> uris = new ArrayList<Uri>();
		String image1Path = Constant.IMAGE_PATH + cardName + ".vcf";
		String image2Path = Constant.IMAGE_PATH + cardName + ".png";
		File vcfFile = new File(image1Path);
		File imgFile = new File(image2Path);
		if (vcfFile.exists() || imgFile.exists()) {
			String[] filePaths = new String[] { image1Path, image2Path };
			for (String file : filePaths) {
				File fileIn = new File(file);
				Uri u = Uri.fromFile(fileIn);
				uris.add(u);
			}
			
			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setAction(android.content.Intent.ACTION_SEND_MULTIPLE);
			shareIntent.setType("message/rfc822");
			shareIntent.setType("image/*");
			
			shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
			
			return shareIntent;	 
			//startActivity(Intent.createChooser(shareIntent, "Share card and vcf to..."));
        }
		return null;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		notificationModel.addChangeListener(this);
		wallPostModel.addChangeListener(this);
	}

	@Override
	public void onClick(View v) {
		
		  /*switch (v.getId()) { case R.id.socialize_screen_all_lyt:
		  
		  break; case R.id.socialize_screen_group_lyt:
		  
		  break; case R.id.socialize_screen_club_lyt:
		  
		  break; default: break; }*/
		  
	}


	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if (event.getPropertyName().equals("refereshwallpost")) {
			/*AbstractGetWallPost abstractGetWallPost = wallPostModel.getWallPostResponse();
			if(abstractGetWallPost != null){
				if(abstractGetWallPost.getStatus() == 1){
					
					if(abstractGetWallPost.getResponse().getWallPost().size() > 0){
						
						Constant.mGetWallPostArrayList = abstractGetWallPost.getResponse().getWallPost();
						if(Constant.mGetWallPostArrayList.size() > 0){
							getWallPostArrayList = Constant.mGetWallPostArrayList;
						}
						
						adpater.notifyDataSetChanged();
						if(Constant.mGetWallPostArrayList.size() >= 5)
							adpater.notifyHasMore();
					}
					
					updateDashBoardContents(abstractGetWallPost.getResponse().getDashBoard());
					
				}else{
					L.ToastMessage(getActivity(), ""+abstractGetWallPost.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}*/
		}else if (event.getPropertyName().equals("paginate_notificaiton")) {
			/*AbstractNotification abstractNotification = notificationModel.getPaginateNotification();
			if(abstractNotification  !=null){
				if(abstractNotification.getStatus() == 1){
					//getHelper().insertNotificationList(abstractNotification.getResponse());
				//Util.showAlert(getActivity(),"Message", "Success");


				}else{
					Util.showAlert(getActivity(),"Message", abstractNotification.getError());
				}
			}
			else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}*/
		}else if(event.getPropertyName().equals("PaginateWallPost")){
			AbstractPaginateGetWallPost abstractPaginateGetWallPost = wallPostModel.getPaginateWallpost();
			if(abstractPaginateGetWallPost  !=null){
				if(abstractPaginateGetWallPost.getStatus() == 1){
					if(isNormalRefresh){
						isNormalRefresh = false;
						Constant.mGetWallPostArrayList = abstractPaginateGetWallPost.getResponse().getWall();
						if(Constant.mGetWallPostArrayList.size() > 0){
							getWallPostArrayList = Constant.mGetWallPostArrayList;
						}
						if(Constant.mGetWallPostArrayList.size()==0){
							inflate_stub.setVisibility(View.VISIBLE);
						}else{
							inflate_stub.setVisibility(View.GONE);
						}
						
						adpater.notifyDataSetChanged();
						if(Constant.mGetWallPostArrayList.size() >= 5)
							adpater.notifyHasMore();
					}else{
						if(abstractPaginateGetWallPost.getResponse().getWall() == null || abstractPaginateGetWallPost.getResponse().getWall().isEmpty()){
							adpater.notifyEndOfList();
							if(Constant.mGetWallPostArrayList.size()==0){
								inflate_stub.setVisibility(View.VISIBLE);
							}else{
							inflate_stub.setVisibility(View.GONE);
							}
						}else if(abstractPaginateGetWallPost.getResponse().getWall() != null && abstractPaginateGetWallPost.getResponse().getWall().size() > 0){
							adpater.addEntriesToBottom(abstractPaginateGetWallPost.getResponse().getWall());
							if(abstractPaginateGetWallPost.getResponse().getWall().size() < 10)
								adpater.notifyEndOfList();
							else
								adpater.notifyHasMore();
						}else{
							adpater.notifyEndOfList();
						}
					}
					
					if(abstractPaginateGetWallPost.getResponse().getDashBoard() != null){
						
						SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Forrestii", Context.MODE_PRIVATE);
						sharedPreferences.edit().putInt("coins", Integer.parseInt(abstractPaginateGetWallPost.getResponse().getDashBoard().getTotalCoin())).commit();
						sharedPreferences.edit().putInt("badgeCount", Integer.parseInt(abstractPaginateGetWallPost.getResponse().getDashBoard().getNotification())).commit();
						getHelper().insertDashBoard(abstractPaginateGetWallPost.getResponse().getDashBoard());
			   	     	
					}
						
						
				}else{
					adpater.notifyEndOfList();
					if(Constant.mGetWallPostArrayList.size()==0){
						inflate_stub.setVisibility(View.VISIBLE);
					}else{
					inflate_stub.setVisibility(View.GONE);
					}
					Util.showAlert(getActivity(),"Message", "No more wall posts yet");
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
		}
	}

	private void updateView(int index, int reply_count){
	    View v = listView.getChildAt(index - 
	    		listView.getFirstVisiblePosition());

	    if(v == null)
	       return;

	   // TextView someText = (TextView) v.findViewById(R.id.wall_subview_reply_count);
	  //  someText.setText(""+reply_count);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == REPLY_REQUEST_CODE){
			
			final int index = data.getIntExtra("pos", -1);
			int reply_count = data.getIntExtra("reply_count", -1);
			if(index != -1 && reply_count !=0){
				final int firstVisiblePos = listView.getFirstVisiblePosition();
				getWallPostArrayList = Constant.mGetWallPostArrayList;
				adpater.notifyDataSetChanged();
				
				new Handler().post(new Runnable() {
					
					@Override
					public void run() {
						
						View v = listView.getChildAt(0);
						int top = (v == null) ? 0 : v.getTop();
						
						listView.setSelectionFromTop(firstVisiblePos,top);
					}
				});
				
				updateView(index,reply_count);	
			}
			
		}else if(resultCode == WALL_CREATE_REQUEST_CODE){
			
			ArrayList<GetWallPost> abstractGetWallPost = Constant.mGetWallPostArrayList;
			if(abstractGetWallPost == null)
				abstractGetWallPost = new ArrayList<GetWallPost>();
			
			GetWallPost getWallPost = new GetWallPost();
			GetWallPostContents getWallPostContents = new GetWallPostContents();
			getWallPostContents.setMessage(""+data.getStringExtra("reply_contents"));
			getWallPostContents.setPostId(data.getIntExtra("post_id",-1));
			getWallPostContents.setUpdatedOn(Calendar.getInstance().getTimeInMillis());
			
			getWallPostContents.setReferral(new ArrayList<RefferalPost>());
			getWallPostContents.setReplies(new ArrayList<ReplyPost>());
			
			AbstractSkillSet abstractSkillSet = new AbstractSkillSet();
			abstractSkillSet.setSkillId(data.getIntExtra("skill_id",-1));
			abstractSkillSet.setSkillName(data.getStringExtra("skill_name"));
			getWallPostContents.setSkill(abstractSkillSet);//SkillsSet Setter
			getWallPostContents.setPostByUserProfile(getHelper().getDBUserProfile()); //UserProfile Setter
			
			getWallPost.setWallPosts(getWallPostContents);//WallPostContens Setter
			
			abstractGetWallPost.add(0,getWallPost);
			Constant.mGetWallPostArrayList = abstractGetWallPost;
			
			if(Constant.mGetWallPostArrayList.size()==0){
				inflate_stub.setVisibility(View.VISIBLE);
			}else{
				inflate_stub.setVisibility(View.GONE);
			} 
			
			getWallPostArrayList = abstractGetWallPost;
			adpater.notifyDataSetChanged();
			
		} else if(resultCode == DELETE_WALL_POST){
			
			final int index = data.getIntExtra("pos", -1);
			if(index != -1){
				Constant.mGetWallPostArrayList.remove(index-2);
				getWallPostArrayList = Constant.mGetWallPostArrayList;
				adpater.notifyDataSetChanged();
				if(Constant.mGetWallPostArrayList.size()==0){
					inflate_stub.setVisibility(View.VISIBLE);
				}else{
					inflate_stub.setVisibility(View.GONE);
				} 
			}
			
		} if(resultCode == REFERRAL_REQUEST_CODE){
			
			RefferalPost refferalPost  = (RefferalPost) data.getSerializableExtra("refferalPost");
			final int position = data.getIntExtra("pos", -1);
			ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
			GetWallPost getWallPost = getWallPostList.get(position);
			GetWallPostContents getWallPostContents = getWallPost.getWallPosts();
			ArrayList<RefferalPost> referralPostsList = getWallPostContents.getReferral();
			referralPostsList.add(0,refferalPost);
			getWallPostContents.setReferral(referralPostsList);
			getWallPost.setWallPosts(getWallPostContents);
			getWallPostList.set(position, getWallPost);
			
			Constant.mGetWallPostArrayList = getWallPostList;
			getWallPostArrayList = getWallPostList;
			adpater.notifyDataSetChanged();
			
		} else if(resultCode == FINISH_ALL){
			startActivity(new Intent(getActivity(), AuthenticationActivity.class));
			getActivity().finish();
		} else if (requestCode == FORRESTII_REQUEST_CODE) {
			if (resultCode == -1) {
				getWallPostArrayList = Constant.mGetWallPostArrayList;
				adpater.notifyDataSetChanged();
				if(Constant.mGetWallPostArrayList.size()==0){
					inflate_stub.setVisibility(View.VISIBLE);
				}else{
					inflate_stub.setVisibility(View.GONE);
				} 
			}
		}
	}

	class CustomAdapter extends BaseAdapter implements OnScrollListener{
		
		private Activity mActivity;
		private LayoutInflater mLayoutInflater;
		protected boolean canScroll = false;
		protected boolean rowEnabled = true;
		
		public CustomAdapter(Activity activity) {
			this.mActivity = activity;
			mLayoutInflater = (LayoutInflater) mActivity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);		
		}
		
		public void clearEntries() {
			getWallPostArrayList.clear();
			notifyDataSetChanged();
		}
		
		public void lock() {
			canScroll = false;
		}	
		
		public void unlock() {
			canScroll = true;
		}

	    @Override
	    public boolean isEnabled(int position) {
	    	return rowEnabled; 
	    }
		public void setRowEnabled(boolean rowEabled) {
			this.rowEnabled = rowEabled;
		}
		
		@Override
		public int getCount() {
			return getWallPostArrayList.size();
		}

		@Override
		public GetWallPost getItem(int position) {
			return getWallPostArrayList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			
			ViewHolder holder = null;
			View v = convertView;
			
			if (v == null) {
				
				v = mLayoutInflater.inflate(R.layout.frg_wallpost_list_item, null);
				
				holder = new ViewHolder();
				//holder.layout_parent = (RelativeLayout)v.findViewById(R.id.wall_post_layout_parent);
				holder.txt_message = (TextView) v
						.findViewById(R.id.wall_post_message);
				holder.txt_username = (TextView) v
						.findViewById(R.id.wall_post_txt_userName);
				holder.img_user = (ImageView) v
						.findViewById(R.id.wall_post_img_user);
				holder.txt_profession = (TextView) v
						.findViewById(R.id.wall_subview_profession);
				holder.txt_count = (TextView) v
						.findViewById(R.id.wall_subview_count);
				holder.img_expand = (ImageView)v.findViewById(R.id.wall_post_img_expand);
				holder.txt_time = (TextView)v.findViewById(R.id.wall_post_txt_time);

				
				holder.txt_refersomeone = (TextView)v.findViewById(R.id.wall_post_txt_refersomeone);
				holder.wall_post_txt_comment = (TextView)v.findViewById(R.id.wall_post_txt_comment);
				//holder.txt_reffered_persons = (TextView) v.findViewById(R.id.wall_txt_referred_persons);
				holder.txt_header_postedby = (TextView)v.findViewById(R.id.wall_post_txt_postedby);
				holder.txt_header_need = (TextView)v.findViewById(R.id.wall_post_txt_need);
				holder.wallPostLytRefersomeone = (LinearLayout)v.findViewById( R.id.wall_post_lyt_refersomeone );
				holder.wallPostLytComment = (LinearLayout)v.findViewById( R.id.wall_post_lyt_comment );

				
				v.setTag(holder);
				
			} else {
				
				holder = (ViewHolder) v.getTag();
				
			}
			
			holder.txt_count.setTypeface(mFontLight);
			holder.txt_time.setTypeface(mFontLight);

			holder.txt_message.setTypeface(mFontLight);
			holder.txt_username.setTypeface(mFontDark);
			holder.txt_profession.setTypeface(mFontDark);
			holder.wall_post_txt_comment.setTypeface(mFontDark);
			holder.txt_refersomeone.setTypeface(mFontDark);
			holder.txt_header_postedby.setTypeface(mFontDark);
			holder.txt_header_need.setTypeface(mFontDark);
			AbstractUserProfile userProfile = getItem(position).getWallPosts().getPostByUserProfile();
			
			ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + userProfile.getPhotoID(), holder.img_user,options);
			
			String firstName = null;
			if(abstractUserProfile.getUserId() == userProfile.getUserId()){
				//firstName = userProfile.getFirstName();
				firstName = "You";
			}else{
				if(userProfile.getFirstName() == null)
					firstName = "Forrestii User";
				else
					firstName = userProfile.getFirstName();
			}
			
			//holder.txt_refersomeone.setBackground(getResources().getDrawable(R.drawable.btn_selector));
			
			holder.wallPostLytComment.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Util.setTouchDelay(mActivity, v);

					WallPostCommentDialogFragment newFragment = WallPostCommentDialogFragment.newInstance(position,getItem(position),WallPostFragment.this);
				    newFragment.show(getFragmentManager(), "dialog");
				}
			});
			holder.img_expand.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(), WallPostExpandActivity.class);
					intent.putExtra("position", position);
					intent.putExtra("WallPostContent", getItem(position));
					startActivityForResult(intent,FORRESTII_REQUEST_CODE);
				}
			});
			
			holder.wallPostLytRefersomeone.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Util.setTouchDelay(mActivity, v);
					final int userId = getItem(position).getWallPosts().getPostByUserProfile().getUserId();
					//My post
					if(userId == abstractUserProfile.getUserId()){
						
						if(getItem(position).getWallPosts().getReferral().size() > 0 ) {
							WallPostReferralsDialogFragment newFragment = WallPostReferralsDialogFragment.newInstance(position,getItem(position));
						        newFragment.show(getFragmentManager(), "dialog");
						} else {
							L.ToastMessage(getActivity(), "No referrals yet!");
						}
						
					} else {
						//Friends post with already having referal
						if(getItem(position).getWallPosts().getReferral() != null && getItem(position).getWallPosts().getReferral().size() >0){
							boolean isPick = false;
							boolean isCurrentUser = false;
							int referralFromUserId = 0;
							
							ArrayList<RefferalPost> referralList = getItem(position).getWallPosts().getReferral();
							if(referralList !=null && referralList.size() > 0){
								for(RefferalPost refferalPost : referralList){
									if(refferalPost.isPick()){
										isPick = true;
										if(refferalPost.getReferralFromUserProfile() != null){
											referralFromUserId = refferalPost.getReferralFromUserProfile().getUserId();
											break;
										}
									} else {
										if(refferalPost.getReferralFromUserProfile() != null){
											if(abstractUserProfile.getUserId() == refferalPost.getReferralFromUserProfile().getUserId()){
												isCurrentUser = true;
												referralFromUserId = refferalPost.getReferralFromUserProfile().getUserId();
											}
										}
									}
								}
							}
							
							if(isPick){
								if(referralFromUserId != 0){
									//picked my referal
									if (abstractUserProfile.getUserId() == referralFromUserId || isCurrentUser) {
										WallPostReferralsDialogFragment newFragment = WallPostReferralsDialogFragment.newInstance(position,getItem(position));
								        newFragment.show(getFragmentManager(), "dialog");
									} else {
										L.ToastMessage(getActivity(), "Already someone picked this card, so you can't refer your friends");
									}	
								} else {
									L.ToastMessage(getActivity(), "Already someone picked this card, so you can't refer your friends");
								}
							} else {
								//you refered already
								if(isCurrentUser) {
									WallPostReferralsDialogFragment newFragment = WallPostReferralsDialogFragment.newInstance(position,getItem(position));
							        newFragment.show(getFragmentManager(), "dialog");
									//L.ToastMessage(getActivity(), "You already referred one of your friend.");
								} else {
									//Not refered yet
									if(getHelper().getDBT2Friends().size() >0 ){
										Intent intent = new Intent(v.getContext(),ReferSomeOneActivity.class);
										intent.putExtra("postId", getItem(position).getWallPosts().getPostId());
										intent.putExtra("postByUserId", getItem(position).getWallPosts().getPostByUserProfile().getUserId());
										intent.putExtra("mobile", getItem(position).getWallPosts().getPostByUserProfile().getMobile());
										intent.putExtra("skillSetId", getItem(position).getWallPosts().getSkill().getSkillId());
										intent.putExtra("position", position);
										startActivityForResult(intent,REFERRAL_REQUEST_CODE);
									}else{
										Util.showAlert(getActivity(), "Message", "No friendsList.");
									}
								}
							}
								
						} else {
							//refer someone to your friend
							if(getHelper().getDBT2Friends().size() >0 ){
								Intent intent = new Intent(v.getContext(),ReferSomeOneActivity.class);
								intent.putExtra("postId", getItem(position).getWallPosts().getPostId());
								intent.putExtra("postByUserId", getItem(position).getWallPosts().getPostByUserProfile().getUserId());
								intent.putExtra("mobile", getItem(position).getWallPosts().getPostByUserProfile().getMobile());
								intent.putExtra("position", position);
								intent.putExtra("skillSetId", getItem(position).getWallPosts().getSkill().getSkillId());
								//intent.putExtra("", "");
								startActivityForResult(intent,REFERRAL_REQUEST_CODE);
							}else{
								Util.showAlert(getActivity(), "Message", "No friendsList.");
							}
						}
					}
				}
			});
			holder.txt_username.setSelected(true);
			holder.txt_username.setText(firstName);
			holder.txt_profession.setSelected(true);
			holder.txt_profession.setText(getItem(position).getWallPosts().getSkill().getSkillName());
			
			String text_msg = getItem(position).getWallPosts().getMessage().trim().toString();
			if(text_msg != null && text_msg.length() > 0){
				holder.txt_message.setText(getItem(position).getWallPosts().getMessage().trim().toString());
			}else{
				
				holder.txt_message.setText("I need "+getItem(position).getWallPosts().getSkill().getSkillName()+". Can anyone please refer me a good one!!");
			}
			
			if(abstractUserProfile.getUserId() == getItem(position).getWallPosts().getPostByUserProfile().getUserId()){
				holder.txt_count.setText(""+getItem(position).getWallPosts().getReplies().size()+" Replies & "+getItem(position).getWallPosts().getReferral().size()+" Referrals" );	
			}else{
				holder.txt_count.setText(""+getItem(position).getWallPosts().getReplies().size()+" Replies");
			}
			
			holder.txt_time.setText(Util.updatedOnTime(getItem(position).getWallPosts().getUpdatedOn()));
			
			getReferralDetils(position, holder);
			/*if(userId.equals(abstractUserProfile.getUserId().toString().trim())){
				
				ArrayList<RefferalPost> refferalPosts = getItem(position).getWallPosts().getReferral();
				StringBuilder builder = new StringBuilder();
				if(refferalPosts != null && refferalPosts.size() > 0){
					for (RefferalPost refferalPost : getItem(position).getWallPosts().getReferral()) {
						if(refferalPost.getReferredUserProfile() != null){
							String fName = refferalPost.getReferredUserProfile().getFirstName();
							String lName = refferalPost.getReferredUserProfile().getLastName();
							
							builder.append(""+(fName != null ? fName : "") + " "+(lName != null ? lName : "")+" ");
						}else if(refferalPost.getReferredQuickCard() != null){
							String fName = refferalPost.getReferredQuickCard().getFirstName();
							String lName = refferalPost.getReferredQuickCard().getLastName();
							
							builder.append(""+(fName != null ? fName : "") + " "+(lName != null ? lName : "")+" ");
						}else if(refferalPost.getForrestiiFriend() != null){
							builder.append("Forrestii Friend" + " ");
						}
					}
				}
				//holder.txt_reffered_persons.setVisibility(View.VISIBLE);
				//holder.txt_reffered_persons.setText(""+builder.toString());	
			}else{
				//holder.txt_reffered_persons.setVisibility(View.GONE);
			}*/
				
			return v;
		}
		
		private void getReferralDetils(int position , ViewHolder holder){
			
			final int userId = getItem(position).getWallPosts().getPostByUserProfile().getUserId();
			if(userId == abstractUserProfile.getUserId()){
				
				holder.txt_refersomeone.setText(""+getItem(position).getWallPosts().getReferral().size()+" Referrals");				
				
			} else {
				if(getItem(position).getWallPosts().getReferral() != null && getItem(position).getWallPosts().getReferral().size() >0){
					boolean isPick = false;
					boolean isCurrentUser = false;
					int referralFromUserId = 0;
					ArrayList<RefferalPost> referralList = getItem(position).getWallPosts().getReferral();
					if(referralList !=null && referralList.size() > 0){
						for(RefferalPost refferalPost : referralList){
							if(refferalPost.isPick()){
								isPick = true;
								if(refferalPost.getReferralFromUserProfile() != null){
									referralFromUserId = refferalPost.getReferralFromUserProfile().getUserId();
									break;
								}
								
							} else {
								if(refferalPost.getReferralFromUserProfile() != null){
									if(abstractUserProfile.getUserId() == refferalPost.getReferralFromUserProfile().getUserId()){
										isCurrentUser = true;
									}
								}
							}
						}
					}
					if(isPick){
						if(referralFromUserId != 0){
							if (abstractUserProfile.getUserId() == referralFromUserId) {
								holder.txt_refersomeone.setText("Picked Refferal");
							} else {
								holder.txt_refersomeone.setText("Already Picked!");
							}	
						} else {
							if(isCurrentUser)
								holder.txt_refersomeone.setText("Post Closed!");
							else
								holder.txt_refersomeone.setText("Already Picked!");
						}
					} else {
						if(isCurrentUser){
							holder.txt_refersomeone.setText("You've referred!");
						}else {
							//txt_expand_ReferCard.setVisibility(View.VISIBLE);
							holder.txt_refersomeone.setText("Refer someone");	
						}
					}
						
				}else{
					holder.txt_refersomeone.setText("Refer someone");
				}
			}
			
		}
		
		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			
			
			
			if (view instanceof CustomWallPostListview) {
				// In scroll-to-top-to-load mode, when the list view scrolls to the first visible position it reaches the top
				/*if (loadingMode == LoadingMode.SCROLL_TO_TOP && firstVisibleItem == 0 && canScroll) {
					onScrollNext();
				}*/
				// In scroll-to-bottom-to-load mode, when the sum of first visible position and visible count equals the total number
				// of items in the adapter it reaches the bottom
				//L.d("On Scroll ****" + canScroll);
				if (firstVisibleItem + visibleItemCount - 1 == getCount() && canScroll) {
					//L.d("On Scroll ****" + firstVisibleItem  + " " + visibleItemCount + " "+canScroll);
					onScrollNext();
				}
			}
		}

		public void onScrollNext() {
			
			lock();
			JSONObject object = new JSONObject();
			 try {
				object.put("postToUserProfile",new JSONObject().put("userId", getHelper().getUserId()));
				object.put("StartRow",adpater.getCount());
				object.put("MaximumRow",10);
				if(Constant.mGetWallPostArrayList.size() > 0)
					object.put("WallId", Constant.mGetWallPostArrayList.get(Constant.mGetWallPostArrayList.size() - 1).getWallId());
				wallPostModel.paginateWallPost(Constant.GET_PAGINATE_WALLPOST, object);
				
			 } catch (JSONException e) {
				e.printStackTrace();
			 }
			 
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {}

		/*@Override
		public final View getView(int position, View convertView, ViewGroup parent) {
			return getInfiniteScrollListView(position, convertView, parent);
		}*/

		public void addEntriesToBottom(ArrayList<GetWallPost> entries) {
			getWallPostArrayList.addAll(entries);
			notifyDataSetChanged();
		}
		
		public void notifyEndOfList() {
			// When there is no more to load use the lock to prevent loading from happening
			lock();
			// More actions when there is no more to load
			//listView.endOfList();
			if (listView != null) {
				listView.endOfList();
			}
		}

		public void notifyHasMore() {
			// Release the lock when there might be more to load
			unlock();
			// More actions when it might have more to load
			if (listView != null) {
				listView.hasMore();
			}
		}
		class ViewHolder {
			private TextView txt_message, txt_username,wall_post_txt_comment, 
									txt_profession,txt_count,txt_refersomeone,txt_header_postedby,txt_header_need,txt_time ;
			private ImageView img_user,img_expand;
			private LinearLayout wallPostLytComment, wallPostLytRefersomeone;
			//private RelativeLayout layout_parent;
		}

	}

	@Override
	public void uploadComments(int position) {

		if(position != -1){
			final int firstVisiblePos = listView.getFirstVisiblePosition();
			getWallPostArrayList = Constant.mGetWallPostArrayList;
			adpater.notifyDataSetChanged();
			
			new Handler().post(new Runnable() {
				
				@Override
				public void run() {
					
					View v = listView.getChildAt(0);
					int top = (v == null) ? 0 : v.getTop();
					
					listView.setSelectionFromTop(firstVisiblePos,top);
				}
			});
		}
	}

}

package com.strobilanthes.forrestii.model.viewreviews;

import java.io.Serializable;

public class AbstractReviewFF implements Serializable {
	
	private static final long serialVersionUID = 1L;

	
	private String workNumber4;

    private String dirID;

    private String workNumber2;

    private String workNumber3;

    private String ffid;

    private String fullAddress;

    private String website;

   // private Location location;

    private String companyName;

    private String workNumber1;

    private String addressLine2;

    private String addressLine1;

    private String mobileNumber4;

    private String mobileNumber3;

   // private SkillMaster1 skillMaster1;

    private String email;

    private String userId;

    private String reviewCount;

    private String pocfirstName;

    private float totalRating;

    private String mobileNumber2;

    private String poclastName;

    private String mobileNumber1;

    public String getWorkNumber4 ()
    {
        return workNumber4;
    }

    public void setWorkNumber4 (String workNumber4)
    {
        this.workNumber4 = workNumber4;
    }

    public String getDirID ()
    {
        return dirID;
    }

    public void setDirID (String dirID)
    {
        this.dirID = dirID;
    }

    public String getWorkNumber2 ()
    {
        return workNumber2;
    }

    public void setWorkNumber2 (String workNumber2)
    {
        this.workNumber2 = workNumber2;
    }

    public String getWorkNumber3 ()
    {
        return workNumber3;
    }

    public void setWorkNumber3 (String workNumber3)
    {
        this.workNumber3 = workNumber3;
    }

    public String getFfid ()
    {
        return ffid;
    }

    public void setFfid (String ffid)
    {
        this.ffid = ffid;
    }

    public String getFullAddress ()
    {
        return fullAddress;
    }

    public void setFullAddress (String fullAddress)
    {
        this.fullAddress = fullAddress;
    }

    public String getWebsite ()
    {
        return website;
    }

    public void setWebsite (String website)
    {
        this.website = website;
    }

    

    public String getCompanyName ()
    {
        return companyName;
    }

    public void setCompanyName (String companyName)
    {
        this.companyName = companyName;
    }

    public String getWorkNumber1 ()
    {
        return workNumber1;
    }

    public void setWorkNumber1 (String workNumber1)
    {
        this.workNumber1 = workNumber1;
    }

    public String getAddressLine2 ()
    {
        return addressLine2;
    }

    public void setAddressLine2 (String addressLine2)
    {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine1 ()
    {
        return addressLine1;
    }

    public void setAddressLine1 (String addressLine1)
    {
        this.addressLine1 = addressLine1;
    }

    public String getMobileNumber4 ()
    {
        return mobileNumber4;
    }

    public void setMobileNumber4 (String mobileNumber4)
    {
        this.mobileNumber4 = mobileNumber4;
    }

    public String getMobileNumber3 ()
    {
        return mobileNumber3;
    }

    public void setMobileNumber3 (String mobileNumber3)
    {
        this.mobileNumber3 = mobileNumber3;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    public String getReviewCount ()
    {
        return reviewCount;
    }

    public void setReviewCount (String reviewCount)
    {
        this.reviewCount = reviewCount;
    }

    public String getPocfirstName ()
    {
        return pocfirstName;
    }

    public void setPocfirstName (String pocfirstName)
    {
        this.pocfirstName = pocfirstName;
    }

    public float getTotalRating ()
    {
        return totalRating;
    }

    public void setTotalRating (float totalRating)
    {
        this.totalRating = totalRating;
    }

    public String getMobileNumber2 ()
    {
        return mobileNumber2;
    }

    public void setMobileNumber2 (String mobileNumber2)
    {
        this.mobileNumber2 = mobileNumber2;
    }

    public String getPoclastName ()
    {
        return poclastName;
    }

    public void setPoclastName (String poclastName)
    {
        this.poclastName = poclastName;
    }

    public String getMobileNumber1 ()
    {
        return mobileNumber1;
    }

    public void setMobileNumber1 (String mobileNumber1)
    {
        this.mobileNumber1 = mobileNumber1;
    }
}

package com.strobilanthes.forrestii.util;

import android.app.ProgressDialog;
import android.content.Context;

import com.strobilanthes.forrestii.R;

public class CustomProgressbar extends ProgressDialog{
	
	public CustomProgressbar(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		//setContentView(R.layout.view_progressbar);
		setProgressStyle(ProgressDialog.STYLE_SPINNER);
		setProgressDrawable(context.getResources().getDrawable(R.drawable.pinkprogress));
		setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.pinkprogress));
		// set title and message
		//setIndeterminate(true);
		//setTitle("Please wait");
		setMessage("Loading please wait...");
		setCancelable(false);
		/*imageHolders = new ArrayList<ImageView>();
		imageHolders.add((ImageView)findViewById(R.id.imgOne));
		imageHolders.add((ImageView)findViewById(R.id.imgTwo));
		imageHolders.add((ImageView) findViewById(R.id.imgThree));*/
	}
	
	
	
	/*@Override
	public void dismiss() {
		stopped = false;
		if(animationThread.isAlive())
			animationThread.interrupt();
		
		super.dismiss();
	}


	@Override
	public void show() {
		
		stopped = true;
		animationThread = new Thread(this, "Progress");
		animationThread.start();
		
		super.show();
	}*/

}

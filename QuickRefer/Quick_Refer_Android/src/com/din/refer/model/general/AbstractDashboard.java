package com.strobilanthes.forrestii.model.general;

import java.io.Serializable;

public class AbstractDashboard implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String Rating;
	private String ContactCount;
	private String ReferralIn;
	private String ReferralOut	;
	private String TotalCoin;
	private String Notification;
	private String Pending;
	
	public String getRating() {
		return Rating;
	}
	public void setRating(String rating) {
		Rating = rating;
	}
	public String getContactCount() {
		return ContactCount;
	}
	public void setContactCount(String contactCount) {
		ContactCount = contactCount;
	}
	public String getReferralIn() {
		return ReferralIn;
	}
	public void setReferralIn(String referralIn) {
		ReferralIn = referralIn;
	}
	public String getReferralOut() {
		return ReferralOut;
	}
	public void setReferralOut(String referralOut) {
		ReferralOut = referralOut;
	}
	public String getTotalCoin() {
		return TotalCoin;
	}
	public void setTotalCoin(String totalCoin) {
		TotalCoin = totalCoin;
	}
	/*public String getPending() {
		return Pending;
	}
	public void setPending(String pending) {
		Pending = pending;
	}*/
	public String getNotification() {
		return Notification;
	}
	public void setNotification(String notification) {
		Notification = notification;
	}
	
}

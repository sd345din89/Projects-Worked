package com.strobilanthes.forrestii.model.userprofile;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.model.viewreviews.AbstractViewReviews;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.UserProfileVolleyTasks;

public class UserProfileImpl implements UserProfile,VolleyTasksListener{
	
	private AbstractEditUserProfile abstractEditUserProfile; 
	private AbstractUpdateUserProfile abstractUpdateUserProfile; 
	List<PropertyChangeListener> listener=new ArrayList<PropertyChangeListener>();
	private Activity context;
	private CustomProgressbar mCustomProgressbar;
	private AbstractViewReviews abstractReviewProfile;

	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners( PropertyChangeEvent evt ){
		for ( Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator.hasNext(); ){
			PropertyChangeListener name = (PropertyChangeListener)iterator.next();
			name.propertyChange(evt);
		}
	}
	
	@Override
	public void editUserProfile(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new UserProfileVolleyTasks("edituserprofile", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void requestUserProfile(String url, JSONObject postdata) {
	
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonObject) {
		/*if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}*/
		try {
	        if ((this.mCustomProgressbar != null) && this.mCustomProgressbar.isShowing()) {
	            this.mCustomProgressbar.dismiss();
	        }
	    } catch (final IllegalArgumentException e) {
	    	e.printStackTrace();
	    } catch (final Exception e) {
	    	e.printStackTrace();
	    } finally {
	        this.mCustomProgressbar = null;
	    }  
		if(method_name.equals("edituserprofile")){
			AbstractEditUserProfile previousResult= abstractEditUserProfile;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractEditUserProfile = gson.fromJson(jsonObject.toString(), AbstractEditUserProfile.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractEditUserProfile); 
			notifyListeners(event);
		}else if(method_name.equals("updateuserprofile")){
			AbstractUpdateUserProfile previousResult= abstractUpdateUserProfile;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractUpdateUserProfile = gson.fromJson(jsonObject.toString(), AbstractUpdateUserProfile.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractUpdateUserProfile); 
			notifyListeners(event);
		}else if(method_name.equals("UserProfileReviews")){
			AbstractViewReviews previousResult = abstractReviewProfile;
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractReviewProfile = gson.fromJson(jsonObject.toString(), AbstractViewReviews.class);


			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractReviewProfile);
			
			notifyListeners(event);
			
		}else if(method_name.equals("ForrestiiProfileReviews")){
			AbstractViewReviews previousResult = abstractReviewProfile;
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractReviewProfile = gson.fromJson(jsonObject.toString(), AbstractViewReviews.class);


			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractReviewProfile);
			
			notifyListeners(event);
			
		}else if(method_name.equals("QuickcardProfileReviews")){
			AbstractViewReviews previousResult = abstractReviewProfile;
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractReviewProfile = gson.fromJson(jsonObject.toString(), AbstractViewReviews.class);


			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractReviewProfile);
			
			notifyListeners(event);
			
		}else if(method_name.equals("UserFriendsReviews")){
			AbstractViewReviews previousResult = abstractReviewProfile;
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractReviewProfile = gson.fromJson(jsonObject.toString(), AbstractViewReviews.class);


			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractReviewProfile);
			
			notifyListeners(event);
			
		}
		
	}

	@Override
	public void updateUserProfile(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new UserProfileVolleyTasks("updateuserprofile", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	@Override
	public void handleError(String method_name, Exception e) {
		try {
	        if ((this.mCustomProgressbar != null) && this.mCustomProgressbar.isShowing()) {
	            this.mCustomProgressbar.dismiss();
	        }
	    } catch (final IllegalArgumentException ex) {
	    	ex.printStackTrace();
	    } catch (final Exception ex) {
	    	ex.printStackTrace();
	    } finally {
	        this.mCustomProgressbar = null;
	    }  
	}
	
	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	@Override
	public AbstractUpdateUserProfile getUserUpdatedProfile() {
		return abstractUpdateUserProfile;
	}

	
	@Override
	public AbstractEditUserProfile getUserEditedProfile() {
		return abstractEditUserProfile;
	}
	@Override
	public void viewUserProfileReviews(String url, JSONObject postdata) {
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new UserProfileVolleyTasks("UserProfileReviews", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void viewQuickCardUsersReviews(String url, JSONObject postdata) {
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new UserProfileVolleyTasks("QuickcardProfileReviews", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void viewForrestiiFriendUsersReviews(String url, JSONObject postdata) {
		// TODO Auto-generated method stub
		ForrestiiApplication.getInstance().addToRequestQueue(
				new UserProfileVolleyTasks("ForrestiiProfileReviews", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void viewUserFriendsReviews(String url, JSONObject postdata) {
		// TODO Auto-generated method stub
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new UserProfileVolleyTasks("UserFriendsReviews", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractViewReviews getReviewsRatings() {
		// TODO Auto-generated method stub
		return abstractReviewProfile;
	}
	
}

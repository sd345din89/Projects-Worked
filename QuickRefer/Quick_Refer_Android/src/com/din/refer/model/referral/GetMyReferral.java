package com.strobilanthes.forrestii.model.referral;

public class GetMyReferral {
	private String postID;
	private String referredFrom;
	private String referredTo;
	private String referredUserID;
	private String type;
	private String id;

	public String getPostID() {
		return postID;
	}

	public void setPostID(String postID) {
		this.postID = postID;
	}

	public String getReferredFrom() {
		return referredFrom;
	}

	public void setReferredFrom(String referredFrom) {
		this.referredFrom = referredFrom;
	}

	public String getReferredTo() {
		return referredTo;
	}

	public void setReferredTo(String referredTo) {
		this.referredTo = referredTo;
	}

	public String getReferredUserID() {
		return referredUserID;
	}

	public void setReferredUserID(String referredUserID) {
		this.referredUserID = referredUserID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}

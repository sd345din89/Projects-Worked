package kra.accounts.tracker.models;

public class CollectionDetails {
	
	private String name;
	private String address;
	private String phoneNum;
	private String customerAccId;
	private String customerId;
	private String AccountId;
	private String principalAmount;
	private String principalDate;
	private String insertDueFromDate;
	private String security;
	private String description;
	private String principalPaid;
	
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}
	public void setAdderess(String address){
		this.address=address;
	}
	public String getAddress(){
		return address;
	}
	public void setPhoneNum(String phoneNum){
		this.phoneNum=phoneNum;
	}
	public String getPhoneNum(){
		return phoneNum;
	}
	
	public void setcustomerAccId(String customerAccId){
		this.customerAccId=customerAccId;
	}
	public String getcustomerAccId(){
		return customerAccId;
	}
	public void setcustomerId(String customerId){
		this.customerId=customerId;
	}
	public String getcustomerId(){
		return customerId;
	}
	public void setAccountId(String AccountId){
		this.AccountId=AccountId;
	}
	public String getAccountId(){
		return AccountId;
	}
	
	public void setprincipalAmount(String principalAmount){
		this.principalAmount=principalAmount;
	}
	public String getprincipalAmount(){
		return principalAmount;
	}
	public void setprincipalDate(String principalDate){
		this.principalDate=principalDate;
	}
	public String getprincipalDate(){
		return principalDate;
	}
	public void setinsertDueFromDate(String insertDueFromDate){
		this.insertDueFromDate=insertDueFromDate;
	}
	public String getinsertDueFromDate(){
		return insertDueFromDate;
	}
	
	public void setsecurity(String security){
		this.security=security;
	}
	public String getsecurity(){
		return security;
	}
	public void setdescription(String description){
		this.description=description;
	}
	public String getdescription(){
		return description;
	}
	public void setprincipalPaid(String principalPaid){
		this.principalPaid=principalPaid;
	}
	public String getprincipalPaid(){
		return principalPaid;
	}
}

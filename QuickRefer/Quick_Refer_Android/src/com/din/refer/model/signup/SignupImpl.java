package com.strobilanthes.forrestii.model.signup;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.model.lookuplist.AbstractLookUpList;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.LookupListVolleyTasks;
import com.strobilanthes.forrestii.volleytasks.SignupVolleyTasks;

public class SignupImpl implements Signup,VolleyTasksListener{

	private AbstractSignup abstractSignup; 
	private AbstractLookUpList abstractLookUpList;
	List<PropertyChangeListener> listener=new ArrayList<PropertyChangeListener>();
	private Activity context;
	private CustomProgressbar mCustomProgressbar;
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners( PropertyChangeEvent evt ){
		for ( Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator.hasNext(); ){
			PropertyChangeListener name = (PropertyChangeListener)iterator.next();
			name.propertyChange(evt);
		}
	}
	
	@Override
	public void handleResult(String method_name,JSONObject jsonObject) {
		
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		if(method_name.equals("registration")){
			AbstractSignup previousResult= abstractSignup;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractSignup = gson.fromJson(jsonObject.toString(), AbstractSignup.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractSignup); 
			notifyListeners(event);
		}else if(method_name.equals("lookuplist")){
			AbstractLookUpList previousResult= abstractLookUpList;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractLookUpList = gson.fromJson(jsonObject.toString(), AbstractLookUpList.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractLookUpList); 
			notifyListeners(event);
		}
		
	}

	@Override
	public void handleError(String method_name,Exception e) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		AbstractSignup previousResult= abstractSignup;
		abstractSignup = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this,method_name, previousResult, null); 
		notifyListeners(event);
	}

	@Override
	public void requestRegistrationDetails(String url,JSONObject postdata) {
		//new AuthenticationTasks("registration",this,postdata).execute(url);
		if (mCustomProgressbar == null) {
			mCustomProgressbar = new CustomProgressbar(getContext());
			mCustomProgressbar.show();
		}
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SignupVolleyTasks("registration", this, url,
						postdata).setRetryPolicy(new DefaultRetryPolicy(
						Constant.SOCKET_TIMEOUT,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));	
	}

	@Override
	public void requestLookupdetails(String url, JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(
				new LookupListVolleyTasks("lookuplist", this, url,
						postdata).setRetryPolicy(new DefaultRetryPolicy(
						Constant.SOCKET_TIMEOUT,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));	
	}

	
	@Override
	public void doValidation(String content) {
		
	}

	@Override
	public AbstractSignup getSignupDetails() {
		return abstractSignup;
	}
	
	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	
	@Override
	public AbstractLookUpList getLookupDetails() {
		return abstractLookUpList;
	}
}

package com.strobilanthes.forrestii.model.login;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Login extends ForrestiiSubject{
	void requestLoginDetails(String url,JSONObject postdata);
	void doValidation(String content);
	AbstractLogin getLoginDetails();
	void updateAuthDetails();
}

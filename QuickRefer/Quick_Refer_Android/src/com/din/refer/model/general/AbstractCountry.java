package com.strobilanthes.forrestii.model.general;

import java.io.Serializable;
import java.util.ArrayList;

public class AbstractCountry implements Serializable {
	private static final long serialVersionUID = 1L;

	private int countryId;
	private String countryCode;
	private String countryName;
	private String phoneCountryCode;
	private ArrayList<States> states;

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getPhoneCountryCode() {
		return phoneCountryCode;
	}

	public void setPhoneCountryCode(String phoneCountryCode) {
		this.phoneCountryCode = phoneCountryCode;
	}

	public ArrayList<States> getStates() {
		return states;
	}

	public void setStates(ArrayList<States> states) {
		this.states = states;
	}

	public class States implements Serializable {
		private static final long serialVersionUID = 1L;

		private int stateId;
		private String stateCode;
		private String stateName;

		private ArrayList<Cities> cities;

		public int getStateId() {
			return stateId;
		}

		public void setStateId(int stateId) {
			this.stateId = stateId;
		}

		public ArrayList<Cities> getCities() {
			return cities;
		}

		public void setCities(ArrayList<Cities> cities) {
			this.cities = cities;
		}

		public String getStateName() {
			return stateName;
		}

		public void setStateName(String stateName) {
			this.stateName = stateName;
		}

		public String getStateCode() {
			return stateCode;
		}

		public void setStateCode(String stateCode) {
			this.stateCode = stateCode;
		}

		@Override
		public String toString() {
			return stateName;
		}

	}

	public static class Cities implements Serializable {
		private static final long serialVersionUID = 1L;

		private int cityId;
		private String cityCode;
		private String cityName;
		private ArrayList<Pincodes> pincodes;

		public int getCityId() {
			return cityId;
		}

		public void setCityId(int cityId) {
			this.cityId = cityId;
		}

		public String getCityCode() {
			return cityCode;
		}

		public void setCityCode(String cityCode) {
			this.cityCode = cityCode;
		}

		public String getCityName() {
			return cityName;
		}

		public void setCityName(String cityName) {
			this.cityName = cityName;
		}

		public ArrayList<Pincodes> getPincodes() {
			return pincodes;
		}

		public void setPincodes(ArrayList<Pincodes> pincodes) {
			this.pincodes = pincodes;
		}

		@Override
		public String toString() {
			return cityName;
		}
		
	}

	public class Pincodes implements Serializable {
		private static final long serialVersionUID = 1L;

		private int pincodeId;
		private int pincodeCode;

		public int getPincodeId() {
			return pincodeId;
		}

		public void setPincodeId(int pincodeId) {
			this.pincodeId = pincodeId;
		}

		public int getPincodeCode() {
			return pincodeCode;
		}

		public void setPincodeCode(int pincodeCode) {
			this.pincodeCode = pincodeCode;
		}

		@Override
		public String toString() {
			return ""+pincodeCode;
		}
		
	}

	@Override
	public String toString() {
		return countryName;
	}
	
}

package com.strobilanthes.forrestii.model.referral;

import java.io.Serializable;


public class AbstractReferredQuickCard implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String mobileNumber;
	private String emailAddress;
	private int quickCardId;
	private long updatedOn;
	private float totalRating;
	private int reviewCount;

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public int getQuickCardId() {
		return quickCardId;
	}
	public void setQuickCardId(int quickCardId) {
		this.quickCardId = quickCardId;
	}
	public long getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}
	public float getTotalRating() {
		return totalRating;
	}
	public void setTotalRating(float totalRating) {
		this.totalRating = totalRating;
	}
	public int getReviewCount() {
		return reviewCount;
	}
	public void setReviewCount(int reviewCount) {
		this.reviewCount = reviewCount;
	}
	
}

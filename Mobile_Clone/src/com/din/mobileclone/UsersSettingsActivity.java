//------------------------------------------------------------------------------
//
//    (c) Copyright 2014.Dinesh Kumar M. All Rights Reserved.
//
//------------------------------------------------------------------------------

package com.din.mobileclone;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;

public class UsersSettingsActivity extends SherlockPreferenceActivity implements OnSharedPreferenceChangeListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.my_location_demo);
        
        getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Settings");
		
		
		addPreferencesFromResource(R.xml.preferences);
		
		// show the current value in the settings screen
	    for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++) {
	      initSummary(getPreferenceScreen().getPreference(i));
	    }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
		
		if (item.getItemId() == android.R.id.home) {
			finish();
		}

		return super.onOptionsItemSelected(item);
	}

    @Override
    protected void onResume() {
      super.onResume();
      getPreferenceScreen().getSharedPreferences()
          .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
      super.onPause();
      getPreferenceScreen().getSharedPreferences()
          .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
        String key) {
      updatePreferences(findPreference(key));
    }

    private void initSummary(Preference p) {
      if (p instanceof PreferenceCategory) {
        PreferenceCategory cat = (PreferenceCategory) p;
        for (int i = 0; i < cat.getPreferenceCount(); i++) {
          initSummary(cat.getPreference(i));
        }
      } else {
        updatePreferences(p);
      }
    }

    private void updatePreferences(Preference p) {
      if (p instanceof EditTextPreference) {
        EditTextPreference editTextPref = (EditTextPreference) p;
        p.setSummary(editTextPref.getText());
      }else if(p instanceof CheckBoxPreference){
    	  CheckBoxPreference checkBox = (CheckBoxPreference)p;
    	  p.setDefaultValue(checkBox.isChecked());
      }
    }
    
   
}

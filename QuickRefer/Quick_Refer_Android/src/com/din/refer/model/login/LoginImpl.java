package com.strobilanthes.forrestii.model.login;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.LoginVolleyTasks;


public class LoginImpl implements Login,VolleyTasksListener{

	private AbstractLogin abstractLogin; 
	List<PropertyChangeListener> listener=new ArrayList<PropertyChangeListener>();
	private Activity context;
	private CustomProgressbar mCustomProgressbar;
	
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners( PropertyChangeEvent evt ){
		for ( Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator.hasNext(); ){
			PropertyChangeListener name = (PropertyChangeListener)iterator.next();
			name.propertyChange(evt);
		}
	}
	
	@Override
	public void handleResult(String method_name,JSONObject jsonObject) {

		try {
	        if ((this.mCustomProgressbar != null) && this.mCustomProgressbar.isShowing()) {
	            this.mCustomProgressbar.dismiss();
	        }
	    } catch (final IllegalArgumentException e) {
	    	e.printStackTrace();
	    } catch (final Exception e) {
	    	e.printStackTrace();
	    } finally {
	        this.mCustomProgressbar = null;
	    }  
//		((LoginActivity) getContext()).setSupportProgressBarIndeterminateVisibility(false);
		
		AbstractLogin previousResult= abstractLogin;

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.serializeNulls();
		Gson gson = gsonBuilder.create();	
		abstractLogin = gson.fromJson(jsonObject.toString(), AbstractLogin.class);
		
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractLogin); 
		notifyListeners(event);
	}

	@Override
	public void handleError(String method_name,Exception e) {
		try {
	        if ((this.mCustomProgressbar != null) && this.mCustomProgressbar.isShowing()) {
	            this.mCustomProgressbar.dismiss();
	        }
	    } catch (final IllegalArgumentException ex) {
	    	ex.printStackTrace();
	    } catch (final Exception ex) {
	    	ex.printStackTrace();
	    } finally {
	        this.mCustomProgressbar = null;
	    }  
//		((LoginActivity) getContext()).setSupportProgressBarIndeterminateVisibility(false);
		
		AbstractLogin previousResult= abstractLogin;
		abstractLogin = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this,method_name, previousResult, null); 
		notifyListeners(event);
	}

	@Override
	public void requestLoginDetails(String url,JSONObject postdata) {
		//new AuthenticationTasks("login",this,postdata).execute(url);
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
//		((LoginActivity) getContext()).setSupportProgressBarIndeterminateVisibility(true);
		
		//((LoginActivity) getActivity()).setSupportProgressBarIndeterminateVisibility(true);
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new LoginVolleyTasks("login", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));	
	}

	@Override
	public void doValidation(String content) {
		
	}

	@Override
	public AbstractLogin getLoginDetails() {
		return abstractLogin;
	}

	@Override
	public void updateAuthDetails() {
	}

	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}
}

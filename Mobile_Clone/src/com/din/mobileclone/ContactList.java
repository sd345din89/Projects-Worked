//------------------------------------------------------------------------------
//
//    (c) Copyright 2014.Dinesh Kumar M. All Rights Reserved.
//
//------------------------------------------------------------------------------

package com.din.mobileclone;

import java.io.Serializable;

public class ContactList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String personName;
	private String phoneNum;
	private String personImage;
	private String personEmailId;
	private String contactId;
	public ContactList(){
	}
	
	public String getContactId(){
		return contactId;
	}
	public void setContactId(String contactId){
		this.contactId = contactId;
	}
	
	public String getPersonName(){
		return personName;
	}
	public void setPersonName(String personName){
		this.personName=personName;
	}
	
	public String getPhoneNum(){
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum){
		this.phoneNum=phoneNum;
	}
	
	public String getPersonImage(){
		return personImage;
	}
	public void setPersonImage(String personImage){
		this.personImage=personImage;
	}
	
	public String getPersonEmailId(){
		return personEmailId;
	}
	public void setPersonEmailId(String personEmailId){
		this.personEmailId = personEmailId;
	}
	
}

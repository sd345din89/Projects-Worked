package com.strobilanthes.forrestii.model.quickcard;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Quickcard extends ForrestiiSubject {

	void createQuickCard(String url, JSONObject postdata);

	void updateQuickCard(String url, JSONObject postdata);

	AbstractQuickcard getQuickcardDetails();

}

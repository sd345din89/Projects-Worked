package com.strobilanthes.forrestii.model.wallpost;


public class AbstractDelete{
	
	private int status;
	private String error;
	//private ArrayList<GetWallPost> getWallPosts = new ArrayList<GetWallPost>();
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	/*public ArrayList<GetWallPost> getGetWallPosts() {
		return getWallPosts;
	}

	public void setGetWallPosts(GetWallPost getWallPost) {
		this.getWallPosts.add(getWallPost);
	}*/

}

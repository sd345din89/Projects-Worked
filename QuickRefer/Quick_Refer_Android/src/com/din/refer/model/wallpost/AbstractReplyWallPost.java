package com.strobilanthes.forrestii.model.wallpost;


public class AbstractReplyWallPost {
	
	private int status;
	private String error;
	private ReplyResponse response;
	//private ArrayList<GetWallPost> getWallPosts = new ArrayList<GetWallPost>();
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}



	/*public ArrayList<GetWallPost> getGetWallPosts() {
		return getWallPosts;
	}

	public void setGetWallPosts(GetWallPost getWallPost) {
		this.getWallPosts.add(getWallPost);
	}*/

	public ReplyResponse getReplyResponse() {
		return response;
	}

	public void setReplyResponse(ReplyResponse response) {
		this.response = response;
	}



	public class ReplyResponse{
		private int replyId;
		
		public int getReplyId() {
			return replyId;
		}

		public void setReplyId(int replyId) {
			this.replyId = replyId;
		}
	}
}

package kra.accounts.tracker.models;




import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import org.apache.poi.hssf.record.DBCellRecord;

import android.R.bool;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import kra.accounts.tracker.R;
import kra.accounts.tracker.database.KraFinanceDbManager;



public class Util {
	
	
	public int getPendingAmountCount(KraFinanceDbManager dbManager){
		int count = -1;
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		Long currentDate = System.currentTimeMillis();
		if(Constants.DEBUG_LOG)Log.d("Current time", ""+currentDate);
		
		String pendingCount = "SELECT COUNT(customerAccId) FROM CustomerAccount WHERE interestPaidUptoDate < "+ currentDate +" AND  isPrincipalPaid='n'";
		
		Cursor cursor = database.rawQuery(pendingCount, null);
		
		if(cursor != null){
			if(cursor.moveToFirst()){
				count = cursor.getInt(0);
			}
		}
		
		
		
		cursor.close();
		database.close();
		
		return count;
	}
	
	public long getPendingAmountCountForCustomer(KraFinanceDbManager dbManager, String customerId, boolean isSummery){
		
		long count = -1;
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		Long currentDate = System.currentTimeMillis();
		if(Constants.DEBUG_LOG)Log.d("Current time", ""+currentDate);
		String pendingCount;
		
		if(!isSummery)
			pendingCount = "SELECT MIN(interestPaidUptoDate) FROM CustomerAccount WHERE interestPaidUptoDate < "+ currentDate +" AND  isPrincipalPaid='n'  AND customerId="+customerId;
		else
			pendingCount = "SELECT MIN(interestPaidUptoDate) FROM CustomerAccount WHERE isPrincipalPaid='n' AND  customerId="+customerId;

		
		Cursor cursor = database.rawQuery(pendingCount, null);
		
		if(cursor != null){
			if(cursor.moveToFirst()){
				count = Long.parseLong(cursor.getString(0));
			}
		}
		
		
		
		cursor.close();
		database.close();
		
		return count;
	}
	
	
	public String getAccountCount(KraFinanceDbManager dbManager, String customerId){
		
		String accCount = null;
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		
		/*String query = "SELECT ca.accountId FROM CustomerAccount ca, CustomerMaster cm "+
						"WHERE cm.customerId = ca.customerId AND ca.isPrincipalPaid='n' AND ca.customerId ="+customerId+
						" ORDER BY ca.interestPaidUptoDate";*/
		
		String query = "SELECT ca.accountId FROM CustomerAccount ca, CustomerMaster cm "+
				"WHERE cm.customerId = ca.customerId AND ca.isPrincipalPaid='n' AND ca.customerId ="+customerId+
				" ORDER BY ca.interestPaidUptoDate";
		
		
		
		Cursor cursor = database.rawQuery(query, null);
		
		if(cursor != null){
			accCount = ""+cursor.getCount();
		}
		
		cursor.close();
		
		database.close();
		
		return accCount;
		
	}
	
	
	public ArrayList<CustomerAccountDetails> getUserPendingList(KraFinanceDbManager dbManager, int customerId, boolean isFromSummary){
		
		ArrayList<CustomerAccountDetails> customerAccountDetails = null;
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		
		Long currentDate = System.currentTimeMillis();
		if(Constants.DEBUG_LOG)Log.d("Current time", ""+currentDate);
		String query ;
		
		if(isFromSummary){
			
			query = "SELECT ca.customerId,ca.principalAmt,ca.interestPaidUptoDate,ca.accountId,ca.principalDate,cm.address, cm.ContactNumber, cm.name, ca.security, ca.customerAccId FROM CustomerAccount ca, CustomerMaster cm "+
					"WHERE cm.customerId = ca.customerId AND ca.isPrincipalPaid='n' AND ca.customerId ="+customerId+
					" ORDER BY ca.interestPaidUptoDate";
			
		}else{
			query = "SELECT ca.customerId,ca.principalAmt,ca.interestPaidUptoDate,ca.accountId,ca.principalDate,cm.address, cm.ContactNumber, cm.name, ca.security , ca.customerAccId FROM CustomerAccount ca, CustomerMaster cm "+
					"WHERE cm.customerId = ca.customerId AND interestPaidUptoDate < "+currentDate+" AND  ca.isPrincipalPaid='n' AND ca.customerId ="+customerId+
					" ORDER BY ca.interestPaidUptoDate";
			
			
		}
		
		Log.d("CHECK", ""+query);
		
		
		Cursor cursor = database.rawQuery(query, null);
		
		if(cursor != null){
			
			if(Constants.DEBUG_LOG)Log.d("CHECK", "cursor length="+cursor.getCount()+" column count="+cursor.getColumnCount());
			
			if(cursor.moveToFirst()){
				customerAccountDetails = new ArrayList<CustomerAccountDetails>();
				
				do{
					CustomerAccountDetails  accountDetails =  new CustomerAccountDetails();
					
					accountDetails.setcustomerId(""+cursor.getInt(0));
					accountDetails.setprincipalAmount(""+cursor.getInt(1));
					accountDetails.setinsertDueFromDate(""+cursor.getString(2));
					accountDetails.setAccountId(""+cursor.getInt(3));
					accountDetails.setprincipalDate(""+cursor.getString(4));
					accountDetails.setCustomerAddress(""+cursor.getString(5));
					accountDetails.setCustomerMobile1(""+cursor.getString(6));
					accountDetails.setCustomerName(""+cursor.getString(7));
					accountDetails.setsecurity(""+cursor.getString(8));
					accountDetails.setcustomerAccId(""+cursor.getString(9));
					
					customerAccountDetails.add(accountDetails);
					
				}while(cursor.moveToNext());
				
			}
		}
		
		cursor.close();
		database.close();
		
		
		return customerAccountDetails;
		
	}
	
	public void insertLoginUser(KraFinanceDbManager dbManager, String userName, String password){
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		
		database.delete("LoginUser", null, null);
		
		ContentValues contentValues = new ContentValues();
		contentValues.put("userName", userName);
		contentValues.put("password", password);
		
		database.insert("LoginUser", null, contentValues);
		
		database.close();
		
		
	}
	
	
	
	
	
	public String  checkLoginStatus(KraFinanceDbManager dbManager){
		String loginUser = null;
		SQLiteDatabase database = dbManager.getWritableDatabase();
		
		Cursor cursor = database.query("LoginUser", new String[]{"userName"}, null, null, null, null, null);
		if(cursor != null){
			if(cursor.moveToFirst()){
				loginUser = cursor.getString(0);
			}
		}
		
		
		cursor.close();
		database.close();
		
		return loginUser;
	}
	
	
	public void deleteLoginUSer(KraFinanceDbManager dbManager){
		SQLiteDatabase database = dbManager.getWritableDatabase();
		
		database.delete("LoginUser", null, null);
		
		database.close();
	}
	
	public Cursor getTransactionCursor(SQLiteDatabase database){
		
		Cursor cursor = database.query("Transcations", null, null, null, null, null, null);
		return cursor;
	}
	
	
	public Cursor getCustomerAccountsCursor(SQLiteDatabase database){
		
		Cursor cursor = database.query("CustomerAccount", new String[]{"customerAccId","customerId","accountId","principalAmt","principalDate","interestPaidUptoDate","security","isPrincipalPaid"}, null, null, null, null, null);
		return cursor;
	}
	
	public Cursor getCustomerMasterCursor(SQLiteDatabase database){
		
		Cursor cursor = database.query("CustomerMaster", null, null, null, null, null, null);
		return cursor;
	}
	
	public Cursor getAccountMasterCursor(SQLiteDatabase database){
		
		Cursor cursor = database.query("AccountMaster", null, null, null, null, null, null);
		return cursor;
	}
	
	
	
	public void updateUserPrincipalPaid(KraFinanceDbManager dbManager, String customerId, String accId){
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		
		ContentValues  values = new ContentValues();
		values.put("isPrincipalPaid", "y");
		
		int res = database.update("CustomerAccount", values, "custAccountId="+customerId+" AND accountId="+accId, null);
		if(Constants.DEBUG_LOG)Log.d("CHECK", ">>>>>cus id="+customerId+" accId="+accId+" res="+res);
		
		
		database.close();
		
		
	}
	
	public AccountDetails getAccountDetails(KraFinanceDbManager dbManager,String accId){
			
			AccountDetails accountDetails= null;
			
			SQLiteDatabase database = dbManager.getWritableDatabase();
			Long currentDate = System.currentTimeMillis();
			if(Constants.DEBUG_LOG)Log.d("Current time", ""+currentDate);
			
			String pendingCount = "SELECT accountName,desc FROM AccountMaster WHERE accountId="+accId ;
			
			Cursor cursor = database.rawQuery(pendingCount, null);
			
			if(cursor != null){
				if(cursor.moveToFirst()){
					
					accountDetails = new AccountDetails();
					accountDetails.setaccountName(cursor.getString(0));
					accountDetails.setdescription(cursor.getString(1));
				}
			}
			cursor.close();
			database.close();
			
			return accountDetails;
			
		}

	
	public ArrayList<CustomerAccountDetails> getInterestPendingUserList(KraFinanceDbManager dbManager, int sortByDate, int sortByName, int sortByPriAmount, int filterByPeriod, String accountId, String customerName){
		
		ArrayList<CustomerAccountDetails> customerAccountDetails = null;
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		Long currentDate = System.currentTimeMillis();
		String dateSort = "Min";
		
		/*Log.d("CHECK", ">>>"+sortBy);
		if(sortBy != 0){
			dateSort = "Max";
		}*/
		
		String query = "SELECT ca.customerId,ca.principalAmt,ca.interestPaidUptoDate,ca.accountId,ca.principalDate, cm.name FROM CustomerAccount ca, CustomerMaster cm "+
		"WHERE cm.customerId = ca.customerId "; 
		if(accountId != null){
			query = query+" AND ca.accountId="+accountId; 
		}
		if(customerName != null){
			query = query+" AND LOWER(cm.name) LIKE LOWER('%"+customerName+"%')";
		}
		if(filterByPeriod == -1){
			
			query =query+" And interestPaidUptoDate=("+
			"SELECT MIN(interestPaidUptoDate) FROM CustomerAccount casum WHERE interestPaidUptoDate < "+currentDate+"  AND isPrincipalPaid='n' "+
			" And cm.customerId=casum.customerId)";
			
		}else{

			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			
			if(filterByPeriod ==0){
				
				calendar.add(Calendar.MONTH, -1);
				
			}else if(filterByPeriod ==1){
				
				calendar.add(Calendar.MONTH, -3);
				
			}else{
				
				calendar.add(Calendar.YEAR, -1);
				
			}
			
			if(Constants.DEBUG_LOG)Log.d("CHECK", "due date="+convertLongToDate(""+calendar.getTimeInMillis(), "dd.MM.yyyy"));

			query =query+" And interestPaidUptoDate=("+
					"SELECT MIN(interestPaidUptoDate) FROM CustomerAccount casum WHERE interestPaidUptoDate < "+currentDate+" AND interestPaidUptoDate > "+calendar.getTimeInMillis()+" AND isPrincipalPaid='n' "+
					" And cm.customerId=casum.customerId)";
		}
		
		if(sortByName == 0){
			query = query + " ORDER BY ca.partyName";
		}else if(sortByName == 1){
			query = query + " ORDER BY ca.partyName DESC";
		}
		
		if(sortByDate == 0){
			query = query + " ORDER BY ca.interestPaidUptoDate";
		}else if(sortByDate == 1){
			query = query + " ORDER BY ca.interestPaidUptoDate DESC";
		}
		
		if(sortByPriAmount == 0){
			query = query + " ORDER BY ca.principalAmt";
		}else if(sortByPriAmount == 1){
			query = query + " ORDER BY ca.principalAmt DESC";
		}
		
		/*String query = "SELECT * FROM PartyList WHERE " +
						"duesFrom < "+currentDate;
		
		if(accountId != null){
			query = query + " AND inBook='"+accountId+"'";
		}
		
		if(customerName != null){
			query = query + " AND LOWER(partyName) LIKE LOWER('%"+customerName+"%')";
		}
		
		query = query + " ORDER BY duesFrom ASC";*/
		
		
		
		if(Constants.DEBUG_LOG)Log.d("CHECK", "query="+query);

		Cursor cursor = database.rawQuery(query, null);
		
		if(cursor != null){
			
			if(cursor.moveToFirst()){
				
				customerAccountDetails = new ArrayList<CustomerAccountDetails>();
				do{
					CustomerAccountDetails  accountDetails =  new CustomerAccountDetails();
					
					accountDetails.setcustomerId(""+cursor.getString(0));
					accountDetails.setprincipalAmount(""+cursor.getString(1));
					accountDetails.setinsertDueFromDate(""+cursor.getString(2));
					accountDetails.setAccountId(""+cursor.getString(3));
					accountDetails.setprincipalDate(""+cursor.getString(4));
					accountDetails.setCustomerName(""+cursor.getString(5));
					
					
					customerAccountDetails.add(accountDetails);
					
				}while(cursor.moveToNext());
				
				
			}
		}
		
		return customerAccountDetails;
	}
	
	
	public ArrayList<CustomerAccountDetails> getSummeryUserList(KraFinanceDbManager dbManager,int sortByDate, int sortByName, int sortByPriAmount, int filterByPeriod, String accountId, String customerName){
		
		ArrayList<CustomerAccountDetails> customerAccountDetails = null;
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		/*String dateSort = "Min";
		
		Log.d("CHECK", ">>>"+sortBy);
		if(sortBy != 0){
			dateSort = "Max";
		}*/
		
		String query = "SELECT DISTINCT ca.interestPaidUptoDate,ca.principalAmt,ca.customerId,ca.accountId,ca.principalDate, cm.name FROM CustomerAccount ca, CustomerMaster cm "+
		"WHERE cm.customerId = ca.customerId "; 
		
		if(accountId != null){
			query = query+" AND accountId="+accountId; 
		}
		
		if(customerName != null){
			query = query+" AND LOWER(cm.name) LIKE LOWER('%"+customerName+"%')";
		}
		if(filterByPeriod == -1){
			query =query+" And interestPaidUptoDate=("+
			"SELECT MIN(interestPaidUptoDate) FROM CustomerAccount casum WHERE isPrincipalPaid='n'"+
			" And cm.customerId=casum.customerId)";
		
		}else{
	
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			
			if(filterByPeriod ==0){
				
				calendar.add(Calendar.MONTH, -1);
				
			}else if(filterByPeriod ==1){
				
				calendar.add(Calendar.MONTH, -3);
				
			}else{
				
				calendar.add(Calendar.YEAR, -1);
				
			}
		
			if(Constants.DEBUG_LOG)Log.d("CHECK", "due date="+convertLongToDate(""+calendar.getTimeInMillis(), "dd.MM.yyyy"));

			query =query+" And interestPaidUptoDate=("+
					"SELECT MIN(interestPaidUptoDate) FROM CustomerAccount casum WHERE interestPaidUptoDate < "+System.currentTimeMillis()+" AND interestPaidUptoDate > "+calendar.getTimeInMillis()+" AND isPrincipalPaid='n' "+
					" And cm.customerId=casum.customerId)";
		}
		
		if(sortByName == 0){
			query = query + " ORDER BY ca.partyName";
		}else if(sortByName == 1){
			query = query + " ORDER BY ca.partyName DESC";
		}
		
		if(sortByDate == 0){
			query = query + " ORDER BY ca.interestPaidUptoDate";
		}else if(sortByDate == 1){
			query = query + " ORDER BY ca.interestPaidUptoDate DESC";
		}
		
		if(sortByPriAmount == 0){
			query = query + " ORDER BY ca.principalAmt";
		}else if(sortByPriAmount == 1){
			query = query + " ORDER BY ca.principalAmt DESC";
		}
		
		
		
		Log.d("CHECK","query="+query);
		
		Cursor cursor = database.rawQuery(query, null);
		
		if(cursor != null){
			Log.d("CHECK", "cursor length="+cursor.getCount());
			
			if(cursor.moveToFirst()){
				
				customerAccountDetails = new ArrayList<CustomerAccountDetails>();
				do{
					CustomerAccountDetails  accountDetails =  new CustomerAccountDetails();
					
					accountDetails.setcustomerId(""+cursor.getString(2));
					accountDetails.setprincipalAmount(""+cursor.getString(1));
					accountDetails.setinsertDueFromDate(""+cursor.getString(0));
					accountDetails.setAccountId(""+cursor.getString(3));
					accountDetails.setprincipalDate(""+cursor.getString(4));
					accountDetails.setCustomerName(""+cursor.getString(5));

					customerAccountDetails.add(accountDetails);
					
				}while(cursor.moveToNext());
				
				
			}
		}
		
		return customerAccountDetails;
	}
	
	
	public String convertLongToDate(String dateObj, String dateFormat){
		
		String date= new SimpleDateFormat(dateFormat, Locale.getDefault()).format(Long.parseLong(dateObj)).toString();
		//if(Constants.DEBUG_LOG)Log.d("CHECK", "Date="+date);
		
		return date;
	}
	
	public ProgressDialog showProgressView(Context context, String message){
		
		ProgressDialog dialog = new ProgressDialog(context);
		dialog.setCancelable(false);
		dialog.show();
        dialog.getWindow().setGravity(Gravity.BOTTOM );
        
        
        LayoutInflater inflator = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=inflator.inflate(R.layout.progress_view, null);
        TextView progressmessage=(TextView)view.findViewById(R.id.progressText);
        progressmessage.setText(message);
        
        Animation animate=AnimationUtils.loadAnimation(context, R.anim.bottom_to_top);
		view.startAnimation(animate);

       
		dialog.getWindow().setContentView(view);
        LayoutParams params = dialog.getWindow().getAttributes();
        params.dimAmount=(float)0.0;
        params.width=LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(params);
        
        
        return dialog;
        
	}
	
	public void showAlert(Context context, String title, String message) {

			try {
				AlertDialog.Builder builder = new Builder(context);
				builder.setTitle(title);
				builder.setMessage(message);

				builder.setPositiveButton("OK", null);

				builder.show();

			} catch (Exception e) {
				e.printStackTrace();
			}

	}
		
	
	
	// *****************Collection Details*****************
	public CollectionDetails getCollectionDetails(KraFinanceDbManager dbManager,int customerAccId, boolean isFromsummary){
		
		CollectionDetails collectionDetails=new CollectionDetails();
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		Long currentDate = System.currentTimeMillis();
		if(Constants.DEBUG_LOG)Log.d("Current time", ""+currentDate);
		
		
		String pendingCount = "SELECT ca.customerId,ca.accountId,cm.name,cm.address,cm.ContactNumber,ca.principalAmt,ca.principalDate,ca.interestPaidUptoDate FROM  CustomerMaster cm,CustomerAccount ca"
								+" WHERE cm.customerId = ca.customerId AND ";
		if(!isFromsummary)
			pendingCount = pendingCount+"interestPaidUptoDate < "+currentDate+" AND ";
		
		
		pendingCount = pendingCount+"isPrincipalPaid='n' AND ca.customerAccId="+customerAccId
								+" ORDER BY ca.interestPaidUptoDate";
		
		Log.d("CHECK", "pendingCount="+pendingCount);
		
		Cursor cursor = database.rawQuery(pendingCount, null);
		
		
		if(cursor != null){
			
			if(cursor.moveToFirst()){
				
				collectionDetails.setcustomerId(""+cursor.getInt(0));
				collectionDetails.setAccountId(""+cursor.getInt(1));
				collectionDetails.setName(cursor.getString(2));
				collectionDetails.setAdderess(cursor.getString(3));
				collectionDetails.setPhoneNum(cursor.getString(4));
				//collectionDetails.setcustomerAccId(""+cursor.getInt(2));
				collectionDetails.setprincipalAmount(""+cursor.getInt(5));
				collectionDetails.setprincipalDate(""+cursor.getString(6));

				collectionDetails.setinsertDueFromDate(""+""+cursor.getString(7));
			//	collectionDetails.setsecurity(cursor.getString(9));
			//	collectionDetails.setdescription(cursor.getString(10));
			//	collectionDetails.setprincipalPaid(cursor.getString(11));	
			}
		}
		cursor.close();
		database.close();
		
		return collectionDetails;
	}
	
	public AccountDetails getAccountDetails(KraFinanceDbManager dbManager,int accId){
		
		AccountDetails accountDetails=new AccountDetails();
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		Long currentDate = System.currentTimeMillis();
		if(Constants.DEBUG_LOG)Log.d("Current time", ""+currentDate);
		
		String pendingCount = "SELECT accountName,desc FROM AccountMaster WHERE accountId="+accId ;
		
		Cursor cursor = database.rawQuery(pendingCount, null);
		
		if(cursor != null){
			if(cursor.moveToFirst()){
				accountDetails.setaccountName(cursor.getString(0));
				accountDetails.setdescription(cursor.getString(1));
			}
		}
		cursor.close();
		database.close();
		
		return accountDetails;
	}

	public void updateTransactionTable(KraFinanceDbManager dbManager,long updatedInterestdueDate, int accId, long dueDate, long paidDate, String collectedBy, String payMode, String comments, String lat, String lon, String deviceId){
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		
		//database.delete("LoginUser", null, null);
		
		ContentValues contentValues = new ContentValues();
		contentValues.put("custAccountId", accId);
		contentValues.put("dueDate", dueDate);
		contentValues.put("paidDate", paidDate);
		contentValues.put("collectedby", collectedBy);
		contentValues.put("paymentMode", payMode);
		contentValues.put("comments", comments);
		contentValues.put("lat", lat);
		contentValues.put("lon", lon);
		contentValues.put("deviceId", deviceId);
		
		database.insert("Transcations", null, contentValues);
		
		contentValues = new ContentValues();
		contentValues.put("interestPaidUptoDate", updatedInterestdueDate);
		
		//database.insert("Transcations", null, contentValues);*/
		
		database.update("CustomerAccount", contentValues, "customerAccId="+accId , null);
		
		database.close();
		
		
	}
	
	//=====================================================
	
	public void connectToCustomerMob(Context context, String mobNumber){
		Log.v("CHECK Call", "*********");
		Intent callIntent = new Intent(Intent.ACTION_CALL);
	    callIntent.setData(Uri.parse("tel:"+mobNumber));
	    context.startActivity(callIntent);
	}

	public String formatAmount(String amount){
		NumberFormat formatter = new DecimalFormat("##,##,##,###");
		return formatter.format(Double.parseDouble(amount));
		
	}
	
	public String[] getAllAccountList(KraFinanceDbManager dbManager){
		String[] accList = null;
		SQLiteDatabase database = dbManager.getWritableDatabase();
		
		Cursor  cursor = database.query("AccountMaster", new String[]{"accountName"}, null, null, null, null, null);
		
		if(cursor != null){
			if(cursor.moveToFirst()){
				int index = 0;
				accList = new String[cursor.getCount() + 1];
				do{
					accList[index] = cursor.getString(0);
					index++;
				}while(cursor.moveToNext());
				
				accList[index] = "All account";
			}
		}
		
		cursor.close();
		database.close();
		
		return accList;
	}
	
	public String getAccountId(KraFinanceDbManager dbManager, SQLiteDatabase db, String accName){
		
		String accId = "-1";
		
		SQLiteDatabase database = null;
		
		if(db == null)
			database = dbManager.getWritableDatabase();
		else
			database = db;
		
		Cursor  cursor = database.query("AccountMaster", new String[]{"accountId"}, "accountName='"+accName+"'", null, null, null, null);
		
		if(cursor != null){
			if(cursor.moveToFirst()){
				accId = cursor.getString(0);
			}
		}
		
		
		
		cursor.close();
		
		if(db == null)
			database.close();
		
		return accId;
		
	}
	
	public String getDeviceUDID(Context context){
		
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String tmDevice = "" + tm.getDeviceId();
		String tmSerial = "" + tm.getSimSerialNumber();
	    String androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

	    UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
	    Log.d("DEVICE-UDID",""+deviceUuid.toString());
	    
	    return deviceUuid.toString();
	    
	}
	
	
	 public String getTransactionDueDate(KraFinanceDbManager dbManager,int accId){
			
			String dueDate="";
			SQLiteDatabase database = dbManager.getWritableDatabase();

			String pendingCount = "SELECT dueDate FROM Transcations WHERE custAccountId="+accId ;
			
			Cursor cursor = database.rawQuery(pendingCount, null);
			
			if(cursor != null){
				if(cursor.moveToFirst()){
					dueDate = cursor.getString(0);
				}
			}
			cursor.close();
			database.close();
			
			return dueDate;
		}
		
		public ArrayList<TransactionDetails> getTransactionDetails(KraFinanceDbManager dbManager,int accId){

			ArrayList<TransactionDetails> arrayList =new ArrayList<TransactionDetails>();
			TransactionDetails transactionDetails=new TransactionDetails();
			
			SQLiteDatabase database = dbManager.getWritableDatabase();
			
			String pendingCount = "SELECT dueDate,paidDate FROM Transcations WHERE custAccountId="+accId+" ORDER BY dueDate DESC LIMIT 10";
			
			Cursor cursor = database.rawQuery(pendingCount, null);
			
			Log.d("ExcelParcer", "Cursor Count = " + cursor.getCount());
			
			if(cursor != null){
				if(cursor.moveToFirst()){
					do{
						//Log.d("ExcelParcer", "Due Date DB = " +cursor.getString(0));
						
						
						transactionDetails.setdueDate(cursor.getString(0));
						transactionDetails.setpaidDate(cursor.getString(1));
						arrayList.add(transactionDetails);
						transactionDetails=new TransactionDetails();
						
					}while(cursor.moveToNext());
				}
			}
			cursor.close();
			database.close();
			
			return arrayList;
		}

	
	public int countDueDateInMonth(String dueDate){
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		
		long resultForMonths = 0;
		if(calendar.getTimeInMillis() > Long.parseLong(dueDate)){
			
			resultForMonths = calendar.getTimeInMillis() - Long.parseLong(dueDate);
			if(Constants.DEBUG_LOG)Log.d("CHECK",dueDate+"<<>>"+resultForMonths);
			calendar.setTimeInMillis(resultForMonths);
			
		    return calendar.get(Calendar.MONTH);
		    
		}else{
			
			return 0;
		}
	   
	}
	
	public int getNumOfMonths(long start,long end){
		
		//============= Start Time ================
		
		if(Constants.DEBUG_LOG)Log.d("CHECK", convertLongToDate(""+start, "dd.MM.yyyy")+"<<<>>>"+convertLongToDate(""+end, "dd.MM.yyyy"));
		
		Calendar interestDueCal = Calendar.getInstance();
		Date date =new Date(start);
		interestDueCal.setTime(date);
		int day = interestDueCal.get(Calendar.DAY_OF_MONTH);
		int month=interestDueCal.get(Calendar.MONTH)+1;
		int year=interestDueCal.get(Calendar.YEAR);
		
		Calendar startCal = Calendar.getInstance();
		startCal.add(Calendar.DAY_OF_MONTH, day);
		startCal.add(Calendar.MONTH, month);
		startCal.add(Calendar.YEAR, year);
		
		int startMonth =  startCal.get(Calendar.MONTH);
		int startYear = startCal.get(Calendar.YEAR);
		//=============================================
		
		//===============End Time===============
		Calendar dueCurrentCal = Calendar.getInstance();
		date =new Date(end);
		dueCurrentCal.setTime(date);
		int dueDay = dueCurrentCal.get(Calendar.DAY_OF_MONTH);
		int dueMonth=dueCurrentCal.get(Calendar.MONTH)+1;
		int dueYear=dueCurrentCal.get(Calendar.YEAR);
		
		Calendar endCal=Calendar.getInstance();
		endCal.add(Calendar.DAY_OF_MONTH, dueDay);
		endCal.add(Calendar.MONTH, dueMonth);
		endCal.add(Calendar.YEAR, dueYear);
		//=================================================
		
		int endYear =  endCal.get(Calendar.YEAR);
		int diffYear = endYear - startYear;
		int endMonth = endCal.get(Calendar.MONTH);
		
		
		int insceptionMonths = 0;
		if(year == dueYear ){
			Calendar cal=Calendar.getInstance();
			cal.clear();
			cal.setTimeInMillis(end - start);
			insceptionMonths =cal.get(Calendar.MONTH);
		}else{
			insceptionMonths = diffYear * 12 + (endMonth - startMonth);
		}
		
		
		return insceptionMonths;
	}

	
	public boolean checkNameIsExistInCustomerMaster(SQLiteDatabase database, String name, String contactNumber, String address){
		
		boolean isExist = false;
		
		Cursor cursor = database.query("CustomerMaster", null, "name='"+name+"' AND ContactNumber='"+contactNumber+"' AND address='"+address+"'", null, null, null, null);
		
		if(cursor != null){
			if(cursor.moveToFirst()){
				isExist = true;
			}
		}

		cursor.close();
		return isExist;
		
	}
	
	public int checkNameIsExistInCustomerAccount(SQLiteDatabase database, String customerId, String contactNumber, String accId, String priAmt, String priDate){
		
		int customerAccId = -1;
		
		Cursor cursor = database.query("CustomerAccount", new String[]{"customerAccId"}, "customerId='"+customerId+"' AND accountId='"+accId+"' AND ContactNumber='"+contactNumber+"' AND principalAmt='"+priAmt+"' AND principalDate='"+priDate+"'", null, null, null, null);
		
		if(cursor != null){
			if(cursor.moveToFirst()){
				customerAccId = cursor.getInt(0);
			}
		}

		cursor.close();
		
		return customerAccId;
	}
	
	public boolean checkAccountIsExist(SQLiteDatabase database, String accName){
		
		boolean isExist = false;
		
		Cursor cursor = database.query("AccountMaster", null, "accountName='"+accName+"'", null, null, null, null);
		
		if(cursor != null){
			if(cursor.moveToFirst()){
				isExist = true;
			}
		}

		cursor.close();
		return isExist;
	}
	
	public int getCustomerId(SQLiteDatabase database, String cusName, String contactNumber){
		int customerId = -1;
		Cursor cursor = database.query("CustomerMaster", new String[]{"customerId"}, "name='"+cusName+"' AND ContactNumber='"+contactNumber+"'", null, null, null, null);
		
		if(cursor != null){
			if(cursor.moveToFirst()){
				customerId = cursor.getInt(0);
			}
		}
		
		cursor.close();
		
		return customerId;
	}
	
	public boolean checkContainsAnyChar(String string){
		if(string == null)return false;
		
		try{
			if( string.matches(".*[a-zA-Z]+.*")){
	           return true;
	        } else {
	        	
	            return false;   
	        }
		}catch (Exception e) {
			return false;
		}
	}

}

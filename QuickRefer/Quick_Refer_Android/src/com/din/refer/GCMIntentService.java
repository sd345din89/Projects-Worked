/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.strobilanthes.forrestii;


import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.strobilanthes.forrestii.activities.MainActivity;
import com.strobilanthes.forrestii.util.Constant;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";
   // static int numWallPostMessages = 0 , numNotifyMessages = 0;;

    public GCMIntentService() {
        super(Constant.SENDER_ID);
    }

    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
       // displayMessage(context, getString(R.string.gcm_registered));
       // ServerUtilities.register(context, registrationId);
        SharedPreferences sharedPreferences = getSharedPreferences("Forrestii", MODE_PRIVATE);
        sharedPreferences.edit().putString("regid", registrationId).commit();
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        /*displayMessage(context, getString(R.string.gcm_unregistered));
        if (GCMRegistrar.isRegisteredOnServer(context)) {
            ServerUtilities.unregister(context, registrationId);
        } else {
            // This callback results from the call to unregister made on
            // ServerUtilities when the registration to the server failed.
            Log.i(TAG, "Ignoring unregister callback");
        }*/
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        
        Log.i(TAG, ""+intent.toString());
        Log.i(TAG, ""+intent.getExtras().toString());
        if(intent != null){
        	Bundle bundle = intent.getExtras();
        	if(bundle != null){
        		 String action = bundle.getString("message");
	    	     JSONObject jsonObject = null;
	    	     String description = null;
	    	     int coins = 0 , badgeCount = 0;
	    	     int notifytypeId = 0;
	    	     try {
    				jsonObject = new JSONObject(action);
    				description = jsonObject.getString("description");
    				notifytypeId = jsonObject.getInt("notifytypeId");
    				coins = jsonObject.getInt("coins");
    				badgeCount = jsonObject.getInt("badgeCount");
	    	     } catch (JSONException e) {
    				e.printStackTrace();
	    	     }
	    	    
	    	     SharedPreferences sharedPreferences = getSharedPreferences("Forrestii", MODE_PRIVATE);
	    	     Editor editor = sharedPreferences.edit();
	    	     int mPCoins = sharedPreferences.getInt("coins", 0);
	    	     mPCoins+=coins;
	    	     editor.putInt("coins", mPCoins).commit();
	    	     editor.putInt("badgeCount", badgeCount).commit();
	    	        
    	        Intent mainActIntent = new Intent(context, MainActivity.class);
    	       
    	        mainActIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra("message", action);
    	        
    	        int notifyId = 0;
    	        /*if(notifytypeId == 6)
    	        	notifyId = 1;
    	        else if(notifytypeId == 10)
    	        	notifyId = 1;*/
    	        
    	        generateNotification(context,description,mainActIntent,notifyId , notifytypeId,badgeCount);
        	}
        }
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        //String message = getString(R.string.gcm_deleted, total);
        //displayMessage(context, message);
        // notifies user
       // generateNotification(context, message);
    }

    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
       // displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        /*displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));*/
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     * @param notifyId 
     * @param notifytypeId 
     * @param badgeCount 
     */
    private static void generateNotification(Context context, String message,Intent intent, int notifyId, int notifytypeId, int badgeCount) {
    	
    	NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
    	builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS);
    	builder.setWhen(System.currentTimeMillis());
    	builder.setSmallIcon(R.drawable.forrestii_icon);
        builder.setTicker(message);
        if(notifyId == 1 ){
        	builder.setContentTitle("Forrestii Wall Post !!!");
        }else{
        	builder.setContentTitle("Forrestii Notifications");
        }
        
        if(notifytypeId == 6){
        	if(badgeCount > 1)
            	builder.setContentText(badgeCount + " messages");
            else
            	builder.setContentText(message);
        }else if(notifytypeId == 10){
        	if(badgeCount > 1)
            	builder.setContentText(badgeCount + " messages");
            else
            	builder.setContentText(message);
        }else{
        	if(badgeCount > 1)
            	builder.setContentText(badgeCount + " messages");
            else
            	builder.setContentText(message);
        }
        
        //.setColor(context.getResources().getColor(R.color.theme_primary))
            // Note: setColor() is available in the support lib v21+.
            // We commented it out because we want the source to compile 
            // against support lib v20. If you are using support lib
            // v21 or above on Android L, uncomment this line.
        builder.setContentIntent(PendingIntent.getActivity(context, 0,intent,PendingIntent.FLAG_UPDATE_CURRENT
                | PendingIntent.FLAG_ONE_SHOT));
        builder.setAutoCancel(true);
        
    	 ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(notifyId, builder.build());
    }

}

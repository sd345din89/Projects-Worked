package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.settings.AbstractPassword;
import com.strobilanthes.forrestii.model.settings.Settings;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class SettingsChangePwdFragment extends BaseFragment<DBAccess> {

	private Typeface mFontLight,mFontDark;
	@InjectView(R.id.settings_pwd_old)							private EditText set_pwd_edtOldPwd;
	@InjectView(R.id.settings_pwd_new)							private EditText set_pwd_edtNewPwd;
	@InjectView(R.id.settings_pwd_reEnterNewPwd)				private EditText set_pwd_edtReEnterNewPwd;
	@InjectView(R.id.settings_pwd_header)                       private TextView set_pwd_header;
	private String strOldPwdfromDB,strUserMobileNo;
	@Inject Settings settingsModel;

   @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frg_settings_password, null);
		super.initilizeContents();
		return view;
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		fontStyles();
		settingsModel.setContext(getActivity());
		strOldPwdfromDB = getHelper().getPwd();
		if(getHelper().getDBUserProfile() != null)
			strUserMobileNo = getHelper().getDBUserProfile().getMobile();
		else
			return;
		
		set_pwd_edtOldPwd.setFilters(Util.restrictSpaceInEdit(set_pwd_edtOldPwd));
		set_pwd_edtNewPwd.setFilters(Util.restrictSpaceInEdit(set_pwd_edtNewPwd));
		set_pwd_edtReEnterNewPwd.setFilters(Util.restrictSpaceInEdit(set_pwd_edtReEnterNewPwd));

		L.d("OldPwd++"+strOldPwdfromDB);

	}
	public void fontStyles(){
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		set_pwd_edtOldPwd.setTypeface(mFontDark);
		set_pwd_edtNewPwd.setTypeface(mFontDark);
		set_pwd_header.setTypeface(mFontLight);
		set_pwd_edtReEnterNewPwd.setTypeface(mFontDark);
						}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("password_update")) {
			AbstractPassword abstractPassword = settingsModel.updatePasswordDetails();
			UpdatePwdDetails(abstractPassword);
			
		}
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		default:
			break;
		}
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.settings_change_menu, menu);
		MenuItem menuItem = menu.findItem(R.id.action_done);
		menuItem.setVisible(true);
		menuItem.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				// TODO Auto-generated method stub
				Util.hideSoftKeyboard(getActivity(), set_pwd_edtReEnterNewPwd);
				if (set_pwd_edtOldPwd.getText().toString().length() <= 0 && set_pwd_edtNewPwd.getText().toString().length() <= 0 && set_pwd_edtReEnterNewPwd.getText().toString().length() <= 0) {
					Util.showAlert(getActivity(), "Message","Please enter your password");
				} else if(set_pwd_edtOldPwd.getText().toString().length() <= 0){
					Util.showAlert(getActivity(), "Message","Please enter your old password");
				}else if(set_pwd_edtNewPwd.getText().toString().length() <= 0){
					Util.showAlert(getActivity(), "Message","Please enter your new password");
				}else if (set_pwd_edtReEnterNewPwd.getText().toString().length() <= 0) {
					Util.showAlert(getActivity(), "Message","Please re-enter your password");
				} else if(!set_pwd_edtOldPwd.getText().toString().equals(strOldPwdfromDB)){
					Util.showAlert(getActivity(), "Message","Your old password is incorrect");
				} else if(set_pwd_edtNewPwd.getText().toString().trim().length() < 8 && set_pwd_edtReEnterNewPwd.getText().toString().trim().length() < 8){
					Util.showAlert(getActivity(), "Message",
							"New Password should have eight characters minimum");
				}else if(!set_pwd_edtNewPwd.getText().toString().equals(set_pwd_edtReEnterNewPwd.getText().toString())){
					Util.showAlert(getActivity(), "Message",
							"Password mismatch");
				}else if(set_pwd_edtReEnterNewPwd.getText().toString().equals(strOldPwdfromDB) || set_pwd_edtNewPwd.getText().toString().equals(strOldPwdfromDB)){
					Util.showAlert(getActivity(), "Message",
							"New password should differ from old password");
				}else if(set_pwd_edtReEnterNewPwd.getText().toString().equals(strUserMobileNo)){
					Util.showAlert(getActivity(), "Message",
							"New password should differ from your mobile no");
				}else {
						JSONObject jobject = new JSONObject();
						try {
							jobject.put("password", set_pwd_edtOldPwd.getText().toString());
							jobject.put("newPassword", set_pwd_edtNewPwd.getText().toString());
							jobject.put("userId", getHelper().getUserId());
							settingsModel.updatePwdDetails(
									Constant.CHANGE_PASSWORD, jobject);
						} catch (JSONException e) {
							e.printStackTrace();
						}
				}
				return false;
			}
		});
	}
	private void UpdatePwdDetails(AbstractPassword abstractPassword) {
		if(abstractPassword  !=null){
			if(abstractPassword.getStatus() == 1){
				
				getHelper().updatePwd(set_pwd_edtNewPwd.getText().toString(),set_pwd_edtOldPwd.getText().toString());
				//txt_genPwd.setText(set_pwd_edtNewPwd.getText().toString());
				set_pwd_edtOldPwd.setText("");
				set_pwd_edtNewPwd.setText("");
				set_pwd_edtReEnterNewPwd.setText("");
				
				//txt_Change.setVisibility(View.GONE);
				
				L.ToastMessage(getActivity(), "Your password is successfully updated");
				getActivity().finish();
			}else{
				Util.showAlert(getActivity(),"Message", abstractPassword.getError());
				set_pwd_edtOldPwd.setText("");
				set_pwd_edtNewPwd.setText("");
				set_pwd_edtReEnterNewPwd.setText("");
			
				//txt_Change.setVisibility(View.GONE);
			}
		
		}else{
			Util.showAlert(getActivity(),"Message", "Request Failed.");
		}	
	}
	@Override
	public void onResume() {
		super.onResume();
		settingsModel.addChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		settingsModel.removeChangeListener(this);
		Util.hideSoftKeyboard(getActivity(), set_pwd_edtReEnterNewPwd);
	}
}
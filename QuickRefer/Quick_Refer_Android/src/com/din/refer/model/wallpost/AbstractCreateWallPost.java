package com.strobilanthes.forrestii.model.wallpost;


public class AbstractCreateWallPost{
	
	private int status;
	private String error;
	private CreatePostResponse response;
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	
	public CreatePostResponse getCreatePostResponse() {
		return response;
	}

	public void setCreatePostResponse(CreatePostResponse response) {
		this.response = response;
	}


	public class CreatePostResponse{
		private int postid;

		public int getPostid() {
			return postid;
		}

		public void setPostid(int postid) {
			this.postid = postid;
		}
		
	}
	
}

package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import roboguice.fragment.RoboSherlockFragment;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.slidingtab.ScrollTabHolder;

public abstract class BaseFragment<H extends SQLiteOpenHelper> extends RoboSherlockFragment implements PropertyChangeListener,OnClickListener{

	//protected Util util;
	protected DisplayImageOptions options;
	protected ImageLoadingListener animateFirstListener;
	
	private volatile H helper;
	private volatile boolean created = false;
	private volatile boolean destroyed = false;
	
	protected ScrollTabHolder mScrollTabHolder;

	public void setScrollTabHolder(ScrollTabHolder scrollTabHolder) {
		mScrollTabHolder = scrollTabHolder;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (helper == null) {
			helper = getHelperInternal(getActivity());
			created = true;
		}
		
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
	}

	protected H getHelperInternal(Context context) {
		@SuppressWarnings({ "unchecked"})
		H newHelper = (H) new DBAccess(context);
		return newHelper;
	}
	
	public H getHelper() {
		if (helper == null) {
			if (!created) {
				throw new IllegalStateException("A call has not been made to onCreate() yet so the helper is null");
			} else if (destroyed) {
				throw new IllegalStateException(
						"A call to onDestroy has already been made and the helper cannot be used after that point");
			} else {
				throw new IllegalStateException("Helper is null for some unknown reason");
			}
		} else {
			return helper;
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}

		return null;
	}// End of Oncreate

	protected  void initilizeContents() {
		
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.default_user)
		.showImageForEmptyUri(R.drawable.default_user)
		.showImageOnFail(R.drawable.default_user)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.considerExifParams(true)
		//.displayer(new RoundedBitmapDisplayer(60))
		.build();
		
		 animateFirstListener = new AnimateFirstDisplayListener();
		 
		//util = new Util(getActivity());
		
	}// End of Load ContentsS
	
	
	public boolean onBackPressed() {
		//SocializeFragment.lyt_socialize_header.setVisibility(View.VISIBLE);
         /*FragmentManager fm = getChildFragmentManager();
         if (fm.getBackStackEntryCount() == 1) {
             return false;
         } else {
             fm.popBackStack();
             return true;
         }*/
         return false;
     }
	
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

}// End of Fragment

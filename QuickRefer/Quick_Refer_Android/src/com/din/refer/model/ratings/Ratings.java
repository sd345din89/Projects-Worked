package com.strobilanthes.forrestii.model.ratings;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Ratings extends ForrestiiSubject {
	
	void ratingCard(String url, JSONObject postdata);
	
	AbstractRatings getRatingResponse();

}

package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.callbacks.LocationChangeListener;
import com.strobilanthes.forrestii.activities.MainActivity;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.coins.AbstractCoins;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.general.AbstractDashboard;
import com.strobilanthes.forrestii.model.login.AbstractLogin;
import com.strobilanthes.forrestii.model.login.AbstractLogin.CommonList;
import com.strobilanthes.forrestii.model.login.Login;
import com.strobilanthes.forrestii.model.settings.AbstractNotificationSettings;
import com.strobilanthes.forrestii.model.settings.SettingsDetails;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;


public class LoginFragment extends BaseFragment<DBAccess> implements OnClickListener,LocationChangeListener{

	//Login
	@InjectView(R.id.login_screen_mobileNo)					private EditText edtMobileNo;
	@InjectView(R.id.login_screen_edtPassword)				private EditText edtPassword;
	@InjectView(R.id.login_screen_forgetpassword_txt)		private TextView txt_forgot_pass;
	@InjectView(R.id.login_screen_btnLogin)					private TextView btn_login;
	
	@Inject Login loginModel;
	
	private static Location mLocation;
	private Typeface mFontLight,mFontDark;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.frg_login, null);

		loginModel.setContext(getActivity());
		
		super.initilizeContents();

		return view;
	}
	
	public void fontStyles() {
		
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		
		txt_forgot_pass.setTypeface(mFontDark);
		edtPassword.setTypeface(mFontDark);
		edtMobileNo.setTypeface(mFontDark);
		txt_forgot_pass.setTypeface(mFontDark);
		btn_login.setTypeface(mFontDark);
		
	}
 
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		txt_forgot_pass.setOnClickListener(this);
		btn_login.setOnClickListener(this);
		edtPassword.setFilters(Util.restrictSpaceInEdit(edtPassword));

		fontStyles();
	}
	
	
	@Override
	public void onResume(){
		super.onResume();
		loginModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		loginModel.removeChangeListener(this);
		edtMobileNo.setError(null);
		edtPassword.setError(null);
		Util.hideSoftKeyboard(getActivity(), edtMobileNo);
		Util.stopAlert();
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if(event.getPropertyName().equals("login")){
			
			btn_login.setEnabled(true);
			
			AbstractLogin abstractLogin = loginModel.getLoginDetails();
			if(abstractLogin != null){
				updateLoginDetails(abstractLogin);
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
		}
	}

	private void updateLoginDetails(final AbstractLogin abstractLogin){
		
		if(abstractLogin.getStatus() == 1) {
			
			getHelper().insertPageId(Constant.PAGE_MAIN_PAGE);
			
			AbstractUserProfile userProfile = abstractLogin.getLoginResponse().getUserProfile();
			if(userProfile != null){
				getHelper().insertUserProfile(userProfile);
				getHelper().insertUserMaster(userProfile.getUserId(), userProfile.getMobile(),edtPassword.getText().toString());
			}
			
			AbstractDashboard dashBoard = abstractLogin.getLoginResponse().getDashBoard();
			if(dashBoard != null){
				getHelper().insertDashBoard(dashBoard);
			}
			
			ArrayList<SettingsDetails> settingsDetails = abstractLogin.getLoginResponse().getSettings();
			if(settingsDetails != null){
				getHelper().insertSettingsDetails(settingsDetails.get(0));
			}
			
			CommonList commonpList = abstractLogin.getLoginResponse().getCommonplist();
			if(commonpList != null){
				
				ArrayList<AbstractSkillSet> skills = commonpList.getSkills();
				
				ArrayList<AbstractNotificationSettings> notify = commonpList.getNotify();
				
				ArrayList<AbstractCoins> coinType = commonpList.getCoinType();
				
				getHelper().insertSkillset(skills);
				
				//getHelper().insertNotifyType(notify);
				
				getHelper().insertTemplate(commonpList.getTemplate());
				
				getHelper().insertServerInfo(commonpList.getServerInfo().get(0));
				
				getHelper().insertCountryList(commonpList.getCountry());
				
				getHelper().insertLocation(commonpList.getLocation());
				
			}
			
			ArrayList<GetWallPost> getWallPostList = abstractLogin.getLoginResponse().getGetWallPostsList();
			if(getWallPostList != null){
				Constant.mGetWallPostArrayList = getWallPostList;
				//getHelper().insertGetWallPost(getWallPostList);
			}
			
			ArrayList<FriendsList> friendsListT2= abstractLogin.getLoginResponse().getFriendList();
			if(friendsListT2 != null){
				getHelper().insertFriendsList(friendsListT2,null);
			}
			
			Intent i = new Intent(getActivity(),MainActivity.class);
			startActivity(i);
			getActivity().setResult(Activity.RESULT_OK);
			getActivity().finish();
			
		} else {
			if(abstractLogin.getError().toString().trim().equals("OTP verification pending")){
				
			}else if(abstractLogin.getError().toString().trim().equals("Password not set")){
				
			}else if(abstractLogin.getError().toString().trim().equals("Profile not Complete")){
				
			}else {
				Util.showAlert(getActivity(),"Message", ""+abstractLogin.getError());	
			}
		}
		//L.ToastMessage(AuthenticationActivity.this, "Login = "+authentication.getIsSuccess());
	}

	@Override
	public void onClick(View v) {
		
		Util.hideSoftKeyboard(getActivity(), v);

		switch (v.getId()) {
		
		case R.id.login_screen_forgetpassword_txt:
			//if (Util.isValidPhoneNumber(edtMobileNo.getText().toString())&&PhoneNumberUtils.isGlobalPhoneNumber(edtMobileNo.getText().toString()) && edtMobileNo.getText().toString().length() >= 10) {
	        FragmentTransaction fragmentTransaction =  getFragmentManager().beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left, R.anim.out_to_right);
	        fragmentTransaction.addToBackStack(null);
	        Bundle args = new Bundle();
	        args.putString("mob", edtMobileNo.getText() != null ? edtMobileNo.getText().toString() : "");
	        ForgotPwdFragment forgotPwdFragment = new ForgotPwdFragment();
	        forgotPwdFragment.setArguments(args);
	        fragmentTransaction.replace(R.id.login_root, forgotPwdFragment);
	        fragmentTransaction.commit();
//		}else{
//				edtMobileNo.setError("Please enter a valid mobile number");
//			}
			break;
		case R.id.login_screen_btnLogin:
			
			String mobile_num = edtMobileNo.getText().toString();
			String pwd = edtPassword.getText().toString();
	
			if (mobile_num.trim().length() <= 0) {
				edtMobileNo.requestFocus();
				edtMobileNo.setError("Please enter your mobile number");
			} else if (pwd.trim().length() <= 0) {
				edtPassword.requestFocus();
				edtPassword.setError("Please enter your password");
			}else if (!Util.isValidPhoneNumber(mobile_num) || !PhoneNumberUtils.isGlobalPhoneNumber(mobile_num) || mobile_num.length() < 10) {
				edtMobileNo.requestFocus();
				edtMobileNo.setError("Please enter a valid mobile number");
			}else if(pwd.trim().length() < 8){
				edtPassword.setError("Password should have a minimum of 8 characters");
			}else if(pwd.trim().length() > 15){
				edtPassword.setError("Password must be less than 15 characters");
			}else {
				try {
					// Login
					if (Util.isConnectedNetwork(getActivity())) {
						 btn_login.setEnabled(false);

						 SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Forrestii", Context.MODE_PRIVATE);
						 sharedPreferences.edit().putString("latitude", ""+(mLocation != null ? mLocation.getLatitude() : 0)).commit();
						 sharedPreferences.edit().putString("longitude", ""+(mLocation != null ? mLocation.getLongitude() : 0)).commit();
						 sharedPreferences.edit().putBoolean("storyActionbar", true).commit();
						 sharedPreferences.edit().putBoolean("storyMenuDrawer", true).commit();
						 
						 JSONObject object = new JSONObject();
						 
						 object.put("userProfile", new JSONObject().put("mobile", mobile_num).put("password", pwd).put("isNeedLookupList", true));
						 object.put("latitude", mLocation != null ? mLocation.getLatitude() : 0);
						 object.put("longitude", mLocation != null ? mLocation.getLongitude() : 0);
						 object.put("deviceType", "android");
						 object.put("deviceId",  sharedPreferences.getString("regid", "No device id found"));
						 
						 loginModel.requestLoginDetails(Constant.LOGIN_USER,object);
					} else {
						 Util.showAlert(getActivity(),"Message","Please check your internet connection");
	
					}
					 //queue.add(jsObjRequest);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			break;
		default:
			break;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		mLocation = location; 
	}

	

}

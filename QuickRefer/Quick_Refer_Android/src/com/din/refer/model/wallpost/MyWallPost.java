package com.strobilanthes.forrestii.model.wallpost;

import java.util.ArrayList;

public class MyWallPost{
	
	private String message;
	private String postId;
	private ArrayList<AbstractReplyWallPost> abstractReplyWallPosts = new ArrayList<AbstractReplyWallPost>();
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public ArrayList<AbstractReplyWallPost> getAbstractReplyWallPosts() {
		return abstractReplyWallPosts;
	}

	public void setAbstractReplyWallPosts(AbstractReplyWallPost abstractReplyWallPost) {
		this.abstractReplyWallPosts.add(abstractReplyWallPost);
	}

}

package com.strobilanthes.forrestii.model.settings;

public class AbstractChange {
	private AbstractResponse response;
	private int status;
	private String error;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public AbstractResponse getResponse() {
		return response;
	}

	public void setResponse(AbstractResponse response) {
		this.response = response;
	}

	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}

	public class AbstractResponse{
		private String OTPCode;
		

		public String getOTPCode() {
			return OTPCode;
		}


		public void setOTPCode(String oTPCode) {
			OTPCode = oTPCode;
		}

		
		
	}
	
}

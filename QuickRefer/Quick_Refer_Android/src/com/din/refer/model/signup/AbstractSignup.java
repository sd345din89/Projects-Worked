package com.strobilanthes.forrestii.model.signup;


public class AbstractSignup{

	private String error;
	private int status;
	//private SignupResponse response;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	/*public SignupResponse getResponse() {
		return response;
	}
	public void setResponse(SignupResponse response) {
		this.response = response;
	}*/

	/*public class SignupResponse{
		private int OTPCode;
		
		public int getOtpCode() {
			return OTPCode;
		}
		public void setOtpCode(int otpCode) {
			this.OTPCode = otpCode;
		}
	}*/
}

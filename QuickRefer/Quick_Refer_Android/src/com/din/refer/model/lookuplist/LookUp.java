package com.strobilanthes.forrestii.model.lookuplist;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface LookUp extends ForrestiiSubject {

	void createQuickCard(String url, JSONObject postdata);

	void updateQuickCard(String url, JSONObject postdata);

	AbstractLookUpList getQuickcardDetails();

}

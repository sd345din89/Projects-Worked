package com.strobilanthes.forrestii.model.viewreviews;

import java.io.Serializable;

import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

public class AbstractReviewProfile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private AbstractReviewQuickcard quickCard;

	private AbstractUserProfile ratingFromUserProfile;

	private float rating;

	private long updatedOn;

	private String ratingId;

	private AbstractReviewFF forrestiiFriend;

	private String review;

	public AbstractReviewQuickcard getQuickCard() {
		return quickCard;
	}

	public void setQuickCard(AbstractReviewQuickcard quickCard) {
		this.quickCard = quickCard;
	}

	public AbstractUserProfile getRatingFromUserProfile() {
		return ratingFromUserProfile;
	}

	public void setRatingFromUserProfile(
			AbstractUserProfile ratingFromUserProfile) {
		this.ratingFromUserProfile = ratingFromUserProfile;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public long getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getRatingId() {
		return ratingId;
	}

	public void setRatingId(String ratingId) {
		this.ratingId = ratingId;
	}

	public AbstractReviewFF getForrestiiFriend() {
		return forrestiiFriend;
	}

	public void setForrestiiFriend(AbstractReviewFF forrestiiFriend) {
		this.forrestiiFriend = forrestiiFriend;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	

}

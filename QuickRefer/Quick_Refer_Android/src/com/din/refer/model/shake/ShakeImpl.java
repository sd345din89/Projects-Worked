package com.strobilanthes.forrestii.model.shake;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.volleytasks.ShakeVolleyTasks;

public class ShakeImpl implements Shake,VolleyTasksListener{

	private AbstractShakeDetails abstractShakeDetails;
	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private Activity context;
	
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}
	
	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonobject) {
		if(method_name.equals("shake")){

			AbstractShakeDetails previousResult = abstractShakeDetails;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractShakeDetails = gson.fromJson(jsonobject.toString(), AbstractShakeDetails.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractShakeDetails);
			notifyListeners(event);
		} 
	}

	@Override
	public void handleError(String method_name, Exception e) {
		AbstractShakeDetails previousResult = abstractShakeDetails;
		abstractShakeDetails = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				previousResult, null);
		notifyListeners(event);
	}

	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	@Override
	public void reuestShakeDetails(String url, JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(
				new ShakeVolleyTasks("shake", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractShakeDetails getAbstractShakeDetails() {
		return abstractShakeDetails;
	}

}

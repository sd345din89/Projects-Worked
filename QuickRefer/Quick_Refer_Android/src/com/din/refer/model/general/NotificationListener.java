package com.strobilanthes.forrestii.model.general;

public interface NotificationListener {

	public void onResumeFragment();
	
}

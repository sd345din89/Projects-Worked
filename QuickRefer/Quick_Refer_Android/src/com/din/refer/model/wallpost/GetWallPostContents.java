package com.strobilanthes.forrestii.model.wallpost;

import java.io.Serializable;
import java.util.ArrayList;

import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

public class GetWallPostContents implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//private int status;
	//private String error;
	private int postId;
	private String message;
	private long updatedOn;
	private AbstractUserProfile postByUserProfile;
	private AbstractSkillSet skill;
	private ArrayList<RefferalPost> referral;
	private ArrayList<ReplyPost> replies;

	public AbstractUserProfile getPostByUserProfile() {
		return postByUserProfile;
	}

	public void setPostByUserProfile(AbstractUserProfile postByUserProfile) {
		this.postByUserProfile = postByUserProfile;
	}

	public AbstractSkillSet getSkill() {
		return skill;
	}

	public void setSkill(AbstractSkillSet skill) {
		this.skill = skill;
	}

	public ArrayList<RefferalPost> getReferral() {
		return referral;
	}

	public void setReferral(ArrayList<RefferalPost> referral) {
		this.referral = referral;
	}

	public ArrayList<ReplyPost> getReplies() {
		return replies;
	}

	public void setReplies(ArrayList<ReplyPost> replies) {
		this.replies = replies;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public long getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}	
}

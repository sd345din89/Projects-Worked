package com.strobilanthes.forrestii.model.general;

import org.json.JSONObject;

public interface VolleyTasksListener {
	public void handleResult (String method_name,JSONObject jsonobject);
	public void handleError (String method_name, Exception e);
}

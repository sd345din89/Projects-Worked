package com.strobilanthes.forrestii.model.search;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Search extends ForrestiiSubject {
	
	void searchForrestiiFrnd(String url,JSONObject postdata);

	AbstractSearchForrestiiFriend getForrestiiFrnd();
}

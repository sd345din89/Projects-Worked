	package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.SparseArrayCompat;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;
import com.nineoldandroids.view.ViewHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.fragments.ProfileT2andT3BasicInfoFragment;
import com.strobilanthes.forrestii.act.fragments.ProfileT2andT3SkillFragment;
import com.strobilanthes.forrestii.act.fragments.ScrollTabHolderFragment;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.invite.AbstractInviteFriend;
import com.strobilanthes.forrestii.model.invite.InviteRequest;
import com.strobilanthes.forrestii.model.skillset.UserSkillsSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.slidingtab.AlphaForegroundColorSpan;
import com.strobilanthes.forrestii.slidingtab.PagerSlidingTabStrip;
import com.strobilanthes.forrestii.slidingtab.ScrollTabHolder;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;

public class FriendsProfileT3Activity extends BaseActivity<DBAccess> implements ScrollTabHolder, ViewPager.OnPageChangeListener,PropertyChangeListener,OnClickListener {
	
	private AbstractUserProfile abstractUserProfile;
	private ArrayList<FriendsList> mT2FriendList;
	private DisplayImageOptions optionsImgCard;
	private Typeface mFontLight,mFontDark;
	private PagerSlidingTabStrip mPagerSlidingTabStrip;
	private ViewPager mViewPager;
	private PagerAdapter mPagerAdapter;
	private int mActionBarHeight;
	private int mMinHeaderHeight;
	private int mHeaderHeight;
	private int mMinHeaderTranslation;
	private TypedValue mTypedValue = new TypedValue();
	private SpannableString mSpannableString;
	private AlphaForegroundColorSpan mAlphaForegroundColorSpan;
	private String strPrimarySkillName;
	private ArrayList<String>profileSecondarySkillsAL = new ArrayList<String>();
	private ArrayList<String>TotalSkillAl = new ArrayList<String>();
	public static int isFriend;
	private boolean FriendRequest = false;
	private boolean isPick;
	
	@InjectView(R.id.header) 						private View mHeader;
	@InjectView(R.id.profilT3_img_card)				private ImageView profileT3_imgCard;
	@InjectView(R.id.profileT3_userName)			private TextView profileT3_txtUserName;
	@InjectView(R.id.profileT3_skillName)			private TextView profileT3_txtSkillName;
	@InjectView(R.id.profileT3_txt_addfriend)		private TextView profileT2_txt_addfriend;
	@InjectView(R.id.profileT3_ratingBar)			private RatingBar profileT3RatingBar;
	@InjectView(R.id.profileT3_points)				private TextView profileT3_txtPoints;
	@InjectView(R.id.profileT3_commonCards)			private TextView profileT3_commonCards;
	@InjectView(R.id.profileT3lyt_common_cards)		private LinearLayout profileT3lyt_common_cards;
	@InjectView(R.id.profileT3_img_user)			private RoundedImageView profileT3_img_user;
	@InjectView(R.id.profileT3_lyt_addfriend)		private LinearLayout profileT3_addFriend;
	
	@Inject InviteRequest  inviteRequestModel;
	
	private int mPosition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_profile_t3);
		
		loadFonts();
		getActionBar().setTitle("Profile");
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		mMinHeaderHeight = getResources().getDimensionPixelSize(R.dimen.up_min_header_height);
		mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.up_header_height);
		mMinHeaderTranslation = -mMinHeaderHeight /*+ getActionBarHeight();*/;

		mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setOffscreenPageLimit(2);

		mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
		mPagerAdapter.setTabHolderScrollingContent(this);
		mPagerSlidingTabStrip.setTextSize((int)Util.getDimensPxtoDp(14, this));
	    mPagerSlidingTabStrip.setTypeface(mFontDark, Typeface.NORMAL);
		mViewPager.setAdapter(mPagerAdapter);

		mPagerSlidingTabStrip.setViewPager(mViewPager);
		mPagerSlidingTabStrip.setOnPageChangeListener(this);
		mSpannableString = new SpannableString(getString(R.string.app_name));
		mAlphaForegroundColorSpan = new AlphaForegroundColorSpan(0xffffffff);
		
//		ViewHelper.setAlpha(getActionBarIconView(), 0f);
		
		getSupportActionBar().setBackgroundDrawable(null);
		optionsImgCard = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.card_default3)
		.showImageForEmptyUri(R.drawable.card_default3)
		.showImageOnFail(R.drawable.card_default3)
		.cacheInMemory(true).cacheOnDisc(true).considerExifParams(true)
		//.displayer(new RoundedBitmapDisplayer(80))
		.build();
		profileT3_addFriend.setOnClickListener(this);
		abstractUserProfile = (AbstractUserProfile) getIntent().getSerializableExtra("ProfileT3");
		mT2FriendList = getHelper().getDBT2Friends();
		isFriend = getIntent().getIntExtra("isFriend", 0);
		mPosition = getIntent().getIntExtra("position", 0);
		isPick = getIntent().getBooleanExtra("isPick", false);
		if(abstractUserProfile !=null){ 
			profileT3_txtUserName.setText(abstractUserProfile.getFirstName()+" "+abstractUserProfile.getLastName());
			profileT3_txtUserName.setSelected(true);
		if( abstractUserProfile.getUserCardInfo() != null &&  abstractUserProfile.getUserCardInfo().size() > 0)
			
			ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + abstractUserProfile.getPhotoID(), profileT3_img_user, options,animateFirstListener);
			if (isFriend == 1 || isPick) 
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_CARD_NAME + abstractUserProfile.getUserCardInfo().get(0).getCardName(), profileT3_imgCard, optionsImgCard,animateFirstListener);	
			else
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_CARD_MASK_NAME + abstractUserProfile.getUserCardInfo().get(0).getCardName(), profileT3_imgCard, optionsImgCard,animateFirstListener);
		
			if(abstractUserProfile.getUserSkillSets().size()>0){
				for (UserSkillsSet userSkillsSet:abstractUserProfile.getUserSkillSets()) {
					if(userSkillsSet.isPrimarySkill()){
						strPrimarySkillName = userSkillsSet.getSkills().getSkillName();
						
					}else if(!userSkillsSet.isPrimarySkill()){
						profileSecondarySkillsAL.add(userSkillsSet.getSkills().getSkillName());
						//strSecondarySkillName = strSecondary.substring(1,strSecondary.length()-1);
					}else{
						//strSecondarySkillName = "No Skills";
					}
					
				}
			}
	
			if(profileSecondarySkillsAL.size()>0){
				for (int i = 0; i < profileSecondarySkillsAL.size(); i++) {
					TotalSkillAl.add(profileSecondarySkillsAL.get(i));
				}
			}else{
				TotalSkillAl.add("No Skills");
			}
			profileT3_addFriend.setVisibility(View.INVISIBLE);
			profileT3_txtSkillName.setText(strPrimarySkillName);
			profileT3_txtPoints.setText(String.valueOf(abstractUserProfile.getTotalCoin()));
			profileT3RatingBar.setRating(abstractUserProfile.getTotalRating());
			profileT3_txtSkillName.setSelected(true);
			getActionBar().setTitle(abstractUserProfile.getFirstName());
			for(FriendsList friendList : mT2FriendList){
				if(abstractUserProfile.getMobile().equals(friendList.getFriendUserProfile().getMobile())){
					isFriend = 1;
					break;
				}
				
			}
			
			updateRequest(isFriend);
			
		}
	}
	private void updateRequest(int isFriend) {
		switch (isFriend) {
		case 0:
			profileT2_txt_addfriend.setText("Request Sent");
			profileT3_img_user.setClickable(false);
			profileT3_img_user.setEnabled(false);
			profileT3_addFriend.setVisibility(View.VISIBLE);
			FriendRequest = false;
			break;
		case 1:
			profileT2_txt_addfriend.setText("Friend");
			profileT3_img_user.setClickable(false);
			profileT3_img_user.setEnabled(false);
			profileT3_addFriend.setVisibility(View.VISIBLE);
			FriendRequest = false;
			break;
		case 2:
			profileT2_txt_addfriend.setText("Add Friend");
			profileT3_img_user.setClickable(true);
			profileT3_img_user.setEnabled(true);
			profileT3_addFriend.setVisibility(View.VISIBLE);
			FriendRequest = true;
			break;
		case 3:
			profileT2_txt_addfriend.setText("Add Friend");
			profileT3_img_user.setClickable(true);
			profileT3_img_user.setEnabled(true);
			profileT3_addFriend.setVisibility(View.VISIBLE);
			FriendRequest = true;
			break;
		case 4:
			profileT2_txt_addfriend.setText("Add Friend");
			profileT3_img_user.setClickable(false);
			profileT3_img_user.setEnabled(false);
			profileT3_addFriend.setVisibility(View.VISIBLE);
			FriendRequest = true;
			break;
		default:
			FriendRequest = false;
			break;
		}
	}
	private void loadFonts() {
		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		profileT3_txtUserName.setTypeface(mFontLight);
		profileT3_txtSkillName.setTypeface(mFontLight);
		profileT3_txtPoints.setTypeface(mFontDark);
		profileT3_commonCards.setTypeface(mFontDark);
	}
	@Override
	public void onResume() {
		super.onResume();
		inviteRequestModel.addChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		inviteRequestModel.removeChangeListener(this);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.profileT3_lyt_addfriend:
			if (FriendRequest) {
				new AlertDialog.Builder(v.getContext(),AlertDialog.THEME_HOLO_LIGHT)
				.setMessage("Do you want to add this person on Forrestii?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						JSONObject postdata = new JSONObject();
						try {
							postdata.put("addedByUserProfile",new JSONObject().put("userId",getHelper().getUserId()));
							postdata.put("FriendUserProfile",new JSONObject().put("userId",abstractUserProfile.getUserId()));
							inviteRequestModel.sendFriendRequest(Constant.SEND_FRIEND_REQUEST, postdata);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}).setNegativeButton("Cancel",null).show();	
			}
			break;

		default:
			break;
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				
				if(getSupportFragmentManager().getBackStackEntryCount() == 0){
					onBackPressed();
				} else {
					getSupportFragmentManager().popBackStack();
				}
				
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void finish() {
		setResult(RESULT_OK,new Intent().putExtra("position", mPosition));
		super.finish();
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if(event.getPropertyName().equals("sendfriendrequest")){
			AbstractInviteFriend abstractFriendRequest = inviteRequestModel.getFriendsRequestData();
			if (abstractFriendRequest != null) {
				if (abstractFriendRequest.getStatus() == 1){
					Util.showAlert(this, "Message","Successfully invitation sent.");
					updateRequest(0);
					
//					setResult(RESULT_OK, new Intent().putExtra("position", mPosition));
				}
				else
					Util.showAlert(this, "Message", ""+ abstractFriendRequest.getError());
			} else {
				Util.showAlert(this, "Message", "Request Failed.");
			}
		}
	}
	@Override
	public void adjustScroll(int scrollHeight) {
		
	}
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount, int pagePosition) {
		if (mViewPager.getCurrentItem() == pagePosition) {
			int scrollY = getScrollY(view);
			ViewHelper.setTranslationY(mHeader, Math.max(-scrollY, mMinHeaderTranslation));
//			float ratio = clamp(ViewHelper.getTranslationY(mHeader) / mMinHeaderTranslation, 0.0f, 1.0f);
//			setTitleAlpha(clamp(5.0F * ratio - 4.0F, 0.0F, 1.0F));
		}
	}
	public static float clamp(float value, float max, float min) {
		return Math.max(Math.min(value, min), max);
	}
	public int getScrollY(AbsListView view) {
		View c = view.getChildAt(0);
		if (c == null) {
			return 0;
		}

		int firstVisiblePosition = view.getFirstVisiblePosition();
		int top = c.getTop();

		int headerHeight = 0;
		if (firstVisiblePosition >= 1) {
			headerHeight = mHeaderHeight;
		}

		return -top + firstVisiblePosition * c.getHeight() + headerHeight;
	}
	@Override
	public void onPageScrollStateChanged(int arg0) {
		
	}
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}
	@Override
	public void onPageSelected(int position) {
		SparseArrayCompat<ScrollTabHolder> scrollTabHolders = mPagerAdapter.getScrollTabHolders();
		ScrollTabHolder currentHolder = scrollTabHolders.valueAt(position);

		currentHolder.adjustScroll((int) (mHeader.getHeight() + ViewHelper.getTranslationY(mHeader)));
		
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public int getActionBarHeight() {
		if (mActionBarHeight != 0) {
			return mActionBarHeight;
		}
		
		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB){
			getTheme().resolveAttribute(android.R.attr.actionBarSize, mTypedValue, true);
		}else{
			getTheme().resolveAttribute(R.attr.actionBarSize, mTypedValue, true);
		}
		
		mActionBarHeight = TypedValue.complexToDimensionPixelSize(mTypedValue.data, getResources().getDisplayMetrics());
		
		return mActionBarHeight;
	}
	private void setTitleAlpha(float alpha) {
		mAlphaForegroundColorSpan.setAlpha(alpha);
		mSpannableString.setSpan(mAlphaForegroundColorSpan, 0, mSpannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setTitle(mSpannableString);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private ImageView getActionBarIconView() {
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			return (ImageView)findViewById(android.R.id.home);
		}

		return (ImageView)findViewById(android.R.id.home);
	}
	public class PagerAdapter extends FragmentPagerAdapter {

		private SparseArrayCompat<ScrollTabHolder> mScrollTabHolders;
		private final String[] TITLES = { "BasicInfo","Skills"};
		private ScrollTabHolder mListener;

		public PagerAdapter(FragmentManager fm) {
			super(fm);
			mScrollTabHolders = new SparseArrayCompat<ScrollTabHolder>();
		}

		public void setTabHolderScrollingContent(ScrollTabHolder listener) {
			mListener = listener;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return TITLES[position];
		}

		@Override
		public int getCount() {
			return TITLES.length;
		}

		@Override
		public Fragment getItem(int position) {
			ScrollTabHolderFragment fragment = null;

		switch (position) {

		case 0:
			fragment = (ScrollTabHolderFragment) new ProfileT2andT3BasicInfoFragment();
			Bundle b = new Bundle();
			b.putSerializable("AbstractUserProfile", abstractUserProfile);
			b.putInt(ProfileT2andT3BasicInfoFragment.ARG_POSITION, position);
			b.putString("category","t3");
			b.putBoolean("isPick", isPick);
			fragment.setArguments(b);
			break;
		case 1:
			
			fragment = (ScrollTabHolderFragment) new ProfileT2andT3SkillFragment();
			mScrollTabHolders.put(position, fragment);
			Bundle args = new Bundle();
			args.putString("primarySkill", strPrimarySkillName);
			args.putSerializable("secondaryskill", TotalSkillAl); 
			args.putInt(ProfileT2andT3SkillFragment.ARG_POSITION, position);
			fragment.setArguments(args);
			
			break;
		default:
			break;

		}
		mScrollTabHolders.put(position, fragment);
		if (mListener != null) {
			fragment.setScrollTabHolder(mListener);
			
		}

			return fragment;
		}

		public SparseArrayCompat<ScrollTabHolder> getScrollTabHolders() {
			return mScrollTabHolders;
		}

	}

}

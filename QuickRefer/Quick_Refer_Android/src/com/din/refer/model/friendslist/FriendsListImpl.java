package com.strobilanthes.forrestii.model.friendslist;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.ContactsVolleyTasks;


public class FriendsListImpl implements Friends, VolleyTasksListener {

	//private AbstractFriendsList abstractT2FriendsList;
	private AbstractFriendsList abstractFriendsList;
	private AbstractUserList abstractUserList;
	private CustomProgressbar mCustomProgressbar;
	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private Activity context;

	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonObject) {
		
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		if (method_name.equals("T3fromT2")) {
			AbstractFriendsList previousResult = abstractFriendsList;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractFriendsList = gson.fromJson(jsonObject.toString(), AbstractFriendsList.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					method_name, previousResult, abstractFriendsList);
			notifyListeners(event);

		} else if (method_name.equals("FriendsT2")) {
			AbstractFriendsList previousResult = abstractFriendsList;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractFriendsList = gson.fromJson(jsonObject.toString(), AbstractFriendsList.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					method_name, previousResult, abstractFriendsList);
			notifyListeners(event);
		} else if (method_name.equals("FriendsT3")) {
			
			AbstractFriendsList previousResult = abstractFriendsList;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractFriendsList = gson.fromJson(jsonObject.toString(), AbstractFriendsList.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					method_name, previousResult, abstractFriendsList);
			notifyListeners(event);
		}else if (method_name.equals("sendfriendrequest")) {
			/*AbstractFriendRequest abstractFrndRequest = new AbstractFriendRequest();
			AbstractFriendRequest previousResult = abstractFriendRequest;
		
			abstractFriendRequest = abstractFrndRequest;

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					method_name, previousResult, abstractFriendsList);
			notifyListeners(event);*/
		}else if(method_name.equals("unfriend")){
			/*AbstractFriendsList mFriendsList = new AbstractFriendsList();
			AbstractFriendsList previousResult = abstractFriendsList;
		
			abstractFriendsList = mFriendsList;

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					method_name, previousResult, abstractFriendsList);
			notifyListeners(event);*/
		}else if (method_name.equals("user_details")) {
			
			AbstractUserList previousResult = abstractUserList;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractUserList = gson.fromJson(jsonObject.toString(), AbstractUserList.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					method_name, previousResult, abstractUserList);
			notifyListeners(event);
		}
	}

	@Override
	public void handleError(String method_name, Exception e) {
		
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				null, null);
		notifyListeners(event);
	}

	@Override
	public void doValidation(String content) {

	}

	@Override
	public void requestT2ContactsList(String url, JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(
				new ContactsVolleyTasks("FriendsT2", this, url,
						postdata).setRetryPolicy(new DefaultRetryPolicy(
						Constant.SOCKET_TIMEOUT,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
						DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractFriendsList getT2ContactsList() {	
		return abstractFriendsList;
	}

	@Override
	public void requestT3ContactsList(String url, JSONObject postdata) {
		
		/*mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show(); */
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new ContactsVolleyTasks("T3fromT2", this, url,
						postdata).setRetryPolicy(new DefaultRetryPolicy(
						Constant.SOCKET_TIMEOUT,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
						DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public  AbstractFriendsList getT3ContactsList() {
		
		return abstractFriendsList;
	}

	
	@Override
	public void unFriendRequest(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show(); 
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new ContactsVolleyTasks("unfriend", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void requestUserDetails(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show(); 
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new ContactsVolleyTasks("user_details", this, url,
						postdata).setRetryPolicy(new DefaultRetryPolicy(
						Constant.SOCKET_TIMEOUT,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
						DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractUserList getUserDetails() {
		return abstractUserList;
	}
	
	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}
}

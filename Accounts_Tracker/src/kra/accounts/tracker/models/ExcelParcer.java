package kra.accounts.tracker.models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import kra.accounts.tracker.database.KraFinanceDbManager;

import org.apache.poi.hssf.record.aggregates.RecordAggregate.PositionTrackingVisitor;
import org.apache.poi.hssf.record.chart.DatRecord;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataFormatter;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class ExcelParcer {
	
	private Context context;
		
	private KraFinanceDbManager dbManager;
	
	private HSSFWorkbook workbook;
	
	private Util util;
	
	
	
	public ExcelParcer(HSSFWorkbook xlWorkBook, KraFinanceDbManager dbManager, Util util){
		
		this.dbManager = dbManager;
		
		//dbManager.reCreateDataBaseTables();
		
		this.workbook = xlWorkBook;
		
		this.util = util;
		
		/*parseAccountsDataFromExcel();
		parseCustomerAccDataFromExcel();
		parseCustomersDataFromExcel();
		parseTransactionDataFromExcel();*/
		
		parseAccountDataFromExcel();
		
	}
	
	
	public ExcelParcer(KraFinanceDbManager dbManager, Context context){
		this.dbManager = dbManager;
		this.context = context;
		
	}
	
	
	
	
	
	
	public boolean exportExcelData(){
		
		try {
			Util util = new Util();
			workbook = new HSSFWorkbook(context.getAssets().open("KRAfinance.xls"));
			
			
			SQLiteDatabase database = dbManager.getWritableDatabase();
			
			/*
			 * Write Tracsaction data into Excel 
			 */
			
			HSSFSheet sheet = workbook.getSheet(Constants.TRANSACTIONS_XL_SHEET_NAME);
			
			
			int row = 1;
			Cursor cursor = util.getTransactionCursor(database);
			if(cursor != null){
				if(cursor.moveToFirst()){
					do{
						Row hssfRow = sheet.createRow(row);
						hssfRow.createCell(0).setCellValue(cursor.getInt(0));
						hssfRow.createCell(1).setCellValue(cursor.getInt(1));
						hssfRow.createCell(2).setCellValue(util.convertLongToDate(cursor.getString(2), "dd/MM/yyyy"));
						hssfRow.createCell(3).setCellValue(util.convertLongToDate(cursor.getString(3), "dd/MM/yyyy"));
						hssfRow.createCell(4).setCellValue(cursor.getString(4));
						hssfRow.createCell(5).setCellValue(cursor.getString(5));
						hssfRow.createCell(6).setCellValue(cursor.getString(6));
						hssfRow.createCell(7).setCellValue(cursor.getDouble(7));
						hssfRow.createCell(8).setCellValue(cursor.getDouble(8));
						hssfRow.createCell(9).setCellValue(cursor.getString(9));
						
						row++;
						
					}while(cursor.moveToNext());
				}
			}
			
			/*
			 * Write customer Accounts data into Excel 
			 */
			
			
			sheet = workbook.getSheet(Constants.CUSTOMER_ACCOUNTS_XL_SHEET_NAME);
			row = 1;
			cursor = util.getCustomerAccountsCursor(database);
			if(cursor != null){
				if(cursor.moveToFirst()){
					do{
						
						Row hssfRow = sheet.createRow(row);
						
						hssfRow.createCell(0).setCellValue(cursor.getInt(0));
						hssfRow.createCell(1).setCellValue(cursor.getInt(1));
						hssfRow.createCell(2).setCellValue(cursor.getInt(2));
						hssfRow.createCell(3).setCellValue(cursor.getString(3));
						hssfRow.createCell(4).setCellValue(util.convertLongToDate(cursor.getString(4), "dd/MM/yyyy"));
						hssfRow.createCell(5).setCellValue(util.convertLongToDate(cursor.getString(5), "dd/MM/yyyy"));
						hssfRow.createCell(6).setCellValue("n");
						hssfRow.createCell(7).setCellValue("");
						if(util.checkContainsAnyChar(cursor.getString(6))){
								hssfRow.createCell(6).setCellValue("y");
								hssfRow.createCell(7).setCellValue(""+cursor.getString(6));
						}
						
						hssfRow.createCell(8).setCellValue(cursor.getString(7));
						
						row++;
					}while(cursor.moveToNext());
				}
			}
			
			 
			/*
			 * Write customer Master data into Excel 
			 */
			sheet = workbook.getSheet(Constants.CUSTOMERS_XL_SHEET_NAME);
			row = 1;
			cursor = util.getCustomerMasterCursor(database);
			if(cursor != null){
				if(cursor.moveToFirst()){
					do{
						
						Row hssfRow = sheet.createRow(row);
						
						hssfRow.createCell(0).setCellValue(cursor.getInt(0));
						hssfRow.createCell(1).setCellValue(cursor.getString(1));
						hssfRow.createCell(2).setCellValue(cursor.getString(2));
						hssfRow.createCell(3).setCellValue(cursor.getString(3));
						//hssfRow.createCell(4).setCellValue(cursor.getString(4));
						
						
						row++;
						
					}while(cursor.moveToNext());
				}
			}
			
			/*
			 * Write  Accounts Master data into Excel 
			 */
			 
			sheet = workbook.getSheet(Constants.ACCOUNTS_XL_SHEET_NAME);
			row = 1;
			cursor = util.getAccountMasterCursor(database);
			if(cursor != null){
				if(cursor.moveToFirst()){
					do{
						HSSFRow hssfRow = sheet.createRow(row);
						
						hssfRow.createCell(0).setCellValue(cursor.getInt(0));
						hssfRow.createCell(1).setCellValue(cursor.getString(1));
						hssfRow.createCell(2).setCellValue(cursor.getString(2));
						
						row++;
						
					}while(cursor.moveToNext());
				}
			}
			 
			
			cursor.close();
			
			database.close();
			
			File exportFile = new File(Environment.getExternalStorageDirectory()+File.separator+"KRA_Finance_Export_"+util.convertLongToDate(""+System.currentTimeMillis(), "dd_MM_yyyy")+".xls");
			
			if(!exportFile.exists()){
				exportFile.createNewFile();
			}else{
				exportFile.delete();
			}
			
			FileOutputStream fos = new FileOutputStream(exportFile);
			
			workbook.write(fos);
			
			fos.close();
			
			
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}
	
	
	

	private void parseAccountDataFromExcel(){
		
		HSSFSheet sheet = workbook.getSheet(Constants.ACCOUNT_DATA_SHEET);
		
		Iterator<?> rowIterator = sheet.rowIterator();
		int index = 0;
		
		SQLiteDatabase database = dbManager.getWritableDatabase();
		
		//database.delete("AccountMaster", null, null);
		database.beginTransaction();
		
		
        while (rowIterator.hasNext()){
        	
            HSSFRow hssfRow = (HSSFRow) rowIterator.next();
            
            
            if(index++ != 0){
            	
            	Cell cell = hssfRow.getCell(7);
            	if(cell != null){
            		cell.setCellType(Cell.CELL_TYPE_STRING);
            	}
            	
            	
            	
            	int customerId = convertObjectToInt(hssfRow.getCell(0)); 		// Account Name
            	String security = (String)getCellValue(hssfRow.getCell(1)); 	// Account desc
            	Long principalDate =  convertDateObjIntoLong(getCellValue(hssfRow.getCell(2)), "dd.MM.yyyy");	//Due Date
            	String PartyName =  (String)getCellValue(hssfRow.getCell(3)); 
            	int principalAmount = convertObjectToInt(hssfRow.getCell(4));
            	Long duesFrom = getDueData(hssfRow.getCell(5), principalDate);
            	String inAccBook = (String)getCellValue(hssfRow.getCell(6));
            	String contactNumber =  (String)getCellValue(hssfRow.getCell(7));
            	String address = (String)getCellValue(hssfRow.getCell(8));
            	String isPrincipalPaid = (String)getCellValue(hssfRow.getCell(9));
            	
            	Log.d("CHECK", index+">>"+util.convertLongToDate(""+principalDate, "dd.MM.yyyy")+">>>"+util.convertLongToDate(""+duesFrom, "dd.MM.yyyy"));
            	
            	
            	if(!util.checkAccountIsExist(database, inAccBook)){
            		String INSERT = "insert into AccountMaster (accountName, desc) values ('"+inAccBook+"', '')";
            		
            		database.execSQL(INSERT);
            	}
            	
            	
            	
            	
            	
            	
            	if(!util.checkNameIsExistInCustomerMaster(database, PartyName, contactNumber, address)){
            		
            		String INSERT = "INSERT INTO CustomerMaster (name,ContactNumber, address) values('"+PartyName+"','"+contactNumber+"','"+address+"')";
            		database.execSQL(INSERT);
            	}
            	
            	int cusId = util.getCustomerId(database, PartyName, contactNumber);
            	String accountId = util.getAccountId(null, database, inAccBook);
            	
            	int customerAccId = util.checkNameIsExistInCustomerAccount(database, ""+cusId, contactNumber, accountId, ""+principalAmount, ""+principalDate);
            	
            	if(customerAccId == -1){
            		
            		String INSERT = "insert into CustomerAccount (customerId, security, principalDate, partyName, principalAmt, interestPaidUptoDate, accountId, ContactNumber, isPrincipalPaid) values ("+cusId+"," +
        					"'"+security+"'," +
        					""+principalDate+"," +
        					"'"+PartyName+"',"+
        					""+principalAmount+"," +
        					""+duesFrom+"," +
        					"'"+accountId+"'," +
        					"'"+contactNumber+"'," +
        					"'"+isPrincipalPaid+"')";
            	
            		
            		database.execSQL(INSERT);
            		
            		if(Constants.DEBUG_LOG) Log.d("CHECK", "PartyName="+PartyName+" contactNumber="+contactNumber+" Principal date="+util.convertLongToDate(""+principalDate, "dd.MM.yyyy")+" due Date="+util.convertLongToDate(""+duesFrom, "MMM-yyyy"));
            		
            		
            	}else{
            		
            		ContentValues values = new ContentValues();
            		values.put("customerId", cusId);
            		values.put("security", security);
            		values.put("principalDate", principalDate);
            		values.put("partyName", PartyName);
            		values.put("principalAmt", principalAmount);
            		values.put("interestPaidUptoDate", duesFrom);
            		values.put("accountId", accountId);
            		values.put("ContactNumber", contactNumber);
            		values.put("isPrincipalPaid", isPrincipalPaid);
            		
            		
            		int res = database.update("CustomerAccount", values, "customerAccId='"+customerAccId+"'", null);
            		
            		if(Constants.DEBUG_LOG)Log.d("CHECK", customerAccId+" Name is Already exist updated "+res);
            	}
            	
            }
        }
        
        
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
		
	}
	
	private long getDueData(HSSFCell hssfCell, long principalDate){
		
		Long duesFrom = (long) 0 ;
		
		if(hssfCell != null){
    		
    		String dueDate = (String)getCellValue(hssfCell);
    		
    		if(dueDate == null){
    			
    			duesFrom = setEmptyDueDate(principalDate);
    			
    		}else{
    			
    			if(dueDate.length() ==0){
    				
    				duesFrom = setEmptyDueDate(principalDate);
    				
    			}else if(dueDate.equals("Default")){
    				
    				duesFrom = setDefaultDueDate(principalDate);
    				
    			}else{
                	duesFrom = convertDateObjIntoLong(getCellValue(hssfCell), "dd-MMM-yy");	//Due Date

    			}
    		}
    	}else{
			duesFrom = setEmptyDueDate(principalDate);
		}
		
		
		return duesFrom;
		
	}
	
	private long setDefaultDueDate(Long principalDate){
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTimeInMillis(principalDate);
		
		calendar.add(Calendar.YEAR, 1);
		
		//if(Constants.DEBUG_LOG)Log.d("CHECK", ">>>>>Due date="+util.convertLongToDate(""+calendar.getTimeInMillis(), "MMM-yy"));
		
		return calendar.getTimeInMillis();
		
		
	}
	
	private long setEmptyDueDate(long principalDate){
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTimeInMillis(principalDate);
		
		calendar.add(Calendar.MONTH, 1);
		
		//if(Constants.DEBUG_LOG)Log.d("CHECK", ">>>>>Due date="+util.convertLongToDate(""+calendar.getTimeInMillis(), "MMM-yy"));
		
		return calendar.getTimeInMillis();
		
	}
	
	
	private String convertObjectToString(Object object){
		return String.valueOf(object);
	}
	
	private int convertObjectToInt(HSSFCell hssfCell){
		if(hssfCell != null){
			if(hssfCell.getCellType() == Cell.CELL_TYPE_NUMERIC)
				return (int)hssfCell.getNumericCellValue();
		}
		return 0;
		
		
	}
	
	private Long convertDateObjIntoLong(Object dataObj, String dateFormat){
    	try {
    		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			return sdf.parse(String.valueOf(dataObj)).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
    	return (long) 0;
    	
	}
	
	private double convertObjectToDouble(HSSFCell hssfCell){
		
		if(hssfCell != null){
			if(hssfCell.getCellType() == Cell.CELL_TYPE_NUMERIC)
				return hssfCell.getNumericCellValue();
		}
		return 0;
	}
	
	
	private Object getCellValue(HSSFCell hssfCell){
		
		Object cellValue = "";
		
		if(hssfCell != null){
			switch (hssfCell.getCellType()) {
			
		        case Cell.CELL_TYPE_STRING:
		        	cellValue = hssfCell.getRichStringCellValue().getString();
						
		            break;
		            
		        case Cell.CELL_TYPE_NUMERIC:
		            if (DateUtil.isCellDateFormatted(hssfCell)) {
		            	cellValue =  hssfCell.getDateCellValue();
		            	//HSSFDateUtil dateUtil = new HSSFDateUtil();
		            	//double date = dateUtil.getExcelDate(hssfCell.getDateCellValue());
		            	
		            	if(Constants.DEBUG_LOG)Log.d("CHECK", "Date="+cellValue);
		            } else {
		            	cellValue = hssfCell.getNumericCellValue();
		            	if(Constants.DEBUG_LOG)Log.d("CHECK", "numeric="+cellValue);
		            }
		            break;
		            
		        case Cell.CELL_TYPE_BOOLEAN:
		        	cellValue = hssfCell.getBooleanCellValue();
		            break;
		            
		        case Cell.CELL_TYPE_FORMULA:
		        	cellValue = hssfCell.getCellFormula();
		            break;
		            
		        default :
		        	cellValue = "";
		            
			}
		}
		
		return cellValue;
	}

}

package com.strobilanthes.forrestii.model.general;

import java.beans.PropertyChangeListener;

import android.app.Activity;

public interface ForrestiiSubject {
	public void addChangeListener(PropertyChangeListener newListener);

	public void removeChangeListener(PropertyChangeListener listenerToRemove);

	void setContext(Activity context);

	Activity getContext();
}

package com.strobilanthes.forrestii.model.search;

import java.util.ArrayList;
import java.util.List;

import com.strobilanthes.forrestii.model.ratings.RatingList;

public class AbstractSearchCard {
	private boolean success;
	private List<Friends> friendsList = new ArrayList<Friends>();
	private List<FriendOfFriends> friendofFriendList = new ArrayList<FriendOfFriends>();
	private List<RatingList> ratingsList = new ArrayList<RatingList>();

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<Friends> getFriendsList() {
		return friendsList;
	}

	public void setFriendsList(Friends friends) {
		this.friendsList.add(friends);
	}

	public List<FriendOfFriends> getFriendofFriendList() {
		return friendofFriendList;
	}

	public void setFriendofFriendList(FriendOfFriends friendofFriendList) {
		this.friendofFriendList.add(friendofFriendList);
	}

	public List<RatingList> getRatingsList() {
		return ratingsList;
	}

	public void setRatingsList(RatingList ratingsList) {
		this.ratingsList.add(ratingsList);
	}
}

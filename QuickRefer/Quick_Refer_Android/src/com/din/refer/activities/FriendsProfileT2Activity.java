package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.SparseArrayCompat;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.google.inject.Inject;
import com.nineoldandroids.view.ViewHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.fragments.ProfileT2andT3BasicInfoFragment;
import com.strobilanthes.forrestii.act.fragments.ProfileT2andT3SkillFragment;
import com.strobilanthes.forrestii.act.fragments.ScrollTabHolderFragment;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.friendslist.AbstractUserList;
import com.strobilanthes.forrestii.model.friendslist.Friends;
import com.strobilanthes.forrestii.model.skillset.UserSkillsSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.userprofile.UserProfileImpl;
import com.strobilanthes.forrestii.model.viewreviews.AbstractReviewProfile;
import com.strobilanthes.forrestii.model.viewreviews.AbstractViewReviews;
import com.strobilanthes.forrestii.slidingtab.AlphaForegroundColorSpan;
import com.strobilanthes.forrestii.slidingtab.PagerSlidingTabStrip;
import com.strobilanthes.forrestii.slidingtab.ScrollTabHolder;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;

public class FriendsProfileT2Activity extends BaseActivity<DBAccess> implements ScrollTabHolder, ViewPager.OnPageChangeListener,PropertyChangeListener,OnClickListener  {

	private PagerSlidingTabStrip mPagerSlidingTabStrip;
	private ViewPager mViewPager;
	private PagerAdapter mPagerAdapter;
	private int mActionBarHeight;
	private int mMinHeaderHeight;
	private int mHeaderHeight;
	private int mMinHeaderTranslation;
	private TypedValue mTypedValue = new TypedValue();
	private SpannableString mSpannableString;
	private AlphaForegroundColorSpan mAlphaForegroundColorSpan;
	private AbstractUserProfile abstractUserProfile = null;
	private DisplayImageOptions optionsImgCard;
	private String strPrimarySkillName;
	private ArrayList<String>TotalSkillAL = new ArrayList<String>();
	private ArrayList<String>profileSecondarySkillsAL = new ArrayList<String>();
	private Typeface mFontLight,mFontDark;
	private boolean isPick = false;
	
	@Inject UserProfileImpl userProfileModel;
	@Inject Friends contactsModel;

	@InjectView(R.id.header) 						private View mHeader;
	@InjectView(R.id.profilT2_img_card)				private ImageView profileT2_imgCard;
	@InjectView(R.id.profileT2_userName)			private TextView profileT2_txtUserName;
	@InjectView(R.id.profileT2_skillName)			private TextView profileT2_txtSkillName;
	@InjectView(R.id.profileT2_ratingBar)			private RatingBar profileT2RatingBar;
	@InjectView(R.id.profileT2_points)				private TextView profileT2_txtPoints;
	@InjectView(R.id.profileT2_commonCards)			private TextView profileT2_commonCards;
	@InjectView(R.id.profileT2_txt_friend)			private TextView txt_friend_status;
	@InjectView(R.id.profileT2lyt_common_cards)		private LinearLayout profileT2lyt_common_cards;
	@InjectView(R.id.profileT2_img_user)			private RoundedImageView profileT2_img_user;
	//@InjectView(R.id.profileT2_lyt_rewards)	    private LinearLayout profileT2lyt_rewards;
	//@InjectView(R.id.profileT2_txt_rewards)		private TextView profileT2Txt_rewards;
	private ArrayList<AbstractReviewProfile> mAbstractViewReviews;

	private int mStatus = -1;
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		contactsModel.setContext(this);
		userProfileModel.setContext(this);

		
		getSupportActionBar().setTitle("Profile");
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		
		getSupportActionBar().setBackgroundDrawable(null);
		
		optionsImgCard = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.card_default3)
		.showImageForEmptyUri(R.drawable.card_default3)
		.showImageOnFail(R.drawable.card_default3)
		.cacheInMemory(true).cacheOnDisc(true).considerExifParams(true)
		//.displayer(new RoundedBitmapDisplayer(80))
		.build();
		
		abstractUserProfile =  (AbstractUserProfile) getIntent().getSerializableExtra("ProfileT2");
		mStatus = getIntent().getIntExtra("friend_status", -1);
		
		if(abstractUserProfile != null){
			
			getSupportActionBar().setTitle(abstractUserProfile.getFirstName());
			
			JSONObject postObject1 = new JSONObject();
			try {
				postObject1.put("userId",abstractUserProfile.getUserId());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			contactsModel.requestUserDetails(Constant.GET_USER_PROFILE,postObject1);
			
		}
		showUserFriendsReviews();

	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
    
		MenuInflater actionbarmenuinflater = getSupportMenuInflater();
		actionbarmenuinflater.inflate(R.menu.overflow_menu, menu);
		   
		    return super.onCreateOptionsMenu(menu);
	}
	public void showUserFriendsReviews(){
		JSONObject json = new JSONObject();
		try {
			json.put("userProfile",new JSONObject().put("userId", abstractUserProfile.getUserId()));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		userProfileModel.viewUserFriendsReviews(Constant.VIEW_REFERRALS_REVIEW, json);
	}
	
	private void loadUI(){
		
		if(mStatus != -1){
			if(mStatus == 0)
				txt_friend_status.setText("Add Friend");
		}
		
		fontStyles();
		
		//getSupportActionBar().setDisplayUseLogoEnabled(false);
		
		mMinHeaderHeight = getResources().getDimensionPixelSize(R.dimen.up_min_header_height);
		mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.up_header_height);
		mMinHeaderTranslation = -mMinHeaderHeight /*+ getActionBarHeight();*/;

		mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setOffscreenPageLimit(2);

		mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
		mPagerAdapter.setTabHolderScrollingContent(this);

		mViewPager.setAdapter(mPagerAdapter);
		mPagerSlidingTabStrip.setTextSize((int)Util.getDimensPxtoDp(14, this));
	    mPagerSlidingTabStrip.setTypeface(mFontDark, Typeface.NORMAL);
		mPagerSlidingTabStrip.setViewPager(mViewPager);
		mPagerSlidingTabStrip.setOnPageChangeListener(this);
		mSpannableString = new SpannableString(getString(R.string.app_name));
		mAlphaForegroundColorSpan = new AlphaForegroundColorSpan(0xffffffff);
		
		profileT2lyt_common_cards.setOnClickListener(this);
		
//		ViewHelper.setAlpha(getActionBarIconView(), 0f);			
	}
	public void reportDialogOptions(){
	     
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	     
	    // Set the dialog title
	    builder.setTitle("")
	     
	    // specify the list array, the items to be selected by default (null for none),
	    // and the listener through which to receive call backs when items are selected
	    // again, R.array.choices were set in the resources res/values/strings.xml
	    .setSingleChoiceItems(R.array.choices, 0, new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface arg0, int arg1) {
	        	
	        	
	        	int selectedPosition = ((AlertDialog)arg0).getListView().getCheckedItemPosition();
	             updateReport(selectedPosition);
	        }
	 
	    })
	            
//	     // Set the action buttons
//	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//	        @Override
//	        public void onClick(DialogInterface dialog, int id) {
//	            // user clicked OK, so save the mSelectedItems results somewhere
//	            // or return them to the component that opened the dialog
//	            
//	        }
//	    })
//	     
//	    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//	        @Override
//	        public void onClick(DialogInterface dialog, int id) {
//	            // removes the dialog from the screen
//	             
//	        }
//	    })
	     
	    .show();
	     
	}
	public void blockDialog(){
	     
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	     
	    // Set the dialog title
	    builder.setTitle("Do you want to block this user")
	     
	   
	            
	     // Set the action buttons
	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int id) {
	            // user clicked OK, so save the mSelectedItems results somewhere
	            // or return them to the component that opened the dialog
	            
	        }
	    })
	     
	    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int id) {
	            // removes the dialog from the screen
	             
	        }
	    })
	     
	    .show();
	     
	}
	public void updateReport(int position){
		switch (position) {
		case 0:
			Util.showAlert(this, "", "content");
			break;
		case 1:
			Util.showAlert(this, "", "account");

		    break;	
		default:
			break;
		}
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				
				if(getSupportFragmentManager().getBackStackEntryCount() == 0){
					onBackPressed();
				} else {
					getSupportFragmentManager().popBackStack();
				}
				
				return true;
			case R.id.reviews:
				if(mAbstractViewReviews!=null && mAbstractViewReviews.size()>0){
				Intent rewardsIntent = new Intent(FriendsProfileT2Activity.this, UserRatingReviewActivity.class);
				Bundle friendsObject = new Bundle();
				friendsObject.putSerializable("UserFriendDetails", mAbstractViewReviews);
				friendsObject.putString("reviews_view", "UserFriendsReviews");

				//rewardsIntent.putExtra("reviews_view", "UserFriendsReviews");
				//rewardsIntent.putExtra("UserFriendDetails", mAbstractViewReviews);
				rewardsIntent.putExtras(friendsObject);
				startActivity(rewardsIntent);
			}else{
				Util.showAlert(FriendsProfileT2Activity.this, "", "No reviews");
			}
				return true;
			
			case R.id.report:
				Util.showAlert(this, "", "report");
				reportDialogOptions();
				return true;
			case R.id.block:
				Util.showAlert(this, "", "block");
				blockDialog();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onBackPressed() {
		finish();		
		super.onBackPressed();
	}
	private void fontStyles() {
		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		profileT2_txtUserName.setTypeface(mFontDark);
		profileT2_txtSkillName.setTypeface(mFontLight);
		profileT2_txtPoints.setTypeface(mFontDark);
		profileT2_commonCards.setTypeface(mFontDark);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public int getActionBarHeight() {
		if (mActionBarHeight != 0) {
			return mActionBarHeight;
		}
		
		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB){
			getTheme().resolveAttribute(android.R.attr.actionBarSize, mTypedValue, true);
		}else{
			getTheme().resolveAttribute(R.attr.actionBarSize, mTypedValue, true);
		}
		
		mActionBarHeight = TypedValue.complexToDimensionPixelSize(mTypedValue.data, getResources().getDisplayMetrics());
		
		return mActionBarHeight;
	}
	
	private void setTitleAlpha(float alpha) {
		
		mAlphaForegroundColorSpan.setAlpha(alpha);
		mSpannableString.setSpan(mAlphaForegroundColorSpan, 0, mSpannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setTitle(mSpannableString);
		
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private ImageView getActionBarIconView() {
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			return (ImageView)findViewById(android.R.id.home);
		}

		return (ImageView)findViewById(android.R.id.home);
	}

	public class PagerAdapter extends FragmentPagerAdapter {

		private SparseArrayCompat<ScrollTabHolder> mScrollTabHolders;
		private final String[] TITLES = { "General","Skills"};
		private ScrollTabHolder mListener;

		public PagerAdapter(FragmentManager fm) {
			super(fm);
			mScrollTabHolders = new SparseArrayCompat<ScrollTabHolder>();
		}

		public void setTabHolderScrollingContent(ScrollTabHolder listener) {
			mListener = listener;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return TITLES[position];
		}

		@Override
		public int getCount() {
			return TITLES.length;
		}

		@Override
		public Fragment getItem(int position) {
			ScrollTabHolderFragment fragment = null;

			switch (position) {

			case 0:
				fragment = (ScrollTabHolderFragment) new ProfileT2andT3BasicInfoFragment();
				Bundle b = new Bundle();
				b.putSerializable("AbstractUserProfile", abstractUserProfile);
				b.putInt(ProfileT2andT3BasicInfoFragment.ARG_POSITION, position);
				b.putString("category","t2");
				fragment.setArguments(b);
				
				break;
			case 1:
				fragment = (ScrollTabHolderFragment) new ProfileT2andT3SkillFragment();
				Bundle args = new Bundle();
				args.putString("primarySkill", strPrimarySkillName);
				args.putSerializable("secondaryskill", TotalSkillAL);
				args.putInt(ProfileT2andT3SkillFragment.ARG_POSITION, position);
				fragment.setArguments(args);
				
				break;
			default:
				break;
			}
			mScrollTabHolders.put(position, fragment);
			if (mListener != null) {
				fragment.setScrollTabHolder(mListener);
			}

				return fragment;

		}

		public SparseArrayCompat<ScrollTabHolder> getScrollTabHolders() {
			return mScrollTabHolders;
		}

	}
	@Override
	public void onPageScrollStateChanged(int arg0) {
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		
	}

	@Override
	public void onPageSelected(int position) {
		SparseArrayCompat<ScrollTabHolder> scrollTabHolders = mPagerAdapter.getScrollTabHolders();
		ScrollTabHolder currentHolder = scrollTabHolders.valueAt(position);

		currentHolder.adjustScroll((int) (mHeader.getHeight() + ViewHelper.getTranslationY(mHeader)));
	}

	@Override
	public void adjustScroll(int scrollHeight) {
			
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount, int pagePosition) {
		
		if (mViewPager.getCurrentItem() == pagePosition) {
			int scrollY = getScrollY(view);
			ViewHelper.setTranslationY(mHeader, Math.max(-scrollY, mMinHeaderTranslation));
//			float ratio = clamp(ViewHelper.getTranslationY(mHeader) / mMinHeaderTranslation, 0.0f, 1.0f);
//			setTitleAlpha(clamp(5.0F * ratio - 4.0F, 0.0F, 1.0F));
		}
		
	}
	public static float clamp(float value, float max, float min) {
		return Math.max(Math.min(value, min), max);
	}
	public int getScrollY(AbsListView view) {
		View c = view.getChildAt(0);
		if (c == null) {
			return 0;
		}

		int firstVisiblePosition = view.getFirstVisiblePosition();
		int top = c.getTop();

		int headerHeight = 0;
		if (firstVisiblePosition >= 1) {
			headerHeight = mHeaderHeight;
		}

		return -top + firstVisiblePosition * c.getHeight() + headerHeight;
	}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("user_details")){
			AbstractUserList abstractUserList = contactsModel.getUserDetails();
			if (abstractUserList != null) {
				if (abstractUserList.getStatus() == 1) {
					setContentView(R.layout.act_profile_t2);
					
					abstractUserProfile = abstractUserList.getResponse();
					
					loadUI();
					
					updateUserProfile(abstractUserProfile);
					
				} else {
					profileT2_commonCards.setText("0");
					Util.showAlert(this, "Message", ""+abstractUserList.getError());
					//finish();
				}
			}else {
				Util.showAlert(this, "Message", "Request Failed.");
				//finish();
			}
		}else if (event.getPropertyName().equals("UserFriendsReviews")){
			AbstractViewReviews abstractReviewList = userProfileModel.getReviewsRatings();
			if (abstractReviewList != null) {
				if (abstractReviewList.getStatus() == 1) {
					
					 mAbstractViewReviews = abstractReviewList.getResponse();
//					if(mAbstractViewReviews!=null){
//					reviewListAdapter = new ReviewListAdapter(this,mAbstractViewReviews);
//					T3contactsListview.setAdapter(reviewListAdapter);
//					}
				} else {
					//Util.showAlert(this, "", abstractReviewList.getError());
				}
			}else {
				Util.showAlert(this, "Message", "Request Failed");
				//finish();
			}
		}
	}
	
	private void updateUserProfile(AbstractUserProfile abstractUserProfile2) {
		// TODO Auto-generated method stub
		if(abstractUserProfile2 != null) {  //Check abstractuserprofile!=null
			
			profileT2_txtUserName.setText(abstractUserProfile2.getFirstName()+" "+abstractUserProfile2.getLastName());
			if( abstractUserProfile2.getCardName() != null)
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_CARD_NAME + abstractUserProfile2.getCardName(), profileT2_imgCard, optionsImgCard,
					animateFirstListener);
			else
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_CARD_NAME + "null", profileT2_imgCard, optionsImgCard,
						animateFirstListener);
			
			ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + abstractUserProfile2.getPhotoID(), profileT2_img_user, options,
					animateFirstListener);
			profileT2_txtUserName.setSelected(true);
			profileT2_txtSkillName.setText("");
			
			if(abstractUserProfile2.getUserSkillSets().size()>0){

				for (UserSkillsSet userSkillsSet:abstractUserProfile2.getUserSkillSets()) {
					if(userSkillsSet.isPrimarySkill()){
						strPrimarySkillName = userSkillsSet.getSkills().getSkillName();
						profileT2_txtSkillName.setText(""+userSkillsSet.getSkills().getSkillName());
					}else if(!userSkillsSet.isPrimarySkill()){
						profileSecondarySkillsAL.add(userSkillsSet.getSkills().getSkillName());
						//strSecondarySkillName = strSecondary.substring(1,strSecondary.length()-1);
					}else{
						//strSecondarySkillName = "No Skills";
					}
				}
			}
			profileT2_txtSkillName.setSelected(true);
			if(profileSecondarySkillsAL.size()>0){
				for (int i = 0; i < profileSecondarySkillsAL.size(); i++) {
					TotalSkillAL.add(profileSecondarySkillsAL.get(i));
				}
				}else{
					TotalSkillAL.add("No Skills");
				}
			
			profileT2_txtPoints.setText(String.valueOf(abstractUserProfile2.getTotalCoin()));
			profileT2RatingBar.setRating(abstractUserProfile2.getTotalRating());
			profileT2_commonCards.setText(""+(abstractUserProfile2.getContactCount()));
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		contactsModel.addChangeListener(this);
		userProfileModel.addChangeListener(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		contactsModel.removeChangeListener(this);
		userProfileModel.removeChangeListener(this);

	}
	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.profileT2lyt_common_cards:
			if(abstractUserProfile != null && abstractUserProfile.getContactCount() > 1){
				Intent intent = new Intent(FriendsProfileT2Activity.this,FriendsListActivity.class);
				intent.putExtra("profile_view", "T3");
				intent.putExtra("T2UserId", ""+abstractUserProfile.getUserId());
				startActivity(intent);
			} else {
				Util.showAlert(v.getContext(), "Message", "You are their only friend at the moment!");
			}
			break;
		case R.id.profileT2_img_friend:
			AlertDialog.Builder alertbuilder = new AlertDialog.Builder(v.getContext(), AlertDialog.THEME_HOLO_LIGHT);
			alertbuilder.setTitle("Message");
			alertbuilder.setMessage("Do you want to unfriend ");

			alertbuilder.setPositiveButton("Ok",new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog,
								int which) {
						
							JSONObject unfriendObject = new JSONObject();
							try {
								unfriendObject.put("Id",abstractUserProfile.getUserId());
							} catch (JSONException e) {
								e.printStackTrace();
							}
							contactsModel.unFriendRequest(
									Constant.UN_FRIEND, unfriendObject);
						}
					});
			alertbuilder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							finish();
						}
					});

			alertbuilder.show();
			break;
		default:
			break;
		}
	}
}

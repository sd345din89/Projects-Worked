package com.strobilanthes.forrestii.model.skillset;

import java.io.Serializable;


public class AbstractSkillSet implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int skillId;
	private String skillName;
	private boolean selected;

	public int getSkillId() {
		return skillId;
	}
	public void setSkillId(int skillId) {
		this.skillId = skillId;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName.toString().trim();
	}
	
	@Override
	public String toString() {
		return skillName;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return this.selected;
	}

}

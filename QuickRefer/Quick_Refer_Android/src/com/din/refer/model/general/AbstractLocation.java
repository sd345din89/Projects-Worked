package com.strobilanthes.forrestii.model.general;

import java.io.Serializable;

public class AbstractLocation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String locationName;
	private int id;
	
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return locationName;
	}
	
}

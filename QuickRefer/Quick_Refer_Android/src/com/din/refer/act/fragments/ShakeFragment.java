package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.callbacks.LocationChangeListener;
import com.strobilanthes.forrestii.activities.FriendsListActivity;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.shake.AbstractShakeDetails;
import com.strobilanthes.forrestii.model.shake.Shake;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;

public class ShakeFragment extends BaseFragment<DBAccess> implements SensorEventListener,OnConnectionFailedListener,ConnectionCallbacks,LocationChangeListener, LocationListener{
	
	/** Minimum movement force to consider. */
	private static final int MIN_FORCE = 17;
//	private static final int MIN_FORCE = 10;
	/**
     * Minimum times in a shake gesture that the direction of movement needs to
	 * change.
	*/
	private static final int MIN_DIRECTION_CHANGE = 3;
	
	/** Maximum pause between movements. */
	private static final int MAX_PAUSE_BETHWEEN_DIRECTION_CHANGE = 200;
	
	/** Maximum allowed time for shake gesture. */
	private static final int MAX_TOTAL_DURATION_OF_SHAKE = 400;
	
	private SensorManager mSensorManager;
	private Vibrator vibe;
	
	/** Time when the gesture started. */
	private long mFirstDirectionChangeTime = 0;
	
	/** Time when the last movement started. */
	private long mLastDirectionChangeTime;
	
	/** How many movements are considered so far. */
	private int mDirectionChangeCount = 0;
	
	/** The last x position. */
	private float lastX = 0;
	
	/** The last y position. */
	private float lastY = 0;
	
	/** The last z position. */
	private float lastZ = 0;
	
	private Dialog builder;
	
	private boolean isShakeWorking = false;
	
	@InjectView (R.id.profile_edu_txt_hint)				private TextView loadingText;
	@InjectView (R.id.progressBar1)						private ProgressBar loadigBar;
	@InjectView (R.id.shakeImage)						private ImageView shakeImage;
	
	@Inject Shake  shakeModel;
	private Animation shakeAnimation;
	private static Location mLocation;
	private LocationClient mLocationClient;
	private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setNumUpdates(2)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	
	private static ShakeFragment frag = null;
	public static ShakeFragment newInstance() {
		if(frag == null)
			frag = new ShakeFragment();
        return frag;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.frg_shake_and_share, container,false);
		super.initilizeContents();
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		shakeModel.setContext(getActivity());
		vibe = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
		mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
		shakeAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
		
		getActivity().getActionBar().setTitle("Shake");
		getActivity().getActionBar().setDisplayShowHomeEnabled(true);
		getActivity().getSupportFragmentManager();
		super.onViewCreated(view, savedInstanceState);
	}
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onResume() {
		Util.isLocationServiceON(getActivity());
		setUpLocationClientIfNeeded();
		mLocationClient.connect();
		shakeModel.addChangeListener(this);
		mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_UI);
		
		super.onResume();
	}
	
	private void setUpLocationClientIfNeeded() {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(
                    getActivity(),
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
	}
	@Override
	public void onPause() {
		if (mLocationClient != null) {
			mLocationClient.disconnect();
		}
		shakeModel.removeChangeListener(this);
		mSensorManager.unregisterListener(this);
		super.onPause();
	}

	@Override
	public void onSensorChanged(SensorEvent event) {

		// get sensor data
	    float x = event.values[SensorManager.DATA_X];
	    float y = event.values[SensorManager.DATA_Y];
	    float z = event.values[SensorManager.DATA_Z];

	    // calculate movement
	    float totalMovement = Math.abs(x + y + z - lastX - lastY - lastZ);

	    if (totalMovement > MIN_FORCE) {

	      // get time
	      long now = System.currentTimeMillis();

	      // store first movement time
	      if (mFirstDirectionChangeTime == 0) {
	        mFirstDirectionChangeTime = now;
	        mLastDirectionChangeTime = now;
	      }

	      // check if the last movement was not long ago
	      long lastChangeWasAgo = now - mLastDirectionChangeTime;
	      if (lastChangeWasAgo < MAX_PAUSE_BETHWEEN_DIRECTION_CHANGE) {

	        // store movement data
	        mLastDirectionChangeTime = now;
	        mDirectionChangeCount++;

	        // store last sensor data 
	        lastX = x;
	        lastY = y;
	        lastZ = z;

	        // check how many movements are so far
	        if (mDirectionChangeCount >= MIN_DIRECTION_CHANGE) {

	          // check total duration
	          long totalDuration = now - mFirstDirectionChangeTime;
	          if (totalDuration < MAX_TOTAL_DURATION_OF_SHAKE) {
	        	  if(builder != null){
	        		  if(!builder.isShowing())
	        			  onShake();
	        		  /*else if(builder.isShowing())
	        			  builder.dismiss();
	        			  */
	        	  }else{
	        		  if(!isShakeWorking){
	        			  isShakeWorking = true;
	        			  onShake();
	        		  }
	        			 
	        	  }
	            resetShakeParameters();
	          }
	        }

	      } else {
	        resetShakeParameters();
	      }
	    }
	}
	//shake found
	private void onShake(){
		vibe.vibrate(100);
		showDialog();
	}
	
	private void showDialog(){
		loadingText.setVisibility(View.VISIBLE);
		loadigBar.setVisibility(View.VISIBLE);
		shakeImage.startAnimation(shakeAnimation);
		
		JSONObject jobject = new JSONObject();
		try {
			/*SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Forrestii", Context.MODE_PRIVATE);
			String lattitude = sharedPreferences.getString("latitude", "0");
			String longitude = sharedPreferences.getString("longitude", "0");*/
			
			if (mLocation != null) {
				double lattitude = mLocation.getLatitude();
				double longitude = mLocation.getLongitude();
				if (lattitude != 0.0 && longitude != 0.0 ) {
					jobject.put("userProfile", new JSONObject().put("userId", getHelper().getDBUserProfile().getUserId()));
					jobject.put("latitude", lattitude);
					jobject.put("longitude", longitude);
					
					shakeModel.reuestShakeDetails(Constant.FORRESTII_SHAKE, jobject);	
				} else {
					LocationError("Unable to locate your device. Please shake again");
				}
				
			} else {
				LocationError("Not getting location shake again");
			}
			
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
	}
	
	void LocationError(String message){
		Util.showAlert(getActivity(), "", message);
		loadingText.setVisibility(View.GONE);
		loadigBar.setVisibility(View.GONE);
		shakeImage.clearAnimation();
		isShakeWorking = false;
	}
	//Reset Parameters
	private void resetShakeParameters() {
	    mFirstDirectionChangeTime = 0;
	    mDirectionChangeCount = 0;
	    mLastDirectionChangeTime = 0;
	    lastX = 0;
	    lastY = 0;
	    lastZ = 0;
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("shake")) {
			isShakeWorking = false;
			
			AbstractShakeDetails abstractShakeDetails = shakeModel.getAbstractShakeDetails();
			
			if(abstractShakeDetails  != null) {
				if(abstractShakeDetails.getStatus() == 1){
					Intent intent = new Intent(getActivity(),FriendsListActivity.class);
					intent.putExtra("forrestii_shake", abstractShakeDetails);
					intent.putExtra("profile_view", "T3");
					startActivity(intent);
				} else {
					Util.showAlert(getActivity(),"Message", "No users found now. Please try later...");
				}
			} else {
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
			loadingText.setVisibility(View.GONE);
			loadigBar.setVisibility(View.GONE);
			shakeImage.clearAnimation();
		}		
	}

	@Override
	public void onLocationChanged(Location location) {
		mLocation = location;
	}

	@Override
	public void onConnected(Bundle arg0) {
		mLocationClient.requestLocationUpdates(REQUEST, this);
	}

	@Override
	public void onDisconnected() {
//		mLocationClient.removeLocationUpdates(this);
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		LocationError("Not Getting Location ...");
	}

}

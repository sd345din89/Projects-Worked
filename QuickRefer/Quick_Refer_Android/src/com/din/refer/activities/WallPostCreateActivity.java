package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.AbstractCreateWallPost;
import com.strobilanthes.forrestii.model.wallpost.WallPost;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class WallPostCreateActivity extends BaseActivity<DBAccess>  implements PropertyChangeListener {

	@InjectView(R.id.create_wallpost_skillset)    private AutoCompleteTextView autocomplete_skillset;
	@InjectView(R.id.create_wallpost_comments)    private EditText edit_comments;
	@InjectView(R.id.create_wallpost_count)   	  private TextView txt_count;
	@InjectView(R.id.create_wallpost_header)   	  private TextView txtHeader;

	
	@Inject WallPost wallPostModel;

	private Typeface mFontLight,mFontDark;
	private int postId = -1;
	private int skillId = -1;
	private String skillSetName = null;
	//private int SkillsetId;
	private ArrayList<AbstractSkillSet> skillSet;
	private AbstractUserProfile abstractUserProfile = null;
	private String mPostContent = null,mAbusedString;
	private ArrayList<String> mAbusedWords = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_create_post);
		getSupportActionBar().setTitle("Create Your Post");
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		fontStyles();
		
		skillSet = getHelper().getDBSkillset();
		abstractUserProfile = getHelper().getDBUserProfile();
		
		autocomplete_skillset.setAdapter(new ArrayAdapter<AbstractSkillSet>(this,  android.R.layout.simple_list_item_1, skillSet));
		//autocomplete_skillset.addTextChangedListener(Util.allowOnlyText(this, autocomplete_skillset));

		autocomplete_skillset.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				AbstractSkillSet abstractSkillSet = (AbstractSkillSet) parent.getItemAtPosition(position);
				skillSetName =  abstractSkillSet.getSkillName();
				skillId = abstractSkillSet.getSkillId();
				
				L.d("Selected SkillName = "+abstractSkillSet.toString());
			}
		});
		
		
		/*autocomplete_skillset.setOnFocusChangeListener(new OnFocusChangeListener() {
		    public void onFocusChange(View v, boolean hasFocus) {
		        if(hasFocus) {
		        	autocomplete_skillset.showDropDown();
		        }
		    }
		});*/
		
		autocomplete_skillset.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				skillId = -1;
				skillSetName = s.toString();
			}
		});
		Util.hideEditTextError(edit_comments);
		edit_comments.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
				txt_count.setText(250 - s.toString().length() + "/250");
		        
			}
		});

       try {

    	   	String line;
    	   	AssetManager assetManager = this.getAssets();
    	    InputStream input = assetManager.open("abusedwords.txt");
    	    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		    while (( line = reader.readLine()) != null) {
		    	mAbusedWords.add(line);
		    	L.i(line);
		    	//mLine = line;
		    }
		    input.close();
    	    
           
       } catch (IOException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
		wallPostModel.setContext(this);
		
		fontStyles();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.wall_create_menu, menu);
		MenuItem menuItem = menu.findItem(R.id.action_done);
		menuItem.setVisible(true);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case android.R.id.home:
				
				Util.hideSoftKeyboard(this,edit_comments);
				
				if(getSupportFragmentManager().getBackStackEntryCount() == 0){
					finish();
				}else{
					getSupportFragmentManager().popBackStack();
				}
			break;
			case R.id.action_done:
				
				Util.hideSoftKeyboard(this,edit_comments);
				
				mPostContent = edit_comments.getText().toString();
				mPostContent = mPostContent.replace("\\n", " ").toString().trim();
				
//				for (int i = 0; i <= (mPostContent.length() - mAbusedWords.get(0).length()); i++) {
//			           if (mPostContent.regionMatches(i, mAbusedWords.get(0), 0, mAbusedWords.get(0).length())) {
//			              foundIt = true;
//			              //System.out.println(mPostContent.substring(i, i + mAbusedWords.get(0).length()));
//			              mLine = mPostContent.substring(i, i + mAbusedWords.get(0).length());
//			              break;
//			           }
//			        }
				boolean foundIt = false;
				for(int i=0; i<mAbusedWords.size();i++){
					
					if(mPostContent.contains(mAbusedWords.get(i))){
						foundIt = true;
						break;
					}
				}
				
				if(!foundIt){
				if(skillId !=-1 && skillSetName != null){
					
					new AlertDialog.Builder(WallPostCreateActivity.this,AlertDialog.THEME_HOLO_LIGHT).setMessage("Whom should this post reach?")
					.setPositiveButton("Friends", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							getRequestDetails(false);
							
						}
					}).setNeutralButton("Friends of friends",new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							getRequestDetails(true);
						}
					}).show();
				}else{
					autocomplete_skillset.setError("Please select skillset.");
				}}else{
					edit_comments.setFocusableInTouchMode(true);
					edit_comments.requestFocus();
					edit_comments.setError("Inappropriate words will not be accepted");
					//L.i("++++++++++++++word++++"+mLine);
				}
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void getRequestDetails(boolean isPostToT3){
		JSONObject object = new JSONObject();
		 try {
			object.put("message",mPostContent);
			object.put("postByUserProfile",new JSONObject().put("userId", getHelper().getUserId()).put("firstName", abstractUserProfile.getFirstName().toString().trim()));
			object.put("Skill",new JSONObject().put("skillId",skillId));
			object.put("isPostToT3",isPostToT3);
			
			wallPostModel.createWallPost(Constant.CREATE_POST, object);
			
		 } catch (JSONException e) {
			e.printStackTrace();
		 }
		 
	}
	@Override
	public void finish() {
		Intent intent = new Intent();
		
		if(postId != -1 && skillId !=-1 && skillSetName != null){
			intent.putExtra("reply_contents", edit_comments.getText().toString());
			intent.putExtra("post_id",postId);
			intent.putExtra("skill_id",skillId);
			intent.putExtra("skill_name",skillSetName);
			setResult(22,intent );
		}else{
			setResult(999,intent );
		}
		super.finish();
	}

	@Override
	public void onResume(){
		super.onResume();
		wallPostModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		wallPostModel.removeChangeListener(this);
		Util.hideSoftKeyboard(this, edit_comments);
		Util.stopAlert();
	}
	
	public void fontStyles() {

		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		autocomplete_skillset.setTypeface(mFontDark);
		txtHeader.setTypeface(mFontLight);
		edit_comments.setTypeface(mFontDark);
		txt_count.setTypeface(mFontDark);
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if(event.getPropertyName().equals("dowallpost")){
			AbstractCreateWallPost abstractCreateWallPost = wallPostModel.getCreateWallPostResponse();
			if(abstractCreateWallPost != null){
				if(abstractCreateWallPost.getStatus() == 1){
					postId = abstractCreateWallPost.getCreatePostResponse().getPostid();
					finish();
				}else{
					Util.showAlert(WallPostCreateActivity.this,"Message", ""+abstractCreateWallPost.getError());
				}
			}else{
				Util.showAlert(WallPostCreateActivity.this,"Message", "Request Failed.");
			}
		}
	}


	/*public class CustomSkillsetAdapter extends ArrayAdapter<AbstractSkillSet> {
	    
	    public CustomSkillsetAdapter(Context context, ArrayList<AbstractSkillSet> Skillset) {
	       super(context, R.layout.post_subview, Skillset);
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	      
	       ViewHolder holder; // view lookup cache stored in tag
	       if (convertView == null) {
	           holder= new ViewHolder();
	          LayoutInflater inflater = LayoutInflater.from(getContext());
	          convertView = inflater.inflate(R.layout.post_subview, parent, false);
	          holder.SkillSetname = (TextView) convertView.findViewById(R.id.txt_sub_post);
	          convertView.setTag(holder);
	       } else {
	           holder = (ViewHolder) convertView.getTag();
	       }
	       holder.SkillSetname.setTypeface(mFont);
	       holder.SkillSetname.setText(skillSet.get(position).getSkillName());
	       return convertView;
	   }
	}
	private static  class ViewHolder {
	    TextView SkillSetname;
	   TextView Skillsetid;
	}*/

}



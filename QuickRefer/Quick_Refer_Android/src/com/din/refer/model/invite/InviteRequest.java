package com.strobilanthes.forrestii.model.invite;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface InviteRequest extends ForrestiiSubject{
	void requestMobileInviteList(String url , JSONObject postdata);
	void requestEmailInviteList(String url , JSONObject postdata);
	
	
	void setBundleContactData(String bundleData);
	String getBundleContactData();
	
	void doValidation();
	AbstractMobileInviteList getMobileInviteDetails();
	AbstractEmailInviteList getEmailInviteDetails();
	
	void sendFriendRequest(String url, JSONObject postdata);
	
	AbstractInviteFriend getFriendsRequestData();
}

package com.strobilanthes.forrestii.model.rewards;

public interface Authentication {
	void requestRegistrationDetails(String postdata);
	void requestLoginDetails(String postdata);
	void doValidation();
	void getAuthDetails();
	void updateAuthDetails();
}

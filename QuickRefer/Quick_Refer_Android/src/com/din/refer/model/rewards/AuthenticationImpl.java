package com.strobilanthes.forrestii.model.rewards;

import com.google.inject.Singleton;

@Singleton
public class AuthenticationImpl implements Authentication{

	
	@Override
	public void requestRegistrationDetails(String postdata) {
		
	}

	@Override
	public void requestLoginDetails(String postdata) {
		//new AuthenticationTasks(this).execute(postdata);
	}

	@Override
	public void doValidation() {
		
	}

	@Override
	public void getAuthDetails() {
	}

	@Override
	public void updateAuthDetails() {
	}

}

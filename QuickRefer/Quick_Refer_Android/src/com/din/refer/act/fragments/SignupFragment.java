package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.lookuplist.AbstractLookUpList;
import com.strobilanthes.forrestii.model.otpvalidation.AbstractOtpValidation;
import com.strobilanthes.forrestii.model.otpvalidation.AbstractResendOTP;
import com.strobilanthes.forrestii.model.otpvalidation.OtpValidation;
import com.strobilanthes.forrestii.model.signup.AbstractSignup;
import com.strobilanthes.forrestii.model.signup.Signup;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;


public class SignupFragment extends BaseFragment<DBAccess> {

	@InjectView(R.id.signup_screen_countrycode)     private EditText signupScreenCountrycode;
	@InjectView(R.id.signup_screen_mobileno)        private EditText signupScreenMobileno;
	@InjectView(R.id.signup_screen_otp_header_lyt)  private LinearLayout signupScreenOtpHeaderLyt;
	@InjectView(R.id.signup_screen_otpcode)         private EditText signupScreenOtpcode;
	@InjectView(R.id.signup_screen_resend_otpcode)  private TextView signupResendOTP;
	@InjectView(R.id.signup_screen_btnsignUp)       private Button loginScreenBtnsignUp;

	private Typeface mFontLight,mFontDark;

	@Inject Signup signupModel;
	@Inject OtpValidation otpValidationModel; 
	
	private String countryCodeId = "1";
	private boolean isOTPScreen  = false;
	private String[] countryCodes;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.frg_signup, null);

		super.initilizeContents();

		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		signupModel.setContext(getActivity());
		otpValidationModel.setContext(getActivity());
		
		loginScreenBtnsignUp.setOnClickListener(this);
		signupResendOTP.setOnClickListener(this);
		
		countryCodes = getResources().getStringArray(R.array.CountryCodes);
		
		//signupScreenCountrycode.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.frg_countrycode_list_item,android.R.id.text1, countryCodes));
		
//		signupScreenCountrycode.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view,
//					int position, long id) {
//				
//				String countryId = (String)parent.getItemAtPosition(position);
//				
//				//countryCodeId = countryId.split(",")[0];
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//				
//			}
//		});
		signupScreenCountrycode.setText(countryCodes[0]);
		signupScreenCountrycode.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				loadCountryCode();
			}
		});
		String total="A verification code has been sent to your mobile number if you have not yet received it CLICK HERE to resend it.";
		
		SpannableString ss1=  new SpannableString(total);
		    ss1.setSpan(new MyClickableSpan(total), 88, 98, 0);
		    ss1.setSpan(new RelativeSizeSpan(1.10f), 88, 98, 0);
		    signupResendOTP.setText(ss1);
		    signupResendOTP.setMovementMethod(LinkMovementMethod.getInstance());  
	}
	public void loadCountryCode() {

	    
	   // final CharSequence[] items = { "John", "Michael", "Vincent", "Dalisay" };

	    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    builder.setTitle("Choose your country code");
	    builder.setItems(countryCodes, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int position) {

	            // will toast your selection
	            //showToast("Name: " + items[item]);
	        	signupScreenCountrycode.setText(countryCodes[position]);
	            dialog.dismiss();

	        }
	    }).show();
	}
	public void fontStyles() {
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
//		signupScreenCountrycode.setTypeface(mFontDark);
		signupScreenMobileno.setTypeface(mFontDark);
		signupScreenOtpcode.setTypeface(mFontDark);
		signupResendOTP.setTypeface(mFontDark);
		loginScreenBtnsignUp.setTypeface(mFontDark);
	}
	@Override
	public boolean onBackPressed() {	
		return super.onBackPressed();
	}


	@Override
	public void onResume(){
		super.onResume();
		signupModel.addChangeListener(this);	
		otpValidationModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		signupModel.removeChangeListener(this);
		otpValidationModel.removeChangeListener(this);
		Util.hideSoftKeyboard(getActivity(), signupScreenMobileno);
		Util.stopAlert();
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if(event.getPropertyName().equals("registration")){
			loginScreenBtnsignUp.setEnabled(true);
			AbstractSignup abstractSignup = signupModel.getSignupDetails();
			
			getHelper().insertUserMaster(1234, signupScreenMobileno.getText().toString().trim(),"");
			
			if(abstractSignup != null){
				if(abstractSignup.getStatus() == 1){
					updateRegistration();
				} else {
					if(abstractSignup.getError().toString().trim().equals("OTP verification pending")){
						updateRegistration();
					} else if(abstractSignup.getError().toString().trim().equals("Password not set") || abstractSignup.getError().toString().trim().equals("Profile not Complete")){
						updateOTPLogin();	
					} else {
						Util.showAlert(getActivity(),"Message", ""+abstractSignup.getError());
						
					}
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}
		}else if(event.getPropertyName().equals("otp_verify")){
			loginScreenBtnsignUp.setEnabled(true);
			AbstractOtpValidation abstractOtpValidation = otpValidationModel.getOtpValidationDetails();
			if(abstractOtpValidation != null){
				if(abstractOtpValidation.getStatus() == 1){
					//getHelper().updateUserId(abstractOtpValidation.getResponse().getUserProfile().getUserId());
					//getHelper().insertUserProfile(abstractOtpValidation.getResponse().getUserProfile());
					updateOTPLogin();
				} else {
					Util.showAlert(getActivity(),"Message", ""+abstractOtpValidation.getError());
					
				}
			
			} else {
				Util.showAlert(getActivity(),"Message", "Request Failed");
				
			}
		}else if (event.getPropertyName().equals("resendotp")) {
			AbstractResendOTP abstractResendOTP = otpValidationModel.getResendOtpValidationDetails();
			if (abstractResendOTP != null) {
				if(abstractResendOTP.getStatus() == 1){
					signupScreenOtpcode.setText("");
					Util.showAlert(getActivity(), "Message","Verification code has been sent to your mobile");
				} else {
					Util.showAlert(getActivity(), "Message",""+abstractResendOTP.getError());
				}
			} else {
				Util.showAlert(getActivity(), "Message","Request Failed");
			}
		} else if(event.getPropertyName().equals("lookuplist")){
			
			AbstractLookUpList abstractLookUpList = signupModel.getLookupDetails();
			if(abstractLookUpList != null){
				
				getHelper().insertSkillset(abstractLookUpList.getResponse().getSkills());
				//getHelper().insertNotifyType(abstractOtpValidation.getResponse().getNotify());
				getHelper().insertTemplate(abstractLookUpList.getResponse().getTemplate());
				getHelper().insertServerInfo(abstractLookUpList.getResponse().getServerInfo().get(0));
				getHelper().insertCountryList(abstractLookUpList.getResponse().getCountry());
				getHelper().insertLocation(abstractLookUpList.getResponse().getLocation());
				
				FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left, R.anim.out_to_right);
		        fragmentTransaction.addToBackStack(null);
		        AskUserProfileFragment askUserProfileFragment = new AskUserProfileFragment();
		        fragmentTransaction.replace(R.id.signup_root, askUserProfileFragment);
		        fragmentTransaction.commit();

			} else {
				L.i("Request Failed");
			}
		}
	}
	
	private void updateOTPLogin() {
		
		isOTPScreen = false;
		signupModel.requestLookupdetails(Constant.USER_LOOKUP_LIST, null);
		
		/*FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left, R.anim.out_to_right);
        fragmentTransaction.addToBackStack(null);
        AskUserProfileFragment askUserProfileFragment = new AskUserProfileFragment();
        fragmentTransaction.replace(R.id.signup_root, askUserProfileFragment);
        fragmentTransaction.commit();*/
	}

	private void updateRegistration(){
		
		getHelper().insertUserMaster(1234, signupScreenMobileno.getText().toString().trim(),"");
		
		isOTPScreen = true;
		loginScreenBtnsignUp.setText("Verify code");
		signupScreenMobileno.clearFocus();
		signupScreenMobileno.setFocusable(false);
		signupScreenMobileno.setClickable(false);
		signupScreenMobileno.setEnabled(false);
		signupScreenMobileno.setFocusableInTouchMode(false);
		
		signupScreenOtpHeaderLyt.setVisibility(View.VISIBLE);
		signupScreenOtpcode.requestFocus();
		signupScreenOtpcode.setFocusable(true);
	
		Util.toggleSoftKeyboard(getActivity());
		

	}

	@Override
	public void onClick(View v) {
		Util.hideSoftKeyboard(getActivity(),v);
		switch (v.getId()) {
		case R.id.signup_screen_btnsignUp:
			
			if(isOTPScreen){
				if(signupScreenOtpcode.getText().toString() != null && signupScreenOtpcode.getText().toString().length() > 0){
					String mobileNum = signupScreenMobileno.getText().toString().trim();
					if(Util.isValidPhoneNumber(mobileNum) && PhoneNumberUtils.isGlobalPhoneNumber(mobileNum) && mobileNum.length() >= 10){
						 JSONObject object = new JSONObject();
						 try {
							 if(Util.isConnectedNetwork(getActivity())){
								loginScreenBtnsignUp.setEnabled(false);
								object.put("OTPUserProfile",new JSONObject().put("mobile", mobileNum));
								object.put("OTPCode", signupScreenOtpcode.getText().toString());
								
								otpValidationModel.requestOTPValidationDetails(Constant.OTP_LOGIN,object);		
							 }
							
						 } catch (JSONException e) {
							e.printStackTrace();
						 }
					} else {
						signupScreenMobileno.requestFocus();

						signupScreenMobileno.setError("Please enter a valid mobile number");
						
					}	
				} else {
					signupScreenOtpcode.requestFocus();
					signupScreenOtpcode.setError("Please enter verification code");
				}		
			} else {
				String mobileno = signupScreenMobileno.getText().toString();
				
				if(mobileno.length() <= 0){
					signupScreenMobileno.requestFocus();

					signupScreenMobileno.setError("Please enter your mobile number");
					return;
				} else {
					if(Util.isValidPhoneNumber(mobileno) && PhoneNumberUtils.isGlobalPhoneNumber(mobileno) && mobileno.length() >= 10 ){
						try {
							if(Util.isConnectedNetwork(getActivity())){
								 loginScreenBtnsignUp.setEnabled(false);
								 JSONObject object = new JSONObject();
								 object.put("mobile", mobileno);
								 object.put("ObjCountryMaster",new JSONObject().put("countryId",countryCodeId));
								 signupModel.requestRegistrationDetails(Constant.REGISTER,object);
								
								// if(getHelper().getDBSkillset() == null)
//								 signupModel.requestLookupdetails(Constant.USER_LOOKUP_LIST, null);
							} else {
								signupScreenMobileno.requestFocus();

								signupScreenMobileno.setError("Please check your internet connection");
								
							}	
						} catch (JSONException e) {
							e.printStackTrace();
						}
						
					} else {
						signupScreenMobileno.requestFocus();

						signupScreenMobileno.setError("Please enter a valid mobile number");
					} 
				}
			}
			break;
		case R.id.signup_screen_resend_otpcode:
//			Util.setTouchDelay(getActivity(), signupResendOTP);
//			JSONObject object = new JSONObject();
//			try {
//				object.put("mobile", getHelper().getMobileNo());
//				object.put("ObjCountryMaster", new JSONObject().put("countryId", 1));
//				otpValidationModel.resendOTP(Constant.RESEND_OTP, object);
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//			L.ToastMessage(getActivity(), "Resending verification code...");
			
			break;
		default:
			break;
		}
	}
	 class MyClickableSpan extends ClickableSpan{     
		   String clicked;
		   public MyClickableSpan(String string)  
		   {
		    super();
		    clicked =string;
		   }
		   public void onClick(View tv) 
		   {
				Util.setTouchDelay(getActivity(), signupResendOTP);
				JSONObject object = new JSONObject();
				try {
					object.put("mobile", getHelper().getMobileNo());
					object.put("ObjCountryMaster", new JSONObject().put("countryId", 1));
					otpValidationModel.resendOTP(Constant.RESEND_OTP, object);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				L.ToastMessage(getActivity(), "Resending verification code...");
		   }
		   public void updateDrawState(TextPaint ds)
		   {
		     ds.setColor(getResources().getColor(R.color.forrestii_pink_color));//set text color 
		     ds.setUnderlineText(true); // set to false to remove underline
		   }
		  } 
}

package com.strobilanthes.forrestii.model.shake;

import java.io.Serializable;
import java.util.ArrayList;

import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;


public class AbstractShakeDetails implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int status;
	private String error;
	private AbstractResponse response;
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public AbstractResponse getResponse() {
		return response;
	}

	public void setResponse(AbstractResponse response) {
		this.response = response;
	}

	public class AbstractResponse implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private ArrayList<FriendsList> FriendList;
		private ArrayList<GeneralListData> GeneralList;
		
		public ArrayList<FriendsList> getFriendList() {
			return FriendList;
		}
		public void setFriendList(ArrayList<FriendsList> friendList) {
			this.FriendList = friendList;
		}
		public ArrayList<GeneralListData> getGeneralList() {
			return GeneralList;
		}
		public void setGeneralList(ArrayList<GeneralListData> generalList) {
			this.GeneralList = generalList;
		}
	}
	public class GeneralListData implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private AbstractUserProfile userProfile;

		public AbstractUserProfile getUserProfile() {
			return userProfile;
		}

		public void setUserProfile(AbstractUserProfile userProfile) {
			this.userProfile = userProfile;
		}
	}
}

package kra.accounts.tracker.models;

public class CustomerAccountDetails {
	private String customerAccId;
	private String customerId;
	private String AccountId;
	private String principalAmount;
	private String principalDate;
	private String insertDueFromDate;
	private String security;
	private String description;
	private String principalPaid;
	
	private String customerAddress;
	private String mobile1;
	private String mobile2;
	private String customerName;
	
	public void setcustomerAccId(String customerAccId){
		this.customerAccId=customerAccId;
	}
	public String getcustomerAccId(){
		return customerAccId;
	}
	
	public void setcustomerId(String customerId){
		this.customerId=customerId;
	}
	public String getcustomerId(){
		return customerId;
	}
	
	public void setAccountId(String AccountId){
		this.AccountId=AccountId;
	}
	public String getAccountId(){
		return AccountId;
	}
	
	public void setprincipalAmount(String principalAmount){
		this.principalAmount=principalAmount;
	}
	public String getprincipalAmount(){
		return principalAmount;
	}
	
	public void setprincipalDate(String principalDate){
		this.principalDate=principalDate;
	}
	public String getprincipalDate(){
		return principalDate;
	}
	
	public void setinsertDueFromDate(String insertDueFromDate){
		this.insertDueFromDate=insertDueFromDate;
	}
	public String getinsertDueFromDate(){
		return insertDueFromDate;
	}
	
	public void setsecurity(String security){
		this.security=security;
	}
	public String getsecurity(){
		return security;
	}
	
	public void setdescription(String description){
		this.description=description;
	}
	public String getdescription(){
		return description;
	}
	
	public void setprincipalPaid(String principalPaid){
		this.principalPaid=principalPaid;
	}
	public String getprincipalPaid(){
		return principalPaid;
	}
	
	public void setCustomerName(String cusName){
		this.customerName = cusName;
	}
	public String getCustomerName(){
		return this.customerName;
	}
	
	public void setCustomerAddress(String cusAddress){
		this.customerAddress = cusAddress;
	}
	public String getCustomerAddress(){
		return this.customerAddress;
	}
	
	public void setCustomerMobile1(String mobile1){
		this.mobile1 = mobile1;
	}
	public String getCustomerMobile1(){
		return this.mobile1;
	}
	
	public void setCustomerMobile2(String mobile2){
		this.mobile2 = mobile2;
	}
	public String getCustomerMobile2(){
		return this.mobile2;
	}
}

package com.strobilanthes.forrestii.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.actionbarsherlock.view.MenuItem;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.fragments.SettingsChangeEmailFragment;
import com.strobilanthes.forrestii.act.fragments.SettingsChangeMobileFragment;
import com.strobilanthes.forrestii.act.fragments.SettingsChangePwdFragment;
import com.strobilanthes.forrestii.act.fragments.SettingsNotifyEmailFragment;
import com.strobilanthes.forrestii.act.fragments.SettingsTermsConditionFragment;
import com.strobilanthes.forrestii.database.DBAccess;

public class SettingsFragmentActivity extends BaseActivity<DBAccess> {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content_frame);
		int check = getIntent().getExtras().getInt("fragment");
		onClick(check);
		
		getSupportActionBar().setTitle("Settings");
		
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		
	}
	
	public void onClick(int v) {
		switch (v) {
			case 0:
				 getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SettingsNotifyEmailFragment()).commit();
				 
				break;
			case 1:
				getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SettingsChangeMobileFragment()).commit();
				
				break;
			case 2:
				getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SettingsChangeEmailFragment()).commit();
				
				break;
			case 3:
				getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SettingsChangePwdFragment()).commit();
				
				break;
			case 4:
				getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SettingsTermsConditionFragment()).commit();
				
				break;
			default:
				break;
		}
	}
	
	private void loadFragments(Fragment fragment){
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}

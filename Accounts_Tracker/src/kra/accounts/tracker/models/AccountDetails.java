package kra.accounts.tracker.models;

public class AccountDetails {
	private String accountId;
	private String accountName;
	private String description;

	public void setAccountId(String accountId){
		this.accountId=accountId;
	}
	public String getAccountId(){
		return accountId;
	}
	public void setaccountName(String accountName){
		this.accountName=accountName;
	}
	public String getaccountName(){
		return accountName;
	}
	public void setdescription(String description){
		this.description=description;
	}
	public String getdescription(){
		return description;
	}
}

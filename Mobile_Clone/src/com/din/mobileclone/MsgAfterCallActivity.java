//------------------------------------------------------------------------------
//
//    (c) Copyright 2014.Dinesh Kumar M. All Rights Reserved.
//
//------------------------------------------------------------------------------

package com.din.mobileclone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class MsgAfterCallActivity extends SherlockActivity {

	private BroadcastReceiver send_BroadcastReceiver,delivered_BroadcastReceiver;
	private static final String TAG = "MainActivity";
	private boolean callFromApp=false; // To control the call has been made from the application
	private boolean callFromOffHook=false; // To control the change to idle state is from the app call
	private TelephonyManager manager;
	//private PhoneStateListener listener;
	private long startTime,endTime;
	private String numberToCall = null;
	private  EditText editText;
	private List<Map<String, String>> list = new ArrayList<Map<String, String>>();
	 
	private IntentFilter intentFilter;
	private BroadcastReceiver intentReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			callForwarding(intent.getExtras().getString("phonenum"));
		}
	};
	
	private ListView listview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msg_activity_dial);
        
        setTheme(R.style.Theme_Sherlock);
        
       // ActionBar actionBar = getSupportActionBar();
        //actionBar.set
       // actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_launcher));
       // actionBar.setDisplayShowTitleEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Track Mobile");
        
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        editText = (EditText) findViewById(R.id.editText1);
        listview = (ListView) findViewById(R.id.callHistoryListview);
        
        //editText.setText("9751059416");
        ((Button) findViewById(R.id.btn_callnumber)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//String[] str  = editText.getText().toString().split(",");
				
				//sendSMS("5556", "5554");
				//isCallForwardActivated();
				//callForwarding(""+editText.getText().toString()); //9751059416
				
				callDialPad(""+editText.getText().toString());
			}
		});;
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	
        return true;
    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	if (item.getItemId() == android.R.id.home) {
			finish();
		}
    	/*switch (item.getItemId()) {
    	  case 1:
    	   Toast.makeText(MsgAfterCallActivity.this,
    	     "You have Pressed 'Setting' Menu Button", Toast.LENGTH_LONG)
    	     .show();
    	   return true;
    	  }*/
    	
    	return super.onOptionsItemSelected(item);
	}


	private void callDialPad(String phoneNum){
    	
    	/*listener = new MyPhoneStateListener();
    	 manager = (TelephonyManager)    
   			 this.getSystemService(TELEPHONY_SERVICE);     
   			 manager.listen(listener, 
   			 PhoneStateListener.LISTEN_CALL_STATE ); */
   			 
   		callFromApp =true;
   		//Calls 
    	Intent dial = new Intent();
    	dial.setAction("android.intent.action.DIAL");
    	dial.setData(Uri.parse("tel:"+phoneNum));
    	startActivity(dial); 
    }
    
    private void isCallForwardActivated(){
    	manager = (TelephonyManager)    
    			 this.getSystemService(TELEPHONY_SERVICE);     
    			 manager.listen(new MyPhoneStateListener(), 
    			 PhoneStateListener.LISTEN_CALL_FORWARDING_INDICATOR ); 
    }
    
    class MyPhoneStateListener extends PhoneStateListener{  

		  @Override
		    public void onCallForwardingIndicatorChanged(boolean cfi) {
		      
			  //Log.i(TAG,"onCallForwardingIndicatorChanged  CFI ="+cfi);
			  Toast.makeText(MsgAfterCallActivity.this, "Call Forware Activate = "+cfi, Toast.LENGTH_SHORT).show();
		      // preferences.edit().putBoolean("CALL_FORWARD_ACTIVE", cfi).commit();
		      super.onCallForwardingIndicatorChanged(cfi);
		      
		    }

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			long totalTime =0 ;
			numberToCall = editText.getText().toString();
			 switch (state) {
	         
			 case TelephonyManager.CALL_STATE_RINGING:
				 Log.d(TAG, "Call Ringing..."); 
				 break;
	         case TelephonyManager.CALL_STATE_OFFHOOK: //Call is established
	          if (callFromApp) {
	              callFromApp=false;
	              callFromOffHook=true;
	             
	              startTime = System.currentTimeMillis();
	              
	              Log.d(TAG, "Inital Start Time = "+startTime); 
	              
	             /* try {
	                Thread.sleep(500); // Delay 0,5 seconds to handle better turning on loudspeaker
	              } catch (InterruptedException e) {
	              }*/
	           
	              /*//Activate loudspeaker
	              AudioManager audioManager = (AudioManager)
	                                          getSystemService(Context.AUDIO_SERVICE);
	              audioManager.setMode(AudioManager.MODE_IN_CALL);
	              audioManager.setSpeakerphoneOn(true);*/
	           }
	           break;
	         
	        case TelephonyManager.CALL_STATE_IDLE: //Call is finished
	          if (callFromOffHook) {
	        	    endTime = System.currentTimeMillis();
	        	    
	                callFromOffHook=false;
	               // AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	               // audioManager.setMode(AudioManager.MODE_NORMAL); //Deactivate loudspeaker
	                
	                /*manager.listen(listener, // Remove listener
	                      PhoneStateListener.LISTEN_NONE);*/
	                Log.d(TAG, "Start Time = "+startTime); 
	                totalTime = endTime - startTime;
	                Log.d(TAG, "End Time = "+endTime); 
	                Log.d(TAG, "Final Time = "+(int) (totalTime / 1000) % 60+" secs"); 
	                
	                totalTime = (int) (totalTime / 1000) % 60;
	                
	                Map<String, String> map = new HashMap<String, String>();
	                map.put("calltonumber","Dialed Number "+numberToCall);
                    map.put("callduration","Call Duration "+totalTime + " secs");
                    list.add(map);
	                
	                SimpleAdapter adapter = new SimpleAdapter(MsgAfterCallActivity.this, list, R.layout.msg_activity_details_subview, new String[] { "calltonumber", "callduration" }, new int[] { R.id.txt_subviewcall_to_num, R.id.txt_subviewcall_duration });
	                
	                listview.setAdapter(adapter);
	             }
	          break;
	         }
			 
			super.onCallStateChanged(state, incomingNumber);
		}
		
		
		@Override
		public void onCellLocationChanged(CellLocation location) {
			super.onCellLocationChanged(location);
		}

		}
    
    private void callForwarding(String phoneNum){
    	String callForwardString = "**21*"+phoneNum+"#";   
		Intent intentCallForward = new Intent(Intent.ACTION_CALL);
		Uri uri2 = Uri.fromParts("tel", callForwardString, "#");
		intentCallForward.setData(uri2);                                
		startActivity(intentCallForward);
    }
    private void sendSMS(String phoneNumber , String textMessage){
    	String SENT = "SMS_SENT";
    	String DELIVERED = "SMS_DELIVERED";
    	PendingIntent sendPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
    	PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
    	//---when the SMS has been sent---
    	send_BroadcastReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(MsgAfterCallActivity.this, "SMS SENT",Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Toast.makeText(MsgAfterCallActivity.this, "Generic Failure",Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Toast.makeText(MsgAfterCallActivity.this, "Error No Service",Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Toast.makeText(MsgAfterCallActivity.this, "Error Null Pdu",Toast.LENGTH_SHORT).show();
					break;
				case  SmsManager.RESULT_ERROR_RADIO_OFF:
					Toast.makeText(MsgAfterCallActivity.this, "Error Radio Off",Toast.LENGTH_SHORT).show();
					break;
				default:
					break;
				}
			}
		};
		
    	registerReceiver(send_BroadcastReceiver, new IntentFilter(SENT));
    	
    	//---when the SMS has been delivered---
    	delivered_BroadcastReceiver = new BroadcastReceiver() {
			
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(MsgAfterCallActivity.this, "SMS DELIVERED",Toast.LENGTH_SHORT).show();
					break;
				case Activity.RESULT_CANCELED:
					Toast.makeText(MsgAfterCallActivity.this, "SMS NOT DELIVERED",Toast.LENGTH_SHORT).show();
					break;
				default:
					break;
				}
			}
		};
    	registerReceiver(delivered_BroadcastReceiver, new IntentFilter(DELIVERED));
    	
    	SmsManager smsManager = SmsManager.getDefault();
    	smsManager.sendTextMessage(phoneNumber, null, textMessage, sendPI, deliveredPI);
    }
   
	@Override
	protected void onPause() {
		if(send_BroadcastReceiver != null){
			unregisterReceiver(send_BroadcastReceiver);
		}
		
		if(delivered_BroadcastReceiver != null){
			unregisterReceiver(delivered_BroadcastReceiver);
		}
		
		unregisterReceiver(intentReceiver);
		super.onPause();
	}

	@Override
	protected void onResume() {
		registerReceiver(intentReceiver, intentFilter);
		super.onResume();
	}

	@Override
	protected void onStop() {
		
		super.onStop();
	}
    
}

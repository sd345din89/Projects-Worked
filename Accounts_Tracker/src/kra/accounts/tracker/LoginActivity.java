package kra.accounts.tracker;


import kra.accounts.tracker.database.KraFinanceDbManager;
import kra.accounts.tracker.models.Util;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class LoginActivity extends Activity{

	private Util util;
	private KraFinanceDbManager dbManager;
	private EditText loginUserNameTxt;
	private EditText loginPasswordTxt;
	private String checkExistingLogin;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.loginscreen);
		
		util = new Util();
		
		dbManager = new KraFinanceDbManager(this);
		
		checkExistingLogin = util.checkLoginStatus(dbManager);
		
		if(checkExistingLogin == null){
		
			loginUserNameTxt = (EditText)findViewById(R.id.login_user_name_txt);
			loginPasswordTxt = (EditText)findViewById(R.id.login_password_txt);
			
			
			(findViewById(R.id.login_btn)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					attemptLoginUser();
				}
			});
			
		}else{
			
			moveToHomeScreen();
			
		}
		
		
	}
	
	private void moveToHomeScreen(){
		Intent moveToHomeScreen = new Intent(this, MainActivity.class);
		moveToHomeScreen.putExtra("LoginUser", ""+checkExistingLogin);
		startActivity(moveToHomeScreen);
		finish();
	}
	
	private void attemptLoginUser(){
		if(loginUserNameTxt.getText().toString().length() == 0 ){
			util.showAlert(this, "Login", "Please Enter Username.");
		}else if(loginPasswordTxt.getText().toString().length() == 0 ){
			util.showAlert(this, "Login", "Please Enter password.");
		}else{
			if(loginPasswordTxt.getText().toString().equalsIgnoreCase("admin12")){
				util.insertLoginUser(dbManager, loginUserNameTxt.getText().toString(), loginPasswordTxt.getText().toString());
				checkExistingLogin = loginUserNameTxt.getText().toString();
				moveToHomeScreen();
			}else{
				util.showAlert(this, "Login", "Please enter Valid Password.");
			}
		}
	}
	
	

}

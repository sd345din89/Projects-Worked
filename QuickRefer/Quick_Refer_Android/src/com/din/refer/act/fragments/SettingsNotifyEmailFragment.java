package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.settings.AbstractSettings;
import com.strobilanthes.forrestii.model.settings.Settings;
import com.strobilanthes.forrestii.model.settings.SettingsDetails;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class SettingsNotifyEmailFragment extends BaseFragment<DBAccess> implements OnCheckedChangeListener{

	@InjectView(R.id.notificaiton_push_toggle1)   private ToggleButton friendReqReceivedToggle;
	@InjectView(R.id.notificaiton_push_toggle2)   private ToggleButton receiveReferralCardToggle;
	@InjectView(R.id.notificaiton_push_toggle3)   private ToggleButton sendReferralCardToggle;
	
	@InjectView(R.id.notificaiton_push_txt1)	  private TextView txt_someone_SendsFrndRequest;
	@InjectView(R.id.notificaiton_push_txt2)	  private TextView txt_someone_refersYou;
	@InjectView(R.id.notificaiton_push_txt3)	  private TextView txt_refersYou_someone;
	@InjectView(R.id.txt_pushNotifyMe)	  			private TextView txt_header;

	
	
	private Typeface mFontLight,mFontDark;
	private SettingsDetails abstractSettings = null;
	
   @Inject Settings settingsModel;
	
   private boolean isFriendReqReceived = false,isReceiveReferralCard = false,isSendReferralCard = false;
   
   @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
   
   @Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		inflater.inflate(R.menu.settings_notification_menu, menu);
		MenuItem menuItem = menu.findItem(R.id.action__text_done);
		menuItem.setVisible(true);
		
		super.onCreateOptionsMenu(menu, inflater);
	}
   
   @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.action__text_done){
			
			JSONObject jobject = new JSONObject();
			try {
				
				jobject.put("friendReqReceived", isFriendReqReceived);
				jobject.put("receiveReferralCard", isReceiveReferralCard);
				jobject.put("sendReferralCard", isSendReferralCard);
				jobject.put("userProfile", new JSONObject().put("userId", getHelper().getUserId()));
				jobject.put("settingId", abstractSettings.getSettingId());
				
				settingsModel.updateNotificationSettings(Constant.SETTINGS, jobject);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}	
			
		}
		
		return super.onOptionsItemSelected(item);
	}
   
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frg_settings_email, null);
		
		super.initilizeContents();
		
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		settingsModel.setContext(getActivity());
		
		loadFonts();
		
		friendReqReceivedToggle.setOnCheckedChangeListener(this);
		receiveReferralCardToggle.setOnCheckedChangeListener(this);
		sendReferralCardToggle.setOnCheckedChangeListener(this);
		
		abstractSettings = getHelper().getDBSettingsDetails();
		txt_someone_SendsFrndRequest.setTypeface(mFontDark);
		txt_someone_refersYou.setTypeface(mFontDark);
		txt_refersYou_someone.setTypeface(mFontDark);
		txt_header.setTypeface(mFontLight);
		if(abstractSettings !=null){
			
			isFriendReqReceived = abstractSettings.isFriendReqReceived();
			isReceiveReferralCard = abstractSettings.isReceiveReferralCard();
			isSendReferralCard = abstractSettings.isSendReferralCard();
			
			friendReqReceivedToggle.setChecked(isFriendReqReceived);
			receiveReferralCardToggle.setChecked(isReceiveReferralCard);
			sendReferralCardToggle.setChecked(isSendReferralCard);
		}
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("UpdateNotificationSettings")) {
			AbstractSettings abstractSettings = settingsModel
					.getSettingsDetails();

			if (abstractSettings != null) {
				if (abstractSettings.getStatus() == 1) {
					
					L.ToastMessage(getActivity(), "Successfully Updated");
					
					SettingsDetails settingsDetails = getHelper().getDBSettingsDetails();
					
					settingsDetails.setFriendReqReceived(isFriendReqReceived);
					settingsDetails.setReceiveReferralCard(isReceiveReferralCard);
					settingsDetails.setSendReferralCard(isSendReferralCard);
					
					getHelper().insertSettingsDetails(settingsDetails);
					
					getActivity().finish();
					
				} else {
					//Util.showAlert(getActivity(), "Message", abstractSettings.getError());

				}
			} else {
				Util.showAlert(getActivity(), "Message", "Request Failed.");
			}
		}
	}
	@Override
	public void onResume() {
		super.onResume();
		settingsModel.addChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		settingsModel.removeChangeListener(this);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		}
	}
	private void loadFonts() {
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		txt_someone_SendsFrndRequest.setTypeface(mFontDark);
		txt_someone_refersYou.setTypeface(mFontDark);
		txt_refersYou_someone.setTypeface(mFontDark);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
			
			case R.id.notificaiton_push_toggle1:
				isFriendReqReceived = 	isChecked;
				break;
			
			case R.id.notificaiton_push_toggle2:
				isReceiveReferralCard = 	isChecked;
				break;
				
			case R.id.notificaiton_push_toggle3:
				isSendReferralCard = 	isChecked;
				break;
				
		}
	}
}
package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.settings.AbstractChange;
import com.strobilanthes.forrestii.model.settings.AbstractRemoveMobileNo;
import com.strobilanthes.forrestii.model.settings.AbstractVerify;
import com.strobilanthes.forrestii.model.settings.Settings;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class SettingsChangeMobileFragment extends BaseFragment<DBAccess> {

   @Inject Settings settingsModel;
   
  // @InjectView(R.id.sett_gen_mobileotp)							private EditText edt_mobileVerifyOtpCode;
   @InjectView(R.id.sett_gen_mobileNo)							private EditText edt_mobileNo;
   @InjectView(R.id.sett_gen_btn_Mobile_go)						private Button btn_MobileGo;
   @InjectView(R.id.sett_mobile_resendOTP)						private Button txt_mobileResendOTP;
   @InjectView(R.id.sett_gen_header_mobileNo)   				private TextView settGenHeaderMobileNo;
   @InjectView(R.id.sett_gen_btn_Mobile_timer)   				private TextView txt_timer_mobile;

   
   @InjectView(R.id.sett_gen_lyt_pending_mobile) 				private LinearLayout lyt_pendingMobile;
   @InjectView(R.id.sett_gen_txt_pending_mobile) 				private TextView txt_pendingMobile;
   @InjectView(R.id.sett_gen_img_pending_mobile) 				private ImageView img_delete;
   
   private Typeface mFontLight,mFontDark;
   private String strPendingMobileNo;
   private AbstractUserProfile abstractUserProfile;
   private int cnt = 30;
   private Handler handler = new Handler();
   private boolean checkState = false;
   
   @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.frg_settings_change_mobile, null);
		
		super.initilizeContents();
		
		return view;
	}
	@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
			
			fontStyles();
			
			settingsModel.setContext(getActivity());
			img_delete.setOnClickListener(this);
			btn_MobileGo.setOnClickListener(this);
			txt_mobileResendOTP.setOnClickListener(this);
			abstractUserProfile = getHelper().getDBUserProfile();
			
			updatePendingMobile();

		}
	
	private void updatePendingMobile() {
		if(abstractUserProfile != null){
			strPendingMobileNo = abstractUserProfile.getPendingMobileNo();
			if(strPendingMobileNo !=null && strPendingMobileNo.trim().length() > 0){
				
				checkState = true;
				lyt_pendingMobile.setVisibility(View.VISIBLE);
				txt_pendingMobile.setText(strPendingMobileNo);
				txt_mobileResendOTP.setVisibility(View.VISIBLE);
				txt_mobileResendOTP.setBackgroundResource(R.drawable.new_pink_button_background);
				txt_mobileResendOTP.setClickable(true);
				txt_mobileResendOTP.setEnabled(true);
				edt_mobileNo.setHint("Enter your received code");
				
				Util.setMaxLength(edt_mobileNo, 6);
				
				txt_timer_mobile.setVisibility(View.GONE);
				
			}else{
				checkState = false;

				lyt_pendingMobile.setVisibility(View.GONE);
				edt_mobileNo.setHint("Enter your new mobile number");
				
				Util.setMaxLength(edt_mobileNo, 10);
				
				txt_mobileResendOTP.setVisibility(View.GONE);
				txt_timer_mobile.setVisibility(View.GONE);
				
			}	
		}
	}
	
	private void fontStyles() {
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		btn_MobileGo.setTypeface(mFontDark);
		edt_mobileNo.setTypeface(mFontDark);
		txt_mobileResendOTP.setTypeface(mFontDark);
		settGenHeaderMobileNo.setTypeface(mFontLight);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		 if (event.getPropertyName().equals("change_MobileNo")) {
			AbstractChange abstractChangeMobileNo = settingsModel.changeMobileNo();
			UpdateChangeMobileNo(abstractChangeMobileNo);
			
		}else if (event.getPropertyName().equals("verify_MobileNo")) {
			AbstractVerify abstractVerifyMobileNo = settingsModel.verifyMobileNo();
			UpdateVerifyMobileNo(abstractVerifyMobileNo);
			
		}else if (event.getPropertyName().equals("remove_MobileNo")) {
			AbstractRemoveMobileNo abstractremoveMobileNo = settingsModel.removeMobileNo();
			//UpdateVerifyMobileNo(abstractVerifyMobileNo);
			RemoveMobileNo(abstractremoveMobileNo);
			
		}
	}
	
	Runnable mobileRunnable = new Runnable() {

		@Override
		public void run() {
			cnt--;
			txt_timer_mobile.setText("Resend Code button will be enabled in "+cnt+" seconds");
			handler.postDelayed(mobileRunnable, 1000);
			if(cnt == 0){
				txt_timer_mobile.setText("");
				txt_timer_mobile.setVisibility(View.GONE);
				txt_mobileResendOTP.setBackgroundResource(R.drawable.new_pink_button_background);
				txt_mobileResendOTP.setClickable(true);
				txt_mobileResendOTP.setEnabled(true);
				cnt = 30;
				return;
			}
		}
	};
	private void RemoveMobileNo(AbstractRemoveMobileNo abstractremoveMobileNo) {
		if(abstractremoveMobileNo  != null){
			if(abstractremoveMobileNo.getStatus() == 1){
				lyt_pendingMobile.setVisibility(View.GONE);
				txt_mobileResendOTP.setVisibility(View.GONE);
				txt_timer_mobile.setVisibility(View.GONE);
				checkState = false;
				edt_mobileNo.setText("");
				edt_mobileNo.setHint("Enter your new mobile number");
				Util.setMaxLength(edt_mobileNo, 10);
				AbstractUserProfile userProfile = getHelper().getDBUserProfile();
				//userProfile.setMobile(strUpdatedMobileNo);
				strPendingMobileNo = "";

				userProfile.setPendingMobileNo("");
				getHelper().insertUserProfile(userProfile);


			}else if(abstractremoveMobileNo.getStatus() == 0){
				Util.showAlert(getActivity(),"Message", abstractremoveMobileNo.getError());
				//edt_mobileVerifyOtpCode.setText("");
		}else{
			Util.showAlert(getActivity(),"Message", "Request Failed.");

		}		
	}}
	private void UpdateChangeMobileNo(AbstractChange abstractChangeMobileNo) {
		if(abstractChangeMobileNo  != null){
			if(abstractChangeMobileNo.getStatus() == 1){
				//L.i("property change mobile" +strUpdatedMobileNo);
				checkState = true;
				//edt_mobileNo.setVisibility(View.GONE);
				cnt = 30;
				handler.post(mobileRunnable);
				txt_timer_mobile.setVisibility(View.VISIBLE);
				txt_mobileResendOTP.setVisibility(View.VISIBLE);
				txt_mobileResendOTP.setBackgroundResource(R.drawable.new_pink_button_background_transparent);
				txt_mobileResendOTP.setClickable(false);
				txt_mobileResendOTP.setEnabled(false);
				txt_timer_mobile.setText("");
				edt_mobileNo.setText("");
				edt_mobileNo.setHint("Enter your received code");
				Util.setMaxLength(edt_mobileNo, 6);
				AbstractUserProfile userProfile = getHelper().getDBUserProfile();
				userProfile.setPendingMobileNo(strPendingMobileNo);
				//strPendingMobileNo = strUpdatedMobileNo;
				getHelper().insertUserProfile(userProfile);

				L.ToastMessage(getActivity(), "Verification code has been sent successfully!");
			}else if(abstractChangeMobileNo.getStatus() == 0){
				Util.showAlert(getActivity(),"Message", abstractChangeMobileNo.getError());
				edt_mobileNo.setText("");
				txt_timer_mobile.setVisibility(View.GONE);
				/*txt_mobileResendOTP.setVisibility(View.VISIBLE);
				txt_mobileResendOTP.setBackgroundResource(R.drawable.new_pink_button_background);
				txt_mobileResendOTP.setClickable(true);
				txt_mobileResendOTP.setEnabled(true);*/
			}		
		}else{
			Util.showAlert(getActivity(),"Message", "Request Failed.");

		}		
	}
	private void UpdateVerifyMobileNo(AbstractVerify abstractVerifyMobileNo) {
		if(abstractVerifyMobileNo  !=null){
			if(abstractVerifyMobileNo.getStatus() == 1){
				
				//Util.showAlert(getActivity(),"Message", abstractVerifyMobileNo.getResponse());
				
				L.ToastMessage(getActivity(), "Successfully Mobile Number Changed");
				
				edt_mobileNo.setText("");
				edt_mobileNo.setVisibility(View.VISIBLE);
				edt_mobileNo.setHint("Enter your new mobile number");
				Util.setMaxLength(edt_mobileNo, 10);
				lyt_pendingMobile.setVisibility(View.GONE);
				txt_mobileResendOTP.setVisibility(View.GONE);

				txt_timer_mobile.setVisibility(View.GONE);
				
				AbstractUserProfile userProfile = getHelper().getDBUserProfile();
				userProfile.setMobile(strPendingMobileNo);
				strPendingMobileNo = "";

				userProfile.setPendingMobileNo("");
				getHelper().insertUserProfile(userProfile);

				getActivity().finish();

			}else{
				Util.showAlert(getActivity(),"Message", abstractVerifyMobileNo.getError());
				lyt_pendingMobile.setVisibility(View.GONE);
				edt_mobileNo.setText("");
				txt_timer_mobile.setVisibility(View.GONE);
				txt_mobileResendOTP.setVisibility(View.VISIBLE);
				txt_mobileResendOTP.setBackgroundResource(R.drawable.new_pink_button_background);
				txt_mobileResendOTP.setClickable(true);
				txt_mobileResendOTP.setEnabled(true);
			}
		}else{
			Util.showAlert(getActivity(),"Message", "Request Failed.");
		}
		
	}

	@Override
	public void onResume() {
		super.onResume();
		
		settingsModel.addChangeListener(this);
		
	}

	@Override
	public void onPause() {
		super.onPause();
		
		settingsModel.removeChangeListener(this);
		handler.removeCallbacks(mobileRunnable);
		Util.hideSoftKeyboard(getActivity(), edt_mobileNo);
		
		updatePendingMobile();
	}
	
	@Override
	public void onClick(View v) {
		
		Util.hideSoftKeyboard(getActivity(), v);

		switch (v.getId()) {
		case R.id.sett_gen_img_pending_mobile:
			Util.hideSoftKeyboard(getActivity(), v);
			JSONObject removeObject = new JSONObject();
			try {
				removeObject.put("userId", getHelper().getUserId());
				settingsModel.removeMobileNo(Constant.REMOVE_MOBILENO,removeObject);
				
			} catch (JSONException e) {
				
				e.printStackTrace();
			}
			
			break;
		case R.id.sett_gen_btn_Mobile_go:
			Util.hideSoftKeyboard(getActivity(), v);
			handler.removeCallbacks(mobileRunnable);
			if(checkState){
				if(edt_mobileNo.getText().toString().length()>0){
					JSONObject jobject = new JSONObject();
					try {
						jobject.put("OTPCode", edt_mobileNo.getText().toString());
						jobject.put("userProfile", new JSONObject().put("userId", getHelper().getUserId()));
					} catch (JSONException e) {
						e.printStackTrace();
					}
					settingsModel.verifyMobileNo(Constant.VERIFY_MOBILENO, jobject);
				} else {
					Util.showAlert(getActivity(), "Message", "Please enter the correct code");
					txt_timer_mobile.setVisibility(View.GONE);
				}
			} else {
				//checkState = true;
				AbstractUserProfile userProfiel = getHelper().getDBUserProfile();
				strPendingMobileNo = edt_mobileNo.getText().toString();
				if(userProfiel!=null){
				if(!strPendingMobileNo.equals(userProfiel.getPendingMobileNo())){
					L.i("go button clicked===>"+strPendingMobileNo);
					if(strPendingMobileNo.length()>0  && Util.isValidPhoneNumber(strPendingMobileNo)){
						JSONObject jobject = new JSONObject();
						try {
							
							jobject.put("mobile", strPendingMobileNo);
							jobject.put("userId", getHelper().getUserId());
							settingsModel.changeMobileNo(Constant.CHANGE_MOBILENO, jobject);
							
						} catch (JSONException e) {
							
							e.printStackTrace();
						}	
					}else{
						Util.showAlert(getActivity(), "Message", "Please enter a valid mobile number");
					}
				}else{
					Util.showAlert(getActivity(), "Message", "Please enter the mobile number");
				}
				}
			}
			break;

			
		case R.id.sett_mobile_resendOTP:
			
			handler.removeCallbacks(mobileRunnable);
			//AbstractUserProfile abstractUserProfile = getHelper().getDBUserProfile();
			if(abstractUserProfile !=null && abstractUserProfile.getPendingMobileNo()!=null){
				JSONObject jobject = new JSONObject();
				try {
					jobject.put("mobile", abstractUserProfile.getPendingMobileNo());
					jobject.put("userId", getHelper().getUserId());
					settingsModel.changeMobileNo(Constant.CHANGE_MOBILENO, jobject);
					
				} catch (JSONException e) {
					
					e.printStackTrace();
				}
			
			}
			break;
		default:
			break;
		}
	}
}
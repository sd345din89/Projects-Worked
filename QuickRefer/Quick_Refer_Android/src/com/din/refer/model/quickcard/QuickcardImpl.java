package com.strobilanthes.forrestii.model.quickcard;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.QuickcardVolleyTasks;

public class QuickcardImpl implements Quickcard, VolleyTasksListener {
	private AbstractQuickcard abstractQuickCard;
	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private Activity context;
	private CustomProgressbar mCustomProgressbar;

	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);

	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);

	}

	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}

	@Override
	public AbstractQuickcard getQuickcardDetails() {
		return abstractQuickCard;
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonobject) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		if(method_name.equals("createquickcard")){
		AbstractQuickcard previousResult = abstractQuickCard;

		AbstractQuickcard aQuickCard = new AbstractQuickcard();
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.serializeNulls();
		Gson gson = gsonBuilder.create();	
		aQuickCard = gson.fromJson(jsonobject.toString(), AbstractQuickcard.class);



		abstractQuickCard = aQuickCard;

		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				previousResult, abstractQuickCard);
		notifyListeners(event);
	}
	}

	@Override
	public void handleError(String method_name, Exception e) {
		
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		AbstractQuickcard previousResult = abstractQuickCard;
		abstractQuickCard = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				previousResult, null);
		notifyListeners(event);
	}

	@Override
	public void createQuickCard(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication
				.getInstance()
				.addToRequestQueue(
						new QuickcardVolleyTasks("createquickcard", this, url,
								postdata)
								.setRetryPolicy(new DefaultRetryPolicy(
										Constant.SOCKET_TIMEOUT,
										DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
										DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void updateQuickCard(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication
				.getInstance()
				.addToRequestQueue(
						new QuickcardVolleyTasks("updatequickcard", this, url,
								postdata)
								.setRetryPolicy(new DefaultRetryPolicy(
										Constant.SOCKET_TIMEOUT,
										DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
										DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	
	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

}

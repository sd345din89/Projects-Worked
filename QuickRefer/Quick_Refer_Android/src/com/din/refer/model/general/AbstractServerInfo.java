package com.strobilanthes.forrestii.model.general;

import java.io.Serializable;

public class AbstractServerInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String baseURL;
	private String cardFolderName;
	private String templateFolderName;
	private String logoFolderName	;
	private String profilePhotoFolderName;
	
	public String getCardFolderName() {
		return cardFolderName;
	}
	public void setCardFolderName(String cardFolderName) {
		this.cardFolderName = cardFolderName;
	}
	public String getTemplateFolderName() {
		return templateFolderName;
	}
	public void setTemplateFolderName(String templateFolderName) {
		this.templateFolderName = templateFolderName;
	}
	public String getLogoFolderName() {
		return logoFolderName;
	}
	public void setLogoFolderName(String logoFolderName) {
		this.logoFolderName = logoFolderName;
	}
	public String getProfilePhotoFolderName() {
		return profilePhotoFolderName;
	}
	public void setProfilePhotoFolderName(String profilePhotoFolderName) {
		this.profilePhotoFolderName = profilePhotoFolderName;
	}
	public String getBaseURL() {
		return baseURL;
	}
	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}
}

package com.strobilanthes.forrestii.activities;


import java.io.ByteArrayOutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.fragments.NotificationFragment;
import com.strobilanthes.forrestii.act.fragments.SlidingMenuFragment;
import com.strobilanthes.forrestii.act.fragments.WallPostFragment;
import com.strobilanthes.forrestii.model.general.AbstractServerInfo;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.slidingmenu.lib.SlidingMenu;
import com.strobilanthes.forrestii.slidingmenu.lib.SlidingMenu.OnClosedListener;
import com.strobilanthes.forrestii.slidingmenu.lib.SlidingMenu.OnOpenListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;
 
public class MainActivity extends SlidingFragmentActivity {

	//private Fragment mContent;
	private AbstractUserProfile abstactUserProfile;
	private boolean profileStartCheck = false;
	private SlidingMenuFragment slidingMenuFragment;
	private Intent Profileintent;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AbstractServerInfo abstractServerInfo = getHelper().getServerInfo();
		Constant.IMAGEURL = abstractServerInfo.getBaseURL();
		Constant.FOLDER_CARD_NAME = Constant.IMAGEURL + abstractServerInfo.getCardFolderName()+"/";
		Constant.FOLDER_LOGO_NAME = Constant.IMAGEURL + abstractServerInfo.getLogoFolderName()+"/";
		Constant.FOLDER_PROFILE_PHOTO_NAME = Constant.IMAGEURL + abstractServerInfo.getProfilePhotoFolderName()+"/";
		Constant.FOLDER_TEMPLATES__NAME = Constant.IMAGEURL + abstractServerInfo.getTemplateFolderName()+"/";
		
		abstactUserProfile = getHelper().getDBUserProfile();
		Profileintent = new Intent(MainActivity.this,UserProfileActivity.class);
		if(abstactUserProfile!=null){
			ForrestiiApplication.getImageLoaderInstance().loadImage(Constant.FOLDER_CARD_NAME+abstactUserProfile.getCardName(), new SimpleImageLoadingListener() {

				@Override
				public void onLoadingComplete(String imageUri, View view,
						Bitmap loadedImage) {
					if(loadedImage != null){
		        		Util.writeToFile(abstactUserProfile.getFirstName(), loadedImage);	
		        	}
					//super.onLoadingComplete(imageUri, view, loadedImage);
				}
			
			});
			
			ForrestiiApplication.getImageLoaderInstance().loadImage(Constant.FOLDER_PROFILE_PHOTO_NAME+abstactUserProfile.getPhotoID(), new SimpleImageLoadingListener() {

				@Override
				public void onLoadingComplete(String imageUri, View view,
						Bitmap loadedImage) {
					
					if(loadedImage != null){
		        		
		        		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		        		loadedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
		        		byte[] byteArray = stream.toByteArray();
		        		Util.createVcard(abstactUserProfile,byteArray);
		        		
		        	}else{
		        		
		        		Drawable d = getResources().getDrawable(R.drawable.default_user);
		        		Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
		        		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		        		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		        		byte[] bitmapdata = stream.toByteArray();
		        		Util.createVcard(abstactUserProfile,bitmapdata);
		        		
		        	}
					//super.onLoadingComplete(imageUri, view, loadedImage);
				}
			
			});
		}
		
		// set the Above View
		setContentView(R.layout.content_frame);
		
		String message = getIntent().getStringExtra("message");
		if(message != null){
			/*JSONObject jsonObject = null;
	   	     int notifyTypeId = 0;
	   	     try {
					jsonObject = new JSONObject(message);
					notifyTypeId = jsonObject.getInt("notifytypeId");
	   	     } catch (JSONException e) {
					e.printStackTrace();
	   	     }
	   	     
	   	     if(notifyTypeId != 0){
	   	    	if(notifyTypeId == 6)
		   	    	getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, WallPostFragment.newInstance()).commit();
		   	     else if(notifyTypeId == 10)
		   	    	getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, WallPostFragment.newInstance()).commit();
		   	     else
		   	    	getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, NotificationFragment.newInstance(message)).commit();
		   	    	 
	   	     }else{
	   	    	getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, WallPostFragment.newInstance()).commit();
	   	     }*/
			getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, NotificationFragment.newInstance(message)).commit();   
		}else{
			getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, WallPostFragment.newInstance()).commit();
		}
		
		slidingMenuFragment = SlidingMenuFragment.newInstance();
		// set the Behind View
		setBehindContentView(R.layout.menu_frame);
		getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, slidingMenuFragment).commit();
		setSlidingActionBarEnabled(true);
		// customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
		sm.setOnClosedListener(new OnClosedListener() {
			
			@Override
			public void onClosed() {
//				if (profileStartCheck) {
//					profileStartCheck = false;
//				}
//				startActivity(Profileintent);
			}
		});
		sm.setOnOpenListener(new OnOpenListener() {
			
			@Override
			public void onOpen() {
				slidingMenuFragment.onReload();
				slidingMenuFragment.showStoryBoard();
			}
		});
		//sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);	
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				toggle();
				return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getSupportMenuInflater().inflate(R.menu.actionbar_menu, menu);
		
		/*mShareActionProvider = (ShareActionProvider) menu.findItem(R.id.share)
				.getActionProvider();
		
		Intent intent = getDefaultShareIntent();
		if (intent != null)
			mShareActionProvider.setShareIntent(intent);*/
		
		return true;
	}
	

	public void switchContent(Fragment fragment) {
		getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
		getSlidingMenu().showContent();
	}
	
	public void showContent() {
		profileStartCheck = true;
		//getSlidingMenu().showContent();
		startActivity(Profileintent);
	///	overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

	}
	
	@Override
	public void onBackPressed() {
		
		super.onBackPressed();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
}

package com.strobilanthes.forrestii.model.otpvalidation;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface OtpValidation extends ForrestiiSubject{
	void requestOTPValidationDetails(String url,JSONObject postdata);
	
	void resendOTP(String url, JSONObject postdata);
	
	void doValidation(String content);
	
	AbstractOtpValidation getOtpValidationDetails();
	
	AbstractResendOTP getResendOtpValidationDetails();
	
}

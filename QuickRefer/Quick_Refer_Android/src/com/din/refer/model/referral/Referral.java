package com.strobilanthes.forrestii.model.referral;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;
import com.strobilanthes.forrestii.model.ratings.AbstractRatings;

public interface Referral extends ForrestiiSubject {
	void createFriendReferral(String url, JSONObject postdata);
	AbstractCreateReferral abstractCreateReferral();

	void pickACard(String url, JSONObject postdata);
	AbstractPickandUnPickCard getPickCardResponse();
	
	void unPickACard(String url, JSONObject postdata);
	AbstractPickandUnPickCard getUnPickCardResponse();
	
	void ratingCard(String url, JSONObject postdata);
	AbstractRatings getRatingResponse();
}

package com.strobilanthes.forrestii.model.friendslist;

import java.io.Serializable;

import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;


public class FriendsList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	
	private AbstractUserProfile friendUserProfile;
	
	private int isFriend;
	
	public AbstractUserProfile getFriendUserProfile() {
		return friendUserProfile;
	}
	public void setFriendUserProfile(AbstractUserProfile friendUserProfile) {
		this.friendUserProfile = friendUserProfile;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIsFriend() {
		return isFriend;
	}
	public void setIsFriend(int isFriend) {
		this.isFriend = isFriend;
	}		
}

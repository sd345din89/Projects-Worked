package com.strobilanthes.forrestii.model.wallpost;

import java.io.Serializable;

public class GetWallPost implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private GetWallPostContents posts;
	private long lastUpdatedOn;
	private int wallId;
	

	public int getWallId() {
		return wallId;
	}

	public void setWallId(int wallId) {
		this.wallId = wallId;
	}

	public long getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(long lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public GetWallPostContents getWallPosts() {
		return posts;
	}

	public void setWallPosts(GetWallPostContents posts) {
		this.posts = posts;
	}
}

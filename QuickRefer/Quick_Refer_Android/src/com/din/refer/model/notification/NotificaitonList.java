package com.strobilanthes.forrestii.model.notification;

import java.io.Serializable;

import com.strobilanthes.forrestii.model.referral.AbstractReferredQuickCard;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

public class NotificaitonList {
	
	private int points;
	private int isFriend;
	private int id;
	private int notifyId;
	private boolean read;
	private NotificationNotifyType notifyType;
	private long updatedOn;
	private AbstractReferredQuickCard referredQuickCard;
	private AbstractUserProfile userAProfile;
	private AbstractUserProfile userBProfile;
	private boolean quickRead;

	public long getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}

	public AbstractUserProfile getUserAProfile() {
		return userAProfile;
	}

	public void setUserAProfile(AbstractUserProfile userAProfile) {
		this.userAProfile = userAProfile;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNotifyId() {
		return notifyId;
	}

	public void setNotifyId(int notifyId) {
		this.notifyId = notifyId;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public int getIsFriend() {
		return isFriend;
	}

	public void setIsFriend(int isFriend) {
		this.isFriend = isFriend;
	}

	public NotificationNotifyType getNotifyType() {
		return notifyType;
	}

	public void setNotifyType(NotificationNotifyType notifyType) {
		this.notifyType = notifyType;
	}

	public AbstractUserProfile getUserBProfile() {
		return userBProfile;
	}

	public void setUserBProfile(AbstractUserProfile userBProfile) {
		this.userBProfile = userBProfile;
	}

	public AbstractReferredQuickCard getReferredQuickCard() {
		return referredQuickCard;
	}

	public void setReferredQuickCard(AbstractReferredQuickCard referredQuickCard) {
		this.referredQuickCard = referredQuickCard;
	}

	public boolean isQuickRead() {
		return quickRead;
	}

	public void setQuickRead(boolean quickRead) {
		this.quickRead = quickRead;
	}

	public class NotificationNotifyType implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private int notifytypeId;
		private String notifyName;

		public int getNotifytypeId() {
			return notifytypeId;
		}

		public void setNotifytypeId(int notifytypeId) {
			this.notifytypeId = notifytypeId;
		}

		public String getNotifyName() {
			return notifyName;
		}

		public void setNotifyName(String notifyName) {
			this.notifyName = notifyName;
		}
	}
}
//------------------------------------------------------------------------------
//
//    (c) Copyright 2014.Dinesh Kumar M. All Rights Reserved.
//
//------------------------------------------------------------------------------
package com.din.mobileclone;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SelectingUserSetupBaseAdapter extends BaseAdapter{
	
	private Activity mContext;
	private LayoutInflater mLayoutInflater;
	private int count=0;
	//private Map<String, ContactList> contactList;
	//private ImageLoader imageLoader;
	
	private String[] keys;

	private Map<String, ContactList> tempMap;
	private ArrayList<File> contactList;
	public SelectingUserSetupBaseAdapter(Activity manageLocationActivity, ArrayList<File> map) {
		
		this.mContext = manageLocationActivity;
		this.contactList = map;
		//this.contactList = map;
		//keys = map.keySet().toArray(new String[map.size()]);
		//tempMap = contactList;
		//imageLoader = new ImageLoader(manageLocationActivity);
		mLayoutInflater=(LayoutInflater)manageLocationActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}

	@Override
	public int getCount() {
		return contactList.size();
	}

	@Override
	public File getItem(int pos) {
		
		return contactList.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return 0;
	}

	/*@Override 
    public void notifyDataSetChanged() { 
        super.notifyDataSetChanged(); 
    } */
	
	@Override
	public View getView(final int position, View convertview, ViewGroup arg2) {
		ViewHolder holder = null;
		View v = convertview ; 
		if(v==null){
			
			v = mLayoutInflater.inflate(R.layout.selectingusersetup_adapterscreen, null);
			holder = new ViewHolder();
			holder.selectinguser_friends_txt=(TextView)v.findViewById(R.id.new_selectinguser_friendsname_txt);
			//holder.selectinguser_friendsemail_txt=(TextView)v.findViewById(R.id.new_selectinguser_friendsemail_txt);
			holder.selectinguser_friends_img=(ImageView)v.findViewById(R.id.selectinguser_friends_img);
			//holder.selectinguser_setup_btn=(Button)v.findViewById(R.id.selectinguser_setup_btn);
			v.setTag(holder);
			
		}else{
			holder = (ViewHolder)v.getTag();
		}
		//ContactList contactList = getItem(position);
		//String[] data = contactList.getPersonName().toString().split("&&");
		holder.selectinguser_friends_txt.setText(""+contactList.get(position));
		
		/*if(data[1] == null)
			holder.selectinguser_friendsemail_txt.setText("");
		else{
			if(data[1].equals("null") || data[1].length() == 0)
				holder.selectinguser_friendsemail_txt.setText("");
			else
				holder.selectinguser_friendsemail_txt.setText(""+data[1]);
		}*/
			
		
		//imageLoader.DisplayImage(contactList.getContactId(), holder.selectinguser_friends_img, mContext ,R.drawable.person_no_photo);
		
		
		return v;
	}
	class ViewHolder{
		TextView selectinguser_friends_txt;
		//TextView selectinguser_friendsemail_txt;
		ImageView selectinguser_friends_img;
		
		//Button selectinguser_setup_btn;
		
	}
	
}

package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.activities.FriendsProfileT2Activity;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.custom.views.CustomWallPostListview;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.friendslist.AbstractFriendsList;
import com.strobilanthes.forrestii.model.friendslist.Friends;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.general.NotificationListener;
import com.strobilanthes.forrestii.model.skillset.UserSkillsSet;
import com.strobilanthes.forrestii.pulltorefresh.widget.RefreshableListView.OnUpdateTask;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;

public class ForrestiiT2ContactsFragment extends BaseFragment<DBAccess> implements NotificationListener {

	private Typeface mFontLight,mFontDark;
	private ContactsAdapter contactsAdapter;
	private ArrayList<FriendsList> friendsListT2;
	//private ViewStub inflate_stub;
	
	@Inject Friends contactsModel;
	
	@InjectView(R.id.contacts_txt_emptyview)		private TextView txt_emptyview;
	@InjectView(R.id.contacts_list)					private CustomWallPostListview contactsList;
	
	public static ForrestiiT2ContactsFragment newInstance() {
		ForrestiiT2ContactsFragment	frag = new ForrestiiT2ContactsFragment();
        return frag;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.initilizeContents();

		View view = inflater.inflate(R.layout.frg_t2contactslist, null);
		/*inflate_stub = (ViewStub) view.findViewById(R.id.inflate_stub);
        inflate_stub.inflate();
    	inflate_stub.setVisibility(View.GONE);*/

		
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		super.onViewCreated(view, savedInstanceState);
	
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		txt_emptyview.setTypeface(mFontDark);

		friendsListT2 = getHelper().getDBT2Friends();
		
		/*if(friendsListT2 != null && friendsListT2.size()>0 ){
			inflate_stub.setVisibility(View.GONE);
		} else {
			inflate_stub.setVisibility(View.VISIBLE);
		}*/
		
		if(friendsListT2 != null){
			contactsAdapter = new ContactsAdapter(getActivity() , friendsListT2);
			contactsAdapter.updateContactAL(friendsListT2);
			contactsList.setAdapter(contactsAdapter);
		
		} else {
			JSONObject jobject = new JSONObject();
	   		try {
	   			jobject.put("addedByUserProfile", new JSONObject().put("userId",getHelper().getUserId()));
	   		} catch (JSONException e) {
	   			e.printStackTrace();
	   		}
	   		contactsModel.requestT2ContactsList(Constant.GET_CONTACTS_T2, jobject);
		}
	
		contactsList.setOnUpdateTask(new OnUpdateTask() {
			
			public void updateBackground() {
				
				refreshT2();
			}
	
			public void updateUI() {
				// aa.notifyDataSetChanged();
			}
	
			public void onUpdateStart() {
	
			}
	
		});
}

	class ContactsAdapter extends BaseAdapter {
		private LayoutInflater mLayoutInflater;
		private ArrayList<FriendsList> mFriendsList;
		private String strPrimarySkill;
		
	
		public ContactsAdapter(Activity activity, ArrayList<FriendsList> mFriendsList) {
			
			mLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.mFriendsList = mFriendsList;
	
		}
		public void updateContactAL(ArrayList<FriendsList> abstractFriendsLists) {
			this.mFriendsList  = abstractFriendsLists;
			 /*if(mFriendsList.size() > 0){
	            	inflate_stub.setVisibility(View.GONE);
	            }
	            else{
	            	inflate_stub.setVisibility(View.VISIBLE);
	            }*/
			notifyDataSetChanged();
		}
		@Override
		public int getCount() {
			return mFriendsList.size();
		}
	
		@Override
		public FriendsList getItem(int position) {
			return mFriendsList.get(position);
		}
	
		@Override
		public long getItemId(int position) {
			return 0;
		}
	
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			final ViewHolder holder;
			if (v == null) {
				v = mLayoutInflater.inflate(R.layout.frg_t2contactlist_item, null);
				holder = new ViewHolder();
				holder.contactsListItemImgUser = (RoundedImageView)v.findViewById( R.id.friend_list_item_img_user );
			
				holder.contactsListItemLytLinear = (LinearLayout)v.findViewById( R.id.friend_list_item_lyt_linear );
				holder.contactsListItemTxtName = (TextView)v.findViewById( R.id.friend_list_item_txt_name );
				holder.contactsListItemTxtSkills = (TextView)v.findViewById( R.id.friend_list_item_txt_skills );
				holder.contactsRatingBar = (RatingBar)v.findViewById( R.id.contacts_ratingBar );
				holder.root_forrestii_contacts =  (RelativeLayout)v.findViewById( R.id.root_forrestii_contacts);
				holder.rl_frontview = (LinearLayout)v.findViewById( R.id.rl_frontview);
			
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			
			//((SwipeListView) parent).recycle(v, position);
			holder.contactsListItemTxtName.setTypeface(mFontLight);
			holder.contactsListItemTxtSkills.setTypeface(mFontLight);
			holder.contactsListItemTxtSkills.setSelected(true);

			holder.contactsListItemTxtName.setSelected(true);
			holder.contactsListItemTxtName.setText(mFriendsList.get(position).getFriendUserProfile().getFirstName());
			holder.contactsRatingBar.setRating(mFriendsList.get(position).getFriendUserProfile().getTotalRating());
			ArrayList<UserSkillsSet>skillset = new ArrayList<UserSkillsSet>();
			skillset = mFriendsList.get(position).getFriendUserProfile().getUserSkillSets();
			holder.contactsListItemTxtSkills.setText("");
			
			if(skillset != null && skillset.size()>0){
				for (UserSkillsSet userSkillsSet:skillset) {
					if(userSkillsSet.isPrimarySkill()){
						strPrimarySkill = userSkillsSet.getSkills().getSkillName();
						holder.contactsListItemTxtSkills.setText(strPrimarySkill);
						break;
					}
				}
			}
			
			ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME+mFriendsList.get(position).getFriendUserProfile().getPhotoID(),holder.contactsListItemImgUser,
					options);
			holder.contactsListItemLytLinear.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					FriendsList friendsList = (FriendsList) mFriendsList.get(position);
					Intent intent = new Intent(getActivity(),FriendsProfileT2Activity.class);
					intent.putExtra("ProfileT2",friendsList.getFriendUserProfile());
					startActivityForResult(intent , 44);
					
				}
			});
			holder.root_forrestii_contacts.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					//if(holder.contactsListItemLytRelative.getVisibility() == View.VISIBLE)
					//	return;
					
					//flipit(holder.rl_frontview, holder.contactsListItemLytRelative);
				}
			});
			
			
			return v;
		}
		class ViewHolder {
			private RoundedImageView contactsListItemImgUser;
			private TextView contactsListItemTxtName,contactsListItemTxtSkills;
			private RelativeLayout root_forrestii_contacts;
			private LinearLayout contactsListItemLytLinear,rl_frontview;
			private RatingBar contactsRatingBar;
			
		}
		private Interpolator accelerator = new AccelerateInterpolator();
	    private Interpolator decelerator = new DecelerateInterpolator();
	    
	    private void flipit(View firstView, View secondView) {
	        final View visibleList;
	        final View invisibleList;
	        if (firstView.getVisibility() == View.GONE) {
	            visibleList = secondView;
	            invisibleList = firstView;
	        } else {
	            invisibleList = secondView;
	            visibleList = firstView;
	        }
	        ObjectAnimator visToInvis = ObjectAnimator.ofFloat(visibleList, "rotationX", 0f, 90f);
	        visToInvis.setDuration(300);
	        visToInvis.setInterpolator(accelerator);
	        final ObjectAnimator invisToVis = ObjectAnimator.ofFloat(invisibleList, "rotationX",-90f, 0f);
	        invisToVis.setDuration(300);
	        invisToVis.setInterpolator(decelerator);
	        visToInvis.addListener(new AnimatorListenerAdapter() {
	            @Override
	            public void onAnimationEnd(Animator anim) {
	                visibleList.setVisibility(View.GONE);
	                invisToVis.start();
	                invisibleList.setVisibility(View.VISIBLE);
	               
	            }
	        });
	        visToInvis.start();
	    }
		
	
}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		 if(requestCode==44 && resultCode==Activity.RESULT_OK){
	           getHelper().insertFriendsList(friendsListT2 , null);
	        }
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if (event.getPropertyName().equals("FriendsT2")) {
			AbstractFriendsList abstractContacts = contactsModel.getT2ContactsList();
			if (abstractContacts != null) {
				updateContacts(abstractContacts);
			} else {
				Util.showAlert(getActivity(), "Message", "Request Failed.");
			}
		}
	}
	private void updateContacts(final AbstractFriendsList abstractT2FriendsList) {
		
		if (abstractT2FriendsList.getStatus() == 1) {
			
			friendsListT2 = abstractT2FriendsList.getFriendsListResponse();
			contactsAdapter = new ContactsAdapter(getActivity() , friendsListT2);
			contactsAdapter.updateContactAL(friendsListT2);
			contactsList.setAdapter(contactsAdapter);
			contactsAdapter.notifyDataSetChanged();
			
			getHelper().insertFriendsList(abstractT2FriendsList.getFriendsListResponse() , null);
			
		} else {
			Util.showAlert(getActivity(), "Message","" + abstractT2FriendsList.getError());
		}
	}
	private void refreshT2(){
		
		JSONObject jobject = new JSONObject();
			try {
				jobject.put("addedByUserProfile", new JSONObject().put("userId",getHelper().getUserId()));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			contactsModel.requestT2ContactsList(Constant.GET_CONTACTS_T2, jobject);
	}
	@Override
	public void onClick(View v) {
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		contactsModel.addChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		contactsModel.removeChangeListener(this);
	}
	
	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		super.onCreateOptionsMenu(menu, inflater);

		inflater.inflate(R.menu.contact_menu, menu);
		MenuItem menuItem = menu.findItem(R.id.search);
		
		SearchView searchView = (SearchView)menuItem.getActionView();
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

         searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
         //searchView.setIconifiedByDefault(false); 
         SearchView.OnQueryTextListener textChangeListener  = new SearchView.OnQueryTextListener(){

				@Override
				public boolean onQueryTextSubmit(String newText) {
					
					
					return false;
				}

				@Override
				public boolean onQueryTextChange(String newText) {
					
					
					return false;
				}
         	
         };
           searchView.setOnQueryTextListener(textChangeListener);
	}*/
		
	public void updateT2Contacts(String newText){
			if (newText.length() > 0) {
                System.out.println("on text chnge text: "+newText);
                if(friendsListT2 != null && friendsListT2.size() > 0){
                	ArrayList<FriendsList> contactsfilterAl = new ArrayList<FriendsList>();
        			for (int i = 0; i < friendsListT2.size(); i++) {
        				if (friendsListT2.get(i).getFriendUserProfile().getFirstName().toLowerCase().contains(newText.toLowerCase())) {
        					contactsfilterAl.add(friendsListT2.get(i));
        				}
        				contactsAdapter.updateContactAL(contactsfilterAl);
        			}
                }
            } else {
            	if(friendsListT2 != null && friendsListT2.size() > 0)
            		contactsAdapter.updateContactAL(friendsListT2);
            }
	}
	@Override
	public void onResumeFragment() {
		/*if(contactsAdapter != null){
			if(contactsAdapter.getCount() == 0){
				inflate_stub.setVisibility(View.VISIBLE);
			} else {
				inflate_stub.setVisibility(View.GONE);
			}
		}*/
	}

}
package com.strobilanthes.forrestii.model.notification;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.NotificationVolleyTasks;

public class NotificationImpl implements Notification,VolleyTasksListener{

	private AbstractNotification abstractNotification;
	private AcceptandDeclineNotification acceptandDeclineNotification;
	private AbstractNotifyWall abstractNotifyWall;
	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private Activity context;
	private CustomProgressbar mCustomProgressbar;
	
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}
	
	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonobject) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		 if(method_name.equals("Accept_FriendRequest")){
			AcceptandDeclineNotification previousResult = acceptandDeclineNotification;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			acceptandDeclineNotification = gson.fromJson(jsonobject.toString(), AcceptandDeclineNotification.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, acceptandDeclineNotification);
			notifyListeners(event);
			
		}else if(method_name.equals("Decline_FriendRequest")){
			AcceptandDeclineNotification previousResult = acceptandDeclineNotification;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			acceptandDeclineNotification = gson.fromJson(jsonobject.toString(), AcceptandDeclineNotification.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, acceptandDeclineNotification);
			notifyListeners(event);
			
		}else if(method_name.equals("Refresh_NotificationList")){
			AbstractNotification previousResult = abstractNotification;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractNotification = gson.fromJson(jsonobject.toString(), AbstractNotification.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractNotification);
			notifyListeners(event);
		}else if(method_name.equals("paginate_notificaiton")){
			AbstractNotification previousResult = abstractNotification;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractNotification = gson.fromJson(jsonobject.toString(), AbstractNotification.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractNotification);
			notifyListeners(event);
		}else if(method_name.equals("update_notification_view")){
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			acceptandDeclineNotification = gson.fromJson(jsonobject.toString(), AcceptandDeclineNotification.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					null, acceptandDeclineNotification);
			notifyListeners(event);
			
		}else if(method_name.equals("notify_wall")){
			
			AbstractNotifyWall previousResult = abstractNotifyWall;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractNotifyWall = gson.fromJson(jsonobject.toString(), AbstractNotifyWall.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractNotifyWall);
			notifyListeners(event);
			
		}
		
		
	}

	@Override
	public void handleError(String method_name, Exception e) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		AbstractNotification previousResult = abstractNotification;
		abstractNotification = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				previousResult, null);
		notifyListeners(event);
	}

	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	@Override
	public void reqestAccept(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show(); 
		ForrestiiApplication.getInstance().addToRequestQueue(
				new NotificationVolleyTasks("Accept_FriendRequest", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AcceptandDeclineNotification getRequestedAcceptedResponse() {
		return acceptandDeclineNotification;
	}

	@Override
	public void requestDeclined(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		ForrestiiApplication.getInstance().addToRequestQueue(
				new NotificationVolleyTasks("Decline_FriendRequest", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AcceptandDeclineNotification getDeclinedRequest() {
		return acceptandDeclineNotification;
	}

	/*@Override
	public void refreshNotificationList(String url, JSONObject postdata,boolean isProgressShow ) {
		if(isProgressShow){
			mCustomProgressbar = new CustomProgressbar(getContext());
			mCustomProgressbar.show();
		}
		ForrestiiApplication.getInstance().addToRequestQueue(
				new NotificationVolleyTasks("Refresh_NotificationList", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractNotification getRefreshNotification() {
		return abstractNotification;
	}*/

	@Override
	public void requestPaginateNotificationList(String url, JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(
				new NotificationVolleyTasks("paginate_notificaiton", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractNotification getPaginateNotification() {
		
		return abstractNotification;
	}

	@Override
	public void updateNotificationViewStatus(String url, JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(new NotificationVolleyTasks("update_notification_view", this, url, postdata)
			.setRetryPolicy(new DefaultRetryPolicy(Constant.SOCKET_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
		
	}

	@Override
	public AcceptandDeclineNotification updateNotificationViewStatus() {
		return acceptandDeclineNotification;
	}

	@Override
	public void requestNotifyWallPost(String url, JSONObject postdata, boolean isProgressShow) {
		ForrestiiApplication.getInstance()
						.addToRequestQueue(new NotificationVolleyTasks("notify_wall", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractNotifyWall getNotifyWallPost() {
		return abstractNotifyWall;
	}

}

package com.strobilanthes.forrestii.model.search;

import java.util.ArrayList;

import com.strobilanthes.forrestii.model.ffFriendsList.GetForrestiiFriendList;


public class AbstractSearchForrestiiFriend {
	private int status;
	private String error;
	private AbstractForrestiiFriendResponse response;
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public AbstractForrestiiFriendResponse getResponse() {
		return response;
	}
	public void setResponse(AbstractForrestiiFriendResponse response) {
		this.response = response;
	}
	public class AbstractForrestiiFriendResponse{
		private int PostId;
		private int ReferralId1;
		private ArrayList<GetForrestiiFriendList>ForrestiiFriendList = new ArrayList<GetForrestiiFriendList>();
		
		public int getPostId() {
			return PostId;
		}
		public void setPostId(int postId) {
			PostId = postId;
		}
		public int getReferralId() {
			return ReferralId1;
		}
		public void setReferralId(int ReferralId1) {
			this.ReferralId1 = ReferralId1;
		}
		public ArrayList<GetForrestiiFriendList> getForrestiiFriendList() {
			return ForrestiiFriendList;
		}
		public void setForrestiiFriendList(ArrayList<GetForrestiiFriendList> forrestiiFriendList) {
			this.ForrestiiFriendList = forrestiiFriendList;
		}
		
		
	}
}

package kra.accounts.tracker.adapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;

import kra.accounts.tracker.AccountDetailsActivity;
import kra.accounts.tracker.CollectionActivity;
import kra.accounts.tracker.R;
import kra.accounts.tracker.database.KraFinanceDbManager;
import kra.accounts.tracker.models.AccountDetails;
import kra.accounts.tracker.models.Constants;
import kra.accounts.tracker.models.CustomerAccountDetails;
import kra.accounts.tracker.models.Util;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AccountDetailAdapterScreen extends BaseAdapter{
	
	private AccountDetailsActivity mcontext;
	
	private LayoutInflater mInflater;
	
	private ArrayList<CustomerAccountDetails> userPendingList;
	
	private Util util;
	
	private KraFinanceDbManager dbManager;

	private boolean isFromsummary;
	
	
	
	public AccountDetailAdapterScreen(AccountDetailsActivity activity, ArrayList<CustomerAccountDetails> pendingList, KraFinanceDbManager dbManager, boolean isFromSummary) {
		
		this.mcontext=activity;
		
		this.userPendingList = pendingList;
		
		mInflater = (LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		util = new Util();
		
		this.dbManager = dbManager;
		
		this.isFromsummary = isFromSummary;
		
	}

	@Override
	public int getCount() {
		return userPendingList.size();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder = null;
		
		if(convertView==null){
			
			holder=new ViewHolder();
			convertView=mInflater.inflate(R.layout.account_details_adapter_screen, null);
			
			holder.accountNameTxt = (TextView)convertView.findViewById(R.id.account_details_account_name_txt);
			holder.principalAmountTxt = (TextView)convertView.findViewById(R.id.account_details_principal_amt_txt);
			holder.principalDateTxt = (TextView)convertView.findViewById(R.id.account_details_principal_date_txt);
			holder.interestDueDateTxt = (TextView)convertView.findViewById(R.id.account_details__interest_due_date_txt);
			
			holder.mainLyt = (LinearLayout)convertView.findViewById(R.id.account_details_main_layout);
			
			holder.pendingCountTxt = (TextView)convertView.findViewById(R.id.account_details_pending_count_textview);
			
			holder.pendingMonthLyt = (LinearLayout)convertView.findViewById(R.id.account_details_pending_month_lyt);
			
			holder.pendingMonthTxt = (TextView)convertView.findViewById(R.id.account_details_pending_month_textview);
			
			
			convertView.setTag(holder);
			
		}else{
			
			holder=(ViewHolder) convertView.getTag();
		}
		
		
		
		final CustomerAccountDetails accountDetails = userPendingList.get(position);
		
		if(accountDetails != null){
			
			AccountDetails accData = util.getAccountDetails(dbManager, accountDetails.getAccountId());
			
			if(accData != null)
				holder.accountNameTxt.setText(""+accData.getaccountName());
			
			holder.principalAmountTxt.setText(""+util.formatAmount(accountDetails.getprincipalAmount()));
			holder.principalDateTxt.setText(""+util.convertLongToDate(accountDetails.getprincipalDate(), "dd/MM/yyyy"));
			holder.interestDueDateTxt.setText(""+util.convertLongToDate(accountDetails.getinsertDueFromDate(), "dd/MM/yyyy"));
			
			
			//int dueDateInMonth = util.countDueDateInMonth(accountDetails.getinsertDueFromDate());
			
			int dueDateInMonth = util.getNumOfMonths(Long.parseLong(accountDetails.getinsertDueFromDate()), Calendar.getInstance().getTimeInMillis());

			
			if(dueDateInMonth == 0){
				holder.pendingMonthLyt.setVisibility(View.INVISIBLE);
				
				
			}else{
				holder.pendingMonthLyt.setVisibility(View.VISIBLE);

				holder.pendingCountTxt.setText(""+dueDateInMonth);
				if(dueDateInMonth > 1){
					holder.pendingMonthTxt.setText("Months");
			    }else{
			    	holder.pendingMonthTxt.setText("Month");
			    }
			}
			
			
			holder.mainLyt.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(mcontext, CollectionActivity.class);
					intent.putExtra("CustomerAccId", Integer.parseInt(accountDetails.getcustomerAccId()));
					intent.putExtra("CustomerId", Integer.parseInt(accountDetails.getcustomerId()));
					intent.putExtra("IsFromSummary", isFromsummary);
					
					mcontext.startActivity(intent);
				}
			});
			
		}
		
		return convertView;
	}
	
	private void showPrincipalPaidAlert(final String cusId, final String accId){
		
		AlertDialog.Builder builder = new Builder(mcontext);
		builder.setTitle("Confirm");
		builder.setMessage("Do you want update Principal Paid?");
		
		builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				util.updateUserPrincipalPaid(dbManager, cusId, accId);
				mcontext.getUserPendingData(false);
				
				dialog.dismiss();
			}
		});
		
		builder.setNegativeButton("Cancel", null);
		
		builder.show();
	}
	
	
	class ViewHolder{
		
		private TextView accountNameTxt;
		private TextView principalAmountTxt;
		private TextView principalDateTxt;
		private TextView interestDueDateTxt;
		
		private LinearLayout mainLyt;
		private TextView pendingCountTxt;
		private LinearLayout pendingMonthLyt;
		private TextView pendingMonthTxt;
		
	}
}

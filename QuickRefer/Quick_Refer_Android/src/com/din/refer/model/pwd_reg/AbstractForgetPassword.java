package com.strobilanthes.forrestii.model.pwd_reg;

public class AbstractForgetPassword{

	private String error;
	private int status;
	private ForgetPasswordResponse response;
	
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public ForgetPasswordResponse getResponse() {
		return response;
	}
	public void setResponse(ForgetPasswordResponse response) {
		this.response = response;
	}
	public class ForgetPasswordResponse{
		private String OTPCode;

		public String getOTPCode() {
			return OTPCode;
		}

		public void setOTPCode(String oTPCode) {
			OTPCode = oTPCode;
		}
	}
}

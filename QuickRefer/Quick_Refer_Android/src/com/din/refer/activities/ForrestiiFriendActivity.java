package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.custom.views.CustomWallPostListview;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.ffFriendsList.GetForrestiiFriendList;
import com.strobilanthes.forrestii.model.friendslist.Friends;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.general.AbstractLocation;
import com.strobilanthes.forrestii.model.search.AbstractSearchForrestiiFriend;
import com.strobilanthes.forrestii.model.search.Search;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;
import com.strobilanthes.forrestii.model.wallpost.GetWallPostContents;
import com.strobilanthes.forrestii.model.wallpost.RefferalPost;
import com.strobilanthes.forrestii.model.wallpost.ReplyPost;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class ForrestiiFriendActivity extends BaseActivity<DBAccess> implements PropertyChangeListener{

	private Typeface mFontLight,mFontDark;
	private ArrayList<String> Al = new ArrayList<String>();
	
	@InjectView(R.id.forrestii_frnd_listview) private CustomWallPostListview forrestiiFrndListview;
	@InjectView(R.id.forrestii_frnd_lin_lyt)  private LinearLayout forrestiiFrndLinLyt;
	@InjectView(R.id.forrestii_frnd_skillset) private AutoCompleteTextView forrestiiFrndSkillset;
	@InjectView(R.id.forrestii_frnd_city)     private AutoCompleteTextView forrestiiFrndCity;
	//@InjectView(R.id.forrestii_frnd_state)    private AutoCompleteTextView forrestiiFrndState;
	//@InjectView(R.id.forrestii_frnd_country)  private AutoCompleteTextView forrestiiFrndCountry;
	@InjectView(R.id.forrestii_frnd_search_btn) private Button btn_search;
	@InjectView(R.id.forrestii_frnd_header_hint) private TextView txt_heder;

	@Inject Search searchModel;
	@Inject	Friends contactsModel;
	
	private CustomSearchAdpater adapter;
	private ArrayList<FriendsList> abstractFriendsLists = new ArrayList<FriendsList>();
	private ArrayList<AbstractLocation>abstractLocation;
	private ArrayList<AbstractSkillSet>skillSet;
	private int mSelectedSkillId = -1;
	private String mSelectedSkillName = null,mLocationName = null;
	private int locationId;
	private ArrayAdapter<AbstractLocation> adapterLocation;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_forrestii_friend);
		searchModel.setContext(this);
		fontStyles();
		forrestiiFrndLinLyt.setVisibility(View.VISIBLE);
		forrestiiFrndListview.setVisibility(View.GONE);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		abstractLocation = getHelper().getDBLocation();
		skillSet = getHelper().getDBSkillset();
			
		forrestiiFrndSkillset.setAdapter(new ArrayAdapter<AbstractSkillSet>(this,  android.R.layout.simple_list_item_1, skillSet));
		//forrestiiFrndSkillset.addTextChangedListener(Util.allowOnlyText(this, forrestiiFrndSkillset));
		forrestiiFrndSkillset.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				AbstractSkillSet abstractSkillSet = (AbstractSkillSet) parent.getItemAtPosition(position);
				mSelectedSkillName =  abstractSkillSet.getSkillName();
				mSelectedSkillId = abstractSkillSet.getSkillId();
				
				L.d("Selected SkillName = "+abstractSkillSet.toString());
			}
		});
		adapterLocation = new ArrayAdapter<AbstractLocation>(this,  android.R.layout.simple_list_item_1, abstractLocation);
		forrestiiFrndCity.setAdapter(adapterLocation);
		//forrestiiFrndCity.addTextChangedListener(Util.allowOnlyText(this, forrestiiFrndCity));

		forrestiiFrndCity.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				AbstractLocation abstractSkillSet = (AbstractLocation) parent.getItemAtPosition(position);
				mLocationName = abstractSkillSet.getLocationName();
				locationId = abstractSkillSet.getId();
			}
		});
		forrestiiFrndListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				GetForrestiiFriendList friendsList =  (GetForrestiiFriendList) parent.getItemAtPosition(position);
				Intent intent = new Intent(ForrestiiFriendActivity.this,ForrestiiFriendProfileActivity.class);
				intent.putExtra("ForrestiiFriendProfile",friendsList);
				startActivity(intent);
			}
		});
		
		/*TextWatcher filterTextWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		};

		forrestiiFrndSkillset.addTextChangedListener(filterTextWatcher);*/
		
		
		btn_search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Util.hideSoftKeyboard(ForrestiiFriendActivity.this, forrestiiFrndSkillset);
				if (mSelectedSkillName != null && mSelectedSkillId != -1) {
					if (mLocationName != null && locationId != -1) {
						if (forrestiiFrndCity.getText().toString().trim().length() > 0 || forrestiiFrndSkillset.getText().toString().trim().length() > 0) {
							if (!mSelectedSkillName.equals(forrestiiFrndSkillset.getText().toString())) {
								forrestiiFrndSkillset.requestFocus();
								forrestiiFrndSkillset.setError("Invalid skillset");
							} else if(!mLocationName.equals(forrestiiFrndCity.getText().toString())){
								forrestiiFrndCity.setError("Invalid location");
							}else{
								JSONObject jobject = new JSONObject();
								try {

									jobject.put("skillMaster1",
											new JSONObject().put("skillId",
													mSelectedSkillId));
									jobject.put("location", new JSONObject()
											.put("ID", locationId));
									jobject.put("userId", getHelper()
											.getDBUserProfile().getUserId());

								} catch (JSONException e) {
									e.printStackTrace();
								}

								searchModel
										.searchForrestiiFrnd(
												Constant.FORRESTII_FRND_SEARCH,
												jobject);
							}
						} else {
							Util.showAlert(ForrestiiFriendActivity.this,
									"Message",
									"Please enter required all fields");
						}
					} else {
						forrestiiFrndCity.setFocusableInTouchMode(true);
						forrestiiFrndCity.requestFocus();
						forrestiiFrndCity.setError("Please enter the location");
					}
				} else {
					forrestiiFrndSkillset.setFocusableInTouchMode(true);
					forrestiiFrndSkillset.requestFocus();
					forrestiiFrndSkillset.setError("Please enter skillset");

				}
			}
		});
	}
	private void fontStyles() {
		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		btn_search.setTypeface(mFontDark);
		txt_heder.setTypeface(mFontLight);

		forrestiiFrndSkillset.setTypeface(mFontDark);
		forrestiiFrndCity.setTypeface(mFontDark);
		//forrestiiFrndState.setTypeface(mFontDark);
		//forrestiiFrndCountry.setTypeface(mFontDark);

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void finish() {
		setResult(RESULT_OK);
		super.finish();
	}

	class CustomSearchAdpater extends BaseAdapter {

		private LayoutInflater mLayoutInflater;
		private ImageLoadingListener animateFirstListener;
		ArrayList<GetForrestiiFriendList> abstractForrestiiFriendsLists;
		String listHeader;
		private static final int TYPE_ITEM = 0;
		private static final int TYPE_SEPARATOR = 1;
		private static final int TYPE_MAX_COUNT = TYPE_SEPARATOR + 1;

		public CustomSearchAdpater(FragmentActivity activity,
				ArrayList<GetForrestiiFriendList> abstractForrestiiFriendList) {

			this.abstractForrestiiFriendsLists = abstractForrestiiFriendList;
			mLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//L.d("Friends List Size ****" + abstractForrestiiFriendList.size());
		}

		@Override
		public int getCount() {
			return abstractForrestiiFriendsLists.size();
		}

		@Override
		public GetForrestiiFriendList getItem(int position) {
			return abstractForrestiiFriendsLists.get(position);
		}

		@Override
		public int getItemViewType(int position) {
			if (position == 0 || position == 20) {
				return TYPE_SEPARATOR;
			} else {
				return TYPE_ITEM;
			}
		}

		@Override
		public int getViewTypeCount() {
			return TYPE_MAX_COUNT;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			View v = convertView;
			ViewHolder holder = null;
			int type = getItemViewType(position);

			if (v == null) {
				v = mLayoutInflater.inflate(R.layout.frg_t2contactlist_item, null);
				holder = new ViewHolder();
				holder.txt_userName = (TextView) v
						.findViewById(R.id.friend_list_item_txt_name);
				holder.txt_jobrole = (TextView) v
						.findViewById(R.id.friend_list_item_txt_skills);
				holder.img_user = (ImageView) v
						.findViewById(R.id.friend_list_item_img_user);
//				holder.btn_message_add = (Button) v.findViewById(R.id.search_btn_add_message);
				holder.rating_bar = (RatingBar) v
						.findViewById(R.id.contacts_ratingBar);
//				holder.txt_headername = (TextView) v				.findViewById(R.id.search_subview_header);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			/*if (position == 0) {
				holder.txt_headername.setText("Friends");
				holder.txt_headername.setVisibility(View.VISIBLE);
			} else if (position == 1) {
				holder.txt_headername.setText("Friends of Friends");
				holder.txt_headername.setVisibility(View.VISIBLE);
			} else {
				holder.txt_headername.setVisibility(View.GONE);
			}*/

//			holder.btn_message_add.setText("Add");
			/*holder.btn_message_add.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							JSONObject postdata = new JSONObject();
							try {
								postdata.put("addedByUserProfile",
										new JSONObject().put("userId",
												getHelper().getUserId()));
								// postdata.put("FriendUserProfile", new
								// JSONObject().put("userId",
								// abstractFriendsLists.get(position).getUserId()));

								contactsModel.sendFriendRequest(
										Constant.SEND_FRIEND_REQUEST, postdata);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					});*/
			// holder.txt_headerName.setTypeface(tf);
//			holder.txt_userName.setTypeface(tf);
//			holder.txt_jobrole.setTypeface(tf);
//			holder.btn_message_add.setTypeface(tf);
			
			holder.txt_userName.setText(abstractForrestiiFriendsLists.get(position).getPocfirstName());
			holder.txt_jobrole.setText(abstractForrestiiFriendsLists.get(position).getSkillMaster1().getSkillName());
			holder.rating_bar.setRating(getRatings(abstractForrestiiFriendsLists.get(position).getTotalRating()));
			
			holder.txt_userName.setTypeface(mFontLight);
			holder.txt_jobrole.setTypeface(mFontLight);
			
			/*ForrestiiApplication.getImageLoaderInstance().displayImage(
					Constant.s[0], holder.img_user, options,
					animateFirstListener);*/

			return v;
		}

		class ViewHolder {
			TextView txt_userName, txt_jobrole /*,txt_headername*/;
			ImageView img_user;
//			Button btn_message_add;
			RatingBar rating_bar;
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		contactsModel.removeChangeListener(this);
		searchModel.removeChangeListener(this);
		
		Util.hideSoftKeyboard(this, forrestiiFrndSkillset);
		Util.stopAlert();
	}

	@Override
	public void onResume() {
		super.onResume();
		contactsModel.addChangeListener(this);
		searchModel.addChangeListener(this);

	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {

		 if (event.getPropertyName().equals("SearchForrestiiFriend")) {
			 
			AbstractSearchForrestiiFriend abstractSearchForrestiiFriend = searchModel.getForrestiiFrnd();
			if (abstractSearchForrestiiFriend != null) {
				if (abstractSearchForrestiiFriend.getStatus() == 1) {
					
					ArrayList<GetForrestiiFriendList>abstractForrestiiFriendList = abstractSearchForrestiiFriend.getResponse().getForrestiiFriendList();
					forrestiiFrndListview.setAdapter(new CustomSearchAdpater(this,
							abstractForrestiiFriendList));
					//img_back.setVisibility(View.VISIBLE);

					forrestiiFrndLinLyt.setVisibility(View.GONE);
					forrestiiFrndListview.setVisibility(View.VISIBLE);
					
					ArrayList<GetWallPost> abstractGetWallPost = Constant.mGetWallPostArrayList;
					if(abstractGetWallPost == null)
						abstractGetWallPost = new ArrayList<GetWallPost>();
					
					GetWallPost getWallPost = new GetWallPost();
					getWallPost.setLastUpdatedOn(Calendar.getInstance().getTimeInMillis());
					
					GetWallPostContents getWallPostContents = new GetWallPostContents();
					getWallPostContents.setMessage("");
					getWallPostContents.setPostId(abstractSearchForrestiiFriend.getResponse().getPostId());
					getWallPostContents.setUpdatedOn(Calendar.getInstance().getTimeInMillis());
					
					ArrayList<RefferalPost> refferalPostList = new ArrayList<RefferalPost>();
					for (GetForrestiiFriendList forrestiiFriendList : abstractForrestiiFriendList) {
						RefferalPost refferalPost = new RefferalPost();
						refferalPost.setForrestiiFriend(forrestiiFriendList);
						refferalPost.setReferralId(abstractSearchForrestiiFriend.getResponse().getReferralId());
						refferalPostList.add(refferalPost);
					}
					
					getWallPostContents.setReferral(refferalPostList);
					getWallPostContents.setReplies(new ArrayList<ReplyPost>());
					
					if(mSelectedSkillName != null && mSelectedSkillId != -1){
						AbstractSkillSet abstractSkillSet = new AbstractSkillSet();
						abstractSkillSet.setSkillId(mSelectedSkillId);
						abstractSkillSet.setSkillName(mSelectedSkillName);
						getWallPostContents.setSkill(abstractSkillSet);//SkillsSet Setter
					}
					
					getWallPostContents.setPostByUserProfile(getHelper().getDBUserProfile()); //UserProfile Setter
					
					getWallPost.setWallPosts(getWallPostContents);//WallPostContens Setter
					
					abstractGetWallPost.add(0,getWallPost);
					Constant.mGetWallPostArrayList = abstractGetWallPost;
					
				} else {
					Util.showAlert(this, "Message", abstractSearchForrestiiFriend.getError());
					//btn_area.setText("");
					//btn_skillSet.setText("");
				}
			} else {
				Util.showAlert(this, "Message", "Request Failed.");
			}
			
		}

	}

	public float getRatings(float ratingvaule){
		
		float a = ratingvaule;
		L.d("getRatingValue++" +a);
		int d = (int) Math.ceil(a);
		L.d("SetRatingValue++" +d);

		return d;
	}
	
}

package com.strobilanthes.forrestii.model.wallpost;

import java.io.Serializable;
import java.util.ArrayList;

import com.strobilanthes.forrestii.model.ffFriendsList.GetForrestiiFriendList;
import com.strobilanthes.forrestii.model.ratings.RatingList;
import com.strobilanthes.forrestii.model.referral.AbstractReferredQuickCard;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

public class RefferalPost implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private AbstractUserProfile referralFromUserProfile;
	private AbstractUserProfile referredUserProfile;
	private AbstractReferredQuickCard referredQuickCard;
	private ArrayList<RatingList> rating;
	private GetForrestiiFriendList forrestiiFriend; 
	private int referralId;
	private long updatedOn;
	private boolean pick;
	
	public AbstractUserProfile getReferralFromUserProfile() {
		return referralFromUserProfile;
	}
	public void setReferralFromUserProfile(AbstractUserProfile referralFromUserProfile) {
		this.referralFromUserProfile = referralFromUserProfile;
	}
	public AbstractUserProfile getReferredUserProfile() {
		return referredUserProfile;
	}
	public void setReferredUserProfile(AbstractUserProfile referredUserProfile) {
		this.referredUserProfile = referredUserProfile;
	}
	public ArrayList<RatingList> getRating() {
		return rating;
	}
	public void setRating(ArrayList<RatingList> rating) {
		this.rating = rating;
	}
	public int getReferralId() {
		return referralId;
	}
	public void setReferralId(int referralId) {
		this.referralId = referralId;
	}
	public long getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}
	public boolean isPick() {
		return pick;
	}
	
	public void setPick(boolean pick) {
		this.pick = pick;
	}
	
	public AbstractReferredQuickCard getReferredQuickCard() {
		return referredQuickCard;
	}
	public void setReferredQuickCard(AbstractReferredQuickCard referredQuickCard) {
		this.referredQuickCard = referredQuickCard;
	}
	public GetForrestiiFriendList getForrestiiFriend() {
		return forrestiiFriend;
	}
	public void setForrestiiFriend(GetForrestiiFriendList forrestiiFriend) {
		this.forrestiiFriend = forrestiiFriend;
	}
}

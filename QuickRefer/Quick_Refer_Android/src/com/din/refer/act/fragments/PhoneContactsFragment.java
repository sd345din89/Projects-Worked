package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map.Entry;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.custom.views.CustomWallPostListview;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.imageloader.ImageLoader;
import com.strobilanthes.forrestii.model.contacts.ContactList;
import com.strobilanthes.forrestii.model.general.NotificationListener;
import com.strobilanthes.forrestii.model.invite.AbstractInviteFriend;
import com.strobilanthes.forrestii.model.invite.AbstractMobileInviteList;
import com.strobilanthes.forrestii.model.invite.InviteRequest;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.pulltorefresh.widget.RefreshableListView.OnUpdateTask;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;
//dint change anything
public class PhoneContactsFragment extends BaseFragment<DBAccess>  implements NotificationListener{

	//@InjectView(R.id.phone_contacts_edt_search)	 	private EditText phoneContactsEdtSearch;
	@InjectView(R.id.contacts_list) 			private CustomWallPostListview phonecontactsListview;
	@InjectView(R.id.contacts_txt_emptyview)		private TextView txt_emptyview;
	@Inject InviteRequest  inviteRequestModel;

	private CustomContactAdpater customContactAdpater;
	private LinkedHashMap<String, ContactList> treeMap;
	private Typeface mFontLight,mFontDark;
	//private ViewStub inflate_stub;

	public static PhoneContactsFragment newInstance() {
		PhoneContactsFragment frag = new PhoneContactsFragment();
        return frag;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
	
		super.onViewCreated(view, savedInstanceState);
		txt_emptyview.setText("No phone contacts found...");
		txt_emptyview.setTypeface(mFontDark);
		loadContactsAdapter();
		
		phonecontactsListview.setOnUpdateTask(new OnUpdateTask() {
			
			public void updateBackground() {
//				Looper.prepare();
				getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						new LoadContactsTask().execute();	
					}
				});
			}
	
			public void updateUI() {
				// aa.notifyDataSetChanged();
			}
	
			public void onUpdateStart() {
	
			}
	
		});
		
	}
	private void loadContactsAdapter() {
		
		treeMap = getHelper().getContactList();
		
		if(treeMap != null){
			//txt_listemptytext.setText("No phone contacts found...");
			//phonecontactsListview.setEmptyView(txt_listemptytext);
			phonecontactsListview.setFastScrollEnabled(true);
			customContactAdpater = new CustomContactAdpater(getActivity(),treeMap);
			customContactAdpater.updateContactAL(treeMap);
			phonecontactsListview.setAdapter(customContactAdpater);	
			
		}else{
			new LoadContactsTask().execute();	
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frg_t2contactslist, null);
		super.initilizeContents();
		
		/*inflate_stub = (ViewStub) view.findViewById(R.id.inflate_stub);
        inflate_stub.inflate();
    	inflate_stub.setVisibility(View.GONE);*/
    	
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		
		return view;
	}
	
	class LoadContactsTask extends AsyncTask<Void, Void, Void> {
		CustomProgressbar progressbar;
		
		@Override
		protected void onPreExecute() {
			phonecontactsListview.setEnabled(false);
			phonecontactsListview.setClickable(false);
			/*progressbar = new CustomProgressbar(getActivity());
			progressbar.setMessage("Loading...");
			progressbar.show();*/
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			treeMap = getAllPhoneContacts();
			getHelper().insertContactList(treeMap);
		
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		
			if(Util.isConnectedNetwork(getActivity()))
				compareDataContactsWithDB();
			else
				Util.showAlert(getActivity(), "", "Please double check your internet connection !!!");
			
			phonecontactsListview.setEnabled(true);
			phonecontactsListview.setClickable(true);
		}

	}
	private void compareDataContactsWithDB(){
			
		if(inviteRequestModel.getBundleContactData() != null){
			JSONObject jobject = new JSONObject();
	
			try {
				jobject.put("userId",getHelper().getUserId());
				jobject.put("MobileList",""+inviteRequestModel.getBundleContactData());
				jobject.put("Mobile", getHelper().getMobileNo());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			inviteRequestModel.requestMobileInviteList(Constant.INVITE_MOBILE, jobject);
		}
	}
	
	
	public LinkedHashMap <String, ContactList> getAllPhoneContacts() {
		LinkedHashMap <String, ContactList> sortedMap = new LinkedHashMap<String, ContactList>();
	    ContactList phoneContactInfo=null;     
	    Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
	    Cursor cursor = getActivity().getContentResolver().query(uri,new String[] {ContactsContract.CommonDataKinds.Phone.NUMBER,
	    		ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,ContactsContract.CommonDataKinds.Phone._ID,
	    		ContactsContract.CommonDataKinds.Phone.IN_VISIBLE_GROUP,ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER,
	    		ContactsContract.CommonDataKinds.Phone.IS_PRIMARY}, 
	    		ContactsContract.CommonDataKinds.Phone.IN_VISIBLE_GROUP + " = 1 AND "
	    		+ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER + " = 1 ",
	/*	    		+ContactsContract.CommonDataKinds.Phone.IS_SUPER_PRIMARY+" = 1 ",*/
	    		null,
	    		ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
	    cursor.moveToFirst();
	    StringBuilder builder = new StringBuilder();
	    while (cursor.isAfterLast() == false) {
	        String contactNumber= cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));  
	        String contactName =  cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
	        int phoneContactID = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
	        
	        if(contactNumber != null){
				contactNumber = contactNumber.toString().trim();
				contactNumber = contactNumber.replace(" ", "");
				builder.append(contactNumber);
				builder.append(",");
				
				phoneContactInfo = new ContactList();
				phoneContactInfo.setContactId(phoneContactID+"");             
				phoneContactInfo.setPersonName(contactName);                   
				phoneContactInfo.setPhoneNum(contactNumber); 
	        
		        if (phoneContactInfo != null)
					sortedMap.put(contactNumber, phoneContactInfo);
	        }
	        phoneContactInfo = null; 
			L.d("count "+sortedMap.size()+" Contact Id = " + phoneContactID +" Name = " + contactName +" PhoneNum = " + contactNumber+" GROUP "+cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.IN_VISIBLE_GROUP)));
	        cursor.moveToNext();
	    }       
	 	inviteRequestModel.setBundleContactData(builder.toString());
	    cursor.close();
	    cursor = null;
	    L.d("contacts count "+sortedMap.size());
	    return sortedMap;
	}
	
	public class CustomContactAdpater extends BaseAdapter {
		private Activity mContext;
		private LayoutInflater mLayoutInflater;
		private int count=0;
		private String[] keys;
		private ImageLoader imageLoader;
		private LinkedHashMap<String, ContactList> tempMap;
		
		public CustomContactAdpater(Activity manageLocationActivity, LinkedHashMap<String, ContactList> treeMap) {
			this.mContext = manageLocationActivity;
			this.tempMap = treeMap;
			this.keys = tempMap.keySet().toArray(new String[tempMap.size()]);
			//tempMap = contactList;
			mLayoutInflater=(LayoutInflater)manageLocationActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			imageLoader = new ImageLoader(mContext);
		}

		public void updateContactAL(LinkedHashMap<String, ContactList> treeMap) {
			this.tempMap  = treeMap;
			this.keys = tempMap.keySet().toArray(new String[tempMap.size()]);
			/*if(tempMap.size() > 0){
	            	inflate_stub.setVisibility(View.GONE);
	          }else{
	            	inflate_stub.setVisibility(View.VISIBLE);
	          }*/
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return tempMap.size();
		}

		@Override
		public ContactList getItem(int pos) {
			
			return tempMap.get(keys[pos]);
		}

		@Override
		public long getItemId(int pos) {
			return pos;
		}

		@Override
		public int getViewTypeCount() {
			return 2;
		}
		
		@Override
		public int getItemViewType(int position) {
			 if (getItem(position).isSection()) {
		            return 1;
		     } else {
		            return 0;
		     }
		}
		
		@Override
		public View getView(final int position, View convertview, ViewGroup arg2) {
			View v = convertview;
			final ContactList contactList = getItem(position);
			
			if (getItemViewType(position) == 0){
				if (v == null) {
					v = mLayoutInflater.inflate(R.layout.frg_phonecontacts_list_item, null);
				}
				TextView txt_userName = (TextView) v.findViewById(R.id.phone_contacts_list_item_txt_name);
				TextView	txt_MobileNo = (TextView) v.findViewById(R.id.phone_contacts_list_item_txt_skills);
				RoundedImageView img_usercontactsImg = (RoundedImageView) v.findViewById(R.id.phone_contacts_list_item_img_user);
				TextView img_invite = (TextView) v.findViewById(R.id.phone_contacts_list_img_add_invite);
				//txt_listHeader = (TextView) v.findViewById(R.id.phoneContacts_subview_header);
				//txt_listHeader.setTypeface(tf);
				txt_userName.setTypeface(mFontLight);
				txt_MobileNo.setTypeface(mFontLight);
				img_invite.setTypeface(mFontDark);
					
				if(contactList.getContactType() == 1){
					img_invite.setText("Add");
				} else if(contactList.getContactType() == 2){
					//img_invite .setText("Friends");
				} else if(contactList.getContactType() == 4){
					//img_invite .setImageResource(R.drawable.ic_action_pinktick);
				} else {
					img_invite .setText("Invite");
				}
				
				img_invite.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(contactList.getContactType() == 1){
							new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT)
							.setMessage("Do you want to connect with this person on Forrestii?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									JSONObject postdata = new JSONObject();
									try {
										postdata.put("addedByUserProfile",
												new JSONObject().put("userId",
														getHelper().getUserId()));
										postdata.put("FriendUserProfile",
												new JSONObject().put("userId",
														contactList.getAbstractUserProfile().getUserId()));
										inviteRequestModel.sendFriendRequest(Constant.SEND_FRIEND_REQUEST, postdata);
									} catch (JSONException e) {
										e.printStackTrace();
									}
								}
							}).setNegativeButton("Cancel",null).show();	
						} else if(contactList.getContactType() == 2) {
							Util.showAlert(getActivity(), "", "You are connected on Forrestii");
						} else {
							new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT)
							.setMessage("Do you want to invite them to Forrestii through SMS?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Util.sendSMS(contactList.getPhoneNum(),getActivity());
	
								}
							}).setNegativeButton("Cancel",null).show();	
						}
					}
				});
				txt_userName.setSelected(true);
	
				txt_userName.setText(""+contactList.getPersonName());
				txt_MobileNo.setText(""+contactList.getPhoneNum());
				if(contactList.getAbstractUserProfile() != null){
					ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + contactList.getAbstractUserProfile().getPhotoID(), img_usercontactsImg,options, animateFirstListener);
				} else {
					imageLoader.DisplayImage(contactList.getContactId(), img_usercontactsImg, mContext);
				}	
			} else {
				if (v == null ) 
					v = mLayoutInflater.inflate(R.layout.simple_list_item, null);
				
				TextView txt_userName = (TextView) v.findViewById(R.id.textItem);
				/*((TextView) v.findViewById(R.id.phone_contacts_list_item_txt_skills)).setVisibility(View.GONE);
				((RoundedImageView) v.findViewById(R.id.phone_contacts_list_item_img_user)).setVisibility(View.GONE);
				((TextView) v.findViewById(R.id.phone_contacts_list_img_add_invite)).setVisibility(View.GONE);
				//txt_listHeader = (TextView) v.findViewById(R.id.phoneContacts_subview_header);
*/				if (keys[position].equals("2")) {
					txt_userName.setText("Phone Contacts");
				} else {
					txt_userName.setText("Forrestii Contacts");
				}
				
			}

			return v;
		}
		
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if(event.getPropertyName().equals("MobileInvite")){
			AbstractMobileInviteList abstractMobileInviteList = inviteRequestModel.getMobileInviteDetails();
			if(abstractMobileInviteList != null){
				if(abstractMobileInviteList.getStatus() == 1) {
					String mobile = getHelper().getDBUserProfile().getMobile();
					
					LinkedHashMap<String, ContactList> treeMap1 = new LinkedHashMap<String, ContactList>(treeMap);
					
					treeMap.clear();
					ContactList contactSection = new ContactList();
					contactSection.setSection(true);
					
					treeMap.put("1",contactSection);
					
					ArrayList<AbstractUserProfile> frinedsListInWebservice = abstractMobileInviteList.getResponse().getLstAddUser();
					if(frinedsListInWebservice != null && frinedsListInWebservice.size() > 0){
						for(AbstractUserProfile abstractUserProfile :frinedsListInWebservice){
							ContactList contactList = new ContactList();
							contactList.setAbstractUserProfile(abstractUserProfile);
							contactList.setContactType(1);
							contactList.setPhoneNum(abstractUserProfile.getMobile());
							contactList.setPersonName(abstractUserProfile.getFirstName());
							treeMap.put(abstractUserProfile.getMobile(), contactList);
						}
						
					}
					
					treeMap.put("2", contactSection);
					
					ArrayList<String> phoneNumList = abstractMobileInviteList.getResponse().getInviteList();
					for (String phoneNo : phoneNumList) {
						if (treeMap1.containsKey(phoneNo)) {
							treeMap.put(phoneNo, treeMap1.get(phoneNo));
						}
					}
					
					ArrayList<AbstractUserProfile> freindsList = abstractMobileInviteList.getResponse().getLstFriendUserList();
					for(AbstractUserProfile abstractUserProfile :freindsList){
						if(abstractUserProfile.getMobile().trim().toString() != null){
							if(treeMap.containsKey(abstractUserProfile.getMobile().trim().toString())){
								treeMap.remove(abstractUserProfile.getMobile().trim().toString());
							}
						}
					}
					
					if(mobile != null)
						if(treeMap.containsKey(mobile))
							treeMap.remove(mobile);
					
					if(treeMap != null){
						//customContactAdpater.notifyDataSetChanged();
						getHelper().insertContactList(treeMap);
						
						phonecontactsListview.setFastScrollEnabled(true);
						
						customContactAdpater = new CustomContactAdpater(getActivity(),treeMap);
						customContactAdpater.updateContactAL(treeMap);
						phonecontactsListview.setAdapter(customContactAdpater);
					}
					
					//if(inviteRequestModel.getMobileInviteDetails() == null)
					//	compareDataContactsWithDB();
				} else {

					phonecontactsListview.setFastScrollEnabled(true);
					
					customContactAdpater = new CustomContactAdpater(getActivity(),treeMap);
					customContactAdpater.updateContactAL(treeMap);
					phonecontactsListview.setAdapter(customContactAdpater);
					
					Util.showAlert(getActivity(),"Message", ""+abstractMobileInviteList.getError());
				}
			}else{
				phonecontactsListview.setFastScrollEnabled(true);
				
				customContactAdpater = new CustomContactAdpater(getActivity(),treeMap);
				customContactAdpater.updateContactAL(treeMap);
				phonecontactsListview.setAdapter(customContactAdpater);
				
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}
		}else if(event.getPropertyName().equals("sendfriendrequest")){
			AbstractInviteFriend abstractFriendRequest = inviteRequestModel.getFriendsRequestData();
			if (abstractFriendRequest != null) {
				if (abstractFriendRequest.getStatus() == 1)
					Util.showAlert(getActivity(), "Message","Invited successfully");
				else
					Util.showAlert(getActivity(), "Message", ""+ abstractFriendRequest.getError());
			} else {
				Util.showAlert(getActivity(), "Message", "Request Failed");
			}
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		inviteRequestModel.addChangeListener(this);
		//contactsModel.addChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		inviteRequestModel.removeChangeListener(this);
		//contactsModel.removeChangeListener(this);
	}
	@Override
	public void onClick(View v) {
		
	}
	
	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		super.onCreateOptionsMenu(menu, inflater);

		inflater.inflate(R.menu.contact_menu, menu);
		MenuItem menuItem = menu.findItem(R.id.search);
		menuItem.setVisible(true);
		 
	}*/
	public void updatePhoneContacts(String newText){

		if (newText.length() > 0) {
            System.out.println("on text chnge text: "+newText);

            LinkedHashMap<String, ContactList> contactsfilterAl = new LinkedHashMap<String, ContactList>();
			Iterator<Entry<String, ContactList>> test = treeMap.entrySet().iterator();
            while (test.hasNext()) {
           	 Entry<String, ContactList> data = test.next();
           	 String key = data.getKey();
           	 ContactList value = data.getValue();
           	 if (!value.isSection()) {
           		if(value.getPersonName().toString().toLowerCase(Locale.getDefault()).contains(newText.toLowerCase(Locale.getDefault())))
           			contactsfilterAl.put(key, value);
             } else {
            	 contactsfilterAl.put(key, value);
             }
                customContactAdpater.updateContactAL(contactsfilterAl);
			}         	 
		} else {
			if(customContactAdpater != null)
				customContactAdpater.updateContactAL(treeMap);
		}
	
	}
	@Override
	public void onResumeFragment() {
		/*if(customContactAdpater != null) {
			if(customContactAdpater.getCount() == 0){
				inflate_stub.setVisibility(View.VISIBLE);
			}else{
				inflate_stub.setVisibility(View.GONE);
			}
		}*/
	}
}
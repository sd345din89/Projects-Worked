package kra.accounts.tracker;

import java.util.ArrayList;

import kra.accounts.tracker.adapter.AccountDetailAdapterScreen;
import kra.accounts.tracker.database.KraFinanceDbManager;
import kra.accounts.tracker.models.Constants;
import kra.accounts.tracker.models.CustomerAccountDetails;
import kra.accounts.tracker.models.Util;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class AccountDetailsActivity extends Activity{
	private Util util;
	
	private KraFinanceDbManager dbManager;
	
	private ListView accDetailListView;
	
	private int customerId;
	
	private int sortBy;

	private ImageView securityIndicatorBtn;

	private TextView securityDescTxt;

	private TextView accHolderName;

	private TextView accHolderAddress;

	private TextView accHolderMobile;

	private LinearLayout cusMobileLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.account_details_screen);
		
		util = new Util();
		
		dbManager = new KraFinanceDbManager(this);
		
		customerId = getIntent().getIntExtra("CustomerId", -1);
		sortBy =  getIntent().getIntExtra("SortOrder", -1);
		
		
		accDetailListView = (ListView)findViewById(R.id.acc_detial_list);
		accDetailListView.setEmptyView(((TextView)findViewById(R.id.account_detail_empty_txt)));
		
		
		/*(findViewById(R.id.acc_detail_back_btn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AccountDetailsActivity.this.finish();
			}
		});*/
		
		
		securityIndicatorBtn = (ImageView)findViewById(R.id.pendind_acc_security_img);
		securityIndicatorBtn.setVisibility(View.INVISIBLE);
		
		securityDescTxt = (TextView)findViewById(R.id.pending_acc_security_desc_txt);
		securityDescTxt.setVisibility(View.INVISIBLE);
		
		accHolderName = (TextView)findViewById(R.id.pending_acc_holder_name);
		accHolderName.setVisibility(View.INVISIBLE);
		
		accHolderAddress = (TextView)findViewById(R.id.pending_acc_holder_address);
		accHolderAddress.setVisibility(View.INVISIBLE);
		
		accHolderMobile = (TextView)findViewById(R.id.pending_acc_holder_phone_number);
		//accHolderMobile.setVisibility(View.INVISIBLE);
		
		cusMobileLayout =(LinearLayout)findViewById(R.id.account_details_phone_lyt);
		cusMobileLayout.setVisibility(View.INVISIBLE);
		
		cusMobileLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				AlertDialog.Builder builder = new Builder(AccountDetailsActivity.this);
				builder.setTitle("Confirm");
				builder.setMessage("Do you want to connect call?");
				
				builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					    util.connectToCustomerMob(AccountDetailsActivity.this, accHolderMobile.getText().toString());
					
					}
				});
				
				builder.setNegativeButton("Cancel", null);
				
				builder.show();
				
			}
		});
		
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		
		getUserPendingData(true);
	}


	public void getUserPendingData(boolean updateUi){
		
		boolean isfromSummary = getIntent().getBooleanExtra("IsFromSummary", false);
		
		if(Constants.DEBUG_LOG)	Log.d("CHECK", ">>>>>"+isfromSummary);
		
		ArrayList<CustomerAccountDetails> pendingList = util.getUserPendingList(dbManager, customerId, isfromSummary);
		
		if(pendingList != null){
			
			if(updateUi){
				CustomerAccountDetails accountDetails = pendingList.get(0);
				
				if(accountDetails.getCustomerName() != null){
					accHolderName.setText(""+accountDetails.getCustomerName());
					accHolderName.setVisibility(View.VISIBLE);
				}
				
				if(accountDetails.getCustomerAddress() != null){
					accHolderAddress.setText(""+accountDetails.getCustomerAddress());
					accHolderAddress.setVisibility(View.VISIBLE);
				}
				
				if(accountDetails.getCustomerMobile1() != null){
					accHolderMobile.setText(""+accountDetails.getCustomerMobile1());
					cusMobileLayout.setVisibility(View.VISIBLE);
				}else if(accountDetails.getCustomerMobile2() != null){
					accHolderMobile.setText(""+accountDetails.getCustomerMobile2());
					cusMobileLayout.setVisibility(View.VISIBLE);
				}
				
				if(util.checkContainsAnyChar(accountDetails.getsecurity())){
					securityIndicatorBtn.setVisibility(View.VISIBLE);
					securityDescTxt.setText(""+accountDetails.getsecurity());
					securityDescTxt.setVisibility(View.VISIBLE);
				}
				
				
			}
			
			accDetailListView.setAdapter(new AccountDetailAdapterScreen(this, pendingList, dbManager, isfromSummary));
			
			
		}else{
			accDetailListView.setAdapter(null);
		}
		
	}
	

}

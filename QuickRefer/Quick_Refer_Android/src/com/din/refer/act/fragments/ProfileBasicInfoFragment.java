package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.fragments.ProfileUpdateDialog.UpdateProfileListner;
import com.strobilanthes.forrestii.activities.UserProfileActivity;
import com.strobilanthes.forrestii.model.general.AbstractCountry;
import com.strobilanthes.forrestii.model.general.AbstractCountry.Cities;
import com.strobilanthes.forrestii.model.general.AbstractCountry.Pincodes;
import com.strobilanthes.forrestii.model.general.AbstractCountry.States;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.util.Util;

public class ProfileBasicInfoFragment extends ScrollTabHolderFragment implements OnScrollListener,PreferenceChangeListener,UpdateProfileListner {
	
	private ListView mListView;
//	private ArrayList<String> mListItems;
	private int mPosition;
	private AbstractUserProfile abstractUserProfile;
	public static final String ARG_POSITION = "position";
	String CountryName = "",StateName = "",CityName = "",PincodeName = "";
	ArrayList<AbstractCountry> country;
	private HashMap<Integer, ArrayList<States>> stateHashmap;
	private HashMap<Integer, ArrayList<Cities>> citiesHashMap;
	private HashMap<Integer, ArrayList<Pincodes>> pincodeHashMap;
	private Typeface mFontLight,mFontDark;
	private ProfileBasicInfoAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("COMM1", "Created");
		mPosition = getArguments().getInt(ARG_POSITION);
		abstractUserProfile = (AbstractUserProfile) getArguments().getSerializable("profile");
		
		country = getHelper().getDBCountryList();
		stateHashmap = new HashMap<Integer, ArrayList<States>>();
		citiesHashMap = new HashMap<Integer, ArrayList<Cities>>();
		pincodeHashMap = new HashMap<Integer, ArrayList<Pincodes>>();
		
		updateProfile();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.common_listview, container,false);
		
		mListView = (ListView) v.findViewById(R.id.profileT2_generallistview);

		View placeHolderView = inflater.inflate(R.layout.view_pagestrip_header_placeholder, mListView, false);
		mListView.addHeaderView(placeHolderView);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		mListView.setOnScrollListener(this);
//		mListView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.list_item, android.R.id.text1, mListItems));
		adapter = new ProfileBasicInfoAdapter(getActivity());
		mListView.setAdapter(adapter);
	}
	@Override
	public void adjustScroll(int scrollHeight) {
		if (scrollHeight == 0 && mListView.getFirstVisiblePosition() >= 1) {
			return;
		}
		mListView.setSelectionFromTop(1, scrollHeight);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
	}

	@Override
	public void onClick(View v) {
		
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

		if (mScrollTabHolder != null)
			mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);
	}
	

	public class ProfileBasicInfoAdapter extends BaseAdapter{
		Context con;
		LayoutInflater inflater;
		ViewHolder holder = null;
		public ProfileBasicInfoAdapter(Context context) {
			this.con = context;
			inflater = LayoutInflater.from(context);
		}
		
		@Override
		public int getCount() {
			return 1;
		}
	
		@Override
		public Object getItem(int position) {
			return null;
		}
	
		@Override
		public long getItemId(int position) {
			return position;
		}
	
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				v = inflater.inflate(R.layout.frg_user_profile_basicinfo, null);
				holder = new ViewHolder();
				holder.firstnameText = (TextView) v.findViewById( R.id.profile_firstName);
				holder.lastnameText = (TextView) v.findViewById( R.id.profile_lastName);
				holder.emailText = (TextView) v.findViewById( R.id.profile_Email);
				holder.mobileText = (TextView) v.findViewById( R.id.profile_Mobile);
				holder.addressText = (TextView) v.findViewById( R.id.profile_Address);
				holder.cityText = (TextView) v.findViewById( R.id.profile_City);
				holder.stateText = (TextView) v.findViewById( R.id.profile_State);
				holder.countryText = (TextView) v.findViewById( R.id.profile_Country);
				holder.pinText = (TextView) v.findViewById( R.id.profile_Pincode);
				holder.aboutText = (TextView) v.findViewById( R.id.profile_About);
				holder.profile_Edit = (TextView) v.findViewById( R.id.profile_Edit);
				
				holder.headerGeneral = (TextView) v.findViewById( R.id.profile_header_general);
				holder.headerAbout = (TextView) v.findViewById( R.id.profile_header_about);
				v.setTag(holder);

			}else {
				holder = (ViewHolder) v.getTag();
			}
			holder.headerGeneral.setTypeface(mFontLight);
			holder.headerAbout.setTypeface(mFontLight);
			holder.firstnameText.setTypeface(mFontLight);
			holder.lastnameText.setTypeface(mFontLight);
			holder.emailText.setTypeface(mFontLight);
			holder.mobileText.setTypeface(mFontLight);
			holder.addressText.setTypeface(mFontLight);
			holder.cityText.setTypeface(mFontLight);
			holder.stateText.setTypeface(mFontLight);
			holder.countryText.setTypeface(mFontLight);
			holder.pinText.setTypeface(mFontLight);
			holder.aboutText.setTypeface(mFontLight);
			holder.profile_Edit.setTypeface(mFontDark);
			
			holder.firstnameText.setText(abstractUserProfile.getFirstName() !=null ? abstractUserProfile.getFirstName() : "Forrestii User");
			holder.lastnameText.setText(abstractUserProfile.getLastName()!= null ? abstractUserProfile.getLastName() : "");
			holder.emailText.setText(abstractUserProfile.getEmail() != null ? abstractUserProfile.getEmail() : "");
			holder.mobileText.setText(abstractUserProfile.getMobile() != null ? abstractUserProfile.getMobile() : "");
			holder.addressText.setText(abstractUserProfile.getAddress()!= null ? abstractUserProfile.getAddress() : "");
			holder.cityText.setText(CityName != null ? CityName : "");
			
			holder.firstnameText.setSelected(true);
			holder.lastnameText.setSelected(true);
			holder.emailText.setSelected(true);
			holder.addressText.setSelected(true);
			holder.stateText.setText(StateName != null ? StateName : "");
			holder.countryText.setText(CountryName != null ? CountryName : "");
			holder.pinText.setText(PincodeName != null ? PincodeName: "");
			holder.aboutText.setText(abstractUserProfile.getAbout() != null ? abstractUserProfile.getAbout() : "");
			
			holder.profile_Edit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
				Util.setTouchDelay(getActivity(), v);
					ProfileUpdateDialog newFragment = ProfileUpdateDialog.newInstance(abstractUserProfile,country,citiesHashMap,stateHashmap,pincodeHashMap,ProfileBasicInfoFragment.this);
				        newFragment.show(getFragmentManager(), "dialog");
				}
			});
			
			return v;
		}
		
		class ViewHolder{
			TextView firstnameText,lastnameText,emailText,mobileText,addressText,cityText,stateText,countryText,pinText,aboutText,profile_Edit,headerAbout,headerGeneral;
/*			ImageView firstnameImage,lastNameImage,addressImage,cityImage,stateImage,countryImage,pincodeImage,aboutImage;
			EditText firstnameEdit,lastNameEdit,addressEdit,cityEdit,stateEdit,countryEdit,pincodeEdit,aboutEdit;
*/		}
		
	}


	@Override
	public void preferenceChange(PreferenceChangeEvent pce) {
		
	}
	
	@Override
	public void profileupdated() {
		Log.d("COMM1", "updated");
		abstractUserProfile = getHelper().getDBUserProfile();
		adapter.notifyDataSetChanged();
		updateProfile();
		((UserProfileActivity) getActivity()).updateProfile();
	}
	
	void updateProfile(){
		
		int countryId = abstractUserProfile.getCountryId();
		int stateId = abstractUserProfile.getStateId();
		int cityId = abstractUserProfile.getCityId();
		int pinId = abstractUserProfile.getPincodeId();
		
		for (int ccnt = 0; ccnt < country.size(); ccnt++) {
			
			ArrayList<States> state = country.get(ccnt).getStates();
			if (countryId == (country.get(ccnt).getCountryId())) {
				CountryName = country.get(ccnt).getCountryName();
			}
			stateHashmap.put(country.get(ccnt).getCountryId(),state);
			
			for (int scnt = 0; scnt < state.size(); scnt++) {
				ArrayList<Cities> city = state.get(scnt).getCities();
				
				if (stateId == (state.get(scnt).getStateId()) && countryId == (country.get(ccnt).getCountryId())) {
					StateName = state.get(scnt).getStateName();
				}
				citiesHashMap.put(state.get(scnt).getStateId(),city);
				
				for (int ctcnt = 0; ctcnt < city.size(); ctcnt++) {
					ArrayList<Pincodes> pincode = city.get(ctcnt).getPincodes();
					
					if (cityId == (city.get(ctcnt).getCityId()) && stateId == (state.get(scnt).getStateId()) && countryId==(country.get(ccnt).getCountryId())) {
						CityName = city.get(ctcnt).getCityName();
					}
					pincodeHashMap.put(city.get(ctcnt).getCityId(),pincode);
					for (int pcnt = 0; pcnt < pincode.size(); pcnt++) {
						if (pinId==(pincode.get(pcnt).getPincodeId()) && cityId==(city.get(ctcnt).getCityId()) && stateId==(state.get(scnt).getStateId()) && countryId==(country.get(ccnt).getCountryId())) {
							PincodeName = pincode.get(pcnt).getPincodeCode()+"";
							Log.d("COMM1", "print"+PincodeName);
						}
					}
				}
			}
		}
		
		Log.d("COMM1", CountryName + countryId);
		Log.d("COMM1", StateName + stateId);
		Log.d("COMM1", CityName + cityId);
		Log.d("COMM1", PincodeName + pinId);
		Log.d("COMM1", "print");
	}
}

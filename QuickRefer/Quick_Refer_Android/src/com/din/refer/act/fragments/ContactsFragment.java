package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.general.NotificationListener;
import com.strobilanthes.forrestii.slidingtab.PagerSlidingTabStrip;
import com.strobilanthes.forrestii.util.Util;

public class ContactsFragment extends BaseFragment<DBAccess> implements  ViewPager.OnPageChangeListener {
	
	private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
	private PagerSlidingTabStrip mPagerSlidingTabStrip;
	private PhoneContactsFragment phoneContactsFragment;
	private ForrestiiT2ContactsFragment forrestiiT2ContactsFragment;
	private Typeface mFontLight,mFontDark;

	//private static ContactsFragment frag = null;
	public static ContactsFragment newInstance() {
		ContactsFragment frag = new ContactsFragment();
        return frag;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.frg_contacts, container,false);
		mPagerSlidingTabStrip = (PagerSlidingTabStrip)view.findViewById(R.id.tabs);
		mViewPager = (ViewPager) view.findViewById(R.id.pager);
		mPagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager());
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				Util.toggleSoftKeyboard(getActivity());
			}
		});
		
		return view;
	}
	
	public class PagerAdapter extends FragmentStatePagerAdapter {

		private final String[] TITLES = { "Forrestii", "Phone"};
		public PagerAdapter(FragmentManager fm) {
			super(fm);
		}


		@Override
		public CharSequence getPageTitle(int position) {
			return TITLES[position];
		}

		@Override
		public int getCount() {
			return TITLES.length;
		}

		@Override
		public Fragment getItem(int position) {

		switch (position) {

		case 0:
			if(forrestiiT2ContactsFragment == null)
				forrestiiT2ContactsFragment = ForrestiiT2ContactsFragment.newInstance();
			return forrestiiT2ContactsFragment;
		case 1:
			if(phoneContactsFragment == null)
				phoneContactsFragment = PhoneContactsFragment.newInstance();
			return phoneContactsFragment;

		}
			return null;
		}


		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			super.destroyItem(container, position, object);
		}

		
	}
	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	
		super.onActivityCreated(savedInstanceState);
	
	}
	
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getActivity().getActionBar().setTitle("Contacts");
		getActivity().getActionBar().setDisplayShowHomeEnabled(true);
		getActivity().getSupportFragmentManager();
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		mViewPager.setOffscreenPageLimit(2);
		
        this.mViewPager.setAdapter(this.mPagerAdapter);
        this.mViewPager.setOnPageChangeListener(this);
        mPagerSlidingTabStrip.setTextSize((int)Util.getDimensPxtoDp(14, getActivity()));
        mPagerSlidingTabStrip.setTypeface(mFontDark, Typeface.NORMAL);

        mPagerSlidingTabStrip.setViewPager(mViewPager);
		mPagerSlidingTabStrip.setOnPageChangeListener(this);

	}
	
	@Override
	public void onResume() {
		super.onResume();
		/*if (searchView != null) {
			searchView.setQuery("", false);
			searchView.clearFocus();
			searchView.invalidate();
		}*/
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
	}

	@Override
	public void onClick(View v) {
		
	}

	@Override
	public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {
		
	}

	@Override
	public void onPageSelected(int position) {
        mViewPager.setCurrentItem(position);
        NotificationListener fragment = (NotificationListener) mPagerAdapter.instantiateItem(mViewPager, position);
        if (fragment != null) {
            fragment.onResumeFragment();
        }
	}

	@Override
	public void onPageScrollStateChanged(int paramInt) {
		
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.contact_menu, menu);
		
		MenuItem menuItem = menu.findItem(R.id.search);
		menuItem.setVisible(true);
		
		final SearchView searchView = (SearchView)menuItem.getActionView();
		
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        // searchView.setIconifiedByDefault(true); 
        /*searchView.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					searchView.setIconified(false);
				} else {
					searchView.setIconified(true);
				}
			}
		});
         */        
        EditText search_text = (EditText) searchView.findViewById(searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null));
        search_text.setFilters(new InputFilter[] { new InputFilter.LengthFilter(41) });
         
        SearchView.OnQueryTextListener textChangeListener  = new SearchView.OnQueryTextListener(){

				@Override
				public boolean onQueryTextSubmit(String newText) {
					return false;
				}

				@Override
				public boolean onQueryTextChange(String newText) {
						if (newText != null) {
							
							if (newText.length() > 0) {
								searchView.setIconified(false);
							} else {
								searchView.setIconified(true);
							}
							
							if (mViewPager.getCurrentItem() == 0) {
								if(forrestiiT2ContactsFragment != null)
									forrestiiT2ContactsFragment.updateT2Contacts(newText);
							} else {
								if(phoneContactsFragment != null)
									phoneContactsFragment.updatePhoneContacts(newText);
							}
						} else {
							searchView.setIconified(true);
						}
					
					return false;
				}     	
         };
         searchView.setOnQueryTextListener(textChangeListener);
         
         searchView.setOnCloseListener(new OnCloseListener() {
			
			@Override
			public boolean onClose() {
				InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); 
				inputManager.hideSoftInputFromWindow(searchView.getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
				return false;
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.search) {
			
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onPause() {
		
		super.onPause();
	}

}

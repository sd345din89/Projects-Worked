package com.strobilanthes.forrestii.model.contacts;

import java.io.Serializable;

public class SectionList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String title;

	public SectionList(String title){
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
}

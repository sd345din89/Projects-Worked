package com.strobilanthes.forrestii.model.referral;


public class AbstractPickandUnPickCard {
	
	private int status;
	private String error;
	private String response;
	

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	

	

}

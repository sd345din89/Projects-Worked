package com.strobilanthes.forrestii.model.viewreviews;

import java.util.ArrayList;

public class AbstractViewReviews {
	private ArrayList<AbstractReviewProfile> response;

	private int status;
	private String error;

	public ArrayList<AbstractReviewProfile> getResponse() {
		return response;
	}

	public void setResponse(ArrayList<AbstractReviewProfile> response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}

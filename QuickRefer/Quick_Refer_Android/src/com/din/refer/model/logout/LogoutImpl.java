package com.strobilanthes.forrestii.model.logout;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.volleytasks.LogoutVolleyTasks;

public class LogoutImpl implements Logout,VolleyTasksListener{

	private AbstractLogout abstractLogout; 
	List<PropertyChangeListener> listener=new ArrayList<PropertyChangeListener>();
	private Activity context;
	
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners( PropertyChangeEvent evt ){
		for ( Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator.hasNext(); ){
			PropertyChangeListener name = (PropertyChangeListener)iterator.next();
			name.propertyChange(evt);
		}
	}
	
	@Override
	public void handleResult(String method_name,JSONObject jsonObject) {
		AbstractLogout previousResult= abstractLogout;
		
		AbstractLogout authentication = new AbstractLogout();
		
		try {
				
			authentication.setStatus(jsonObject.getInt("status"));
			if(authentication.getStatus() != 1)
				authentication.setError(jsonObject.getString("error"));
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		abstractLogout = authentication;
		
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractLogout); 
		notifyListeners(event);
	}

	@Override
	public void handleError(String method_name,Exception e) {
		AbstractLogout previousResult= abstractLogout;
		abstractLogout = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this,method_name, previousResult, null); 
		notifyListeners(event);
	}

	@Override
	public void requestLogoutDetails(String url,JSONObject postdata) {
		//new AuthenticationTasks("logout",this,postdata).execute(url);
		ForrestiiApplication.getInstance().addToRequestQueue(
				new LogoutVolleyTasks("logout", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
																														                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	
	@Override
	public AbstractLogout getAuthDetails() {
		return abstractLogout;
	}

	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

}

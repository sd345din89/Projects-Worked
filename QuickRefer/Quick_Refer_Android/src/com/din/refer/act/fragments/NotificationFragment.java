package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.activities.FriendsProfileT2Activity;
import com.strobilanthes.forrestii.activities.WallPostExpandActivity;
import com.strobilanthes.forrestii.custom.views.CustomWallPostListview;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.general.AbstractDashboard;
import com.strobilanthes.forrestii.model.notification.AbstractNotification;
import com.strobilanthes.forrestii.model.notification.AbstractNotifyWall;
import com.strobilanthes.forrestii.model.notification.AcceptandDeclineNotification;
import com.strobilanthes.forrestii.model.notification.NotificaitonList;
import com.strobilanthes.forrestii.model.notification.Notification;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.pulltorefresh.widget.RefreshableListView.OnUpdateTask;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.PostData;
import com.strobilanthes.forrestii.util.Util;

public class NotificationFragment extends BaseFragment<DBAccess>  {

	private Typeface mFontDark,mFontLight;
	private CustomNotifcationAdpater adapter = null;
	//private ViewStub inflate_stub;
	private int NotifyId;
	private boolean isPullToRefresh = false;
	private AbstractUserProfile  abstractUserProfile ;
	private AbstractUserProfile  abstractT2UserProfile = null;
	private int mAccepandDeclinePos = -1;
	private ArrayList<NotificaitonList> mNotificaitonAllList = new ArrayList<NotificaitonList>();;
	
	@InjectView(R.id.contacts_list) 			private CustomWallPostListview notification_listview;
	@InjectView(R.id.contacts_txt_emptyview)	private TextView txt_emptyview;
	
	@Inject Notification notificationModel;
	
	//private static NotificationFragment frag = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	public static NotificationFragment newInstance(String message) {
		
		NotificationFragment frag = new NotificationFragment();
		if(message != null){
			Bundle bundle = new Bundle();
			bundle.putString("message", message);
			frag.setArguments(bundle);
		}
        return frag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.frg_t2contactslist, null);
		super.initilizeContents();
		
		/*inflate_stub = (ViewStub) view.findViewById(R.id.inflate_stub);
        inflate_stub.inflate();
    	inflate_stub.setVisibility(View.GONE);*/
    	
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		
		txt_emptyview.setText("No Notification found...");
		txt_emptyview.setTypeface(mFontDark);
		
		getActivity().getActionBar().setTitle("Notification");
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		
		
		notificationModel.setContext(getActivity());
		
		JSONObject postObject = new JSONObject();
		try {
			postObject.put("userProfile", new JSONObject().put("userId",getHelper().getUserId()));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		notificationModel.updateNotificationViewStatus(Constant.UPDATE_VIEW_STATUS,postObject);
		
		notification_listview.setOnUpdateTask(new OnUpdateTask() {
	
			public void updateBackground() {
				
				JSONObject object = new JSONObject();
				 try {
					 isPullToRefresh = true;
					 object.put("userProfile",new JSONObject().put("userId", getHelper().getUserId()));
					 object.put("MaximumRow",10);
					 notificationModel.requestPaginateNotificationList(Constant.GET_PAGINATED_NOTIFICATIONLIST, object);
					 
				 } catch (JSONException e) {
					e.printStackTrace();
				 }
			}
	
			public void updateUI() {
				// aa.notifyDataSetChanged();
			}
			
			public void onUpdateStart() {
	
			}
		});
		
		LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		notification_listview.setLoadingView(layoutInflater.inflate(R.layout.loading_view_demo, null));
		
		mNotificaitonAllList.clear();
		mNotificaitonAllList.addAll(Constant.mAllNotificationArrayList);
		
		/*if(mNotificaitonAllList.size() < 10){
			isPullToRefresh = true;
		}*/
		
		adapter = new CustomNotifcationAdpater(getActivity());
		notification_listview.setOnScrollListener(adapter);
		
		notification_listview.setAdapter(adapter);
		
		abstractUserProfile = getHelper().getDBUserProfile();
			
		SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Forrestii", Context.MODE_PRIVATE);
		int badgeCount = sharedPreferences.getInt("badgeCount", 0);
		if(badgeCount > 0){
			JSONObject object = new JSONObject();
			 try {
				 isPullToRefresh = true;
				 object.put("userProfile",new JSONObject().put("userId", getHelper().getUserId()));
				 object.put("MaximumRow",10);
				 notificationModel.requestPaginateNotificationList(Constant.GET_PAGINATED_NOTIFICATIONLIST, object);
				 
			 } catch (JSONException e) {
				e.printStackTrace();
			 }
		}else{
			if(Constant.mAllNotificationArrayList != null){
				if(Constant.mAllNotificationArrayList.size() >= 10){
					adapter.notifyHasMore();
				}else{
					JSONObject object = new JSONObject();
					 try {
						
						object.put("userProfile",new JSONObject().put("userId", getHelper().getUserId()));
						object.put("MaximumRow",10);
						notificationModel.requestPaginateNotificationList(Constant.GET_PAGINATED_NOTIFICATIONLIST, object);
						
					 } catch (JSONException e) {
						e.printStackTrace();
					 } 
				}
			}
		}
	}
	
	class CustomNotifcationAdpater extends BaseAdapter implements OnScrollListener {
		
		private LayoutInflater mLayoutInflater;
		protected boolean canScroll = false;
		protected boolean rowEnabled = true;
		
		public CustomNotifcationAdpater(Activity activity) {	
			mLayoutInflater = LayoutInflater.from(activity);
		}
	
		@Override
		public int getCount() {		
			return mNotificaitonAllList.size();
		}
	
		@Override
		public NotificaitonList getItem(int position) {	
			return mNotificaitonAllList.get(position);
		}
	
		@Override
		public long getItemId(int position) {
			return 0;
		}
		
		@Override
	    public boolean isEnabled(int position) {
	    	return rowEnabled; 
	    }
		
		public void setRowEnabled(boolean rowEabled) {
			this.rowEnabled = rowEabled;
		}
	
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			
			ViewHolder holder = null;
			
			if (convertView == null) {
				
				convertView = mLayoutInflater.inflate(R.layout.frg_notification_list_item, null);
				
				holder = new ViewHolder();
				
				holder.notificationText = (TextView) convertView
						.findViewById(R.id.notification_list_item_txt_need);
				holder.img_user = (ImageView) convertView
						.findViewById(R.id.notification_list_item_img_user);
				holder.img_accept = (ImageView) convertView
						.findViewById(R.id.notification_accept);
				holder.img_decline = (ImageView) convertView
						.findViewById(R.id.notification_decline);
				holder.ll_acept_decline = (LinearLayout) convertView
						.findViewById(R.id.notification_lyt_acceptDecline);
				holder.notificationTime = (TextView) convertView
						.findViewById(R.id.notification_list_item_time);
				
				holder.notificationText.setTypeface(mFontLight);
				holder.notificationTime.setTypeface(mFontDark);
				
				convertView.setTag(holder);
			
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			final int notifyTypeId = getItem(position).getNotifyType().getNotifytypeId();
			String firstName = null;
			if(getItem(position).getUserAProfile() !=null){
				
				firstName = getItem(position).getUserAProfile().getFirstName();
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME+getItem(position).getUserAProfile().getPhotoID(), holder.img_user,
					options, animateFirstListener);
	
			}
			
			holder.ll_acept_decline.setVisibility(View.GONE);
			
			if(notifyTypeId == 1){   //First Time Registration	
				String messageText = "Registration success. 1000 coins to you!";
				
				Spannable txtFriendRequest = new SpannableString(messageText);        
				txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), String.valueOf("Registration success.").length() + 1, String.valueOf("Registration success. 1000 coins").length(),0);
				holder.notificationText.setText(txtFriendRequest);
				
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME+abstractUserProfile.getPhotoID(), holder.img_user,
						options, animateFirstListener);
				
			}else if(notifyTypeId == 2){   //Profile Completion
				String messageText = "You have completed your profile and earned 5000 points.";
				
				Spannable txtFriendRequest = new SpannableString(messageText);        
				txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), String.valueOf("You have completed your profile and earned").length() + 1, messageText.length(),0);
				holder.notificationText.setText(txtFriendRequest);
				
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME+abstractUserProfile.getPhotoID(), holder.img_user,
						options, animateFirstListener);
				
			}else if(notifyTypeId == 3){  //xxx Sent friend request
				if(getItem(position).getIsFriend() == 0 ){
					String messageString=  firstName +" has sent you a friend request.";
					  
					Spannable txtFriendRequest = new SpannableString(messageString);        
					txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), 0, firstName != null ?firstName.length():0 ,0);
					//txtFriendRequest.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);
					holder.notificationText.setText(txtFriendRequest);
					holder.ll_acept_decline.setVisibility(View.VISIBLE);
	
					holder.img_accept.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {

							new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT)
							.setMessage("Do you want to accept this friend request?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									JSONObject jobject = new JSONObject();
									try {
										jobject.put("Id", getItem(position).getId());
										mAccepandDeclinePos = position;
										abstractT2UserProfile = getItem(position).getUserAProfile();
									} catch (JSONException e) {
										e.printStackTrace();
									}
									notificationModel.reqestAccept(Constant.ACCEPT_FRIEND_REQUEST, jobject);
								}
							}).setNegativeButton("Cancel",null).show();	
							
						}
					});
					
					holder.img_decline.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT)
							.setMessage("Do you want to decline this friend request?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
								   
									JSONObject jobject = new JSONObject();
									
									try {
										jobject.put("Id", getItem(position).getId());
										mAccepandDeclinePos = position;
									} catch (JSONException e) {
										e.printStackTrace();
									}
									
									notificationModel.requestDeclined(Constant.DECLINED_FRIEND_REQUEST, jobject);	
								}
							}).setNegativeButton("Cancel",null).show();	
						}
					});
				} else if(getItem(position).getIsFriend() == 1 || getItem(position).getIsFriend() == 2){
					
					
					String messageText = "You accepted ";
					String totalText= messageText + (firstName != null ? firstName : "");
					Spannable txtFriendRequest = new SpannableString(totalText +" as your friend");        
				
					txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), messageText.length(), totalText.length(),0);
					txtFriendRequest.setSpan(new RelativeSizeSpan(1.15f),messageText.length(), totalText.length(), 0);
					/*txtFriendRequest.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 
							messageText.length(), totalText.length(), 0);*/
					holder.notificationText.setText(txtFriendRequest);
				}
			}else if(notifyTypeId == 4) {    //Friend Request accepted
				
				if(getItem(position).getIsFriend() == 1 || getItem(position).getIsFriend() == 2){
					String nameString = (firstName != null ? firstName : "");
					String messageString=  " has accepted your friend request. You have received 100 points.";
					String totalString = nameString + messageString;
					Spannable txtFriendRequest = new SpannableString(totalString); 
					txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), 0, nameString.length(),0);
					txtFriendRequest.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);
					/*txtFriendRequest.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 
		                         0,nameString.length(), 0);*/
					holder.notificationText.setText(txtFriendRequest);
				}
				
			}else if(notifyTypeId == 5){   //When you connected with someone
				
				String nameString = (firstName != null ? firstName : "");
				String messageString=  "You are now connected with "+nameString +".";
				Spannable txtFriendRequest = new SpannableString(messageString);        
				  
				txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), String.valueOf("You are now connected with").length()+1, messageString.length(),0);
				holder.notificationText.setText(txtFriendRequest);
				
			}else if(notifyTypeId == 6){   //When someone post on your wall
				
				String nameString = (firstName != null ? firstName : "");
				String messageString=  " has posted on your wall";
				String totalString = nameString + messageString;
				Spannable txtFriendRequest = new SpannableString(totalString);        
				  
				txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), 0, nameString.length(),0);
				txtFriendRequest.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);
				/*txtFriendRequest.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 
	                         0,nameString.length(), 0);*/
			
				holder.notificationText.setText(txtFriendRequest);
				
			}else if(notifyTypeId == 7){   //When xx has refer yy to your post
				
				String yyName = null;
				if(getItem(position).getUserBProfile() !=null){
					yyName = getItem(position).getUserBProfile().getFirstName();
					yyName = (yyName != null ? yyName : "");
				}else if(getItem(position).getReferredQuickCard() !=null){
					yyName = getItem(position).getReferredQuickCard().getFirstName();
					yyName = (yyName != null ? yyName : "");
				}else{
					yyName = "";
				}
				String nameString = (firstName != null ? firstName : "");
				String messageString=  " has referred "+yyName+" to your post.";
				String totalString = nameString + messageString;
				Spannable txtFriendRequest = new SpannableString(totalString);        
				  
				txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), 0, nameString.length(),0);
				txtFriendRequest.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);
				/*txtFriendRequest.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 
	                         0,nameString.length(), 0);*/
			
				holder.notificationText.setText(txtFriendRequest);
				
			}else if(notifyTypeId == 8){   //When you refer xx to yy
				
				String yyName = null;
				if(getItem(position).getUserBProfile() !=null){
					yyName = getItem(position).getUserBProfile().getFirstName();
					yyName = (yyName != null ? yyName : "");
				}else if(getItem(position).getReferredQuickCard() !=null){
					yyName = getItem(position).getReferredQuickCard().getFirstName();
					yyName = (yyName != null ? yyName : "");
				}else{
					yyName = "";
				}
				
				String nameString = (firstName != null ? firstName : "");
				String messageString=  "You have referred "+yyName;
				String totalString =  messageString+" to "+nameString+" and earned 5000 points.";
				Spannable txtFriendRequest = new SpannableString(totalString);        
				  
				txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), String.valueOf("You have referred").length() + 1, messageString.length(),0);
				//txtFriendRequest.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);
			
				holder.notificationText.setText(txtFriendRequest);
				
			}else if(notifyTypeId == 9) {   //When someone rates/reviews you
					
				String nameString = (firstName != null ? firstName : "");
				String messageString =  " has rated your referral. You have received "+getItem(position).getPoints()+" points.";
				String totalString = nameString + messageString;
				Spannable txtFriendRequest = new SpannableString(totalString);        
				txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), 0, nameString.length(),0);
				txtFriendRequest.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);
				holder.notificationText.setText(txtFriendRequest);	
				
			}else if(notifyTypeId == 10) {   //When someone rates/reviews you
				
				String nameString = (firstName != null ? firstName : "");
				String messageString=  " has replied to your wall";
				String totalString = nameString + messageString;
				Spannable txtFriendRequest = new SpannableString(totalString);        
				  
				txtFriendRequest.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), 0, nameString.length(),0);
				txtFriendRequest.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);
				/*txtFriendRequest.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 
	                         0,nameString.length(), 0);*/
			
				holder.notificationText.setText(txtFriendRequest);	
			}  
			
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					
					switch (notifyTypeId) {
					
					case 3:		
						pullToT2(position);
						break;
					case 4:
						pullToT2(position);
						break;
					case 5:
						pullToT2(position);
						break;
					case 6:
						//pullToT2(position);
						pullToWallExpand(position);
						break;
					case 7:
						pullToT2(position);
						break;
					case 8:
						pullToT2(position);
					case 9:
						pullToT2(position);
						break;
					case 10:
						pullToWallExpand(position);
						break;
					default:
						break;
					}
				}

				private void pullToWallExpand(int position) {
					JSONObject object = new JSONObject();
					 try {
						
						object.put("posts",new JSONObject().put("postId", getItem(position).getId()));
						notificationModel.requestNotifyWallPost(Constant.GET_WALLPOST_NOTIFY, object,false);
						
					 } catch (JSONException e) {
						e.printStackTrace();
					 } 
				}

				private void pullToT2(final int position) {
					Intent intent = new Intent(getActivity(),FriendsProfileT2Activity.class);
					if(notifyTypeId == 3)
						if(getItem(position).getIsFriend() == 0 )
							intent.putExtra("friend_status", 0);
					intent.putExtra("ProfileT2",getItem(position).getUserAProfile());
					startActivity(intent);
				}
			});
			
			holder.notificationTime.setText(Util.updatedOnTime(getItem(position).getUpdatedOn()));
			
			return convertView;
		}
		
		public void addEntriesToBottom(ArrayList<NotificaitonList> entries) {
			for(NotificaitonList notificaitonList : entries){
					Constant.mAllNotificationArrayList.add(notificaitonList);
			}
			
			mNotificaitonAllList.clear();
			mNotificaitonAllList.addAll(Constant.mAllNotificationArrayList);
			
			notifyDataSetChanged();
		}
		
		public void notifyEndOfList() {
			// When there is no more to load use the lock to prevent loading from happening
			lock();
			// More actions when there is no more to load
			//listView.endOfList();
			if (notification_listview != null) {
				notification_listview.endOfList();
			}
		}
	
		private void lock() {
			canScroll = false;
		}
	
		public void notifyHasMore() {
			// Release the lock when there might be more to load
			unlock();
			// More actions when it might have more to load
			if (notification_listview != null) {
				notification_listview.hasMore();
			}
		}
		private void unlock() {
			canScroll = true;
	
		}
		class ViewHolder {
			ImageView img_user,img_accept,img_decline;
			TextView notificationText,notificationTime;
			LinearLayout ll_acept_decline;

		}
		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			
			
			
			if (view instanceof CustomWallPostListview) {
				// In scroll-to-top-to-load mode, when the list view scrolls to the first visible position it reaches the top
				/*if (loadingMode == LoadingMode.SCROLL_TO_TOP && firstVisibleItem == 0 && canScroll) {
					onScrollNext();
				}*/
				// In scroll-to-bottom-to-load mode, when the sum of first visible position and visible count equals the total number
				// of items in the adapter it reaches the bottom
				//L.d("On Scroll ****" + canScroll);
				if (firstVisibleItem + visibleItemCount - 1 == getCount() && canScroll) {
					//L.d("On Scroll ****" + firstVisibleItem  + " " + visibleItemCount + " "+canScroll);
					onScrollNext();
				}
			}
		}
	
		public void onScrollNext() {
		

			lock();
			
			JSONObject object = new JSONObject();
			 try {
				
				object.put("userProfile",new JSONObject().put("userId", getHelper().getUserId()));
				object.put("MaximumRow",10);
				object.put("NotifyId",Constant.mAllNotificationArrayList.get(Constant.mAllNotificationArrayList.size()-1).getNotifyId());
	
				notificationModel.requestPaginateNotificationList(Constant.GET_PAGINATED_NOTIFICATIONLIST, object);
				
			 } catch (JSONException e) {
				e.printStackTrace();
			 }
		}
	
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			
		}
}
	@Override
	public void onResume() {
		super.onResume();
		notificationModel.addChangeListener(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		notificationModel.removeChangeListener(this);
	
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		 if (event.getPropertyName().equals("paginate_notificaiton")) {
			AbstractNotification abstractPaginatedNotification = notificationModel.getPaginateNotification();

			if (abstractPaginatedNotification != null) {
				if (abstractPaginatedNotification.getStatus() == 1) {
					if (abstractPaginatedNotification.getResponse() != null && abstractPaginatedNotification.getResponse().size() > 0) {
						
						if(isPullToRefresh){
							isPullToRefresh = false;
							Constant.mAllNotificationArrayList = new ArrayList<NotificaitonList>();
							mNotificaitonAllList = new ArrayList<NotificaitonList>();
						}
						
						adapter.addEntriesToBottom(abstractPaginatedNotification.getResponse());
						
						if (abstractPaginatedNotification.getResponse().size() < 8)
							adapter.notifyEndOfList();
						else
							adapter.notifyHasMore();
						
					} else {
						adapter.notifyEndOfList();
						/*if(Constant.mAllNotificationArrayList.size() == 0){
					       inflate_stub.setVisibility(View.VISIBLE);
					    }else{
						   inflate_stub.setVisibility(View.GONE);
					    }*/
					}
				} else {
					adapter.notifyEndOfList();
					Util.showAlert(getActivity(), "Message","No more notification found");
				}
			} else {
				Util.showAlert(getActivity(), "Message", "Request Failed.");
			}

		}else if(event.getPropertyName().equals("update_notification_view")){
			AcceptandDeclineNotification abstractAcceptDecline = notificationModel.getRequestedAcceptedResponse();
			if(abstractAcceptDecline != null){
				SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Forrestii", Context.MODE_PRIVATE);
				sharedPreferences.edit().putInt("badgeCount", 0).commit();
				
			    NotificationManager nMgr = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
			    nMgr.cancel(0);
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
		}else if (event.getPropertyName().equals("Accept_FriendRequest")) {
			AcceptandDeclineNotification abstractAcceptDecline = notificationModel.getRequestedAcceptedResponse();
			if(abstractAcceptDecline != null){
				if(abstractAcceptDecline.getStatus() == 1){
					Util.showAlert(getActivity(), "Message", abstractAcceptDecline.getResponse());
					UpdateAcceptStatus(abstractAcceptDecline);
					
				}else{
					
					L.ToastMessage(getActivity(), ""+abstractAcceptDecline.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
		}else if(event.getPropertyName().equals("Decline_FriendRequest")){
			AcceptandDeclineNotification abstractAcceptDecline = notificationModel.getDeclinedRequest();
			
			if(abstractAcceptDecline != null){
				if(abstractAcceptDecline.getStatus() == 1){
					Util.showAlert(getActivity(), "Message", abstractAcceptDecline.getResponse());
					UpdateDeclineSatus(abstractAcceptDecline);
				}else{
					
					L.ToastMessage(getActivity(), ""+abstractAcceptDecline.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
		}else if(event.getPropertyName().equals("notify_wall")){
			AbstractNotifyWall abstractNotifyWall = notificationModel.getNotifyWallPost();
			
			if(abstractNotifyWall != null){
				if(abstractNotifyWall.getStatus() == 1){
					if(abstractNotifyWall.getResponse().size() > 0){
						Intent intent = new Intent(getActivity(), WallPostExpandActivity.class);
						intent.putExtra("WallPostContent", abstractNotifyWall.getResponse().get(0));
						startActivity(intent);
					}else{
						L.ToastMessage(getActivity(), "No wall post found");
					}
				}else{
					L.ToastMessage(getActivity(), ""+abstractNotifyWall.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
		}
	}
	
private void UpdateDeclineSatus(AcceptandDeclineNotification abstractAcceptDecline) {
		
		if(Constant.mAllNotificationArrayList.size() > 0){
			if(mAccepandDeclinePos != -1){
				Constant.mAllNotificationArrayList.remove(mAccepandDeclinePos);
				
				mNotificaitonAllList.clear();
				mNotificaitonAllList.addAll(Constant.mAllNotificationArrayList);
				adapter.notifyDataSetChanged();
			}		
		}
		
	}
	private void UpdateAcceptStatus(AcceptandDeclineNotification abstractAcceptDecline) {
		
		if(Constant.mAllNotificationArrayList.size() > 0){
			if(mAccepandDeclinePos != -1){
				if(mAccepandDeclinePos != -1){
					
					NotificaitonList notificaitonList = Constant.mAllNotificationArrayList.get(mAccepandDeclinePos);
					notificaitonList.setIsFriend(1);
					notificaitonList.setUpdatedOn(System.currentTimeMillis());
					Constant.mAllNotificationArrayList.set(mAccepandDeclinePos, notificaitonList);
					if(abstractT2UserProfile != null){
						ArrayList<FriendsList> friendsArrayList = getHelper().getDBT2Friends();
						
						FriendsList friendsList = new FriendsList();
						friendsList.setIsFriend(1);
						friendsList.setFriendUserProfile(abstractT2UserProfile);
						friendsArrayList.add(friendsList);
						
						getHelper().insertFriendsList(friendsArrayList, null);
					}
					
					AbstractDashboard abstractDashboard = getHelper().getDashBoard();
					int count = Integer.parseInt(abstractDashboard.getContactCount()) + 1;
					abstractDashboard.setContactCount(""+count);
					getHelper().insertDashBoard(abstractDashboard);
					
					mNotificaitonAllList.clear();
					mNotificaitonAllList.addAll(Constant.mAllNotificationArrayList);
					adapter.notifyDataSetChanged();
				}
			}
		}
		
	}
	@Override
	public void onClick(View v) {
		
	}
	
}
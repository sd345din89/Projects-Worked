package com.strobilanthes.forrestii.model.friendslist;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Friends extends ForrestiiSubject {
	
	void requestT2ContactsList(String url, JSONObject postdata);
	
	AbstractFriendsList getT2ContactsList();
	
	void requestT3ContactsList(String url, JSONObject postdata);
	
	AbstractFriendsList getT3ContactsList();

	void doValidation(String content);

	void requestUserDetails(String url, JSONObject postdata);
	AbstractUserList getUserDetails();
	

	void unFriendRequest(String url, JSONObject postdata);
	
	//void acceptFriendRequest(String url, JSONObject postdata);

	//AbstractContacts getContactsDetails();
}

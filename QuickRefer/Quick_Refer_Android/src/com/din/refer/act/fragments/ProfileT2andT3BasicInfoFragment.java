package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.activities.FriendsProfileT3Activity;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.util.Util;


public class ProfileT2andT3BasicInfoFragment extends ScrollTabHolderFragment implements OnScrollListener {
	
	private int mPosition;
	public static final String ARG_POSITION = "position";
	private ListView mListView;
	private AbstractUserProfile abstractUserProfile;
	private Typeface mFontLight,mFontDark;
	
	private String mCategory = null;
	private boolean isPick;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.frg_cmn_list_user_t2_t3_basic_skill, null);

		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		
		abstractUserProfile = (AbstractUserProfile)getArguments().getSerializable("AbstractUserProfile");
		mPosition = getArguments().getInt(ARG_POSITION);
		mCategory = getArguments().getString("category");
		isPick = getArguments().getBoolean("isPick");
		
		mListView = (ListView) view.findViewById(R.id.profileT2_generallistview);
		View placeHolderView = inflater.inflate(R.layout.view_pagestrip_header_placeholder, mListView, false);
		mListView.addHeaderView(placeHolderView);
		
		return view;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

	}
	@Override
	public void adjustScroll(int scrollHeight) {
		
		if (scrollHeight == 0 && mListView.getChildCount()>= 1) {
			return;
		}
		mListView.setSelectionFromTop(1, scrollHeight);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mListView.setOnScrollListener(this);
		mListView.setAdapter(new CustomGeneralAdapter(getActivity(), abstractUserProfile));

	
	}
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		
		if (mScrollTabHolder != null)
			mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
		
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		super.onViewCreated(view, savedInstanceState);
		

	}

	class CustomGeneralAdapter extends BaseAdapter{
		private LayoutInflater mLayoutInflater;
		private AbstractUserProfile abstractUserProfile;
		private Activity activity;
		public CustomGeneralAdapter(Activity activity,AbstractUserProfile abstractUserProfile
				) {
			// TODO Auto-generated constructor stub
			mLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.abstractUserProfile = abstractUserProfile;
			this.activity = activity;
		}

		@Override
		public int getCount() {
			
			return 1;
		}
	
		@Override
		public Object getItem(int position) {
			
			return position;
		}
	
		@Override
		public long getItemId(int position) {
			
			return 0;
		}
	
		@Override
		public View getView(int position, View convertView, ViewGroup paramViewGroup) {
			
			View v = convertView;
			ViewHolder holder = null;
			if (v == null) {
				v = mLayoutInflater.inflate(R.layout.frg_cmn_t2_t2_basic_listitem, null);
				holder = new ViewHolder();
				holder.txt_FirstName = (TextView)v.findViewById( R.id.profileT2_firstName);
				holder.txt_LastName = (TextView)v.findViewById( R.id.profileT2_lastName);
				holder.txt_email = (TextView)v.findViewById( R.id.profileT2_Email);
				holder.txt_mobile = (TextView)v.findViewById( R.id.profileT2_Mobile);
				holder.txt_about = (TextView)v.findViewById( R.id.profileT2_About);
				holder.txt_address = (TextView)v.findViewById( R.id.profileT2_Address); 
				holder.txtHeaderAbout = (TextView)v.findViewById( R.id.profileT2_header_about);
				holder.txtHeaderGeneral = (TextView)v.findViewById( R.id.profileT2_header_general); 
				holder.img_call = (ImageView)v.findViewById( R.id.profileT2_img_Mobile);
				holder.img_email = (ImageView)v.findViewById( R.id.profileT2_img_Email);

				holder.txtHeaderGeneral = (TextView)v.findViewById( R.id.profileT2_header_general); 
				v.setTag(holder);
				
			} else {
				holder = (ViewHolder) v.getTag();
			}
			holder.txtHeaderAbout.setTypeface(mFontLight);
			holder.txtHeaderGeneral.setTypeface(mFontLight);
			holder.txt_FirstName.setTypeface(mFontLight);
			holder.txt_LastName.setTypeface(mFontLight);
			holder.txt_email.setTypeface(mFontLight);
			holder.txt_mobile.setTypeface(mFontLight);
			holder.txt_about.setTypeface(mFontLight);
			holder.txt_address.setTypeface(mFontLight);
			
			holder.txt_FirstName.setText(abstractUserProfile.getFirstName());
			holder.txt_LastName.setText(abstractUserProfile.getLastName());
			holder.txt_FirstName.setSelected(true);
			holder.txt_LastName.setSelected(true);
			
			if(mCategory.equals("t3")){
				if(FriendsProfileT3Activity.isFriend == 1 || isPick){
					holder.txt_email.setText(abstractUserProfile.getEmail());
					holder.txt_mobile.setText(abstractUserProfile.getMobile());

					holder.txt_about.setText(abstractUserProfile.getAbout());
					holder.txt_address.setText(abstractUserProfile.getAddress());
				} else {
					holder.img_call.setImageResource(android.R.color.transparent);
					holder.img_email.setImageResource(android.R.color.transparent);

					holder.txt_email.setText(Util.maskEmail(abstractUserProfile.getEmail()));
					holder.txt_mobile.setText(Util.maskMobileNumber(abstractUserProfile.getMobile()));
					holder.txt_about.setText(abstractUserProfile.getAbout());
					holder.txt_address.setVisibility(View.GONE);
				}
			} else {
				holder.txt_email.setText(abstractUserProfile.getEmail());
				holder.txt_mobile.setText(abstractUserProfile.getMobile());
			}
			holder.txt_email.setSelected(true);
			
			final String mobile = holder.txt_mobile.getText().toString();
			final String email = holder.txt_email.getText().toString();
			
			holder.img_call.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(mobile.length()>0){
						if(Util.isValidPhoneNumber(mobile.trim()) && mobile.length() >= 10){
							
							AlertDialog.Builder alertbuilder = new AlertDialog.Builder(v.getContext(),AlertDialog.THEME_HOLO_LIGHT);
							alertbuilder.setTitle("Message");
							alertbuilder.setMessage("Do you want to make a call");
							
							alertbuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Intent callIntent = new Intent(Intent.ACTION_CALL);
							    	callIntent.setData(Uri.parse("tel:"+mobile));
							    	startActivity(callIntent);
								}
							});
							alertbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
								}
							});
							
							alertbuilder.show();
					    	
						}else{
							Util.showAlert(getActivity(), "", "Please enter a valid mobile number");
						}
						
					}else{
						Util.showAlert(getActivity(), "", "No phone number found");
					}	
						
										
				}
			});
			holder.img_email.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					if(Util.isValidEmail(email)){
						AlertDialog.Builder alertbuilder = new AlertDialog.Builder(v.getContext(),AlertDialog.THEME_HOLO_LIGHT);
						alertbuilder.setTitle("Message");
						alertbuilder.setMessage("Do you want to Send E-mail");
						
						alertbuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Intent emailIntent = new Intent(Intent.ACTION_SEND);
								emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});		  
								emailIntent.putExtra(Intent.EXTRA_TEXT, "message");
								emailIntent.setType("message/rfc822");
								startActivity(Intent.createChooser(emailIntent, "Choose an Email client :"));
							}
						});
						alertbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						
						alertbuilder.show();
					}else{
							Util.showAlert(getActivity(), "", "Email id is invalid");
			
					}		
				}
			});
			return v;
		}
		class ViewHolder {
			private TextView txt_FirstName,txt_LastName,txt_email,txt_mobile,txt_address,txt_about,txtHeaderAbout,txtHeaderGeneral;
			LinearLayout lyt_root;
			private ImageView img_call, img_email;
		}
		
}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
	}
	@Override
	public void onClick(View v) {
		
	} 

}

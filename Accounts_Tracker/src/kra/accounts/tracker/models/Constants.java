package kra.accounts.tracker.models;

import java.io.File;
import android.os.Environment;

public class Constants {
	
	public static boolean DEBUG_LOG = true; 
	
	public static File KRA_FINANCE_DATA_EXCEL_FILE = new File(Environment.getExternalStorageDirectory()+File.separator+"fTracker.xls");
	
	public static String ACCOUNTS_XL_SHEET_NAME = "Accounts";
	
	public static String CUSTOMERS_XL_SHEET_NAME = "Customers";
	
	public static String CUSTOMER_ACCOUNTS_XL_SHEET_NAME = "Customer Accounts";
	
	public static String TRANSACTIONS_XL_SHEET_NAME = "Transactions";
	
	public static String ACCOUNT_DATA_SHEET = "Sheet2";

	
}

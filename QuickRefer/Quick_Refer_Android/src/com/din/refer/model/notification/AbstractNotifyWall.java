package com.strobilanthes.forrestii.model.notification;

import java.util.ArrayList;

import com.strobilanthes.forrestii.model.wallpost.GetWallPost;

public class AbstractNotifyWall {
	
	private int status;
	private String error;
	private ArrayList<GetWallPost> response;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ArrayList<GetWallPost> getResponse() {
		return response;
	}

	public void setResponse(ArrayList<GetWallPost> response) {
		this.response = response;
	}
}

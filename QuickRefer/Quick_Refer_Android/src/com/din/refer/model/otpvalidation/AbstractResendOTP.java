package com.strobilanthes.forrestii.model.otpvalidation;

public class AbstractResendOTP {

	private String error;
	private int status;
	//private OTPResponse response;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	/*public OTPResponse getResponse() {
		return response;
	}

	public void setResponse(OTPResponse response) {
		this.response = response;
	}*/

	/*public class OTPResponse {
		private String OTPCode;

		public String getOtpCode() {
			return OTPCode;
		}

		public void setOtpCode(String otpCode) {
			this.OTPCode = otpCode;
		}

	}*/
}

package kra.accounts.tracker.models;

public class TransactionDetails {
	private String dueDate;
	private String paidDate;
	private String collectedby;

	public void setdueDate(String dueDate){
		this.dueDate=dueDate;
	}
	public String getdueDate(){
		return dueDate;
	}
	public void setpaidDate(String paidDate){
		this.paidDate=paidDate;
	}
	public String getpaidDate(){
		return paidDate;
	}
	public void setcollectedby(String collectedby){
		this.collectedby=collectedby;
	}
	public String getcollectedby(){
		return collectedby;
	}
}

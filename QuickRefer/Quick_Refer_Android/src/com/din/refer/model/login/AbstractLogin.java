package com.strobilanthes.forrestii.model.login;

import java.util.ArrayList;

import com.strobilanthes.forrestii.model.coins.AbstractCoins;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.general.AbstractCountry;
import com.strobilanthes.forrestii.model.general.AbstractDashboard;
import com.strobilanthes.forrestii.model.general.AbstractLocation;
import com.strobilanthes.forrestii.model.general.AbstractServerInfo;
import com.strobilanthes.forrestii.model.general.AbstractTemplate;
import com.strobilanthes.forrestii.model.settings.AbstractNotificationSettings;
import com.strobilanthes.forrestii.model.settings.SettingsDetails;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;


public class AbstractLogin{

	private String error;
	private int status;
	
	private LoginResponse response;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public LoginResponse getLoginResponse() {
		return response;
	}
	public void setLoginResponse(LoginResponse response) {
		this.response = response;
	}

	public class LoginResponse{
		
		private ArrayList<GetWallPost> Wall;
		private AbstractUserProfile UserProfile;
		private AbstractDashboard DashBoard;
		private CommonList commonplist;
		private ArrayList<FriendsList> FriendList;
		private ArrayList<AbstractCountry> Country;
		private ArrayList<SettingsDetails> Settings;
		
		public ArrayList<GetWallPost> getGetWallPostsList() {
			return Wall;
		}
		public void setGetWallPostsList(ArrayList<GetWallPost> Wall) {
			this.Wall = Wall;
		}
		public AbstractUserProfile getUserProfile() {
			return UserProfile;
		}
		public void setUserProfile(AbstractUserProfile userProfile) {
			UserProfile = userProfile;
		}
		public CommonList getCommonplist() {
			return commonplist;
		}
		public void setCommonplist(CommonList commonplist) {
			this.commonplist = commonplist;
		}
		public ArrayList<FriendsList> getFriendList() {
			return FriendList;
		}
		public void setFriendList(ArrayList<FriendsList> friendList) {
			FriendList = friendList;
		}
		public AbstractDashboard getDashBoard() {
			return DashBoard;
		}
		public void setDashBoard(AbstractDashboard dashBoard) {
			DashBoard = dashBoard;
		}
		public ArrayList<AbstractCountry> getCountry() {
			return Country;
		}
		public void setCountry(ArrayList<AbstractCountry> country) {
			Country = country;
		}
		public ArrayList<SettingsDetails> getSettings() {
			return Settings;
		}
		public void setSettings(ArrayList<SettingsDetails> settings) {
			Settings = settings;
		}
		
	}
	
	public class CommonList{
		
		private ArrayList<AbstractSkillSet> skills; 
		private ArrayList<AbstractLocation>	Location;
		private ArrayList<AbstractServerInfo> ServerInfo; 
		private ArrayList<AbstractTemplate> Template; 
		private ArrayList<AbstractNotificationSettings> Notify; 
		private ArrayList<AbstractCoins> CoinType;
		private ArrayList<AbstractCountry> Country;
		public ArrayList<AbstractSkillSet> getSkills() {
			return skills;
		}
		public void setSkills(ArrayList<AbstractSkillSet> skills) {
			this.skills = skills;
		}
		
		public ArrayList<AbstractNotificationSettings> getNotify() {
			return Notify;
		}
		public void setNotify(ArrayList<AbstractNotificationSettings> notify) {
			Notify = notify;
		}
		public ArrayList<AbstractCoins> getCoinType() {
			return CoinType;
		}
		public void setCoinType(ArrayList<AbstractCoins> coinType) {
			CoinType = coinType;
		}
		
		public ArrayList<AbstractServerInfo> getServerInfo() {
			return ServerInfo;
		}
		public void setServerInfo(ArrayList<AbstractServerInfo> serverInfo) {
			ServerInfo = serverInfo;
		}
		public ArrayList<AbstractTemplate> getTemplate() {
			return Template;
		}
		public void setTemplate(ArrayList<AbstractTemplate> template) {
			Template = template;
		}
		public ArrayList<AbstractCountry> getCountry() {
			return Country;
		}
		public void setCountry(ArrayList<AbstractCountry> country) {
			Country = country;
		}
		public ArrayList<AbstractLocation> getLocation() {
			return Location;
		}
		public void setLocation(ArrayList<AbstractLocation> location) {
			Location = location;
		}
	}
}

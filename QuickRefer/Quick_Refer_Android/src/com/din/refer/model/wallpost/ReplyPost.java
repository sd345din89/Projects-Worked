package com.strobilanthes.forrestii.model.wallpost;

import java.io.Serializable;

import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

public class ReplyPost implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String message;
	private int replyId;
	private long updatedOn;
	private boolean isReplyDeleted;
	private AbstractUserProfile replyUserProfile;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getReplyId() {
		return replyId;
	}

	public void setReplyId(int replyId) {
		this.replyId = replyId;
	}

	public long getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isReplyDeleted() {
		return isReplyDeleted;
	}

	public void setReplyDeleted(boolean isReplyDeleted) {
		this.isReplyDeleted = isReplyDeleted;
	}

	public AbstractUserProfile getReplyUserProfile() {
		return replyUserProfile;
	}

	public void setReplyUserProfile(AbstractUserProfile replyUserProfile) {
		this.replyUserProfile = replyUserProfile;
	}

}

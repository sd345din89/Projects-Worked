package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import roboguice.inject.InjectView;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.activities.MainActivity;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.util.BadgeView;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;

public class SlidingMenuFragment extends  BaseFragment<DBAccess> {
	
	@InjectView(R.id.lyt_user)                        private LinearLayout lytUser;
	@InjectView(R.id.drawer_item_img_user)            private RoundedImageView drawerItemImgUser;
	@InjectView(R.id.drawer_item_txt_username)        private TextView drawerItemTxtUsername;
	@InjectView(R.id.drawer_item_txt_wall)            private TextView drawerItemTxtWall;
	@InjectView(R.id.drawer_item_txt_forrestii_shake) private TextView drawerItemTxtForrestiiShake;
	@InjectView(R.id.drawer_item_txt_notification)    private TextView drawerItemTxtNotification;
	@InjectView(R.id.drawer_item_txt_settings)        private TextView drawerItemTxtSettings;
	@InjectView(R.id.drawer_item_txt_contacts)        private TextView drawerItemTxtContacts;

    protected ImageLoadingListener animateFirstListener;
	private Typeface mFontLight,mFontDark;
	private AbstractUserProfile abstractUserProfile;
	protected ShowcaseView sv;
	protected int counter = 0;
	private SharedPreferences storyPreference;
	private BadgeView badgeview_notifyCount;
	
	public static SlidingMenuFragment newInstance() {
		SlidingMenuFragment frag = new SlidingMenuFragment();
        return frag;
	}
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.view_drawer_list, null);
		
		super.initilizeContents();
		storyPreference = getActivity().getSharedPreferences("Forrestii", Context.MODE_PRIVATE);

		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		
		
		return view;
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		super.onViewCreated(view, savedInstanceState);
		
		badgeview_notifyCount = new BadgeView(getActivity(),drawerItemTxtNotification);
		
		onReload();
		
		drawerItemTxtWall.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Util.hideSoftKeyboard(getActivity(), v);
					
					Fragment content = WallPostFragment.newInstance();
					if(content!=null){
						switchFragment(content);
					}
				}
			});
		
			drawerItemTxtNotification.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Util.hideSoftKeyboard(getActivity(), v);

					Fragment content = NotificationFragment.newInstance(null);
					if(content!=null){
						switchFragment(content);
					}
				}
			});
			drawerItemTxtContacts.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Util.hideSoftKeyboard(getActivity(), v);

					Fragment content = ContactsFragment.newInstance();
					if(content!=null){
						switchFragment(content);
					}

				}
			});
			drawerItemTxtSettings.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Util.hideSoftKeyboard(getActivity(), v);

					Fragment content = SettingsFragment.newInstance();
					if(content!=null){
						switchFragment(content);
					}
				}
			});
			drawerItemTxtForrestiiShake.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Util.hideSoftKeyboard(getActivity(), v);

					Util.isLocationServiceON(getActivity());
					
					if (Util.LocationEnabled) {
						ShakeFragment content = ShakeFragment.newInstance();
						if(content!=null){
							switchFragment(content);
						}
					} else {
						toggle();
					}
				}
			});
			
			drawerItemImgUser.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//Util.setTouchDelay(getActivity(), v);
					Util.hideSoftKeyboard(getActivity(), v);

					showContent();
					
				}
			});
			drawerItemTxtWall.setTypeface(mFontDark);
			drawerItemTxtForrestiiShake.setTypeface(mFontDark);
			drawerItemTxtNotification.setTypeface(mFontDark);
			drawerItemTxtContacts.setTypeface(mFontDark);
			drawerItemTxtSettings.setTypeface(mFontDark);
			drawerItemTxtUsername.setTypeface(mFontDark);
	}
	
	public void onReload() {
		
		abstractUserProfile = getHelper().getDBUserProfile();
		drawerItemTxtUsername.setText(null);
		drawerItemTxtUsername.setText(abstractUserProfile.getFirstName());
		
		
	    int badgeCount = storyPreference.getInt("badgeCount", -1);
		badgeview_notifyCount.increment(badgeCount);
		
		badgeview_notifyCount.show();
		
		ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + abstractUserProfile.getPhotoID(),
				drawerItemImgUser,null, animateFirstListener);
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		/*SampleAdapter adapter = new SampleAdapter(getActivity());

		for (int i = 0; i < menus.length; i++) {
			adapter.add(new SampleItem(menus[i], images[i]));
		}
		setListAdapter(adapter);*/

	}

	private void switchFragment(Fragment fragment) {
		if (getActivity() == null)
			return;

		if (getActivity() instanceof MainActivity) {
			MainActivity fca = (MainActivity) getActivity();
			fca.switchContent(fragment);
		}

	}
	
	private void showContent() {
		if (getActivity() == null)
			return;

		if (getActivity() instanceof MainActivity) {
			MainActivity fca = (MainActivity) getActivity();
			fca.showContent();
		}

	}
	private void toggle(){
		if (getActivity() == null)
			return;

		if (getActivity() instanceof MainActivity) {
			MainActivity fca = (MainActivity) getActivity();
			fca.getSlidingMenu().showContent();
		}
	}

	/*public class SampleAdapter extends ArrayAdapter<SampleItem> {

		public SampleAdapter(Context context) {
			super(context, 0);
		}

		public View getView(final int position, View view, ViewGroup parent) {
			if (view == null) {
				view = LayoutInflater.from(getContext()).inflate(R.layout.row,
						null);
			}
			ImageView icon = (ImageView) view.findViewById(R.id.row_icon);
			icon.setImageResource(getItem(position).iconRes);
			final TextView title = (TextView) view.findViewById(R.id.row_title);
			title.setText(getItem(position).tag);

			return view;
		}
	}*/
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
	}

	@Override
	public void onClick(View v) {
		
	}
	public void showStoryBoard() {
		if(!storyPreference.getBoolean("storyMenuDrawer", false)){
			ViewTarget target = new ViewTarget(drawerItemImgUser);
			sv = new ShowcaseView.Builder(getActivity())
			.setTarget(target)
			.setContentTitle("User Profile")
			.setContentText("Go here to edit your profile, skills, and see your statistics.")
	        .setOnClickListener(storyClickLisnear)
	        .build();
			sv.setButtonText("Next");		
		}
	}
	
	private OnClickListener storyClickLisnear = new OnClickListener() {
		
		@Override
		public void onClick(View v) {

	        switch (counter) {
	            case 0:
	                sv.setShowcase(new ViewTarget(drawerItemTxtWall), true);
	                sv.setContentTitle("Wall Posts");
	                sv.setContentText("Here is where you post a need, refer friends, and comment on posts.");
	                break;
	            case 1:
	                sv.setShowcase(new ViewTarget(drawerItemTxtContacts), true);
	                sv.setContentTitle("Contacts");
	                sv.setContentText("Your friends on Forrestii and your phone contacts (not yet on Forrestii) are shown here.");
	                break;
	            case 2:
	                sv.setShowcase(new ViewTarget(drawerItemTxtForrestiiShake), true);
	                sv.setContentTitle("Shake N Share");
	                sv.setContentText("Shake your phone to connect with Forrestii users nearby.");
	                break;
	            case 3:
	                sv.setShowcase(new ViewTarget(drawerItemTxtNotification), true);
	                sv.setContentTitle("Notifications");
	                sv.setContentText("You get your notifications regarding friend requests, referrals, coins, and rewards here!");
	                break;
	            case 4:
	                sv.setShowcase(new ViewTarget(drawerItemTxtSettings), true);
	                sv.setContentTitle("Settings");
	                sv.setContentText("Go here to change and update your account settings.");
//	                sv.setButtonText("Close");
	                break;
	            case 5:
	            	/*sv.hide();
	            	storyPreference.edit().putBoolean("storyMenuDrawer", true).commit();
	            	counter = 0;*/
	            	sv.setTarget(ViewTarget.NONE);
	                sv.setContentTitle("You're done! Jump into the fun!");
	                sv.setContentText("You can view this tour again by enabling it in settings.");
	                sv.setButtonText("Close");
	                //setAlpha(0.4f, new ActionItemTarget(getActivity(), R.id.create_post), new ActionItemTarget(getActivity(), R.id.share), new ActionItemTarget(getActivity(), R.id.forrestiifriend));
	                break;
	            case 6:
	            	 sv.hide();
	            	 storyPreference.edit().putBoolean("storyMenuDrawer", true).commit();
	            	 counter = 0;
		             // setAlpha(1.0f, textView1, textView2, textView3);
	            	break;
	        }
	        counter++;
		}
	};
}

package com.strobilanthes.forrestii.activities;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import roboguice.activity.RoboSherlockFragmentActivity;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;

public class BaseActivity<H extends SQLiteOpenHelper> extends RoboSherlockFragmentActivity {

	//protected Util util;
	//protected DBAccess dbAccess;
	//protected ImageLoader imageLoader;
	protected DisplayImageOptions options;
	protected ImageLoadingListener animateFirstListener;
	
	private volatile H helper;
	private volatile boolean created = false;
	private volatile boolean destroyed = false;
	private int mTitleRes;
	
	protected LocationClient mLocationClient;
	
	protected static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(10000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setNumUpdates(2)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (helper == null) {
			helper = getHelperInternal(this);
			created = true;
		}
		super.onCreate(savedInstanceState);
	}

	
	public H getHelper() {
		if (helper == null) {
			if (!created) {
				throw new IllegalStateException("A call has not been made to onCreate() yet so the helper is null");
			} else if (destroyed) {
				throw new IllegalStateException(
						"A call to onDestroy has already been made and the helper cannot be used after that point");
			} else {
				throw new IllegalStateException("Helper is null for some unknown reason");
			}
		} else {
			return helper;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		//releaseHelper(helper);
		//destroyed = true;
	}

	protected H getHelperInternal(Context context) {
		@SuppressWarnings({ "unchecked"})
		H newHelper = (H) new DBAccess(context);
		return newHelper;
	}
	
	
	protected  void initilizeContents() {
		
		//imageLoader = ImageLoader.getInstance();
		//imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.default_user)
		.showImageForEmptyUri(R.drawable.default_user)
		.showImageOnFail(R.drawable.default_user)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.considerExifParams(true)
		//.displayer(new RoundedBitmapDisplayer(90))
		.build();
		
		 animateFirstListener = new AnimateFirstDisplayListener();
	}// End of Load ContentsS
	
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	
}// End of Fragment

package com.strobilanthes.forrestii.model.signup;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;
import com.strobilanthes.forrestii.model.lookuplist.AbstractLookUpList;

public interface Signup extends ForrestiiSubject{
	void requestRegistrationDetails(String url,JSONObject postdata);
	void doValidation(String content);
	AbstractSignup getSignupDetails();
	void requestLookupdetails(String url,JSONObject postdata);
	AbstractLookUpList getLookupDetails();
}

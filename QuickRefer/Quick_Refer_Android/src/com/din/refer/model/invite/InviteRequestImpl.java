package com.strobilanthes.forrestii.model.invite;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.contacts.ContactList;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.volleytasks.InviteUserVolleyTasks;

public class InviteRequestImpl implements InviteRequest, VolleyTasksListener {

	//private AbstractFriendsList abstractT2FriendsList;
	private AbstractMobileInviteList abstractMobileInviteList;
	private AbstractEmailInviteList abstractEmailInviteList;
	private AbstractInviteFriend abstractFriendRequest;
	
	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private Activity context;
	private String bundleData;
	private LinkedHashMap<String, ContactList> treeMap;

	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonObject) {
		
		if (method_name.equals("MobileInvite")) {
			AbstractMobileInviteList previousResult = abstractMobileInviteList;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractMobileInviteList = gson.fromJson(jsonObject.toString(), AbstractMobileInviteList.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					method_name, previousResult, abstractMobileInviteList);
			notifyListeners(event);

		} else if (method_name.equals("FriendsT2")) {
			AbstractEmailInviteList previousResult = abstractEmailInviteList;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractEmailInviteList = gson.fromJson(jsonObject.toString(), AbstractEmailInviteList.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					method_name, previousResult, abstractEmailInviteList);
			notifyListeners(event);
		} else if(method_name.equals("sendfriendrequest")){
			AbstractInviteFriend previousResult = abstractFriendRequest;

			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractFriendRequest = gson.fromJson(jsonObject.toString(), AbstractInviteFriend.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					method_name, previousResult, abstractFriendRequest);
			notifyListeners(event);
		}
	}

	@Override
	public void handleError(String method_name, Exception e) {
		
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				null, null);
		notifyListeners(event);
	}

	
	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	@Override
	public void requestMobileInviteList(String url , JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(
				new InviteUserVolleyTasks("MobileInvite", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void requestEmailInviteList(String url , JSONObject postdata) {
		
	}

	@Override
	public void sendFriendRequest(String url, JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(
				new InviteUserVolleyTasks("sendfriendrequest", this, url,
						postdata).setRetryPolicy(new DefaultRetryPolicy(
						Constant.SOCKET_TIMEOUT,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
						DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
		
	}

	@Override
	public AbstractInviteFriend getFriendsRequestData() {
		return abstractFriendRequest;
	}

	@Override
	public void doValidation() {
		
	}

	@Override
	public AbstractMobileInviteList getMobileInviteDetails() {
		return abstractMobileInviteList;
	
	}

	@Override
	public AbstractEmailInviteList getEmailInviteDetails() {
		return abstractEmailInviteList;
	
	}

	@Override
	public void setBundleContactData(String bundleData) {
		this.bundleData = bundleData;
	}

	@Override
	public String getBundleContactData() {
		return bundleData;
	}
}

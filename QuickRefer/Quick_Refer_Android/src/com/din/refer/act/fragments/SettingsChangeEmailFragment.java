package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.settings.AbstractChange;
import com.strobilanthes.forrestii.model.settings.AbstractRemoveEmailId;
import com.strobilanthes.forrestii.model.settings.AbstractVerify;
import com.strobilanthes.forrestii.model.settings.Settings;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class SettingsChangeEmailFragment extends BaseFragment<DBAccess> {

   @Inject Settings settingsModel;
   
   @InjectView(R.id.sett_gen_edtemail)							private EditText edt_Email;
   @InjectView(R.id.sett_gen_btn_email_go)						private Button btn_emailGo;
   @InjectView(R.id.sett_email_resendOTP)						private Button txt_EmailResendOTP;
   @InjectView(R.id.sett_gen_header_edtemail)   				private TextView settGenHeaderEdtemail;
   @InjectView(R.id.sett_gen_btn_Email_timer)   				private TextView txt_timer_email;

   @InjectView(R.id.sett_gen_lyt_pending_email) 				private LinearLayout lyt_pendingEmail;
   @InjectView(R.id.sett_gen_txt_pending_email) 				private TextView txt_pendingEmail;
   @InjectView(R.id.sett_gen_img_pending_email) 				private ImageView img_delete; 			
   
   private Typeface mFontLight,mFontDark;
   private String strPendingEmail;
   private AbstractUserProfile abstractUserProfile;
   private int cnt = 30;
   private Handler handler = new Handler();
   private boolean checkState = false;

   @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.frg_settings_change_email, null);
		
		super.initilizeContents();
		
		return view;
	}
	@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
			fontStyles();
			settingsModel.setContext(getActivity());
			img_delete.setOnClickListener(this);
			btn_emailGo.setOnClickListener(this);
			txt_EmailResendOTP.setOnClickListener(this);
			abstractUserProfile = getHelper().getDBUserProfile();
			updatePendingEmail();

		}
	private void updatePendingEmail() {
		if(abstractUserProfile != null){
			strPendingEmail = abstractUserProfile.getPendingEMail();
			if(strPendingEmail !=null && strPendingEmail.trim().length()>0){
				checkState = true;
				lyt_pendingEmail.setVisibility(View.VISIBLE);
				txt_pendingEmail.setText(strPendingEmail);
				txt_EmailResendOTP.setVisibility(View.VISIBLE);
				txt_EmailResendOTP.setBackgroundResource(R.drawable.new_pink_button_background);
				txt_EmailResendOTP.setClickable(true);
				txt_EmailResendOTP.setEnabled(true);
				edt_Email.setHint("Enter your received code");
				edt_Email.setInputType(InputType.TYPE_CLASS_NUMBER);
				Util.setMaxLength(edt_Email, 6);
				txt_timer_email.setVisibility(View.GONE);
			}
			
		}else{
			checkState = false;
			lyt_pendingEmail.setVisibility(View.GONE);
			edt_Email.setHint("Enter your new email");
			edt_Email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
			Util.setMaxLength(edt_Email, 255);
			txt_EmailResendOTP.setVisibility(View.GONE);
			txt_timer_email.setVisibility(View.GONE);
		}
	}
	private void fontStyles() {
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		edt_Email.setTypeface(mFontDark);
		btn_emailGo.setTypeface(mFontDark);
		txt_EmailResendOTP.setTypeface(mFontDark);
		settGenHeaderEdtemail.setTypeface(mFontLight);
	
	}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		 if(event.getPropertyName().equals("change_EmailId")){
			AbstractChange abstractChangeEmailId = settingsModel.changeEmailId();
			UpdateChangeEmailId(abstractChangeEmailId);
			
		}else if(event.getPropertyName().equals("verify_EmailId")){
			AbstractVerify abstractVerifyEmailId = settingsModel.verifyEmailId();
			UpdateVerifyEmailId(abstractVerifyEmailId);	
		}else if (event.getPropertyName().equals("remove_EmailId")) {
			AbstractRemoveEmailId abstractremoveEmailId = settingsModel.removeEmailId();
			RemoveEmail(abstractremoveEmailId);
			
		}
		 
	}
	

	Runnable emailRunnable = new Runnable() {

		@Override
		public void run() {
			cnt--;
			txt_timer_email.setText("Resend Code button will be enabled in "+cnt+" seconds");
			handler.postDelayed(emailRunnable, 1000);
			if(cnt == 0){
				txt_timer_email.setVisibility(View.GONE);
				txt_EmailResendOTP.setBackgroundResource(R.drawable.new_pink_button_background);
				txt_EmailResendOTP.setClickable(true);
				txt_EmailResendOTP.setEnabled(true);
				cnt = 30;
				return;
			}
			
			
		}
	};
	private void RemoveEmail(AbstractRemoveEmailId abstractremoveEmail) {
		if(abstractremoveEmail  != null){
			if(abstractremoveEmail.getStatus() == 1){
				lyt_pendingEmail.setVisibility(View.GONE);
				txt_EmailResendOTP.setVisibility(View.GONE);
				txt_timer_email.setVisibility(View.GONE);
				checkState = false;
				edt_Email.setText("");
				edt_Email.setHint("Enter your new email");
				edt_Email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
				Util.setMaxLength(edt_Email, 255);
				AbstractUserProfile userProfile = getHelper().getDBUserProfile();
				//userProfile.setEmail(strUpdatedEmail);
				strPendingEmail = "";

				userProfile.setPendingEMail("");
				getHelper().insertUserProfile(userProfile);


			}else if(abstractremoveEmail.getStatus() == 0){
				Util.showAlert(getActivity(),"Message", abstractremoveEmail.getError());
				//edt_mobileVerifyOtpCode.setText("");
		}else{
			Util.showAlert(getActivity(),"Message", "Request Failed.");

		}		
	}}
	private void UpdateChangeEmailId(AbstractChange abstractChangeEmailId) {
		if(abstractChangeEmailId  != null){
			if(abstractChangeEmailId.getStatus() == 1){
				//edt_Email.setVisibility(View.GONE);
				checkState = true;

				cnt = 30;
				handler.post(emailRunnable);
				txt_timer_email.setVisibility(View.VISIBLE);
				txt_EmailResendOTP.setVisibility(View.VISIBLE);
				txt_EmailResendOTP.setBackgroundResource(R.drawable.new_pink_button_background_transparent);
				txt_EmailResendOTP.setClickable(false);
				txt_EmailResendOTP.setEnabled(false);
				txt_timer_email.setText("");
				edt_Email.setText("");
				edt_Email.setHint("Enter your received code");
				edt_Email.setInputType(InputType.TYPE_CLASS_NUMBER);
				Util.setMaxLength(edt_Email, 6);
				AbstractUserProfile userProfile = getHelper().getDBUserProfile();
				userProfile.setPendingEMail(strPendingEmail);
				//strPendingEmail = strUpdatedEmail;
				getHelper().insertUserProfile(userProfile);

				L.ToastMessage(getActivity(), "Verification code has been sent successfully!");


			}else if(abstractChangeEmailId.getStatus() == 0){
				Util.showAlert(getActivity(),"Message", abstractChangeEmailId.getError());
				edt_Email.setText("");
				txt_timer_email.setVisibility(View.GONE);
				/*txt_EmailResendOTP.setVisibility(View.VISIBLE);
				txt_EmailResendOTP.setBackgroundResource(R.drawable.new_pink_button_background);
				txt_EmailResendOTP.setClickable(true);
				txt_EmailResendOTP.setEnabled(true);*/
			}		
		}else{
			Util.showAlert(getActivity(),"Message", "Request Failed.");

		}		
		
	}
	private void UpdateVerifyEmailId(AbstractVerify abstractVerifyEmailId) {
		if(abstractVerifyEmailId  !=null){
			if(abstractVerifyEmailId.getStatus() == 1){
				//Util.showAlert(getActivity(),"Message", abstractVerifyEmailId.getResponse());
	
				L.ToastMessage(getActivity(), "Successfully Email Id Changed");
				
				edt_Email.setText("");
				edt_Email.setVisibility(View.VISIBLE);
				edt_Email.setHint("Enter your mobile number");				
				lyt_pendingEmail.setVisibility(View.GONE);
				txt_EmailResendOTP.setVisibility(View.GONE);

				txt_timer_email.setVisibility(View.GONE);
				AbstractUserProfile userProfile = getHelper().getDBUserProfile();
				userProfile.setEmail(strPendingEmail);
				L.v("DBPendingEmail++"+strPendingEmail);

				strPendingEmail = "";

				userProfile.setPendingEMail("");
				getHelper().insertUserProfile(userProfile);

				getActivity().finish();
				
			}else{
				Util.showAlert(getActivity(),"Message", abstractVerifyEmailId.getError());
				lyt_pendingEmail.setVisibility(View.GONE);				
				edt_Email.setText("");
				txt_timer_email.setVisibility(View.GONE);
				txt_EmailResendOTP.setVisibility(View.VISIBLE);
				txt_EmailResendOTP.setBackgroundResource(R.drawable.new_pink_button_background);
				txt_EmailResendOTP.setClickable(true);
				txt_EmailResendOTP.setEnabled(true);
			}						
			

		}else{
			Util.showAlert(getActivity(),"Message", "Request Failed.");
		}
	}
	


	

	@Override
	public void onResume() {
		super.onResume();
		settingsModel.addChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		settingsModel.removeChangeListener(this);
		
		handler.removeCallbacks(emailRunnable);
		
		Util.hideSoftKeyboard(getActivity(), edt_Email);
	}
	
	@Override
	public void onClick(View v) {
		Util.hideSoftKeyboard(getActivity(), v);

		switch (v.getId()) {
		
		case R.id.sett_gen_img_pending_email:
			Util.hideSoftKeyboard(getActivity(), v);
			JSONObject removeObject = new JSONObject();
			try {
				removeObject.put("userId", getHelper().getUserId());
				settingsModel.removeEmailId(Constant.REMOVE_EMAIL_ID,removeObject );
				
			} catch (JSONException e) {
				
				e.printStackTrace();
			}
			
			break;
		case R.id.sett_gen_btn_email_go:
			Util.hideSoftKeyboard(getActivity(), v);
//			
			handler.removeCallbacks(emailRunnable);
			if(checkState){
				L.d("CheckTrue++"+""+edt_Email.getText().toString());

				if(edt_Email.getText().toString().length()>0){
				JSONObject jobject = new JSONObject();
				try {
					jobject.put("OTPCode", edt_Email.getText().toString());
					jobject.put("userProfile", new JSONObject().put("userId", getHelper().getUserId()));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				settingsModel.verifyEmailId(Constant.VERIFY_EMAIL_ID, jobject);
			}else{
				Util.showAlert(getActivity(), "Message", "Please enter the correct code");
				txt_timer_email.setVisibility(View.GONE);
			}
		}else {
			L.d("CheckFalse++"+""+edt_Email.getText().toString());
			L.d("CheckFalse++"+""+edt_Email.getText().toString());
           AbstractUserProfile userProfiel = getHelper().getDBUserProfile();
				strPendingEmail = edt_Email.getText().toString();
				if(userProfiel!=null){
				if(!strPendingEmail.equals(userProfiel.getPendingEMail())){
					L.i("go button clicked===>"+strPendingEmail);
					String[] i = strPendingEmail.split("@");
					String beforeText = i[0];
					if(strPendingEmail.length()>0 && Util.isValidEmail(strPendingEmail) && beforeText.length()<=64) {
						
						JSONObject jobject = new JSONObject();
						try {
						jobject.put("email", strPendingEmail);
						jobject.put("userId", getHelper().getUserId());
						settingsModel.changeEmailId(Constant.CHANGE_EMAIL_ID, jobject);
						} catch (JSONException e) {
							
							e.printStackTrace();
						}
					
					}else{
						Util.showAlert(getActivity(), "Message", "Please enter the valid email");
					}
				}else{
					Util.showAlert(getActivity(), "Message", "Please enter the email");

				}
				}
			}	
			
			break;

		case R.id.sett_email_resendOTP:
			Util.hideSoftKeyboard(getActivity(), v);
			handler.removeCallbacks(emailRunnable);
			//cnt = 30;
		
			//AbstractUserProfile userProfile = getHelper().getDBUserProfile();
			
			if(abstractUserProfile !=null && abstractUserProfile.getPendingEMail()!=null){
			
			JSONObject jobject = new JSONObject();
			try {
				
				jobject.put("email", abstractUserProfile.getPendingEMail());
				jobject.put("userId", getHelper().getUserId());
				
				settingsModel.changeEmailId(Constant.CHANGE_EMAIL_ID, jobject);

			} catch (JSONException e) {
				
				e.printStackTrace();
			}
			}
		

			break;

		default:
			break;
		}
	}
}
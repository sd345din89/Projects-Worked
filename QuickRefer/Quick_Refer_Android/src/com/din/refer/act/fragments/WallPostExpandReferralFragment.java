package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.activities.ForrestiiFriendProfileActivity;
import com.strobilanthes.forrestii.activities.FriendsProfileT2Activity;
import com.strobilanthes.forrestii.activities.QuickCardProfileActivity;
import com.strobilanthes.forrestii.activities.ReferSomeOneActivity;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.ffFriendsList.GetForrestiiFriendList;
import com.strobilanthes.forrestii.model.ratings.AbstractRatings;
import com.strobilanthes.forrestii.model.ratings.RatingList;
import com.strobilanthes.forrestii.model.referral.AbstractPickandUnPickCard;
import com.strobilanthes.forrestii.model.referral.AbstractReferredQuickCard;
import com.strobilanthes.forrestii.model.referral.Referral;
import com.strobilanthes.forrestii.model.skillset.UserSkillsSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;
import com.strobilanthes.forrestii.model.wallpost.GetWallPostContents;
import com.strobilanthes.forrestii.model.wallpost.RefferalPost;
import com.strobilanthes.forrestii.model.wallpost.WallPost;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;
//dint change anything
public class WallPostExpandReferralFragment extends BaseFragment<DBAccess> {
	
	@InjectView(R.id.profileT2_generallistview)      	ListView referSomeoneList;
	@InjectView(R.id.refersomeone)      				Button referSomeoneText;
	
	private Typeface mFontLight,mFontDark;
	private ReferSomeoneAdapter referSomeoneAdapter;
	private int mPosition = -1;
	private int mReferralPosition =  0 ;
	private int mPostId = 0;
	private String mReview = null;
	private float mRatings = 1f;
	private int mPostByUserId = 0;
	
	private ArrayList<RefferalPost> refferalPosts;
	private AbstractUserProfile abstractUserProfile;
	private  GetWallPost getWallPost;
	
	@Inject WallPost 				wallPostModel;
	@Inject Referral  				referralModel;
	
    public static WallPostExpandReferralFragment newInstance(int pos, GetWallPost getWallPost) {
		 
    	WallPostExpandReferralFragment frag = new WallPostExpandReferralFragment();
       
    	Bundle args = new Bundle();
        args.putInt("position", pos);
        args.putSerializable("wall_post", getWallPost);
        frag.setArguments(args);
         
         return frag;  
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.common_listview, null);
		
		return view;
	}

	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		wallPostModel.setContext(getActivity());
		referralModel.setContext(getActivity());
		
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		
		int position = getArguments().getInt("position");
		mPosition = position;
		getWallPost = (GetWallPost) getArguments().getSerializable("wall_post");
		mPostId = getWallPost.getWallPosts().getPostId();
		abstractUserProfile = getHelper().getDBUserProfile();
		mPostByUserId = getWallPost.getWallPosts().getPostByUserProfile().getUserId();
		
		refferalPosts = getWallPost.getWallPosts().getReferral();
		
		updateReferals();
		
		referSomeoneText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(mPosition != -1){
					Intent intent = new Intent(getActivity(),ReferSomeOneActivity.class);
					intent.putExtra("postId", getWallPost.getWallPosts().getPostId());
					intent.putExtra("postByUserId", getWallPost.getWallPosts().getPostByUserProfile().getUserId());
					intent.putExtra("mobile", getWallPost.getWallPosts().getPostByUserProfile().getMobile());
					intent.putExtra("skillSetId", getWallPost.getWallPosts().getSkill().getSkillId());
					intent.putExtra("position", mPosition);
					startActivityForResult(intent,0);
					
				}
				
			}
		});
		
	}

	private void updateReferals() {
		
		boolean isPick = false;
		boolean isCurrentUser = false;
		int referralFromUserId = 0;
		ArrayList<RefferalPost> mReferralPost = null;
		if(refferalPosts != null && refferalPosts.size() > 0){
			for (RefferalPost refferalPost : refferalPosts) {
				if(refferalPost.isPick()){
					isPick  = true;
					mReferralPost = new ArrayList<RefferalPost>();
					mReferralPost.add(refferalPost);
					if(refferalPost.getReferralFromUserProfile() != null && refferalPost.getReferralFromUserProfile().getUserId() == abstractUserProfile.getUserId()){
						referralFromUserId = refferalPost.getReferralFromUserProfile().getUserId();
						break;
					}
				} else{
					if(refferalPost.getReferralFromUserProfile() != null){
						if(abstractUserProfile.getUserId() == refferalPost.getReferralFromUserProfile().getUserId()){
							isCurrentUser = true;
							referralFromUserId = refferalPost.getReferralFromUserProfile().getUserId();
							mReferralPost = new ArrayList<RefferalPost>();
							mReferralPost.add(refferalPost);
						}
					}
				}
			}
			
			if(isPick){
				if(mPostByUserId == abstractUserProfile.getUserId()){
					refferalPosts = mReferralPost;
					referSomeoneAdapter = new ReferSomeoneAdapter(getActivity());
					referSomeoneList.setAdapter(referSomeoneAdapter);
				} else {
					if(referralFromUserId != 0){
						if(referralFromUserId == abstractUserProfile.getUserId() || isCurrentUser){
							refferalPosts = mReferralPost;
							referSomeoneAdapter = new ReferSomeoneAdapter(getActivity());
							referSomeoneList.setAdapter(referSomeoneAdapter);
						} else {
							referSomeoneText.setVisibility(View.VISIBLE);
							referSomeoneText.setClickable(false);
							referSomeoneText.setEnabled(false);
							referSomeoneText.setText("Already someone picked this card, so you can't refer your friends");
							referSomeoneText.setBackgroundColor(Color.TRANSPARENT);
							referSomeoneText.setTextColor(getResources().getColor(R.color.forrestii_pink_color));
							referSomeoneList.setVisibility(View.GONE);
						}
					} else {
						referSomeoneText.setVisibility(View.VISIBLE);
						referSomeoneText.setClickable(false);
						referSomeoneText.setEnabled(false);
						referSomeoneText.setText("Already someone picked this card, so you can't refer your friends");
						referSomeoneText.setBackgroundColor(Color.TRANSPARENT);
						referSomeoneText.setTextColor(getResources().getColor(R.color.forrestii_pink_color));
						referSomeoneList.setVisibility(View.GONE);	
					}
					
				}
				
			} else {
				if(isCurrentUser){
					referSomeoneText.setVisibility(View.GONE);
					referSomeoneList.setVisibility(View.VISIBLE);
					
					refferalPosts = mReferralPost;
					referSomeoneAdapter = new ReferSomeoneAdapter(getActivity());
					referSomeoneList.setAdapter(referSomeoneAdapter);
				} else {
					final int userId = getWallPost.getWallPosts().getPostByUserProfile().getUserId();
					if(userId != abstractUserProfile.getUserId()){
						referSomeoneText.setVisibility(View.VISIBLE);
						referSomeoneList.setVisibility(View.GONE);
					} else {
						referSomeoneText.setVisibility(View.GONE);
						referSomeoneList.setVisibility(View.VISIBLE);
						
						refferalPosts = getWallPost.getWallPosts().getReferral();;
						referSomeoneAdapter = new ReferSomeoneAdapter(getActivity());
						referSomeoneList.setAdapter(referSomeoneAdapter);

					}
				}
			}
		} else {
			final int userId = getWallPost.getWallPosts().getPostByUserProfile().getUserId();
			if(userId != abstractUserProfile.getUserId()){
				referSomeoneText.setVisibility(View.VISIBLE);
				referSomeoneList.setVisibility(View.GONE);
			} else {
				referSomeoneText.setVisibility(View.VISIBLE);
				referSomeoneText.setClickable(false);
				referSomeoneText.setEnabled(false);
				referSomeoneText.setText("No refferals yet");
				referSomeoneText.setBackgroundColor(Color.TRANSPARENT);
				referSomeoneText.setTextColor(getResources().getColor(R.color.forrestii_pink_color));
				referSomeoneList.setVisibility(View.GONE);
			}
		}
	}
	
	class ReferSomeoneAdapter extends BaseAdapter {
		
		private final Context context;
		LayoutInflater inflater;
		ViewHolder holder = null;
		private LinearLayout v;

		public ReferSomeoneAdapter(Context context) {
			this.context = context;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return refferalPosts.size();
		}

		@Override
		public RefferalPost getItem(int position) {
			return refferalPosts.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, final ViewGroup parent) {
			
			v = (LinearLayout) convertView;
			if (v == null) {
				
				v = (LinearLayout)inflater.inflate(R.layout.frg_received_referrals_list_item, null);
				
				holder = new ViewHolder();
				
				holder.mainView = v.findViewById(R.id.mainlayout);
				holder.relativepicklay = (LinearLayout) v.findViewById(R.id.relativepicklay); 
				holder.img_user = (ImageView) v.findViewById(R.id.received_referrals_list_item_img_user);
				holder.txt_ReferredUser = (TextView) v.findViewById(R.id.received_referrals_list_item_txt_name);
				holder.refersomeoneListItemTxtSkills = (TextView) v.findViewById(R.id.received_referrals_list_item_txt_skills);
				holder.txt_ReferredByUser = (TextView) v.findViewById(R.id.received_referrals_list_item_txt_referredby);
				
				holder.img_pick = (ImageView) v.findViewById(R.id.received_referrals_btn_pick);
				holder.img_Rating = (ImageView) v.findViewById(R.id.received_referrals_btn_rating);
				holder.img_unpick = (ImageView) v.findViewById(R.id.received_referrals_btn_unpick);
				holder.referrals_txt_pick = (TextView) v.findViewById(R.id.received_referrals_txt_pick);
				holder.txt_ReferredUser.setSelected(true);
				holder.refersomeoneListItemTxtSkills.setSelected(true);
				holder.txt_ReferredUser.setTypeface(mFontDark);
				holder.refersomeoneListItemTxtSkills.setTypeface(mFontDark);
				
				holder.rateLayout =  v.findViewById(R.id.layout_ratecard);
				holder.edt_review = (EditText) v.findViewById(R.id.rate_and_review_edit);
				holder.rating_bar = (RatingBar) v.findViewById(R.id.rate_and_review_ratingBar);
				holder.refer_done = (Button) v.findViewById(R.id.refer_done); 
				holder.profile_rating = (RatingBar) v.findViewById(R.id.profile_ratingBar);
				
				v.setTag(holder);
				
			} else {
				holder = (ViewHolder) v.getTag();
			}

			if(refferalPosts.get(position).isPick()) {
				
				holder.img_pick.setVisibility(View.GONE);
				holder.referrals_txt_pick.setVisibility(View.GONE);
				
				if (refferalPosts.get(position).getReferralFromUserProfile() != null && refferalPosts.get(position).getReferralFromUserProfile().getUserId() == abstractUserProfile.getUserId()) {
					holder.profile_rating.setVisibility(View.VISIBLE);
					holder.img_pick.setVisibility(View.GONE);
					holder.referrals_txt_pick.setVisibility(View.GONE);
					holder.img_Rating.setVisibility(View.GONE);
					holder.img_unpick.setVisibility(View.GONE);
					holder.rateLayout.setVisibility(View.GONE);
					holder.relativepicklay.setBackgroundColor(getResources().getColor(R.color.forrestii_background_grey));
					holder.mainView.setBackgroundColor(getResources().getColor(R.color.forrestii_background_grey));
				} else {
					if(refferalPosts.get(position).getRating() != null && refferalPosts.get(position).getRating().size() > 0) {
						holder.rateLayout.setVisibility(View.VISIBLE);
						holder.relativepicklay.setBackgroundColor(getResources().getColor(R.color.forrestii_background_grey));
						holder.mainView.setBackgroundColor(getResources().getColor(R.color.forrestii_background_grey));
						holder.rating_bar.setRating(refferalPosts.get(position).getRating().get(0).getRating());
						holder.rating_bar.setFocusable(false);
						holder.rating_bar.setFocusableInTouchMode(false);
						String review = refferalPosts.get(position).getRating().get(0).getReview();
						holder.edt_review.setText(review != null ? review : "No reviews found");
						holder.img_unpick.setVisibility(View.GONE);
						holder.img_Rating.setVisibility(View.GONE);
						holder.refer_done.setVisibility(View.GONE);
						holder.profile_rating.setVisibility(View.GONE);
						holder.edt_review.setFocusable(false);
						holder.edt_review.setFocusableInTouchMode(false);
						if(abstractUserProfile.getUserId() == mPostByUserId && refferalPosts.get(position).getRating().size() <= 0){
							holder.rating_bar.setIsIndicator(false);
						} else {
							holder.rating_bar.setIsIndicator(true);
						}
					} else {
						holder.rating_bar.setRating(1);
						holder.relativepicklay.setBackgroundResource(R.color.forrestii_white);
						holder.img_unpick.setVisibility(View.VISIBLE);
						holder.img_Rating.setVisibility(View.VISIBLE);
						holder.refer_done.setVisibility(View.VISIBLE);
						holder.profile_rating.setVisibility(View.GONE);
						holder.rateLayout.setVisibility(View.GONE);
						holder.rating_bar.setIsIndicator(false);
						holder.edt_review.setFocusable(true);
						holder.edt_review.setFocusableInTouchMode(true);
						holder.rating_bar.setFocusable(true);
						holder.rating_bar.setFocusableInTouchMode(true);
					}

					if(refferalPosts.get(position).getRating() != null && refferalPosts.get(position).getRating().size() > 0) {
						holder.rateLayout.setVisibility(View.VISIBLE);
						holder.relativepicklay.setBackgroundColor(getResources().getColor(R.color.forrestii_background_grey));
						holder.mainView.setBackgroundColor(getResources().getColor(R.color.forrestii_background_grey));
						holder.rating_bar.setRating(refferalPosts.get(position).getRating().get(0).getRating());
						holder.rating_bar.setFocusable(false);
						holder.rating_bar.setFocusableInTouchMode(false);
						String review = refferalPosts.get(position).getRating().get(0).getReview();
						holder.edt_review.setText(review != null ? review : "No reviews yet");
						holder.img_unpick.setVisibility(View.GONE);
						holder.img_Rating.setVisibility(View.GONE);
						holder.refer_done.setVisibility(View.GONE);
						holder.profile_rating.setVisibility(View.GONE);
						holder.edt_review.setFocusable(false);
						holder.edt_review.setFocusableInTouchMode(false);
						if(abstractUserProfile.getUserId() == mPostByUserId && refferalPosts.get(position).getRating().size() <= 0){
							holder.rating_bar.setIsIndicator(false);
						} else {
							holder.rating_bar.setIsIndicator(true);
						}
					} else {
						holder.rating_bar.setRating(0);
						holder.relativepicklay.setBackgroundResource(R.color.forrestii_white);
						holder.img_unpick.setVisibility(View.VISIBLE);
						holder.img_Rating.setVisibility(View.VISIBLE);
						holder.refer_done.setVisibility(View.VISIBLE);
						holder.profile_rating.setVisibility(View.GONE);
						holder.rateLayout.setVisibility(View.GONE);
						holder.rating_bar.setIsIndicator(false);
						holder.edt_review.setFocusable(true);
						holder.edt_review.setFocusableInTouchMode(true);
						holder.rating_bar.setFocusable(true);
						holder.rating_bar.setFocusableInTouchMode(true);
					}
					
					holder.img_Rating.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							mReferralPosition = position;
							if (holder.rateLayout.getVisibility() == View.VISIBLE)
								holder.rateLayout.setVisibility(View.GONE);
							else
								holder.rateLayout.setVisibility(View.VISIBLE);
						}
					});
					
					holder.rating_bar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
						
						@Override
						public void onRatingChanged(RatingBar ratingBar, final float rating,
								boolean fromUser) {
							mRatings = rating;
							
						}
					});
					
					holder.refer_done.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							try {
								JSONObject jobject = new JSONObject();
								mReview = holder.edt_review.getText().toString();
								mReview = mReview.replaceAll("\\n", " ");
								if (mRatings != 0/* && mReview.length() > 0*/) {
									
									jobject.put("Skills", new JSONObject().put("skillId", getWallPost.getWallPosts().getSkill().getSkillId()));
									jobject.put("rating", mRatings);
									jobject.put("review", ""+mReview);
									ArrayList<RefferalPost> referralList = getWallPost.getWallPosts().getReferral();
									if(referralList !=null && referralList.size() > 0){
										for(RefferalPost refferalPost : referralList){
											if(refferalPost.isPick()){
												jobject.put("ratingFromUserProfile", new JSONObject().put("userId", refferalPost.getReferredUserProfile().getUserId()));
												
												if(refferalPost.getReferredUserProfile() != null){
													jobject.put("userProfile", new JSONObject().put("userId", refferalPost.getReferralFromUserProfile().getUserId()));
												}else if(refferalPost.getReferredQuickCard() != null){
													jobject.put("quickCard", new JSONObject().put("QuickCardId", refferalPost.getReferredQuickCard().getQuickCardId()));
												}else if(refferalPost.getForrestiiFriend() != null){
													jobject.put("forrestiiFriend", new JSONObject().put("FFID", refferalPost.getForrestiiFriend().getFfid()));
												}
												jobject.put("referral", new JSONObject().put("ReferralId", refferalPost.getReferralId()));
												break;
											}
										}
									}
									referralModel.ratingCard(Constant.RATING_A_CARD, jobject);
	
								} else {
									Util.showAlert(v.getContext(), "", "Rate from 0 - 5");
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					});
				
					holder.img_Rating.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							mReferralPosition = position;
							if (holder.rateLayout.getVisibility() == View.VISIBLE)
								holder.rateLayout.setVisibility(View.GONE);
							else
								holder.rateLayout.setVisibility(View.VISIBLE);
						}
					});
					
					holder.rating_bar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
						
						@Override
						public void onRatingChanged(RatingBar ratingBar, final float rating,
								boolean fromUser) {
							mRatings = rating;
						}
					});
					
					holder.refer_done.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							try {
								JSONObject jobject = new JSONObject();
								mReview = holder.edt_review.getText().toString();
								mReview = mReview.replaceAll("\\n", " ");
								if (mRatings != 0/* && mReview.length() > 0*/) {
									
									jobject.put("Skills", new JSONObject().put("skillId", getWallPost.getWallPosts().getSkill().getSkillId()));
									jobject.put("rating", mRatings);
									jobject.put("review", ""+mReview);
									ArrayList<RefferalPost> referralList = getWallPost.getWallPosts().getReferral();
									if(referralList !=null && referralList.size() > 0){
										for(RefferalPost refferalPost : referralList){
											if(refferalPost.isPick()){
												
												if(refferalPost.getReferredUserProfile() != null)
													jobject.put("ratingFromUserProfile", new JSONObject().put("userId",refferalPost.getReferredUserProfile().getUserId()));
				
												if(refferalPost.getReferredUserProfile() != null){
													jobject.put("userProfile", new JSONObject().put("userId", refferalPost.getReferredUserProfile().getUserId()));
												}else if(refferalPost.getReferredQuickCard() != null){
													jobject.put("quickCard", new JSONObject().put("QuickCardId", refferalPost.getReferredQuickCard().getQuickCardId()));
												}else if(refferalPost.getForrestiiFriend() != null){
													jobject.put("forrestiiFriend", new JSONObject().put("FFID", refferalPost.getForrestiiFriend().getFfid()));
												}
												jobject.put("referral", new JSONObject().put("ReferralId", refferalPost.getReferralId()));
												break;
											}
										}
									}
									referralModel.ratingCard(Constant.RATING_A_CARD, jobject);
	
								} else {
									Util.showAlert(v.getContext(), "", "Rate from 0 - 5");
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					});
				}
			} else {
				if(getCount() == 1){
					if(getItem(position).getReferralFromUserProfile() != null){
						if(abstractUserProfile.getUserId() == getItem(position).getReferralFromUserProfile().getUserId()){
							holder.profile_rating.setVisibility(View.VISIBLE);
							holder.img_pick.setVisibility(View.GONE);
							holder.referrals_txt_pick.setVisibility(View.GONE);
							holder.img_Rating.setVisibility(View.GONE);
							holder.img_unpick.setVisibility(View.GONE);
							holder.rateLayout.setVisibility(View.GONE);
							holder.relativepicklay.setBackgroundColor(getResources().getColor(R.color.forrestii_background_grey));
							holder.mainView.setBackgroundColor(getResources().getColor(R.color.forrestii_background_grey));
						} else {
							holder.img_pick.setVisibility(View.VISIBLE);
							holder.referrals_txt_pick.setVisibility(View.VISIBLE);
							holder.img_Rating.setVisibility(View.GONE);
							holder.img_unpick.setVisibility(View.GONE);
							holder.rateLayout.setVisibility(View.GONE);
							holder.profile_rating.setVisibility(View.GONE);
						}
					} else {
						holder.img_pick.setVisibility(View.VISIBLE);
						holder.referrals_txt_pick.setVisibility(View.VISIBLE);
						holder.img_Rating.setVisibility(View.GONE);
						holder.img_unpick.setVisibility(View.GONE);
						holder.rateLayout.setVisibility(View.GONE);
						holder.profile_rating.setVisibility(View.GONE);
					}
					
				} else {
					holder.img_pick.setVisibility(View.VISIBLE);
					holder.referrals_txt_pick.setVisibility(View.VISIBLE);
					holder.img_Rating.setVisibility(View.GONE);
					holder.img_unpick.setVisibility(View.GONE);
					holder.rateLayout.setVisibility(View.GONE);
					holder.profile_rating.setVisibility(View.GONE);
				}
				
			}
				
			/*holder.relativepicklay.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT)
					.setMessage("Do you want to pick this user?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
							JSONObject jobject = new JSONObject();	
							try {
								mReferralPosition = position;
								jobject.put("ReferralId", getItem(position).getReferralId());
								jobject.put("posts", new JSONObject().put("postId",mPostId));
								jobject.put("referralFromUserProfile", new JSONObject().put("userId", getHelper().getUserId()));
								
								referralModel.pickACard(Constant.PICK_A_CARD, jobject);
								
							} catch (JSONException e) {
								e.printStackTrace();
							}	
						}
					}).setNegativeButton("Cancel",null).show();		
				}
			});
			
			holder.img_unpick.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT)
					.setMessage("Do you want to unpick this user?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							JSONObject jobject = new JSONObject();
							try { 
								jobject.put("ReferralId",getItem(position).getReferralId());
								jobject.put("posts", new JSONObject().put("postId", mPostId));
								
							} catch (JSONException e) {
								e.printStackTrace();
							}
							referralModel.unPickACard(Constant.UNPICK_A_CARD, jobject);
						}
					}).setNegativeButton("Cancel",null).show();		
				}
			});*/
			
			holder.img_user.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					if( getItem(position).getReferredUserProfile() != null){
						Intent i = new Intent(v.getContext(),FriendsProfileT2Activity.class);
						i.putExtra("ProfileT2", getItem(position).getReferredUserProfile());
						i.putExtra("isPick", getItem(position).isPick());
						startActivity(i);
						
					} else if(getItem(position).getReferredQuickCard() != null){
						AbstractReferredQuickCard abstractReferredQuickCard =  (AbstractReferredQuickCard) getItem(position).getReferredQuickCard();
						Intent quickIntent = new Intent(v.getContext(),QuickCardProfileActivity.class);
						quickIntent.putExtra("QuickCardProfile",abstractReferredQuickCard);
						startActivity(quickIntent);
						
					} else if( getItem(position).getForrestiiFriend() != null){
						GetForrestiiFriendList friendsList =  (GetForrestiiFriendList) getItem(position).getForrestiiFriend();
						Intent forrestiiFriend = new Intent(v.getContext(),ForrestiiFriendProfileActivity.class);
						forrestiiFriend.putExtra("ForrestiiFriendProfile",friendsList);
						startActivity(forrestiiFriend);
					}
				}
			});
			  

			if(refferalPosts.get(position).getReferredUserProfile() != null){
				holder.txt_ReferredUser.setText(""+refferalPosts.get(position).getReferredUserProfile().getFirstName());
				holder.profile_rating.setRating(refferalPosts.get(position).getReferredUserProfile().getTotalRating());
				ArrayList<UserSkillsSet> userSkillsSets = refferalPosts.get(position).getReferredUserProfile().getUserSkillSets();
				if(userSkillsSets != null && userSkillsSets.size() > 0){
					for (UserSkillsSet userSkillsSet : userSkillsSets) {
						if(userSkillsSet.isPrimarySkill()){
							holder.refersomeoneListItemTxtSkills.setText(""+userSkillsSet.getSkills().getSkillName());
							break;
						}
					}
				}
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + refferalPosts.get(position).getReferredUserProfile().getPhotoID(), holder.img_user,options,animateFirstListener);
			} else if(refferalPosts.get(position).getReferredQuickCard() != null){
				holder.txt_ReferredUser.setText(""+refferalPosts.get(position).getReferredQuickCard().getFirstName());
				holder.refersomeoneListItemTxtSkills.setText("Quick card user");
				//holder.ratingBar.setRating(refferalPosts.get(position).getReferredQuickCard().getTotalRating());
				holder.profile_rating.setVisibility(View.GONE);
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + refferalPosts.get(position).getReferredQuickCard().getQuickCardId(), holder.img_user,options,animateFirstListener);
				
			} else if(refferalPosts.get(position).getForrestiiFriend() != null){
				holder.txt_ReferredUser.setText(""+refferalPosts.get(position).getForrestiiFriend().getPocfirstName());
				holder.refersomeoneListItemTxtSkills.setText("Forrestii user");
				holder.profile_rating.setRating(refferalPosts.get(position).getForrestiiFriend().getTotalRating());
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + "null", holder.img_user,options,animateFirstListener);
			}
			String totalString = "";
			if(refferalPosts.get(position).getReferralFromUserProfile() != null){
				String firstName = refferalPosts.get(position).getReferralFromUserProfile().getFirstName();
				totalString = "Ref By "+(firstName == null ? "Forrestii User":firstName);
			} else if (refferalPosts.get(position).getForrestiiFriend() !=null){
				totalString = "Ref By Forrestii Friend";
			} else if (refferalPosts.get(position).getReferredQuickCard() !=null){
				String firstName = refferalPosts.get(position).getReferralFromUserProfile().getFirstName();
				totalString = "Ref By " +( firstName == null ? "Quick Card User":firstName);
			}
			
			Spannable nameSpan = new SpannableString(totalString);        
			nameSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 0, 7,0);
			//nameSpan.setSpan(new RelativeSizeSpan(1.00f),  0, 7, 0);
			holder.txt_ReferredByUser.setText(nameSpan);
			
			return v;
		}
		
		class ViewHolder {
			private ImageView img_user,img_Rating,img_pick,img_unpick;
			private TextView txt_ReferredUser,refersomeoneListItemTxtSkills,txt_ReferredByUser,referrals_txt_pick;
			private View rateLayout;
			private EditText edt_review;
			private RatingBar rating_bar;
			private Button refer_done;
			private LinearLayout relativepicklay;
			private View mainView;
			private RatingBar profile_rating;
			// private LinearLayout contacts_root_lyt;
			//private LinearLayout receivedReferralLayoutPick,
			//		receivedReferralLayoutUnpick;
		}
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.comment_enter:
			
			break;

		default:
			break;
		}
	}
	
	@Override
	public void onResume(){
		super.onResume();
		wallPostModel.addChangeListener(this);
		referralModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		wallPostModel.removeChangeListener(this);
		referralModel.removeChangeListener(this);
	}
	
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("pickACard")) {
			
			AbstractPickandUnPickCard abstractReferral = referralModel.getPickCardResponse();
			if(abstractReferral != null){
				if(abstractReferral.getStatus() == 1){
//					L.ToastMessage(getActivity(), "You picked successfully");
					L.ToastMessage(getActivity(), abstractReferral.getResponse());
					ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
					GetWallPost getWallPost = getWallPostList.get((mPosition));
					GetWallPostContents getWallPostContents = getWallPost.getWallPosts();
						ArrayList<RefferalPost> refferalList = getWallPostContents.getReferral();
					if(mReferralPosition != -1){
						ArrayList<RefferalPost> pickedPost = new ArrayList<RefferalPost>();
						refferalList.get(mReferralPosition).setPick(true);
						pickedPost.add(refferalList.get(mReferralPosition));
						refferalPosts = pickedPost;		
					}
						
					refferalList.set(mReferralPosition, refferalList.get(mReferralPosition));
					
					getWallPostContents.setReferral(refferalList);
					getWallPost.setWallPosts(getWallPostContents);
					
					
					getWallPostList.set((mPosition),getWallPost);
					Constant.mGetWallPostArrayList = getWallPostList;
					
					referSomeoneAdapter.notifyDataSetChanged();
					
				}else{
					Util.showAlert(getActivity(), "Message",abstractReferral.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}
			
		}else if(event.getPropertyName().equals("RatingACard")){
			
			AbstractRatings abstractRatings = referralModel.getRatingResponse();
			if(abstractRatings != null){
				if(abstractRatings.getStatus() == 1){
					
					L.ToastMessage(getActivity(), "Successfully rated");
					
					ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
					GetWallPost getWallPost = getWallPostList.get((mPosition));
					GetWallPostContents getWallPostContents = getWallPost.getWallPosts();
					ArrayList<RefferalPost> refferalList = getWallPostContents.getReferral();
					
					RefferalPost refferalPost = refferalList.get(0);
					ArrayList<RatingList> ratingLists = new ArrayList<RatingList>();
					RatingList ratingList = new RatingList();
					ratingList.setRating(mRatings);
					ratingList.setRatingFromUserProfile(abstractUserProfile);
					ratingList.setReview(mReview);
					ratingList.setUpdatedOn(System.currentTimeMillis());
					ratingLists.add(0, ratingList);
					refferalPost.setRating(ratingLists);
//					refferalList.set(0, refferalPost);

					getWallPostContents.setReferral(refferalList);
					getWallPost.setWallPosts(getWallPostContents);
					
					if(mReferralPosition != -1){
						refferalPosts = refferalList;		
					}
					
					refferalList.set(mReferralPosition, refferalList.get(mReferralPosition));
					
					getWallPostList.set((mPosition),getWallPost);
					Constant.mGetWallPostArrayList = getWallPostList;
					
					referSomeoneAdapter.notifyDataSetChanged();
					
				}else{
					Util.showAlert(getActivity(), "Message",""+abstractRatings.getError());
				}	
			}
			else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");

			}
			
		}else if(event.getPropertyName().equals("unPickACard")){
			
			AbstractPickandUnPickCard abstractReferral = referralModel.getPickCardResponse();
			if(abstractReferral != null){
				if(abstractReferral.getStatus() == 1){
					L.ToastMessage(getActivity(), "Successfully unpicked card");
					
					ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
					GetWallPost getWallPost = getWallPostList.get((mPosition));
					GetWallPostContents getWallPostContents = getWallPost.getWallPosts();
						ArrayList<RefferalPost> refferalList = getWallPostContents.getReferral();
					if(mReferralPosition != -1)
						refferalList.get(mReferralPosition).setPick(false);
					refferalList.set(mReferralPosition, refferalList.get(mReferralPosition));
					
					getWallPostContents.setReferral(refferalList);
					getWallPost.setWallPosts(getWallPostContents);
					
					getWallPostList.set((mPosition),getWallPost);
					Constant.mGetWallPostArrayList = getWallPostList;
					
					referSomeoneAdapter.notifyDataSetChanged();
					
				} else {
					Util.showAlert(getActivity(), "Message",abstractReferral.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0) 
			if (resultCode == 88){
				RefferalPost refferalPost  = (RefferalPost) data.getSerializableExtra("refferalPost");
				final int position = data.getIntExtra("pos", -1);
				ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
				GetWallPost getWallPost = getWallPostList.get(position);
				GetWallPostContents getWallPostContents = getWallPost.getWallPosts();
				ArrayList<RefferalPost> referralPostsList = getWallPostContents.getReferral();
				referralPostsList.add(0,refferalPost);
				
				refferalPosts = referralPostsList;
				
				getWallPostContents.setReferral(referralPostsList);
				getWallPost.setWallPosts(getWallPostContents);
				getWallPostList.set(position, getWallPost);
				
				
				//this.getWallPost.getWallPosts().setReferral(referralPostsList);
				
				Constant.mGetWallPostArrayList = getWallPostList;
				
				updateReferals();
				
				referSomeoneAdapter.notifyDataSetChanged();
			}
		super.onActivityResult(requestCode, resultCode, data);
	}
}

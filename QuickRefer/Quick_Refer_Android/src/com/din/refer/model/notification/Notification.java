package com.strobilanthes.forrestii.model.notification;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Notification extends ForrestiiSubject {
	
	
	void reqestAccept(String url,JSONObject postdata);
	AcceptandDeclineNotification getRequestedAcceptedResponse();
	
	void requestDeclined(String url,JSONObject postdata);
	AcceptandDeclineNotification getDeclinedRequest();
	
	void requestNotifyWallPost(String url,JSONObject postdata,boolean isProgressShow);
	AbstractNotifyWall getNotifyWallPost();
	
	void requestPaginateNotificationList(String url,JSONObject postdata);
	AbstractNotification getPaginateNotification();
	
	void updateNotificationViewStatus(String url,JSONObject postdata);
	AcceptandDeclineNotification updateNotificationViewStatus();
}

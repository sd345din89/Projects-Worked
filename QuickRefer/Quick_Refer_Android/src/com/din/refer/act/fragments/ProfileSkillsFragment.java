package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;
import com.strobilanthes.forrestii.model.skillset.UserSkillsSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractEditUserProfile;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.userprofile.UserProfile;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class ProfileSkillsFragment extends ScrollTabHolderFragment implements OnScrollListener,PreferenceChangeListener {
	private int mPosition;
	public static final String ARG_POSITION = "position";
	private Typeface mFontLight,mFontDark;

	private int mPrimarySelectedPos = -1;

	private ArrayList<String>SecondarySkillAl = new ArrayList<String>();
	private AbstractUserProfile abstractUserProfile;
	private ArrayList<AbstractSkillSet>abstractSkillSet;
	private String mSelectedPrimartSkillName = null;
	private int mSelectedPrimaryskillId = -1 ;
	private ArrayList<String>SkillNames = new ArrayList<String>();
	private ArrayList<Integer>SkillIds = new ArrayList<Integer>();
	private CustomGeneralAdapter skilladapter;
	private ArrayList<String>profileSecondarySkillsAL = new ArrayList<String>();
 	private String strPrimarySkillName = null;
 	private ListView mListView;
 	private int checkedList = 0;

 
			
	@Inject UserProfile userProfileModel;

	interface UpdateProfileSkillListner{
		void profileupdated();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View convertedView = inflater.inflate(R.layout.frg_cmn_list_user_t2_t3_basic_skill, null);
	
		Bundle b = getArguments();
		mPosition = getArguments().getInt(ARG_POSITION);

		SecondarySkillAl = (ArrayList<String>) b.getSerializable("secondaryskill");
		strPrimarySkillName = b.getString("primarySkill");
		SkillIds = (ArrayList<Integer>) b.getSerializable("SecondaryskillIds");
		L.d("PrimarySkill+++"+strPrimarySkillName);
		mListView = (ListView)convertedView.findViewById(R.id.profileT2_generallistview);
		View placeHolderView = inflater.inflate(R.layout.view_pagestrip_header_placeholder, mListView, false);
		mListView.addHeaderView(placeHolderView);
		mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		return convertedView;
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		userProfileModel.setContext(getActivity());

		abstractUserProfile = getHelper().getDBUserProfile();
		updateUserInfoDetails(abstractUserProfile);

		abstractSkillSet = getHelper().getDBSkillset();
		
	}
	@Override
	public void adjustScroll(int scrollHeight) {
		if (scrollHeight == 0 && mListView.getChildCount()>= 1) {
			return;
		}
		mListView.setSelectionFromTop(1, scrollHeight);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mListView.setOnScrollListener(this);
		skilladapter = new CustomGeneralAdapter(getActivity());
		mListView.setAdapter(skilladapter);
	}
	

	@Override
	public void onClick(View arg0) {
		
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		if (mScrollTabHolder != null)
			mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);
	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {
		
	}
	class CustomGeneralAdapter extends BaseAdapter{
		private LayoutInflater mLayoutInflater;
		ArrayList<String>Al;
		//		private String strSelectedPrimarySkill;
		private String strSelectedSecondarySkill;
		
		public CustomGeneralAdapter(Activity activity) {
			mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			
			return SecondarySkillAl.size();
		}
	
		@Override
		public Object getItem(int position) {
			
			return position;
		}
	
		@Override
		public long getItemId(int position) {
			
			return 0;
		}
	
		@Override
		public View getView(final int position, View convertView, ViewGroup paramViewGroup) {
			View v = convertView;
			 ViewHolder holder = null;
			if (v == null) {
				
				v = mLayoutInflater.inflate(R.layout.frg_cmn_user_t2_t3_skill_listitem, null);
				holder = new ViewHolder();
				holder.txt_primarySkillName = (TextView)v.findViewById( R.id.edt_primarySkill);
				holder.txt_secondaryskillName = (TextView)v.findViewById(R.id.edt_secondarySkill);
//				holder.img_edtPrimarySkill = (ImageView)v.findViewById( R.id.img_skill_edit);
//				holder.img_edtSecondarySkill = (ImageView)v.findViewById( R.id.img_secondaryskill_edit);
				holder.txt_headerPrimaryskill = (TextView)v.findViewById(R.id.profile_headerprimaryskill);
				holder.txt_headerSecondarySkill = (TextView)v.findViewById(R.id.txt_header_secondary);
				holder.lyt_primarySkill = (LinearLayout)v.findViewById(R.id.profilelyt_primaryskill);
				holder.secondaryTotalLayout = (LinearLayout)v.findViewById(R.id.secondaryTotalLayout);
				holder.img_deleteSecondaryskill = (ImageView)v.findViewById(R.id.img_secondaryskill_delete);
				holder.txt_addnewSkill = (Button)v.findViewById(R.id.add_newSecondarySkill);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			holder.txt_primarySkillName.setTypeface(mFontLight);
			holder.txt_secondaryskillName.setTypeface(mFontLight);
			holder.txt_headerPrimaryskill.setTypeface(mFontLight);
			holder.txt_headerSecondarySkill.setTypeface(mFontLight);
			holder.txt_addnewSkill.setTypeface(mFontDark);
			
			if(position == 0){
				holder.txt_headerPrimaryskill.setVisibility(View.VISIBLE);
				holder.txt_headerSecondarySkill.setVisibility(View.VISIBLE);
				holder.lyt_primarySkill.setVisibility(View.VISIBLE);
				holder.txt_addnewSkill.setVisibility(View.GONE);
			} else {
				holder.txt_headerPrimaryskill.setVisibility(View.GONE);
				holder.txt_headerSecondarySkill.setVisibility(View.GONE);
				holder.lyt_primarySkill.setVisibility(View.GONE);
				holder.txt_addnewSkill.setVisibility(View.GONE);
			}
			
			if (position == SecondarySkillAl.size()-1) {
				holder.txt_addnewSkill.setVisibility(View.VISIBLE);
			}
			
			if (position == 4) {
				holder.txt_addnewSkill.setText("Maximum secondary skills added");
			} else {
				holder.txt_addnewSkill.setText("Add Secondary Skill");
			}
			
			holder.txt_primarySkillName.setText(strPrimarySkillName);
			String secondarySkill = SecondarySkillAl.get(position);
			if (position == 0 && secondarySkill.equals("No Skills")) {
				holder.secondaryTotalLayout.setVisibility(View.GONE);
			} else {
				holder.txt_secondaryskillName.setText(secondarySkill);
				holder.secondaryTotalLayout.setVisibility(View.VISIBLE);
			}
			
			 //  ArrayAdapter<AbstractSkillSet> adapter = new ArrayAdapter<AbstractSkillSet>  
	          // (getActivity(),android.R.layout.simple_dropdown_item_1line,abstractSkillSet); 
			  // holder.txt_primarySkillName.setThreshold(1);
			   // holder.txt_primarySkillName.setAdapter(adapter);
//			   strSelectedPrimarySkill = holder.txt_primarySkillName.getText().toString();
				/*holder.txt_primarySkillName.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent,
							View view, int position, long id) {
					mAbstractSkillSet = (AbstractSkillSet)parent.getItemAtPosition(position);

					}
				});*/
				//ArrayAdapter<AbstractSkillSet> secondarySkilladapter = new ArrayAdapter<AbstractSkillSet>  
	           // (getActivity(),android.R.layout.simple_dropdown_item_1line,abstractSkillSet); 
				//holder.txt_secondaryskillName.setThreshold(1);
				//holder.txt_secondaryskillName.setAdapter(secondarySkilladapter);
				/*holder.txt_secondaryskillName.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent,
							View view, int position, long id) {
						mAbstractSkillSet = (AbstractSkillSet)parent.getItemAtPosition(position);
					}
				});*/
				
				holder.txt_addnewSkill.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						SelectSecondarySkills();
						/*int lastPosition = SecondarySkillAl.size()-1;
						if(SecondarySkillAl.get(lastPosition).length()>0) {
							if(SecondarySkillAl.size()<5){
								SecondarySkillAl.add("");
								SkillIds.add(2);
								skilladapter.notifyDataSetChanged();
							} else {
								Util.showAlert(activity, "", "Only 5 skills ");
							}
						} else {
							Util.showAlert(activity, "", "please edit the skill");

						}*/
					}
				});
				holder.img_deleteSecondaryskill.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						mListView.setVisibility(View.GONE);
//					Util.setTouchDelay(getActivity(), mListView);
						SecondarySkillAl.remove(position);
						SkillIds.remove(position);
						notifyDataSetChanged();

						JSONObject object = new JSONObject(); 
						try {
							object.put("userId", getHelper().getUserId());
							JSONArray jsonArray = new JSONArray();
						
							ArrayList<UserSkillsSet> userSkillSets = abstractUserProfile.getUserSkillSets();
							ArrayList<UserSkillsSet> skillSetList = new ArrayList<UserSkillsSet>();
							if(userSkillSets != null && userSkillSets.size()  > 0){
								for (UserSkillsSet userSkillsSet : userSkillSets) {
									if(userSkillsSet.isPrimarySkill()){
										skillSetList.add(userSkillsSet);
									}
								}
							}
							
			                for(int i = 0; i < SkillIds.size() ; i++) {
			                    	//AbstractSkillSet tag = (AbstractSkillSet) listview.getItemAtPosition(checked.keyAt(i));
			                    	JSONObject jsonObject = new JSONObject();
			                    	jsonObject.put("Skills", new JSONObject().put("skillId",SkillIds.get(i)));
									jsonObject.put("IsPrimarySkill",false);
									jsonArray.put(jsonObject);
									
									UserSkillsSet skillsSet = new UserSkillsSet();
									skillsSet.setPrimarySkill(false);
									AbstractSkillSet abstractSkillSet = new AbstractSkillSet();
									abstractSkillSet.setSkillId(SkillIds.get(i));
									abstractSkillSet.setSkillName(SecondarySkillAl.get(i));
									skillsSet.setSkills(abstractSkillSet);
									skillSetList.add(skillsSet);
			                }
			                
							object.put("UserSkillSets",jsonArray);
							abstractUserProfile.setUserSkillSets(skillSetList);
			                L.d("NewAL++++"+SecondarySkillAl.toString());
			                skilladapter.notifyDataSetChanged();
							userProfileModel.editUserProfile(Constant.UPDATE_USER_PROFILE,object);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						
					}
				});
				strSelectedSecondarySkill = holder.txt_secondaryskillName.getText().toString();
				/*holder.img_edtPrimarySkill.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(strSelectedPrimarySkill.length()>0 && mAbstractSkillSet!=null){
								if(mAbstractSkillSet != null){
								JSONObject object = new JSONObject(); 
								try {
									object.put("userId", getHelper().getUserId());
									JSONObject jsonObject = new JSONObject();
									JSONArray jsonArray = new JSONArray();
									jsonObject.put("Skills", new JSONObject().put("skillId",mAbstractSkillSet.getSkillId()));
									jsonObject.put("IsPrimarySkill",true);
									jsonArray.put(jsonObject);
									object.put("UserSkillSets",jsonArray);
									
									
									ArrayList<UserSkillsSet> skillSets = abstractUserProfile.getUserSkillSets();
									int index = 0;
									boolean isPrimarySkillfound = false;
									for ( UserSkillsSet userSkillsSets : skillSets) {
										if(userSkillsSets.isPrimarySkill()){
											userSkillsSets.setSkills(mAbstractSkillSet);
											skillSets.set(index, userSkillsSets);	
											isPrimarySkillfound = true;
											break;
										}	
										index++;
									}
									if(!isPrimarySkillfound){
										UserSkillsSet skillsSet = new UserSkillsSet();
										skillsSet.setPrimarySkill(true);
										skillsSet.setSkills(mAbstractSkillSet);
										skillSets.add(skillsSet);
									}
									abstractUserProfile.setUserSkillSets(skillSets);
									strPrimarySkillName = mAbstractSkillSet.getSkillName();
									skilladapter.notifyDataSetChanged();
									userProfileModel.editUserProfile(Constant.UPDATE_USER_PROFILE,object);
									
								} catch (JSONException e) {
									e.printStackTrace();
								}
							
							}else{
								Util.showAlert(getActivity(), "", "Please select your skillset");
							}
							
						}else{
							Util.showAlert(activity, "Message", "Please edit the skill");
						}
					}
				});*/
				
			/*holder.img_edtSecondarySkill.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//SparseBooleanArray checked = listview.getCheckedItemPositions();
					if(strSelectedSecondarySkill.length()>0 && mAbstractSkillSet!=null){
					if(SecondarySkillAl.size() <= 5) {
						
						JSONObject object = new JSONObject(); 
						try {
							object.put("userId", getHelper().getUserId());
							
							JSONArray jsonArray = new JSONArray();
						
							ArrayList<UserSkillsSet> userSkillSets = abstractUserProfile.getUserSkillSets();
							ArrayList<UserSkillsSet> skillSetList = new ArrayList<UserSkillsSet>();
							if(userSkillSets != null && userSkillSets.size()  > 0){
								for (UserSkillsSet userSkillsSet : userSkillSets) {
									if(userSkillsSet.isPrimarySkill()){
										skillSetList.add(userSkillsSet);
									}
								}
							}
							L.d("SecondarySkillALbeforeadding+++"+SecondarySkillAl);

							SecondarySkillAl.set(position,mAbstractSkillSet.getSkillName());
							L.d("SkillIdAL+++"+SkillIds.toString());
							L.d("SecondarySkillALafteradding+++"+SecondarySkillAl);

							if(!SkillIds.contains(mAbstractSkillSet.getSkillId())){
								SkillIds.set(position,mAbstractSkillSet.getSkillId());
							}else{
								SkillIds.remove(position);
							}
							L.d("SkillIdALafteradding+++"+SkillIds.toString());

			                for(int i = 0; i < SkillIds.size() ; i++)
			                {
			                    
			                    	//AbstractSkillSet tag = (AbstractSkillSet) listview.getItemAtPosition(checked.keyAt(i));
			                    	JSONObject jsonObject = new JSONObject();
			                    	jsonObject.put("Skills", new JSONObject().put("skillId",SkillIds.get(i)));
									jsonObject.put("IsPrimarySkill",false);
									jsonArray.put(jsonObject);
									
									UserSkillsSet skillsSet = new UserSkillsSet();
									skillsSet.setPrimarySkill(false);
									AbstractSkillSet abstractSkillSet = new AbstractSkillSet();
									abstractSkillSet.setSkillId(SkillIds.get(i));
									abstractSkillSet.setSkillName(SecondarySkillAl.get(i));
									skillsSet.setSkills(abstractSkillSet);
									skillSetList.add(skillsSet);
			
			                    
			                }
			                
							object.put("UserSkillSets",jsonArray);
					
							abstractUserProfile.setUserSkillSets(skillSetList);
							
			                L.d("NewAL++++"+SecondarySkillAl.toString());
			
			                skilladapter.notifyDataSetChanged();
			
							userProfileModel.editUserProfile(Constant.UPDATE_USER_PROFILE,object);
							
						} catch (JSONException e) {
							e.printStackTrace();
						}
						
					}else{
						
						Util.showAlert(getActivity(), "", "You cannot select more than 5 skills");
						
					}
				}else{
					Util.showAlert(getActivity(), "", "Please edit the skill");
				}
				}
			});*/
			return v;
		}
		class ViewHolder {
			private TextView txt_headerPrimaryskill,txt_headerSecondarySkill;
			private ImageView img_deleteSecondaryskill;
			private LinearLayout lyt_primarySkill,secondaryTotalLayout;
			private TextView txt_primarySkillName,txt_secondaryskillName;
			private Button txt_addnewSkill;
		}
		
}
	public void updateUserInfoDetails(AbstractUserProfile abstractProfile){
		
		SecondarySkillAl = new ArrayList<String>();
		SkillIds = new ArrayList<Integer>();
		
		if(abstractProfile.getUserSkillSets() != null && abstractProfile.getUserSkillSets().size() > 0){
			
			new StringBuilder();

			for(UserSkillsSet mAbstractSkillSet : abstractProfile.getUserSkillSets()){
				if(mAbstractSkillSet.isPrimarySkill()){
					
					mSelectedPrimartSkillName = mAbstractSkillSet.getSkills().getSkillName();
					mSelectedPrimaryskillId = mAbstractSkillSet.getSkills().getSkillId();
					mPrimarySelectedPos = SkillIds.indexOf(mSelectedPrimaryskillId) ; 
			
				}else if(!mAbstractSkillSet.isPrimarySkill()){
					SecondarySkillAl.add(mAbstractSkillSet.getSkills().getSkillName());
					SkillIds.add(mAbstractSkillSet.getSkills().getSkillId());
				}
			}
			L.d("UpdateSecondaryskill+++"+SecondarySkillAl.toString());
			if(SecondarySkillAl.size()==0){
				SecondarySkillAl.add("No Skills");
				SkillIds.add(1);
			}
			
			if(mSelectedPrimartSkillName != null){
				strPrimarySkillName = mSelectedPrimartSkillName;

			}else{
				strPrimarySkillName = "No Skill";
			}
		}
	}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if (event.getPropertyName().equals("edituserprofile")) {
			mListView.setVisibility(View.VISIBLE);
			AbstractEditUserProfile profileUpdate = userProfileModel.getUserEditedProfile();
			if (profileUpdate != null) {
				if (profileUpdate.getStatus() == 1) {
				
					getHelper().insertUserProfile(abstractUserProfile);
					
					updateUserInfoDetails(abstractUserProfile);

					L.ToastMessage(getActivity(), "Profile upated successfully");
					
					skilladapter.notifyDataSetChanged();
				}else{
					
				}
			} else {
				Util.showAlert(getActivity(), "Message","Request Failed.");
			}
		} 
	}
	@Override
	public void onResume() {
		super.onResume();
		userProfileModel.addChangeListener(this);
		skilladapter.notifyDataSetChanged();
		
	}

	@Override
	public void onPause() {
		super.onPause();
		userProfileModel.removeChangeListener(this);
		
	}
	
	@Override
	public void preferenceChange(PreferenceChangeEvent pce) {

	}
	void SelectSecondarySkills(){
		if(SecondarySkillAl.size() < 5){
			
			final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			final AlertDialog dialog = builder.create();
			//alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
			builder.setCancelable(true);
			//alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			LayoutInflater inflater = getActivity().getLayoutInflater();
			final View dialoglayout = inflater.inflate(R.layout.view_dg_skills_list, null);
			final ListView listview = (ListView) dialoglayout.findViewById(R.id.user_profile_skills_list);
			final EditText searchSkills = (EditText) dialoglayout.findViewById(R.id.searchSkills);
			final Button submitBtn = (Button) dialoglayout.findViewById(R.id.sumbitBtn);
			listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			
			abstractSkillSet = getHelper().getDBSkillset();
			checkedList = 0;
			if(abstractSkillSet != null && abstractSkillSet.size() > 0){
				int index = 0;
				for(AbstractSkillSet skillsSet : abstractSkillSet){
					for(UserSkillsSet selectedSkillSets : abstractUserProfile.getUserSkillSets()){
						if(skillsSet.getSkillId() == selectedSkillSets.getSkills().getSkillId()){
							if(!selectedSkillSets.isPrimarySkill()){
								skillsSet.setSelected(true);
								abstractSkillSet.set(index, skillsSet);
								checkedList++;
							}
						}
					}
					index ++ ;
				}
			}
			
			final SkillAdapter adapter = new SkillAdapter(getActivity(),abstractSkillSet);
			listview.setAdapter(adapter);
			
			searchSkills.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					 String text = searchSkills.getText().toString().toLowerCase();
                     adapter.filter(text);		
				}
			});
			
			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					
				}
			});
			
			submitBtn.setOnClickListener( new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(listview.getCount() > 0){
						searchSkills.setText("");
						ArrayList<AbstractSkillSet> arrayList = new ArrayList<AbstractSkillSet>();
	                    ArrayList<AbstractSkillSet> arraydata = (ArrayList<AbstractSkillSet>) adapter.arraylistData;

	                    for (int i = 0; i < arraydata.size(); i++) {
	                    	AbstractSkillSet bean = arraydata.get(i);
	                        if (bean.isSelected()) {
	                           arrayList.add(arraydata.get(i));
	                        }
	                    }     
	                    
	                    if (arrayList != null && arrayList.size() == 0) {
							Util.showAlert(getActivity(), "", "No skills selected");
	                    	return;
						}
						
						if(arrayList.size() <= 5) {
							
							JSONObject object = new JSONObject(); 
							try {
								object.put("userId", getHelper().getUserId());
								
								JSONArray jsonArray = new JSONArray();
							
								ArrayList<UserSkillsSet> userSkillSets = abstractUserProfile.getUserSkillSets();
								ArrayList<UserSkillsSet> skillSetList = new ArrayList<UserSkillsSet>();
								if(userSkillSets != null && userSkillSets.size()  > 0){
									for (UserSkillsSet userSkillsSet : userSkillSets) {
										if(userSkillsSet.isPrimarySkill()) {
											skillSetList.add(userSkillsSet);
										}
									}
								}
				                
								for(int i = 0; i < arrayList.size() ; i++) {
				                    if (arrayList.get(i).isSelected()) {
				                    	AbstractSkillSet tag = (AbstractSkillSet) arrayList.get(i);
				                    	JSONObject jsonObject = new JSONObject();
				                    	jsonObject.put("Skills", new JSONObject().put("skillId",tag.getSkillId()));
										jsonObject.put("IsPrimarySkill",false);
										jsonArray.put(jsonObject);
										
										UserSkillsSet skillsSet = new UserSkillsSet();
										skillsSet.setPrimarySkill(false);
										skillsSet.setSkills(tag);
										skillSetList.add(skillsSet);
				                    }
				                }
				                
								object.put("UserSkillSets",jsonArray);
						
								abstractUserProfile.setUserSkillSets(skillSetList);
								
								userProfileModel.editUserProfile(Constant.UPDATE_USER_PROFILE,object);
								
							} catch (JSONException e) {
								e.printStackTrace();
							}
							
						} else {
							Util.showAlert(getActivity(), "", "You cannot select more than 5 skills");
						}
						
						if (dialog.isShowing()) {
							dialog.dismiss();
						}		
					}else{
						Util.showAlert(getActivity(), "", "No search results found");
					}
				}
			});
			
			dialog.setView(dialoglayout);
			dialog.show();
			
		} else {
			Util.showAlert(getActivity(), "", "You cannot select more than 5 skills");
		}	
	}

	public class SkillAdapter extends BaseAdapter {
		private Context mContext;
		private LayoutInflater inflater;
		private ArrayList<AbstractSkillSet> arraylist, arraylistData;

		public SkillAdapter(Context context,ArrayList<AbstractSkillSet> listbean) {
			mContext = context;
			this.arraylistData = listbean;
			inflater = LayoutInflater.from(mContext);

			this.arraylist = new ArrayList<AbstractSkillSet>();
			this.arraylist.addAll(listbean);

		}

		public class ViewHolder {
			TextView txtItemName;
			ImageView imgvCheck;
		}

		@Override
		public int getCount() {
			return arraylistData.size();
		}

		@Override
		public AbstractSkillSet getItem(int position) {
			return arraylistData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View view, ViewGroup parent) {
			final ViewHolder holder;
			if (view == null) {
				holder = new ViewHolder();
				view = inflater.inflate(R.layout.view_dg_skills_list_item, null);
				holder.txtItemName = (TextView) view.findViewById(R.id.txtItemName);
				holder.imgvCheck = (ImageView) view.findViewById(R.id.imgvCheck);
				view.setTag(holder);

			} else {
				holder = (ViewHolder) view.getTag();
			}

			try {
				AbstractSkillSet bean = arraylistData.get(position);

				if (bean.isSelected())
					holder.imgvCheck.setSelected(true);
				else
					holder.imgvCheck.setSelected(false);

				holder.txtItemName.setText(bean.getSkillName());
				holder.txtItemName.setSelected(true);
				holder.txtItemName.setTypeface(mFontDark);
				view.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						for (int i = 0; i < arraylistData.size(); i++) {
							AbstractSkillSet bean = arraylistData.get(position);
							if (i == position) {
								if (bean.isSelected()) {
									bean.setSelected(false);
									checkedList--;
									L.d("NotSelected");
									
								} else {
									L.d("checkedListSize++"+checkedList);
									if (checkedList < 5) {
										bean.setSelected(true);
										checkedList++;
										L.d("Seleetced");
									} else {
										Util.showAlert(mContext, "", "You can add only 5 secondary skills");
									}
								}
							}
							notifyDataSetChanged();
						}
					}
				});
				holder.imgvCheck.setTag(bean);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return view;
		}

		public void filter(String charText) {
			charText = charText.toLowerCase();
			arraylistData.clear();
			if (charText.length() == 0) {
				arraylistData.addAll(arraylist);
			} else {
				for (AbstractSkillSet contact : arraylist) {
					if (contact.getSkillName().toLowerCase().contains(charText)) {
						arraylistData.add(contact);
					}
				}
			}
			notifyDataSetChanged();
		}
	}
}

package com.strobilanthes.forrestii.model.viewreviews;

import java.io.Serializable;

public class AbstractReviewQuickcard implements Serializable{

	private static final long serialVersionUID = 1L;

		private String lastName;

	    private String reviewCount;

	    private String quickCardId;

	    private String emailAddress;

	    private float totalRating;

	    private String firstName;

	    private String mobileNumber;

	    private long updatedOn;

	    public String getLastName ()
	    {
	        return lastName;
	    }

	    public void setLastName (String lastName)
	    {
	        this.lastName = lastName;
	    }

	    public String getReviewCount ()
	    {
	        return reviewCount;
	    }

	    public void setReviewCount (String reviewCount)
	    {
	        this.reviewCount = reviewCount;
	    }

	    public String getQuickCardId ()
	    {
	        return quickCardId;
	    }

	    public void setQuickCardId (String quickCardId)
	    {
	        this.quickCardId = quickCardId;
	    }

	    public String getEmailAddress ()
	    {
	        return emailAddress;
	    }

	    public void setEmailAddress (String emailAddress)
	    {
	        this.emailAddress = emailAddress;
	    }

	    public float getTotalRating ()
	    {
	        return totalRating;
	    }

	    public void setTotalRating (float totalRating)
	    {
	        this.totalRating = totalRating;
	    }

	    public String getFirstName ()
	    {
	        return firstName;
	    }

	    public void setFirstName (String firstName)
	    {
	        this.firstName = firstName;
	    }

	    public String getMobileNumber ()
	    {
	        return mobileNumber;
	    }

	    public void setMobileNumber (String mobileNumber)
	    {
	        this.mobileNumber = mobileNumber;
	    }

	    public long getUpdatedOn ()
	    {
	        return updatedOn;
	    }

	    public void setUpdatedOn (long updatedOn)
	    {
	        this.updatedOn = updatedOn;
	    }
}

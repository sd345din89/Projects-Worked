package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.SparseArrayCompat;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;
import com.nineoldandroids.view.ViewHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.fragments.ProfileBasicInfoFragment;
import com.strobilanthes.forrestii.act.fragments.ProfileSkillsFragment;
import com.strobilanthes.forrestii.act.fragments.ScrollTabHolderFragment;
import com.strobilanthes.forrestii.cropimage.ChooserType;
import com.strobilanthes.forrestii.cropimage.ChosenImage;
import com.strobilanthes.forrestii.cropimage.ImageChooserListener;
import com.strobilanthes.forrestii.cropimage.ImageChooserManager;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.general.AbstractDashboard;
import com.strobilanthes.forrestii.model.skillset.UserSkillsSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.userprofile.UserProfileImpl;
import com.strobilanthes.forrestii.model.viewreviews.AbstractReviewProfile;
import com.strobilanthes.forrestii.model.viewreviews.AbstractViewReviews;
import com.strobilanthes.forrestii.slidingtab.AlphaForegroundColorSpan;
import com.strobilanthes.forrestii.slidingtab.PagerSlidingTabStrip;
import com.strobilanthes.forrestii.slidingtab.ScrollTabHolder;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class UserProfileActivity extends BaseActivity<DBAccess> implements OnClickListener,ScrollTabHolder, ViewPager.OnPageChangeListener,ImageChooserListener,PropertyChangeListener{
	
	private View mHeader;

	private PagerSlidingTabStrip mPagerSlidingTabStrip;
	private ViewPager mViewPager;
	private PagerAdapter mPagerAdapter;
	
	private AbstractUserProfile abstractUserProfile;
	private DisplayImageOptions optionsImgCard;
	private AbstractDashboard abstractDashboard;
	private String strPrimarySkillName = null;
	private Menu optionsMenu;
	@InjectView(R.id.profile_img_card)				private ImageView profile_imgCard;
	@InjectView(R.id.profile_userName)				private TextView profile_txtUserName;
	@InjectView(R.id.profile_skillName)				private TextView profile_txtSkillName;
	@InjectView(R.id.profile_ratingBar)				private RatingBar profileRatingBar;
	@InjectView(R.id.profile_points)				private TextView profile_txtPoints;
	@InjectView(R.id.profile_cards_count)			private TextView profile_commonCards;
	@InjectView(R.id.Lyt_profileContacts)			private LinearLayout profilelyt_common_cards;
	@InjectView(R.id.profile_img_user)				private RoundedImageView profile_img_user;
	@InjectView(R.id.profile_referedin_count)		private TextView profile_referedInCount;
	@InjectView(R.id.profile_referedout_count)		private TextView profile_referedOutCount;
	@Inject UserProfileImpl userProfileModel;

	//@InjectView(R.id.progressBar)                  private ProgressBar pbar;
	//@Inject ViewReviews viewReviewsModel;
	private int mMinHeaderHeight;
	private int mHeaderHeight;
	private int mMinHeaderTranslation;
	private Typeface mFontLight,mFontDark;

	private TypedValue mTypedValue = new TypedValue();
	private SpannableString mSpannableString;
	private AlphaForegroundColorSpan mAlphaForegroundColorSpan;
	ProfileBasicInfoFragment basicInfoFragment;
	private ArrayList<String>profileSecondarySkillsAL = new ArrayList<String>();
	private ArrayList<String>TotalSkillAL = new ArrayList<String>();
	private ArrayList<Integer>SkillIds = new ArrayList<Integer>();
	
	protected Uri mImageCaptureUri;
	private String mFirstName=null, mLastName=null;
	private ArrayList<AbstractReviewProfile> mAbstractViewReviews;

	private int chooserType;

	private ImageChooserManager imageChooserManager;

	private String filePath;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mMinHeaderHeight = getResources().getDimensionPixelSize(R.dimen.up_min_header_height);
		mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.up_header_height);
		mMinHeaderTranslation = -mMinHeaderHeight ;
		
		setContentView(R.layout.act_user_profile);
		fontStyles();
		getActionBar().setTitle("User Profile");
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(true);

		mHeader = findViewById(R.id.header);

		mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setOffscreenPageLimit(3);

		mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
		mPagerAdapter.setTabHolderScrollingContent(this);

		mViewPager.setAdapter(mPagerAdapter);
		mPagerSlidingTabStrip.setTextSize((int)Util.getDimensPxtoDp(14, this));
	    mPagerSlidingTabStrip.setTypeface(mFontDark, Typeface.NORMAL);
		mPagerSlidingTabStrip.setViewPager(mViewPager);
		mPagerSlidingTabStrip.setOnPageChangeListener(this);
		mSpannableString = new SpannableString(getString(R.string.app_name));
		mAlphaForegroundColorSpan = new AlphaForegroundColorSpan(0xffffffff);
		
//		ViewHelper.setAlpha(getActionBarIconView(), 0f);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
//		getSupportActionBar().setBackgroundDrawable(null);

		optionsImgCard = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.card_default3)
		.showImageForEmptyUri(R.drawable.card_default3)
		.showImageOnFail(R.drawable.card_default3)
		.cacheInMemory(true).cacheOnDisc(true).considerExifParams(true)
		//.displayer(new RoundedBitmapDisplayer(60))
		.build();
		profilelyt_common_cards.setOnClickListener(this);
		
		updateProfile();
		showProfileReviews();
		profile_img_user.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AlertDialog.Builder alertbuilder = new AlertDialog.Builder(v.getContext(),AlertDialog.THEME_HOLO_LIGHT);
				alertbuilder.setTitle("");
				alertbuilder.setMessage("Change profile photo");
				
				alertbuilder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						takePicture();
					}
				});
				alertbuilder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						chooseImage();
					}
				});
				
				alertbuilder.show();
				
			}
		
		});
		
	}
	public void showProfileReviews(){
			JSONObject json = new JSONObject();
			try {
				json.put("userProfile",new JSONObject().put("userId", getHelper().getUserId()));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			userProfileModel.viewUserProfileReviews(Constant.VIEW_REFERRALS_REVIEW, json);
		}
	@Override
	protected void onResume() {
		super.onResume();
		userProfileModel.addChangeListener(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		userProfileModel.removeChangeListener(this);

	}

	public void updateProfile() {
		
		abstractUserProfile = getHelper().getDBUserProfile();
		abstractDashboard = getHelper().getDashBoard();
		
		getSupportActionBar().setTitle(abstractUserProfile.getFirstName());
		
		if(abstractUserProfile!=null && abstractDashboard!=null){
			profile_txtUserName.setSelected(true);
			if(abstractUserProfile.getFirstName()!=null){
				mFirstName = abstractUserProfile.getFirstName();
			}
			if(abstractUserProfile.getLastName()!=null){
				mLastName = abstractUserProfile.getLastName();
			}else{
				mLastName = "";
			}
			
			profile_txtUserName.setText(mFirstName + " "+mLastName);
			
			ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + abstractUserProfile.getPhotoID(), profile_img_user,
					null, animateFirstListener);
			
			ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_CARD_NAME + abstractUserProfile.getCardName(), profile_imgCard,
					optionsImgCard, animateFirstListener);
			
			SharedPreferences sharedPreferences = getSharedPreferences("Forrestii", Context.MODE_PRIVATE);
   	     	int coinsCount = sharedPreferences.getInt("coins", 0);
   	     	
			profile_txtPoints.setText(""+coinsCount);
			
			//profile_txtPoints.setText(""+(abstractUserProfile.getTotalCoin()) != null ? ""+ abstractUserProfile.getTotalCoin() : "0");
			profile_referedInCount.setText(abstractDashboard.getReferralIn() !=null ? abstractDashboard.getReferralIn():"0");
			profile_referedOutCount.setText(abstractDashboard.getReferralOut() !=null ? abstractDashboard.getReferralOut():"0");
			profile_commonCards.setText(abstractDashboard.getContactCount() !=null ? abstractDashboard.getContactCount():"0");
			profileRatingBar.setRating(abstractUserProfile.getTotalRating());
			
			

			if(abstractUserProfile.getUserSkillSets() != null && abstractUserProfile.getUserSkillSets().size()>0){
				for (UserSkillsSet userSkillsSet:abstractUserProfile.getUserSkillSets()) {
					if(userSkillsSet.isPrimarySkill()){
//						strPrimarySkillName = userSkillsSet.getSkills().getSkillName();
						profile_txtSkillName.setSelected(true);
						profile_txtSkillName.setText(""+userSkillsSet.getSkills().getSkillName());
					} else if(!userSkillsSet.isPrimarySkill()){
						profileSecondarySkillsAL.add(userSkillsSet.getSkills().getSkillName());
						SkillIds.add(userSkillsSet.getSkills().getSkillId());
						//strSecondarySkillName = strSecondary.substring(1,strSecondary.length()-1);
					}else{
						//strSecondarySkillName = "No Skills";
					}
				}
			}
//			profile_txtSkillName.setText(""+(strPrimarySkillName != null ? strPrimarySkillName : "No Skill"));
		}else{
			profile_txtUserName.setText("Forrestii User");
			profile_referedInCount.setText("0");
			profile_referedOutCount.setText("0");
			profile_commonCards.setText("0");
			
			
		}
		if(profileSecondarySkillsAL.size()>0){
			for (int i = 0; i < profileSecondarySkillsAL.size(); i++) {
				TotalSkillAL.add(profileSecondarySkillsAL.get(i));
			}
			}else{
				TotalSkillAL.add("No Skills");
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		case R.id.ReviewsandRatings:
			if(mAbstractViewReviews!=null && mAbstractViewReviews.size()>0){
				Intent intent = new Intent(UserProfileActivity.this, UserRatingReviewActivity.class);
				Bundle userObject = new Bundle();
				userObject.putSerializable("UserDetails", mAbstractViewReviews);
				userObject.putString("reviews_view", "UserProfileReviews");
				intent.putExtras(userObject);
				startActivity(intent);
			}else{
				Util.showAlert(UserProfileActivity.this, "", "No reviews");
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		finish();		
		super.onBackPressed();
	}
	public void setRefreshActionButtonState(final boolean refreshing) {
	    if (optionsMenu != null) {
	        final MenuItem refreshItem = optionsMenu
	            .findItem(R.id.ic_refresh);
	        if (refreshItem != null) {
	            if (refreshing) {
	                refreshItem.setActionView(R.layout.actionbar_refresh_layout);
	            } else {
	                refreshItem.setActionView(null);
	            }
	        }
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		this.optionsMenu = menu;

		 MenuInflater inflater = getSupportMenuInflater();
		    inflater.inflate(R.menu.aactionbar_menu, menu);
		return true;
	}
	
	public void fontStyles() {

			mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
			mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		 
			profile_txtUserName.setTypeface(mFontLight);
			profile_txtSkillName.setTypeface(mFontDark);
			profile_txtPoints.setTypeface(mFontDark);
			profile_commonCards.setTypeface(mFontDark);
			profile_referedInCount.setTypeface(mFontDark);
			profile_referedOutCount.setTypeface(mFontDark);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.Lyt_profileContacts:
			ArrayList<FriendsList> friendsLists = getHelper().getDBT2Friends();
			if(friendsLists != null && friendsLists.size() > 0){
				Intent intent = new Intent(UserProfileActivity.this,FriendsListActivity.class);
				intent.putExtra("profile_view", "T2");
				startActivity(intent);
//				finish();
			}else{
				Util.showAlert(UserProfileActivity.this, "", "No contacts found");
			}
			
			break;

		default:
			break;
		}
		
	}
	/**************************Parallax****************************/
	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int position) {
		SparseArrayCompat<ScrollTabHolder> scrollTabHolders = mPagerAdapter.getScrollTabHolders();
		ScrollTabHolder currentHolder = scrollTabHolders.valueAt(position);

		currentHolder.adjustScroll((int) (mHeader.getHeight() + ViewHelper.getTranslationY(mHeader)));
	}

	@Override
	public void adjustScroll(int scrollHeight) {
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount, int pagePosition) {
		if (mViewPager.getCurrentItem() == pagePosition) {
			int scrollY = getScrollY(view);
			ViewHelper.setTranslationY(mHeader, Math.max(-scrollY, mMinHeaderTranslation));
//			float ratio = clamp(ViewHelper.getTranslationY(mHeader) / mMinHeaderTranslation, 0.0f, 1.0f);
//			interpolate(mHeaderLogo, getActionBarIconView(), sSmoothInterpolator.getInterpolation(ratio));
//			setTitleAlpha(clamp(5.0F * ratio - 4.0F, 0.0F, 1.0F));
		}
	}
	
	public int getScrollY(AbsListView view) {
		View c = view.getChildAt(0);
		if (c == null) {
			return 0;
		}

		int firstVisiblePosition = view.getFirstVisiblePosition();
		int top = c.getTop();

		int headerHeight = 0;
		if (firstVisiblePosition >= 1) {
			headerHeight = mHeaderHeight;
		}

		return -top + firstVisiblePosition * c.getHeight() + headerHeight;
	}
	
	public static float clamp(float value, float max, float min) {
		return Math.max(Math.min(value, min), max);
	}

	private void setTitleAlpha(float alpha) {
		mAlphaForegroundColorSpan.setAlpha(alpha);
		mSpannableString.setSpan(mAlphaForegroundColorSpan, 0, mSpannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		getSupportActionBar().setTitle(mSpannableString);
	}
	
	private ImageView getActionBarIconView() {
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			return (ImageView)findViewById(android.R.id.home);
		}

		return (ImageView)findViewById(android.R.id.home);
	}
	
	public class PagerAdapter extends FragmentPagerAdapter {

		private SparseArrayCompat<ScrollTabHolder> mScrollTabHolders;
		private final String[] TITLES = { "Basic Info","Skills"};
		private ScrollTabHolder mListener;

		public PagerAdapter(FragmentManager fm) {
			super(fm);
			mScrollTabHolders = new SparseArrayCompat<ScrollTabHolder>();
		}

		public void setTabHolderScrollingContent(ScrollTabHolder listener) {
			mListener = listener;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return TITLES[position];
		}

		@Override
		public int getCount() {
			return TITLES.length;
		}

		@Override
		public Fragment getItem(int position) {
			ScrollTabHolderFragment fragment = null;
			
			
			switch (position) {
	
			case 0:
				fragment = (ScrollTabHolderFragment) new ProfileBasicInfoFragment();				
					Bundle args = new Bundle();
					args.putInt(ProfileBasicInfoFragment.ARG_POSITION, position);
					args.putSerializable("profile", abstractUserProfile);
					fragment.setArguments(args);
					//mScrollTabHolders.put(position, basicInfoFragment);
				break;
			case 1:
				fragment = (ScrollTabHolderFragment) new ProfileSkillsFragment();
				Bundle b = new Bundle();
				b.putInt(ProfileSkillsFragment.ARG_POSITION, position);
	
				b.putSerializable("secondaryskill", TotalSkillAL); 
				b.putString("primarySkill", ""+(strPrimarySkillName != null ? strPrimarySkillName : "No Skill"));
				b.putSerializable("SecondaryskillIds", SkillIds);
				fragment.setArguments(b);
				break;
				
			}
			mScrollTabHolders.put(position, fragment);
			if (mListener != null) {
				fragment.setScrollTabHolder(mListener);
			}
			return fragment;
		}

		public SparseArrayCompat<ScrollTabHolder> getScrollTabHolders() {
			return mScrollTabHolders;
		}

	}
	/**************************Parallax****************************/
	private void userImageUpload(final Bitmap rotatedBitmap){
		new AlertDialog.Builder(UserProfileActivity.this,AlertDialog.THEME_HOLO_LIGHT)
		.setMessage("Do you want to upload this image?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();
				profile_img_user.setImageBitmap(rotatedBitmap);
				
				new ProfileImageUpload().execute(byteArray);
			}
		}).setNegativeButton("Cancel",null).show();	
		
	}
	private void chooseImage() {
		chooserType = ChooserType.REQUEST_PICK_PICTURE;
		imageChooserManager = new ImageChooserManager(this,
				ChooserType.REQUEST_PICK_PICTURE, "myfolder", true);
		imageChooserManager.setImageChooserListener(UserProfileActivity.this);
		try {
		//pbar.setVisibility(View.VISIBLE);
			
			filePath = imageChooserManager.choose();
			setRefreshActionButtonState(true);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void takePicture() {
		chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
		imageChooserManager = new ImageChooserManager(this,
				ChooserType.REQUEST_CAPTURE_PICTURE, "myfolder", true);
		imageChooserManager.setImageChooserListener(this);
		try {
			//pbar.setVisibility(View.VISIBLE);
			filePath = imageChooserManager.choose();
			setRefreshActionButtonState(true);

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void reinitializeImageChooser() {
		imageChooserManager = new ImageChooserManager(this, chooserType,
				"myfolder", true);
		imageChooserManager.setImageChooserListener(this);
		imageChooserManager.reinitialize(filePath);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK
				&& (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
			if (imageChooserManager == null) {
				reinitializeImageChooser();
			}
			imageChooserManager.submit(requestCode, data);
		} else {
			//pbar.setVisibility(View.GONE);
			setRefreshActionButtonState(false);
		}
	}
	class ProfileImageUpload extends AsyncTask<byte[], Void, String> {
		
		private CustomProgressbar progressBar;

		@Override
		protected void onPreExecute() {
			progressBar = new CustomProgressbar(UserProfileActivity.this);
			progressBar.show();
		}

		@Override
		protected String doInBackground(byte[]... params) {

			String response = Util.uploadImage(getHelper().getDBUserProfile().getUserId(), Constant.USER_IMAGE_UPLOAD, params[0]);
			
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			if (progressBar != null)
				progressBar.dismiss();
			
			if(result != null){
				try {
					JSONObject jsonObject = new JSONObject(result);
					int status = jsonObject.getInt("status");
					if(status == 1){
						String photoId = jsonObject.getString("response");
						AbstractUserProfile userProfile = getHelper().getDBUserProfile();
						userProfile.setPhotoID(photoId);
						getHelper().insertUserProfile(userProfile);
						L.ToastMessage(UserProfileActivity.this, "Successfully Image Uploaded");
					}else{
						Util.showAlert(UserProfileActivity.this,"Message", ""+jsonObject.getString("error"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
			}else{
				Util.showAlert(UserProfileActivity.this,"Message", "Request Failed.");
			}	
		}
	}
	@Override
	public void onImageChosen(final ChosenImage image) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				//pbar.setVisibility(View.GONE);
				setRefreshActionButtonState(false);
				int size = (int) Util.getDimensPxtoDp(70,UserProfileActivity.this); 
				Bitmap bitmap = null;
				if(image.getFilePathOriginal() != null)
					bitmap = Util.decodeScaledBitmapFromSdCard(image.getFilePathOriginal(),size);
				
				if(bitmap != null){
					Bitmap rotatedBitmap = Util.getRotatedBitmap(image.getFilePathOriginal(), bitmap);
					userImageUpload(rotatedBitmap);
				} else {
					Util.showAlert(UserProfileActivity.this, "", "File size is too large");
				}
//				profile_img_user.setImageURI(Uri.parse(new File(image
//						.getFileThumbnailSmall()).toString()));
			}
		});
	}

	@Override
	public void onError(final String reason) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				//pbar.setVisibility(View.GONE);
				setRefreshActionButtonState(false);
				Toast.makeText(UserProfileActivity.this, "Failed",
						Toast.LENGTH_LONG).show();
			}
		});
	}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("UserProfileReviews")){
		AbstractViewReviews abstractReviewList = userProfileModel.getReviewsRatings();
			if (abstractReviewList != null) {
				if (abstractReviewList.getStatus() == 1) {
					
					mAbstractViewReviews = abstractReviewList.getResponse();
					
				} else {
					//Util.showAlert(this, "", abstractReviewList.getError());
				}
			}else {
				Util.showAlert(this, "Message", "Request Failed");
				//finish();
			}
		}
	}
}

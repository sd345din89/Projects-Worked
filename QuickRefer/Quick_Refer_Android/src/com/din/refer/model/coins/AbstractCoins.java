package com.strobilanthes.forrestii.model.coins;


public class AbstractCoins {
	
	private int coinTypeId;
	private String coinTypeName;
	private int coinCount;
	
	public int getCoinTypeId() {
		return coinTypeId;
	}
	public void setCoinTypeId(int coinTypeId) {
		this.coinTypeId = coinTypeId;
	}
	public String getCoinTypeName() {
		return coinTypeName;
	}
	public void setCoinTypeName(String coinTypeName) {
		this.coinTypeName = coinTypeName;
	}
	public int getCoinCount() {
		return coinCount;
	}
	public void setCoinCount(int coinCount) {
		this.coinCount = coinCount;
	}
}

package com.strobilanthes.forrestii.model.friendslist;

import java.io.Serializable;
import java.util.ArrayList;

public class AbstractFriendsList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String error;
	private int status;
	private ArrayList<FriendsList> response = new ArrayList<FriendsList>();

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ArrayList<FriendsList> getFriendsListResponse() {
		return response;
	}

	public void setFriendsListResponse(ArrayList<FriendsList> response) {
		this.response = response;
	}

}

package com.strobilanthes.forrestii.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class WifiReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {     
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE); 
        NetworkInfo netInfo = conMan.getActiveNetworkInfo();
        if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI ){ 
           // Log.d("WifiReceiver", "Have Wifi Connection");
        	//L.ToastMessage(context, "Network Type : " +netInfo.getType());
        }else {
           // Log.d("WifiReceiver", "Don't have Wifi Connection");
            //L.ToastMessage(context, "Don't have Wifi Connection");
        }
    }   
}
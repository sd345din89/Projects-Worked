package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;

public class SettingsEmailFragment extends BaseFragment<DBAccess> {


	private Typeface mFontLight,mFontDark;
   
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frg_settings_email, null);
		
		super.initilizeContents();
		
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
	
		return view;
	}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		default:
			break;
		}
	}
}
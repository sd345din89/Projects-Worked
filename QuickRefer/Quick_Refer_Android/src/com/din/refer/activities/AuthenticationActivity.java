package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import roboguice.inject.InjectView;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.login.AbstractLogin;
import com.strobilanthes.forrestii.model.pwd_reg.AbstractPasswordReg;
import com.strobilanthes.forrestii.model.signup.AbstractSignup;
import com.strobilanthes.forrestii.util.CirclePageIndicator;

public class AuthenticationActivity extends BaseActivity<DBAccess> implements PropertyChangeListener,
																					ConnectionCallbacks,
																					OnConnectionFailedListener,
																					LocationListener{
	@InjectView(R.id.auth_screen_btnSignUp)    		private Button  btn_SignUp;
	@InjectView(R.id.auth_screen_btnLogin)   	  	private Button  btn_Login;
	@InjectView(R.id.pager)							private ViewPager viewPager;
	@InjectView(R.id.indicator)                     private CirclePageIndicator mIndicator;

	private Typeface mFontDark;
	private PagerAdapter adapter;
	private int[] imgSlide;
	private String[] txtTitle, txtHint;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.act_auth);
		imgSlide = new int[] {R.drawable.slide_1,
				R.drawable.slide_2,  R.drawable.slide_3, R.drawable.slide_4, R.drawable.slide_5 };
		
		txtHint = new String[] {"Forrestii welcomes you. \nSwipe for a tour...","Showcase your talent & skills \nwith our custom business cards","Shake your phone to find new \nconnections & share your card.","Help & Be Helped by Giving & \nGetting trusted referrals.", "Get multiple rewards for giving \nand getting referrals!"};
		txtTitle = new String[] {"Trust Powered Refferals","CREATE","CONNECT","REFER", "REWARDS"};
		getSupportActionBar().hide();
		
		fontStyles();
		
		adapter = new ViewPagerAdapter(AuthenticationActivity.this,imgSlide, txtTitle, txtHint);
		viewPager.setAdapter(adapter);
		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(viewPager);
	
		viewPager.setPageTransformer(true, new CustomPageTransformer());

		btn_SignUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(AuthenticationActivity.this,SignupActivity.class);
				startActivityForResult(i, 0);
				//finish();
			}
		});
		
		btn_Login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(AuthenticationActivity.this,LoginActivity.class);
				startActivityForResult(i, 0);
				//finish();
			}
		});
	} 
	public class ViewPagerAdapter extends PagerAdapter {
		// Declare Variables
		private Context context;
		
		private int[] imgSlide;
		private String[] txtTitle, txtHint;
		private LayoutInflater inflater;

		public ViewPagerAdapter(Context context, int[] imgSlide, String[] txtTitle, String[] txtHint) {
			this.context = context;
		
			this.imgSlide = imgSlide;
			this.txtTitle = txtTitle;
			this.txtHint = txtHint;
		}

		@Override
		public int getCount() {
			return imgSlide.length;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((RelativeLayout) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {

			ImageView imgflag;
			TextView landtxtTitle, landtxtHint;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View itemView = inflater.inflate(R.layout.viewpager_item, container,
					false);
			imgflag = (ImageView) itemView.findViewById(R.id.landing_img_slide);
			landtxtTitle = (TextView)itemView.findViewById(R.id.landing_txt_title);
			landtxtHint = (TextView)itemView.findViewById(R.id.landing_txt_hint);
			imgflag.setImageResource(imgSlide[position]);
			landtxtTitle.setText(txtTitle[position]);
			landtxtHint.setText(txtHint[position]);
			((ViewPager) container).addView(itemView);
			return itemView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((RelativeLayout) object);

		}
	}
	public class CustomPageTransformer implements ViewPager.PageTransformer {
		  
	    
	    public void transformPage(View view, float position) {
	        int pageWidth = view.getWidth();

	        View imageView = view.findViewById(R.id.landing_img_slide);
	        View contentView = view.findViewById(R.id.landing_txt_hint);
	        View txt_title = view.findViewById(R.id.landing_txt_title);          

	        if (position < -1) { // [-Infinity,-1)
	            // This page is way off-screen to the left
	        } else if (position <= 0) { // [-1,0]
	            // This page is moving out to the left

	            // Counteract the default swipe
	            view.setTranslationX(pageWidth * -position);
	            if (contentView != null) {
	                // But swipe the contentView
	                contentView.setTranslationX(pageWidth * position);
	                txt_title.setTranslationX(pageWidth * position);
	              
	                contentView.setAlpha(1 + position);
	                txt_title.setAlpha(1 + position);
	            }

	            if (imageView != null) {
	                // Fade the image in
	                imageView.setAlpha(1 + position);
	            }

	        } else if (position <= 1) { // (0,1]
	            // This page is moving in from the right
	           
	            // Counteract the default swipe
	            view.setTranslationX(pageWidth * -position);
	            if (contentView != null) {
	                // But swipe the contentView
	                contentView.setTranslationX(pageWidth * position);
	                txt_title.setTranslationX(pageWidth * position);
	                contentView.setAlpha(1 - position);
	                txt_title.setAlpha(1 - position);

	            }
	            if (imageView != null) {
	                // Fade the image out
	                imageView.setAlpha(1 - position);
	            }
	
	        }
	    }
	}
	public void fontStyles() {
		Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		btn_SignUp.setTypeface(mFontDark);
		btn_Login.setTypeface(mFontDark);
	}
 
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				finish();
			}
		} 
		
	}
	
	/**
	 * Load Contents after initilaize view ids.
	 */
	private void loadContents(){
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
	}
	
	private void updateRegistration(final AbstractSignup authentication){
		
	}
	
	private void updateLoginDetails(final AbstractLogin abstractLogin){
		
	}
	
	private void updateOTPLogin(){
		
	}
	
	private void updatePwdRegistration(AbstractPasswordReg abstractPasswordReg){
		
	}
	
	public void moveToUpdateProfile(){
		
	}
	
	@Override
	public void onResume(){
		super.onResume();	
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
	}
	
    private void setUpLocationClientIfNeeded() {
      
    }
	@Override
	public void onLocationChanged(Location location) {
		
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		
	}

	@Override
	public void onDisconnected() {
		
	}
}// End of Activity

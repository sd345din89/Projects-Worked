package com.strobilanthes.forrestii.util;

import java.util.ArrayList;

import android.os.Environment;

import com.strobilanthes.forrestii.model.notification.NotificaitonList;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;

public final class Constant {

	public static final String s = 
			"https://lh6.googleusercontent.com/-55osAWw3x0Q/URquUtcFr5I/AAAAAAAAAbs/rWlj1RUKrYI/s1024/A%252520Photographer.jpg";
	
	// Forrestii webservice url
	///public static final String BASEURL = "http://192.168.101.200:8080/ForrestiiBackEnd/services/data/v1.0/objects/";
	public static final String BASEURL = "https://www.whereiskey.com/ForrestiiBackEnd_v1.9/services/data/v1.0/objects/";

	public static String IMAGEURL = "https://www.whereiskey.com/Forrestii/";
	
	//public static final String TEMP_BASEURL = "http://forrestii.com/ForrestiiServiceV3/webresources/";
	
	//public static final AbstractServerInfo ABSTRACT_SERVER_INFO = null;
	
    public static final String SENDER_ID = "326163705869";

	public static int PAGE_LOGIN = 0;
	//public static int PAGE_SIGNUP= 1;
	//public static int PAGE_OTP_REG = 1;
	//public static int PAGE_PASS_REG_ = 2;
	//public static int PAGE_PROF_UPDATE = 3;
	public static int PAGE_MAIN_PAGE = 4;
	
	public static String FOLDER_CARD_NAME = IMAGEURL + "Cards/";
	public static String FOLDER_CARD_MASK_NAME = IMAGEURL + "Mask/";
	public static String FOLDER_LOGO_NAME = IMAGEURL + "Logos/";
	public static String FOLDER_PROFILE_PHOTO_NAME = IMAGEURL + "Photos/";
	public static String FOLDER_TEMPLATES__NAME = IMAGEURL + "Templates/";
	
	//SignUp=============================================
	public static final String REGISTER = BASEURL + "user/userregistration";
	public static final String OTP_LOGIN = BASEURL + "user/otpverification";
	public static final String SET_NEWPASSWORD = BASEURL + "user/setnewpassword";
	public static final String COMPLETE_REG = BASEURL + "user/completeregistration";
	public static final String RESEND_OTP = BASEURL + "user/resendotp";
	public static final String UPDATE_USERPROFILE = BASEURL + "user/updateuserprofile";
	public static final String FORGET_PASSWORD = BASEURL + "user/forgotpassword";
	public static final String VERIFY_FORGET_PASSWORD = BASEURL + "user/verifyforgotpwd";
	//==================FRIEND REQUEST====================//
	public static final String SEND_FRIEND_REQUEST = BASEURL + "friend/sendfriendrequest";
	public static final String ACCEPT_FRIEND_REQUEST = BASEURL + "friend/acceptfriendrequest";
	public static final String DECLINED_FRIEND_REQUEST = BASEURL + "friend/decline";
	public static final String UN_FRIEND = BASEURL + "friend/unfriend";

	//====================================================
	//Login===============================================
	public static final String LOGIN_USER = BASEURL + "user/login";
	public static final String LOGOUT_USER = BASEURL + "user/logout";
	//====================================================
	
	/* Quickcard */
	public static final String CREATE_REFERRAL_QUICKCARD = BASEURL + "referral/createquickcardreferral";
	public static final String UPDATE_QUICK_CARD = BASEURL + "user/updatequickcard";
	public static final String UPDATE_USER_PROFILE = BASEURL + "user/updateuserprofile";

	// Wall	
	public static final String CREATE_POST = BASEURL+ "post/createpost";
	//public static final String GET_WALL_POST = BASEURL+ "post/getmywallpost";
	public static final String REFRESH_WALL_POST = BASEURL+ "post/refreshmywallpost";
	public static final String UPDATE_POST = BASEURL + "post/updatepost";
	public static final String DELETE_POST = BASEURL + "post/deletepost";
	public static final String HIDE_WALL_POST = BASEURL + "post/deletemywallpost";
	public static final String REPLY_TO_POST = BASEURL+ "post/createreply";
	public static final String DELETE_REPLY = BASEURL + "post/deletereply";
	public static final String UPDATE_REPLY = BASEURL + "post/updatereply";
	public static final String GET_PAGINATE_WALLPOST = BASEURL + "post/getpaginatewallpost";
	public static final String GET_WALLPOST_NOTIFY = BASEURL + "post/getnotifypost";
	
	public static final String GET_CONTACTS_T2 = BASEURL + "friend/getT2";
	public static final String GET_CONTACTS_T3 = BASEURL + "friend/getT3";
	public static final String GET_CONTACTS_T3fromT2 = BASEURL + "friend/getT3fromT2";
	//===================================================

	/* Settings */
	public static final String GET_SETTINGS = BASEURL + "user/getsettings";
	public static final String SETTINGS = BASEURL + "user/settings";
	public static final String GET_USER_PROFILE= BASEURL + "user/getuserprofile";
	
	/* Referral */
	public static final String PICK_A_CARD = BASEURL + "referral/pickacard";
	public static final String CREATE_FRIEND_REFERRAL = BASEURL + "referral/createfriendreferral";
	public static final String UNPICK_A_CARD = BASEURL + "referral/unpickacard";
	public static final String VIEW_REFERRALS_REVIEW = BASEURL + "referral/reviews";

	/* Ratings */
	public static final String RATING_A_CARD = BASEURL + "referral/ratingacard";
	
	/* Notification */
	public static final String GET_PAGINATED_NOTIFICATIONLIST = BASEURL + "notify/getpaginatenotificationlist";
	public static final String UPDATE_VIEW_STATUS = BASEURL + "notify/updateviewstatus";
	public static final String GET_NOTIFY_POST = BASEURL + "post/getnotifypost";
	//public static final String REFRESH_NOTIFY_LIST = BASEURL + "notify/refreshnotificationlist";
	
	/* Change Password */
	public static final String CHANGE_PASSWORD = BASEURL + "user/changepassword";
	
	/* User LookUpList */
	public static final String USER_LOOKUP_LIST= BASEURL + "user/lookuplist";
	
	public static final String USER_IMAGE_UPLOAD= BASEURL + "user/uploadphoto";

	/* User ChangeMobileNo */
	public static final String CHANGE_MOBILENO = BASEURL + "user/changemobileno";
	
	/* User VerifyMobileNo */
	public static final String VERIFY_MOBILENO = BASEURL + "user/verifymobileno";

	/* User RemovePendingMobileNo */
	public static final String REMOVE_MOBILENO = BASEURL + "user/removependingmobile";
	
	/* User RemovePendingEmail */
	public static final String REMOVE_EMAIL_ID  = BASEURL + "user/removependingemail";
	/* Invite */
	public static final String INVITE_MOBILE = BASEURL + "friend/mobileinvite";
	
	/* Share*/
	public static final String SHARE = BASEURL + "friend/shareviasms";
	
	/* Shake*/
	public static final String FORRESTII_SHAKE = BASEURL + "ff/shake";
	
	/* Forrestii Friend Search*/
	public static final String FORRESTII_FRND_SEARCH = BASEURL + "ff/ffsearch";
	
	/* User Change EmailId  */
	public static final String CHANGE_EMAIL_ID = BASEURL + "user/changeemailid";
	
	/* User Verify EmailId  */
	public static final String VERIFY_EMAIL_ID = BASEURL + "user/verifyemailid";
	
	public static final boolean IS_FROM_LOCAL = false;
	public static final boolean LOG_FORRESSTII = true;
	public static final int SOCKET_TIMEOUT = 20000;
	public static final int NUM_OF_TABS = 4;
	public static boolean SUPPORTS_JELLYBEAN_APILEVEL_16 = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN;
	public static boolean SUPPORTS_HONEYCOMB_APILEVEL_11 = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB;
	
	public static final String IMAGE_PATH = Environment.getExternalStorageDirectory().toString() + "/Forrestii/";

	
	public static final int CARD_IMG_WIDTH = 512;
	public static final int CARD_IMG_HEIGHT = 256;

	public static final String CHAT_BASE_URL= "http://chat.forrestii.com:8008/jappix/?email=";

	public static final String PREFS_NAME = "MyPrefs";
	
	public static ArrayList<GetWallPost> mGetWallPostArrayList = new ArrayList<GetWallPost>();
	public static ArrayList<NotificaitonList> mAllNotificationArrayList = new ArrayList<NotificaitonList>();
	//public static ArrayList<NotificaitonList> mRequestNotificationArrayList = new ArrayList<NotificaitonList>();
	//public static ArrayList<NotificaitonList> mRewardsNotificationArrayList = new ArrayList<NotificaitonList>();

	public static final String ALERT_NETWORK = "Please check your internet connection";
	public static final String ALERT_REQUESTFAILED = "Request Failed.";
	/*public static final String ALERT_INVALIDEMAIL = "Request Failed.";
	public static final String ALERT_ENTEROBILE = "Request Failed.";
	public static final String ALERT_INVALIDMOBILE = "Request Failed.";
	public static final String ALERT_ENTERPASSWORD = "Request Failed.";
	public static final String ALERT_REQUESTFAILED = "Request Failed.";
	public static final String ALERT_REQUESTFAILED = "Request Failed.";
	public static final String ALERT_REQUESTFAILED = "Request Failed.";*/
}

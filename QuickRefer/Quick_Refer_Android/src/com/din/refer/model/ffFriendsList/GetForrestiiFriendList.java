package com.strobilanthes.forrestii.model.ffFriendsList;

import java.io.Serializable;

import com.strobilanthes.forrestii.model.general.AbstractLocation;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;



public class GetForrestiiFriendList implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AbstractLocation location;
	private AbstractSkillSet skillMaster1;
	private int ffid;
	private float totalRating;
	private long reviewCount;
	private boolean postToT3;
	private String email;
	private String companyName;
	private String fullAddress;
	private String addressLine1;
	private String addressLine2;
	private String pocfirstName;
	private String poclastName;
	private String website;
	private int workNumber1;
	private int workNumber2;
	private int workNumber3;
	private int workNumber4;
	private long mobileNumber1;
	private long mobileNumber2;
	private long mobileNumber3;
	private long mobileNumber4;
	
	public AbstractSkillSet getSkillMaster1() {
		return skillMaster1;
	}

	public void setSkillMaster1(AbstractSkillSet skillMaster1) {
		this.skillMaster1 = skillMaster1;
	}
	public boolean isPostToT3() {
		return postToT3;
	}

	public void setPostToT3(boolean postToT3) {
		this.postToT3 = postToT3;
	}

	public AbstractLocation getLocation() {
		return location;
	}

	public void setLocation(AbstractLocation location) {
		this.location = location;
	}
	
	public int getFfid() {
		return ffid;
	}

	public void setFfid(int ffid) {
		this.ffid = ffid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getPocfirstName() {
		return pocfirstName;
	}

	public void setPocfirstName(String pocfirstName) {
		this.pocfirstName = pocfirstName;
	}

	public String getPoclastName() {
		return poclastName;
	}

	public void setPoclastName(String poclastName) {
		this.poclastName = poclastName;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public int getWorkNumber1() {
		return workNumber1;
	}

	public void setWorkNumber1(int workNumber1) {
		this.workNumber1 = workNumber1;
	}

	public int getWorkNumber2() {
		return workNumber2;
	}

	public void setWorkNumber2(int workNumber2) {
		this.workNumber2 = workNumber2;
	}

	public int getWorkNumber3() {
		return workNumber3;
	}

	public void setWorkNumber3(int workNumber3) {
		this.workNumber3 = workNumber3;
	}

	public int getWorkNumber4() {
		return workNumber4;
	}

	public void setWorkNumber4(int workNumber4) {
		this.workNumber4 = workNumber4;
	}

	public long getMobileNumber1() {
		return mobileNumber1;
	}

	public void setMobileNumber1(int mobileNumber1) {
		this.mobileNumber1 = mobileNumber1;
	}

	public long getMobileNumber2() {
		return mobileNumber2;
	}

	public void setMobileNumber2(int mobileNumber2) {
		this.mobileNumber2 = mobileNumber2;
	}

	public long getMobileNumber3() {
		return mobileNumber3;
	}

	public void setMobileNumber3(int mobileNumber3) {
		this.mobileNumber3 = mobileNumber3;
	}

	public long getMobileNumber4() {
		return mobileNumber4;
	}

	public void setMobileNumber4(int mobileNumber4) {
		this.mobileNumber4 = mobileNumber4;
	}

	public long getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(long reviewCount) {
		this.reviewCount = reviewCount;
	}

	public float getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(float totalRating) {
		this.totalRating = totalRating;
	}

}

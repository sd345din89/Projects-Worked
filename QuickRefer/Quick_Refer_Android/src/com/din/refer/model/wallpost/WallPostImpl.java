package com.strobilanthes.forrestii.model.wallpost;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.WallPostVolleyTasks;

public class WallPostImpl implements WallPost, VolleyTasksListener {

	private AbstractCreateWallPost abstractCreateWallPost;
	private AbstractGetWallPost abstractGetWallPost;
	private AbstractUpdateWallPost abstractUpdateWallPost;
	private AbstractReplyWallPost abstractReplyWallPost;
	private AbstractDelete abstractDelete;
	private AbstractPaginateGetWallPost abstractPaginateGetWallPost;
	
	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();

	private CustomProgressbar mCustomProgressbar;
	
	private String doWallPost = "dowallpost";
	private String refreshWallPost= "refereshwallpost";
	private String updateWallPost = "updatewallpost";
	private String deleteWallPost = "DeleteWallPost";
	private String hideWallPost = "HideWallPost";
	private String replyWallPost = "ReplyWallPost";
	private String updateReplyPost = "UpdateReplyPost";
	private String deleteReplyPost = "DeleteReplyPost";
	private String paginateWallPost = "PaginateWallPost";
	
	private Activity context;
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonObject) {
		
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		if(method_name.equals(doWallPost)){
			///AbstractCreateWallPost previousResult = abstractWallPost;
			AbstractCreateWallPost previousResult = abstractCreateWallPost;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractCreateWallPost = gson.fromJson(jsonObject.toString(), AbstractCreateWallPost.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,doWallPost,
					previousResult, abstractCreateWallPost);
			notifyListeners(event);
			
		}else if(method_name.equals(refreshWallPost)){
			
			AbstractGetWallPost previousResult = abstractGetWallPost;
			//ArrayList<AbstractGetWallPost> abstractGetWallPosts = new ArrayList<AbstractGetWallPost>();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractGetWallPost = gson.fromJson(jsonObject.toString(), AbstractGetWallPost.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					refreshWallPost, previousResult, abstractGetWallPost);
			notifyListeners(event);
			
		}else if(method_name.equals(updateWallPost)){
			
			AbstractUpdateWallPost previousResult = abstractUpdateWallPost;
		
			PropertyChangeEvent event = new PropertyChangeEvent(this,
					updateWallPost, previousResult, abstractUpdateWallPost);
			notifyListeners(event);
			
		}else if(method_name.equals(deleteWallPost)){
			
			AbstractDelete previousResult = abstractDelete;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractDelete = gson.fromJson(jsonObject.toString(), AbstractDelete.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, deleteWallPost, previousResult, abstractDelete);
			notifyListeners(event);
			
		}else if(method_name.equals(hideWallPost)){
			
			AbstractDelete previousResult = abstractDelete;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractDelete = gson.fromJson(jsonObject.toString(), AbstractDelete.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, hideWallPost, previousResult, abstractDelete);
			notifyListeners(event);
			
		}else if(method_name.equals(replyWallPost)){
			
			AbstractReplyWallPost previousResult = abstractReplyWallPost;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractReplyWallPost = gson.fromJson(jsonObject.toString(), AbstractReplyWallPost.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, replyWallPost, previousResult, abstractReplyWallPost);
			notifyListeners(event);
			
		}else if(method_name.equals(deleteReplyPost)){
			
			AbstractDelete previousResult = abstractDelete;	
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractDelete = gson.fromJson(jsonObject.toString(), AbstractDelete.class);
			
			//abstractDeleteReplyPost = deleteReplyPost;
			PropertyChangeEvent event = new PropertyChangeEvent(this,deleteReplyPost, previousResult, abstractReplyWallPost);
			notifyListeners(event);
			
		}else if(method_name.equals(updateReplyPost)){
			
			AbstractDelete previousResult = abstractDelete;	
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractDelete = gson.fromJson(jsonObject.toString(), AbstractDelete.class);
			
			//abstractDeleteReplyPost = deleteReplyPost;
			PropertyChangeEvent event = new PropertyChangeEvent(this,updateReplyPost, previousResult, abstractDelete);
			notifyListeners(event);
			
		}else if(method_name.equals(paginateWallPost)){
			
			AbstractPaginateGetWallPost previousResult = abstractPaginateGetWallPost;
			//ArrayList<AbstractGetWallPost> abstractGetWallPosts = new ArrayList<AbstractGetWallPost>();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractPaginateGetWallPost = gson.fromJson(jsonObject.toString(), AbstractPaginateGetWallPost.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,
					paginateWallPost, previousResult, abstractPaginateGetWallPost);
			notifyListeners(event);
			
		}				

	}

	@Override
	public void handleError(String method_name,Exception e) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		PropertyChangeEvent event = new PropertyChangeEvent(this,method_name, null, null); 
		notifyListeners(event);
	}

	/*@Override
	public void refreshWallPost(String url, JSONObject postdata) {
		//mCustomProgressbar = new CustomProgressbar(getContext());
		//mCustomProgressbar.show();
		webServiceCall(refreshWallPost, url, postdata);
	}*/

	@Override
	public void createWallPost(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		webServiceCall(doWallPost, url, postdata);
	}

	@Override
	public void updateWallPost(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		webServiceCall(updateWallPost, url, postdata);
		
	}

	@Override
	public void deleteWallPost(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		webServiceCall(deleteWallPost, url, postdata);
		
	}

	@Override
	public void hideWallPost(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		webServiceCall(hideWallPost, url, postdata);
		
	}

	@Override
	public void replyWallPost(String url, JSONObject postdata) {
		//mCustomProgressbar = new CustomProgressbar(getContext());
		//mCustomProgressbar.show();
		webServiceCall(replyWallPost, url, postdata);
		
	}

	@Override
	public void deleteReplyWallPost(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		webServiceCall(deleteReplyPost, url, postdata);
		
	}

	@Override
	public void updateReplyWallPost(String url, JSONObject postdata) {
		//mCustomProgressbar = new CustomProgressbar(getContext());
		//mCustomProgressbar.show();
		webServiceCall(updateReplyPost, url, postdata);
	}

	@Override
	public void paginateWallPost(String url, JSONObject postdata) {
		webServiceCall(paginateWallPost, url, postdata);
	}

	private void webServiceCall(String taskId,String url, JSONObject postdata){
		ForrestiiApplication.getInstance().addToRequestQueue(
				new WallPostVolleyTasks(taskId, this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	@Override
	public void doValidation() {
	}

	/*@Override
	public AbstractGetWallPost getWallPostResponse() {
		return abstractGetWallPost;
	}*/

	@Override
	public AbstractCreateWallPost getCreateWallPostResponse() {
		return abstractCreateWallPost;
	}

	@Override
	public AbstractUpdateWallPost getUpdateWallPostResponse() {
		
		return abstractUpdateWallPost;
	}

	@Override
	public AbstractDelete getDeleteWallPostResponse() {
		
		return abstractDelete;
	}

	@Override
	public AbstractReplyWallPost getReplyWallPostResponse() {
		
		return abstractReplyWallPost;
	}

	@Override
	public AbstractDelete getDeleteReplyPosResponse() {
		
		return abstractDelete;
	}

	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	
	@Override
	public AbstractDelete getHideWallPostResponse() {
		return abstractDelete;
	}

	
	@Override
	public AbstractDelete getUpdateReplyPostResponse() {
		return abstractDelete;
	}

	
	@Override
	public AbstractPaginateGetWallPost getPaginateWallpost() {
		return abstractPaginateGetWallPost;
	}
}

package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.activities.AuthenticationActivity;
import com.strobilanthes.forrestii.activities.SettingsFragmentActivity;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.settings.AbstractVerify;
import com.strobilanthes.forrestii.model.settings.Settings;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;

public class SettingsFragment extends BaseFragment<DBAccess> {

	@InjectView(R.id.setting_header_txt_notification) private TextView settingHeaderTxtNotification;
	@InjectView(R.id.setting_txt_email)               private TextView settingTxtEmail;
	@InjectView(R.id.setting_header_txt_Account)      private TextView settingHeaderTxtAccount;
	@InjectView(R.id.setting_txt_change_email)        private TextView settingChangeEmail;
	@InjectView(R.id.setting_txt_change_mobile)        private TextView settingChangeMobile;

	@InjectView(R.id.setting_txt_password)            private TextView settingTxtPassword;
	@InjectView(R.id.setting_header_txt_others)       private TextView settingHeaderTxtOthers;
	@InjectView(R.id.setting_txt_terms)               private TextView settingTxtTerms;
	@InjectView(R.id.setting_txt_logout)              private Button settingTxtLogout;
	@Inject Settings settingsModel;

	private Typeface mFontLight,mFontDark;

	private static SettingsFragment frag = null;
	public static SettingsFragment newInstance() {
		if(frag == null)
			frag = new SettingsFragment();
        return frag;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
	}
   
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frg_settings_main, null);
		
		super.initilizeContents();
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		getActivity().getActionBar().setTitle("Settings");
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		
		settingsModel.setContext(getActivity());
		
		loadFonts();
		
		settingTxtEmail.setOnClickListener(this);
		settingChangeEmail.setOnClickListener(this);
		settingChangeMobile.setOnClickListener(this);
		settingTxtPassword.setOnClickListener(this);
		settingTxtTerms.setOnClickListener(this);
		settingTxtLogout.setOnClickListener(this);
		
	}
	
	private void loadFragments(Fragment fragment){
		 
		getFragmentManager().beginTransaction().setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left, R.anim.out_to_right)
	        .addToBackStack(null).replace(R.id.content_frame, fragment).commit();
		 
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		case R.id.setting_txt_email:
			loadSettings(0);
//			loadFragments(new SettingsNotifyEmailFragment());
			
			break;
		case R.id.setting_txt_change_mobile:
			loadSettings(1);
//			loadFragments(new SettingsChangeEmailOrMobileFragment());
			
			break;
		case R.id.setting_txt_change_email:
			loadSettings(2);
//			loadFragments(new SettingsChangeEmailOrMobileFragment());
			
			break;
		case R.id.setting_txt_password:
			loadSettings(3);
//			loadFragments(new SettingsChangePwdFragment());
			
			break;
		
		case R.id.setting_txt_terms:
			loadSettings(4);
			break;
		case R.id.setting_txt_logout:
			new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT).setMessage("Do you want to logout?")
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
					JSONObject jobject = new JSONObject();
					try {
						jobject.put("userId",getHelper().getDBUserProfile().getUserId());
						settingsModel.requestLogout(Constant.LOGOUT_USER, jobject);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}).setNegativeButton("Cancel",null).show();
			
			break;
		default:
			break;
		}
	}
	private void loadSettings(int i) {
		Intent intent = new Intent(getActivity(), SettingsFragmentActivity.class);
		intent.putExtra("fragment", i);
		startActivity(intent);
	}

	@Override
	public void onResume() {
		super.onResume();
		settingsModel.addChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		settingsModel.removeChangeListener(this);
	}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		 if(event.getPropertyName().equals("logout")){
			AbstractVerify abstractVerify = settingsModel.getLogoutDetails();
			if(abstractVerify  !=null){
				if(abstractVerify.getStatus() == 1){
					getHelper().logout();
//					getActivity().setResult(1919);
					startActivity(new Intent(getActivity(), AuthenticationActivity.class));
					getActivity().finish();
				}else{
					Util.showAlert(getActivity(),"Message", abstractVerify.getError());
					
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
			
		}
	}
	private void loadFonts() {
		 
		 mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		 mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		 settingHeaderTxtNotification.setTypeface(mFontLight);
		 settingChangeMobile.setTypeface(mFontDark);
		 settingTxtEmail.setTypeface(mFontDark);
		 settingHeaderTxtAccount.setTypeface(mFontLight);
		 settingTxtPassword.setTypeface(mFontDark);
		 settingHeaderTxtOthers.setTypeface(mFontLight);
		 settingTxtTerms.setTypeface(mFontDark);
		 settingTxtLogout.setTypeface(mFontDark);
		 settingChangeEmail.setTypeface(mFontDark);
		 
	}
}
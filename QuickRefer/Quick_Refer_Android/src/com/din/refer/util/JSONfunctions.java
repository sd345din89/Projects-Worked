package com.strobilanthes.forrestii.util;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

public class JSONfunctions {
	public static String result = null;

	/*public static JSONArray getJSONArrayfromURL(String url, int method) {
		
		InputStream is = null;
		JSONArray jArray = null;

		
		// http post or get
		try {
			HttpClient httpclient = new DefaultHttpClient();
			Log.v("jsonFuncions", "try method");

			switch (method) {
			case Constant.GET_METHOD:
				Log.v("jsonFuncions", "get method");
				HttpGet httpget = new HttpGet(url);
				HttpResponse response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
				break;
			case Constant.POST_METHOD:

				HttpPost httppost = new HttpPost(url);
				response = httpclient.execute(httppost);
				entity = response.getEntity();
				is = entity.getContent();
				Log.v("jsonFuncions", "post method");

				break;
			}

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
			Log.v("RESULT: ", result);
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONArray(result);
			
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}*/
	
	public static String doHttpRequest(String url) {
		InputStream is = null;

		Log.v("Request", "Request "+url);
		//JSONArray jArray = null;

		// http post or get
		try {
			HttpClient httpclient = new DefaultHttpClient();

			HttpGet httpget = new HttpGet(url);
			HttpParams httpParams = new BasicHttpParams();
		    HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
		    HttpConnectionParams.setSoTimeout(httpParams, 15000);
		    httpget.setParams(httpParams);
			HttpResponse response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			
		} catch (Exception e) {
			result =null;
			e.printStackTrace();
			//Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
			Log.v("RESULT: ", result);
		} catch (Exception e) {
			result = null;
			e.printStackTrace();
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		return result;
	}

	/*public static JSONObject getJSONObjfromURL(String url, int method) {
		InputStream is = null;
		JSONObject jsonObj = null;

		// http post or get
		try {
			HttpClient httpclient = new DefaultHttpClient();

			switch (method) {
			case Constant.GET_METHOD:
				HttpGet httpget = new HttpGet(url);
				HttpResponse response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
				break;
			case Constant.POST_METHOD:

				HttpPost httppost = new HttpPost(url);
				response = httpclient.execute(httppost);
				entity = response.getEntity();
				is = entity.getContent();
				break;
			}

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
			Log.v("RESULT: ", result);
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jsonObj = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jsonObj;
	}
	
	public static String doHttpRequestPost(String requestUrl, List<NameValuePair> params) {

		String requestString = requestUrl;
		String responseString = "";

		Log.i("Request String ", requestUrl);
		// Clear the response String

		InputStream is = null;

		// http post
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(requestString);			
			httppost.setEntity(new UrlEncodedFormEntity(params));	
			
			for (NameValuePair n:params){
				Log.i("Values ", n.getValue());
			}
			
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			
			is = entity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			
			StringBuilder sb = new StringBuilder();
			String line = null;
			
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			
			is.close();			
			responseString = sb.toString();			
		
			Log.e("responseString", "responseString" + responseString);
			
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}// End of catch
		
		return responseString;
	
	}// End of do http request
*/	
}//End of class 

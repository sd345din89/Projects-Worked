package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.view.MenuItem;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.fragments.QuickCardFragment;
import com.strobilanthes.forrestii.act.fragments.ReferSomeOneFragment;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.wallpost.RefferalPost;
import com.strobilanthes.forrestii.slidingtab.PagerSlidingTabStrip;
import com.strobilanthes.forrestii.util.Util;

public class ReferSomeOneActivity extends BaseActivity<DBAccess> implements PropertyChangeListener,ViewPager.OnPageChangeListener {
	 
	private Typeface mFontLight,mFontDark;
	//private GetWallPost wallPostCotnent;
	private int postId = 0 , skillSetId = 0;
	private int postByUserId = 0;
	private String mobile;
	private int mWallPosition;
	private RefferalPost refferalPost = null;
	private ArrayList<String>friendList = new ArrayList<String>();

	private ViewPager viewPager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_refer);
		
		getSupportActionBar().setTitle("Refer People you know");
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		// Locate the viewpager in activity_main.xml
		viewPager = (ViewPager) findViewById(R.id.pager);
		PagerSlidingTabStrip pagerTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		// pagerTabStrip.setDrawFullUnderline(true);
		//pagerTabStrip.setTabIndicatorColor(0x1C262F);
		// pagerTabStrip.setPadding(10, 0, 10, 0);
		// Set the ViewPagerAdapter into ViewPager
		ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(viewPagerAdapter);
		viewPager.setOnPageChangeListener(this);
		pagerTabStrip.setTextSize((int)Util.getDimensPxtoDp(14, this));
		 pagerTabStrip.setTypeface(mFontDark, Typeface.NORMAL);
		pagerTabStrip.setViewPager(viewPager);
		 pagerTabStrip.setOnPageChangeListener(this);
		//viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
		//viewPager.setCurrentItem(1);
			
		postId = getIntent().getIntExtra("postId",0);
		postByUserId = getIntent().getIntExtra("postByUserId",0);
		mobile = getIntent().getStringExtra("mobile");
		mWallPosition = getIntent().getIntExtra("position",-1);
		skillSetId = getIntent().getIntExtra("skillSetId",0);
		
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				
				if(getSupportFragmentManager().getBackStackEntryCount() == 0){
					finish();
				}else{
					getSupportFragmentManager().popBackStack();
				}
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
	}

	public class ViewPagerAdapter extends FragmentPagerAdapter {

		// Tab Titles
		private String tabtitles[] = new String[] { "Refer your friends", "Create Quick card"};
		Context context;
		private FragmentManager fm;

		// SparseArray<Fragment> registeredFragments = new
		// SparseArray<Fragment>();

		public ViewPagerAdapter(FragmentManager fm) {
			super(fm);
			this.fm = fm;
		}

		@Override
		public int getCount() {
			return tabtitles.length;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			Fragment fragment = (Fragment) super.instantiateItem(container,position);
			// registeredFragments.put(position, fragment);
			return fragment;
		}

		@Override
		public Object instantiateItem(View container, int position) {
			return super.instantiateItem(container, position);
		}

		private String getFragmentTag(int pos) {
			return "android:switcher:" + R.id.pager + ":" + pos;
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			
			case 0:
				return ReferSomeOneFragment.newInstance(postId, postByUserId,mWallPosition,skillSetId);
				// Open FragmentTab3.java
			case 1:
				return QuickCardFragment.newInstance(postId , postByUserId, mobile,mWallPosition);
				
			}
			return null;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// registeredFragments.remove(position);
			// super.destroyItem(container, position, object);
		}

		/*
		 * public Fragment getRegisteredFragment(int position) { return
		 * registeredFragments.get(position); }
		 */

		@Override
		public CharSequence getPageTitle(int position) {
			return tabtitles[position];
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		
	}


	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		
	}


	@Override
	public void onPageSelected(int position) {
		  viewPager.setCurrentItem(position);
	}
}

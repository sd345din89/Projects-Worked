package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.AbstractDelete;
import com.strobilanthes.forrestii.model.wallpost.AbstractReplyWallPost;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;
import com.strobilanthes.forrestii.model.wallpost.ReplyPost;
import com.strobilanthes.forrestii.model.wallpost.WallPost;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;

public class WallPostExpandCommentsFragment extends BaseFragment<DBAccess> {
	
	private Typeface mFontLight,mFontDark;
    private ListView comentsList;
	private static final String ARG_POSITION = "position";
	private CommentsContactsAdapter adapter;
	
	@InjectView(R.id.comment_main_layout) private RelativeLayout commentMainLayout;
	@InjectView(R.id.commentsListview)    private ListView lv;
	@InjectView(R.id.seperatorComments1)  private View seperatorComments1;
	@InjectView(R.id.add_comments)        private LinearLayout addComments;
	@InjectView(R.id.comment_edittext)    private EditText cmt_message;
	@InjectView(R.id.comment_enter)       private ImageView img_send;
	@InjectView(R.id.no_comments_expand)  private TextView no_comments_text;
	@Inject WallPost 				wallPostModel;
	
	private int mPosition = -1;
	private int mPostId = 0;
	private int mPostByUserId = 0;
	private ArrayList<ReplyPost> replyPostArrayList;
	private  GetWallPost getWallPost;
	private String mTextComments  = null;
	private AbstractUserProfile abstractUserProfile;
	private CharSequence[] userItems = {"Edit Comments","Copy Comments","Delete Comments"};
	private CharSequence[] noneUserItems = {"Edit Comments","Copy Comments"};
	private String mUpdatedString = null;
	private AbstractUserProfile mAbstractUserProfile = null;
	private int mReplyId = -1;
	private int mSelectedPostition = -1;
	static UpdateWallpost updateWallpost;
	
	public interface UpdateWallpost{
		void uploadComments(int position);
	}
	
    public static WallPostExpandCommentsFragment newInstance(int pos, GetWallPost getWallPost) {
		 WallPostExpandCommentsFragment frag = new WallPostExpandCommentsFragment();
         Bundle args = new Bundle();
         args.putInt("position", pos);
         args.putSerializable("wall_post", getWallPost);
         frag.setArguments(args);
         return frag;
    }
    
    @Override
    public void onAttach(Activity activity) {
    	updateWallpost = (UpdateWallpost) activity;
    	super.onAttach(activity);
    }
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.frg_exapnd_comments, null);
		
		return view;
	}

	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		wallPostModel.setContext(getActivity());
		
		img_send.setOnClickListener(this);
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		cmt_message.setTypeface(mFontDark);
		
		int position = getArguments().getInt("position");
		mPosition = position;
		getWallPost = (GetWallPost) getArguments().getSerializable("wall_post");
		mPostId = getWallPost.getWallPosts().getPostId();
		mPostByUserId = getWallPost.getWallPosts().getPostByUserProfile().getUserId();
		
		abstractUserProfile = getHelper().getDBUserProfile();
		
		replyPostArrayList = getWallPost.getWallPosts().getReplies();
		if (replyPostArrayList.size() <= 0) {
			lv.setVisibility(View.GONE);
			no_comments_text.setVisibility(View.VISIBLE);
		}
		
		adapter = new CommentsContactsAdapter(getActivity());
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {
				
					
					final ReplyPost replyPost = (ReplyPost) parent.getItemAtPosition(position);
					
					AlertDialog.Builder alertbuilder = new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT);
					//alertbuilder.setTitle("Comments Options");
					AlertDialog dialog = alertbuilder.create();
					alertbuilder.setItems((abstractUserProfile.getUserId() == replyPost.getReplyUserProfile().getUserId())? userItems : noneUserItems, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if(which == 0){
								final EditText input = new EditText(getActivity());
								input.setMaxLines(3);
								int maxLength = 150;
								InputFilter[] FilterArray = new InputFilter[1];
								FilterArray[0] = new InputFilter.LengthFilter(maxLength);
								input.setFilters(FilterArray);			
								input.setText(replyPost.getMessage());
								LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
								                        LinearLayout.LayoutParams.MATCH_PARENT,
								                        LinearLayout.LayoutParams.MATCH_PARENT);
								input.setLayoutParams(lp);
								new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT).setMessage("Update Post")
								.setView(input).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										JSONObject jsonObject = new JSONObject();
										try {
											mSelectedPostition = position;
											mUpdatedString = input.getText().toString();
											if (mUpdatedString.length() > 0) {
												if (mUpdatedString.trim().length() > 0 && !Util.limitSpecialCharacters(mUpdatedString)) {
													input.setError("The comments should not contain any special characters");
													return;
												}
												
												mReplyId = replyPost.getReplyId();
												mAbstractUserProfile = replyPost.getReplyUserProfile();
												jsonObject.put("message",input.getText().toString());
												jsonObject.put("ReplyId",replyPost.getReplyId());
												wallPostModel.updateReplyWallPost(Constant.UPDATE_REPLY, jsonObject);
											} else {
												input.setError("Please enter your comments to post");
												return;
											}
										} catch (JSONException e) {
											e.printStackTrace();
										}
									}
								}).setNegativeButton("Cancel",null).show();
							}else if(which == 1){
								
							}else if(which == 2){
								new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT)
								.setMessage("Do you want to delete this post?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										JSONObject jsonObject = new JSONObject();
										try {
											mSelectedPostition = position;
											jsonObject.put("ReplyId",replyPost.getReplyId());
											wallPostModel.deleteReplyWallPost(Constant.DELETE_REPLY, jsonObject);
										} catch (JSONException e) {
											e.printStackTrace();
										}
									}
								}).setNegativeButton("Cancel",null).show();	
							}
						}
					});
					alertbuilder.show();

			}
		});
		/*cmt_enter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//upload and update comment
//				JSONObject jsonObject = new JSONObject();
				try {
//					int mSelectedPostition = position-2;
//					mUpdatedString = input.getText().toString();
//					String mUpdatedString = cmt_message.getText().toString().trim();
//					Object mReplyId = getWallPost.getWallPosts().getReplies().get(0);
//					Object mAbstractUserProfile = replyPost.getReplyUserProfile();
//					jsonObject.put("message",input.getText().toString());
//					jsonObject.put("ReplyId",replyPost.getReplyId());
//					wallPostModel.updateReplyWallPost(Constant.UPDATE_REPLY, jsonObject);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
	}
	

	@Override
	public boolean onBackPressed() {
		
		return super.onBackPressed();
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.comment_enter:
			
			if(cmt_message.getText().toString().length() > 0){
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(cmt_message.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				
				JSONObject jsonObject = new JSONObject();
				try {
					 mTextComments = cmt_message.getText().toString();
					 mTextComments = mTextComments.replace("\\n", " "); 
					jsonObject.put("posts",new JSONObject().put("postId", mPostId).put("postByUserProfile", new JSONObject().put("userId", mPostByUserId)));
					jsonObject.put("ReplyUserProfile", new JSONObject().put("userId", getHelper().getUserId()));
					//jsonObject.put("postByUserProfile", new JSONObject().put("userId", mPostByUserId));
					jsonObject.put("message",mTextComments);
					wallPostModel.replyWallPost(Constant.REPLY_TO_POST, jsonObject);
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
			}else{
				cmt_message.setError("Please enter your comments to post");
			}
			
			break;

		default:
			break;
		}
	}
	

	class CommentsContactsAdapter extends BaseAdapter {

		private final Context context;
		LayoutInflater inflater;
		
		public CommentsContactsAdapter(Context context) {
			this.context = context;
	        inflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return replyPostArrayList.size() ;
		}

		@Override
		public ReplyPost getItem(int position) {
			return replyPostArrayList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Log.d("expand", "position "+position);
			convertView = inflater.inflate(R.layout.view_dg_comment_list_item, null);
			ImageView commentsListItemImgUser = (ImageView)convertView.findViewById( R.id.comments_list_item_img_user );
			TextView commentsListItemTxtName = (TextView)convertView.findViewById( R.id.comments_list_item_txt_name );
			TextView comments_list_item_txt_time = (TextView)convertView.findViewById( R.id.comments_list_item_txt_time );
			commentsListItemTxtName.setTypeface(mFontDark);
			comments_list_item_txt_time.setTypeface(mFontDark);
			ReplyPost replyPost = getItem(position);
			if(replyPost != null){
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + replyPost.getReplyUserProfile().getPhotoID(), commentsListItemImgUser,options,animateFirstListener);
				String nameString = replyPost.getReplyUserProfile().getFirstName()+" : ";
				String messageString = replyPost.getMessage();
				String totalString = nameString+messageString;

				  Spannable nameSpan = new SpannableString(totalString);        
				 
					
				  nameSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), 0, nameString.length(),0);
				  nameSpan.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);

				commentsListItemTxtName.setText(nameSpan);
				comments_list_item_txt_time.setText(Util.updatedOnTime(replyPost.getUpdatedOn()));
			}
		
			/*ViewHolder holder =null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.fragment_comment_list_item, null);
				
				holder.commentsListItemImgUser = (RoundedImageView)convertView.findViewById( R.id.comments_list_item_img_user );
				holder.commentsListItemTxtName = (TextView)convertView.findViewById( R.id.comments_list_item_txt_name );
				holder.commentsListItemTxtTime = (TextView)convertView.findViewById( R.id.comments_list_item_txt_time );
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
				
			holder.commentsListItemTxtName.setTypeface(mFontDark);
			holder.commentsListItemTxtTime.setTypeface(mFontDark);
			ReplyPost replyPost = getItem(position);
			if(replyPost != null) {
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + replyPost.getReplyUserProfile().getPhotoID(), holder.commentsListItemImgUser,options,animateFirstListener);
				String nameString = replyPost.getReplyUserProfile().getFirstName()+" : ";
				String messageString = replyPost.getMessage();
				String totalString = nameString+messageString;

				  Spannable nameSpan = new SpannableString(totalString);        
				 
					
				  nameSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), 0, nameString.length(),0);
				  nameSpan.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);

				  holder.commentsListItemTxtName.setText(nameSpan);
				  holder.commentsListItemTxtTime.setText(Util.updatedOnTime(replyPost.getUpdatedOn()));
			}*/
			
			return convertView;
		}
		class ViewHolder {
			public RoundedImageView commentsListItemImgUser;
			public TextView commentsListItemTxtName;
			public TextView commentsListItemTxtTime;

		}
	}
	
	@Override
	public void onResume(){
		super.onResume();
		wallPostModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		wallPostModel.removeChangeListener(this);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if(event.getPropertyName().equals("ReplyWallPost")){
			
			AbstractReplyWallPost abstractReplyWallPost = wallPostModel.getReplyWallPostResponse();
			
			if(abstractReplyWallPost != null){
				if(abstractReplyWallPost.getStatus() == 1){
					replyPostArrayList = getWallPost.getWallPosts().getReplies();
					ReplyPost replyPost = new ReplyPost();
					replyPost.setMessage(mTextComments);
					replyPost.setReplyId(abstractReplyWallPost.getReplyResponse().getReplyId());
					replyPost.setReplyUserProfile(getHelper().getDBUserProfile());
					replyPost.setUpdatedOn(Calendar.getInstance().getTimeInMillis());
					replyPostArrayList.add(replyPost);
					
					ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
					GetWallPost getWallPost = getWallPostList.get((mPosition));
					getWallPost.getWallPosts().setReplies(replyPostArrayList);
					getWallPostList.set((mPosition),getWallPost);
					Constant.mGetWallPostArrayList = getWallPostList;
					
					cmt_message.setText("");
					adapter.notifyDataSetChanged();
					updateWallpost.uploadComments(mPosition);
				} else {
					Util.showAlert(getActivity(),"Message", ""+abstractReplyWallPost.getError());
				}
			} else {
				Util.showAlert(getActivity(),"Message", "Request Failed");
				cmt_message.setText("");
			}
			
		} else if(event.getPropertyName().equals("DeleteReplyPost")){
			
			AbstractDelete abstractDelete = wallPostModel.getDeleteReplyPosResponse();
			if(abstractDelete != null){
				if(abstractDelete.getStatus() == 1){
					replyPostArrayList = getWallPost.getWallPosts().getReplies();
					
					replyPostArrayList.remove(mSelectedPostition);
					
					ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
					GetWallPost getWallPost = getWallPostList.get((mPosition));
					getWallPost.getWallPosts().setReplies(replyPostArrayList);
					getWallPostList.set((mPosition),getWallPost);
					Constant.mGetWallPostArrayList = getWallPostList;
					
					cmt_message.setText("");
					adapter.notifyDataSetChanged();
					updateWallpost.uploadComments(mPosition);
				}else{
					Util.showAlert(getActivity(),"Message", ""+abstractDelete.getError());
				}
			}
			
		} else if(event.getPropertyName().equals("UpdateReplyPost")){
			
			AbstractDelete abstractDelete = wallPostModel.getUpdateReplyPostResponse();
			if(abstractDelete != null){
				if(abstractDelete.getStatus() == 1){
					replyPostArrayList = getWallPost.getWallPosts().getReplies();
					ReplyPost replyPost = new ReplyPost();
					replyPost.setMessage(mUpdatedString);
					replyPost.setReplyId(mReplyId);
					replyPost.setReplyUserProfile(mAbstractUserProfile);
					replyPost.setUpdatedOn(Calendar.getInstance().getTimeInMillis());
					replyPostArrayList.set(mSelectedPostition,replyPost);
					
					ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
					GetWallPost getWallPost = getWallPostList.get((mPosition));
					getWallPost.getWallPosts().setReplies(replyPostArrayList);
					getWallPostList.set((mPosition),getWallPost);
					Constant.mGetWallPostArrayList = getWallPostList;
					
					//edit_comments.setText("");
					adapter.notifyDataSetChanged();
					
				} else {
					Util.showAlert(getActivity(),"Message", ""+abstractDelete.getError());
				}
			} else {
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}	
		}
	}
	
}

package com.strobilanthes.forrestii;

import a_vcard.android.text.TextUtils;
import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.google.android.gcm.GCMRegistrar;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.strobilanthes.forrestii.util.Constant;

public class ForrestiiApplication extends Application{
	/**
     * Log or request TAG
     */
    public static final String TAG = "VolleyPatterns";

    /**
     * Global request queue for Volley
     */
    private RequestQueue mRequestQueue;

    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static ForrestiiApplication sInstance;
    
    private static ImageLoader sImageLoader;
    
    @Override
    public void onCreate() {
        super.onCreate();

        // initialize the singleton
        sInstance = this;
        
        sImageLoader = ImageLoader.getInstance();
        
        sImageLoader.init(ImageLoaderConfiguration.createDefault(this));
        
        GCMRegistrar.checkDevice(this);
        
        GCMRegistrar.checkManifest(this);
        
        SharedPreferences sharedPreferences = getSharedPreferences("Forrestii", MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putInt("coins", 0).commit();
        editor.putInt("badgeCount", 0).commit();
        
        final String regId = GCMRegistrar.getRegistrationId(this);
        if (regId.equals("")) {
            GCMRegistrar.register(this, Constant.SENDER_ID);
        } else {
        	//dbAccess.in
        	sharedPreferences.edit().putString("regid", regId).commit();
        }
    }

    public static synchronized ImageLoader getImageLoaderInstance() {
        return sImageLoader;
    }

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized ForrestiiApplication getInstance() {
        return sInstance;
    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     * 
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     * 
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     * 
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}

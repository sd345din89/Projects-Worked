package com.strobilanthes.forrestii.act.callbacks;

import android.location.Location;

public interface LocationChangeListener {
	void onLocationChanged(Location location);
}

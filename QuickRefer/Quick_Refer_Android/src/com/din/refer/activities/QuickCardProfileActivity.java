package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.referral.AbstractReferredQuickCard;
import com.strobilanthes.forrestii.model.userprofile.UserProfileImpl;
import com.strobilanthes.forrestii.model.viewreviews.AbstractReviewProfile;
import com.strobilanthes.forrestii.model.viewreviews.AbstractViewReviews;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class QuickCardProfileActivity extends BaseActivity<DBAccess> implements PropertyChangeListener{
	

	@InjectView(R.id.qc_txt_name)  private TextView qcTxtName;
	@InjectView(R.id.qc_lyt_phone) private LinearLayout qcLytPhone;
	@InjectView(R.id.qc_txt_phone) private TextView qcTxtPhone;
	@InjectView(R.id.qc_img_phone) private ImageView qcImgPhone;
	@InjectView(R.id.qc_lyt_email) private LinearLayout qcLytEmail;
	@InjectView(R.id.qc_txt_email) private TextView qcTxtEmail;
	@InjectView(R.id.qc_img_email) private ImageView qcImgEmail;
	@InjectView(R.id.qc_btn_review) private Button qcBtnReviews;
	
	@Inject UserProfileImpl userProfileModel;

	
	private ArrayList<AbstractReviewProfile> mAbstractViewReviews;

	AbstractReferredQuickCard abstractReferredQuickCard;
	private Typeface mFontLight,mFontDark;
	private boolean isPick;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_quickcard_profile);
		
		getActionBar().setTitle("QuickCard User Profile");
		getActionBar().setDisplayShowTitleEnabled(true);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		fontStyles();

		abstractReferredQuickCard = (AbstractReferredQuickCard)getIntent().getSerializableExtra("QuickCardProfile");
		showQuickcardProfileReviews();

		isPick = getIntent().getBooleanExtra("isPick", false);
		L.d("QuickCardisPick++"+isPick);
		
		if(abstractReferredQuickCard !=null){
			if(isPick){
				String firstName = abstractReferredQuickCard.getFirstName()!=null?abstractReferredQuickCard.getFirstName():"No name";
				String lastName = abstractReferredQuickCard.getLastName()!=null?abstractReferredQuickCard.getLastName():" ";
				qcTxtName.setText(firstName + " " + lastName);
				
	            qcTxtPhone.setText(abstractReferredQuickCard.getMobileNumber()!=null?abstractReferredQuickCard.getMobileNumber():"No mobile number");
	            
	            qcTxtEmail.setText(abstractReferredQuickCard.getEmailAddress()!=null?abstractReferredQuickCard.getEmailAddress():"No Email");
	            qcTxtEmail.setSelected(true);
		
			} else{
    			String firstName = abstractReferredQuickCard.getFirstName()!=null?abstractReferredQuickCard.getFirstName():"No name";
    			String lastName = abstractReferredQuickCard.getLastName()!=null?abstractReferredQuickCard.getLastName():" ";
    			qcTxtName.setText(firstName + " " + lastName);
    			
                qcTxtPhone.setText(abstractReferredQuickCard.getMobileNumber()!=null?Util.maskMobileNumber(abstractReferredQuickCard.getMobileNumber()):"No mobile number");
                
                qcTxtEmail.setText(abstractReferredQuickCard.getEmailAddress()!=null?Util.maskEmail(abstractReferredQuickCard.getEmailAddress()):"No Email");
                qcTxtEmail.setSelected(true);
                
    			qcImgPhone.setImageResource(R.color.transparent);
    			qcImgEmail.setImageResource(R.color.transparent);
			}
		}
			
		qcBtnReviews.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mAbstractViewReviews!=null && mAbstractViewReviews.size()>0){
					Intent quickIntent = new Intent(QuickCardProfileActivity.this, UserRatingReviewActivity.class);
					
					Bundle quickCardObject = new Bundle();
					quickCardObject.putSerializable("QuickcardDetails", mAbstractViewReviews);
					quickCardObject.putString("reviews_view", "QuickcardProfileReviews");
					
					quickIntent.putExtras(quickCardObject);
					startActivity(quickIntent);
				}else {
					Util.showAlert(QuickCardProfileActivity.this, "", "No reviews");

				}
			}
		});
		qcImgPhone.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(qcTxtPhone.getText().toString().length()>0){
					
					if(Util.isValidPhoneNumber(qcTxtPhone.getText().toString().trim()) && qcTxtPhone.getText().toString().length() >= 10){

						AlertDialog.Builder alertbuilder = new AlertDialog.Builder(v.getContext(),AlertDialog.THEME_HOLO_LIGHT);
						alertbuilder.setTitle("Message");
						alertbuilder.setMessage("Do you want to make a call");
						
						alertbuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Intent callIntent = new Intent(Intent.ACTION_CALL);
						    	callIntent.setData(Uri.parse("tel:"+qcTxtPhone.getText().toString()));
						    	startActivity(callIntent);
							}
						});
						alertbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						
						alertbuilder.show();
						
				    	
					}else{
						Util.showAlert(QuickCardProfileActivity.this, "", "Invalid Phone Number");
					}
					
				}else{
					Util.showAlert(QuickCardProfileActivity.this, "", "No phone number found");
				}
			}
		});
		qcImgEmail.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(Util.isValidEmail(qcTxtEmail.getText().toString())){
					
					AlertDialog.Builder alertbuilder = new AlertDialog.Builder(v.getContext(),AlertDialog.THEME_HOLO_LIGHT);
					alertbuilder.setTitle("Message");
					alertbuilder.setMessage("Do you want to Send E-mail");
					
					alertbuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent emailIntent = new Intent(Intent.ACTION_SEND);
							emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{qcTxtEmail.getText().toString()});		  
							emailIntent.putExtra(Intent.EXTRA_TEXT, "message");
							emailIntent.setType("message/rfc822");
							startActivity(Intent.createChooser(emailIntent, "Choose an Email client :"));
						}
					});
					alertbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					
					alertbuilder.show();
				
				}else{
					
					Util.showAlert(QuickCardProfileActivity.this, "", "Email id is invalid");
				
				}
			}
		});
	}
	
	public void showQuickcardProfileReviews(){
		JSONObject json = new JSONObject();
		try {
			json.put("quickCard",new JSONObject().put("QuickCardId", abstractReferredQuickCard.getQuickCardId()));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		userProfileModel.viewQuickCardUsersReviews(Constant.VIEW_REFERRALS_REVIEW, json);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		userProfileModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		userProfileModel.removeChangeListener(this);
	}
	private void fontStyles() {
		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		
		qcTxtName.setTypeface(mFontLight);
		qcTxtPhone.setTypeface(mFontLight);
		qcTxtEmail.setTypeface(mFontLight);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		finish();		
		super.onBackPressed();
	}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("QuickcardProfileReviews")){
			AbstractViewReviews abstractReviewList = userProfileModel.getReviewsRatings();
			if (abstractReviewList != null) {
				if (abstractReviewList.getStatus() == 1) {
					
					mAbstractViewReviews = abstractReviewList.getResponse();
					
				} else {
					//Util.showAlert(this, "", abstractReviewList.getError());
				}
			}else {
				Util.showAlert(this, "Message", "Request Failed");
				//finish();
			}
		}
	}

}

package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.pwd_reg.AbstractPasswordReg;
import com.strobilanthes.forrestii.model.pwd_reg.PasswordReg;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;


public class SetPasswordFragment extends BaseFragment<DBAccess> {
	
	@InjectView(R.id.set_pwd_screen_enterpwd)   private EditText setPwdScreenEnterpwd;
	@InjectView(R.id.set_pwd_screen_reenterpwd) private EditText setPwdScreenReenterpwd;
	@InjectView(R.id.set_pwd_screen_btnDone)    private Button setPwdScreenBtnDone;
	@InjectView(R.id.set_pwd_txt_header)    private TextView setPwdTxtHeader;

	
	@Inject PasswordReg passwordRegModel;
	private Typeface mFontLight,mFontDark;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.frg_set_pwd, null);

		super.initilizeContents();
		
		return view;
	}
	
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		passwordRegModel.setContext(getActivity());
		setPwdScreenBtnDone.setOnClickListener(this);
		setPwdScreenEnterpwd.setFilters(Util.restrictSpaceInEdit(setPwdScreenEnterpwd));
		setPwdScreenReenterpwd.setFilters(Util.restrictSpaceInEdit(setPwdScreenReenterpwd));

		fontStyles();

	}



	public void fontStyles() {

		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
			setPwdScreenEnterpwd.setTypeface(mFontDark);
			setPwdScreenReenterpwd.setTypeface(mFontDark);
			setPwdScreenBtnDone.setTypeface(mFontDark);
			setPwdTxtHeader.setTypeface(mFontLight);
	}
		
		
	@Override
	public boolean onBackPressed() {
		
		return super.onBackPressed();
	}


	@Override
	public void onResume(){
		super.onResume();
		passwordRegModel.addChangeListener(this);
		
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		passwordRegModel.removeChangeListener(this);

	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		 if(event.getPropertyName().equals("password_registration")){
			AbstractPasswordReg abstractPasswordReg = passwordRegModel.getPassRegDetails();
			if(abstractPasswordReg != null){
				updatePwdRegistration(abstractPasswordReg);
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}
		}
	}

	private void updatePwdRegistration(AbstractPasswordReg abstractPasswordReg){
		if(abstractPasswordReg.getStatus() == 1){
			getHelper().updatePwd(setPwdScreenEnterpwd.getText().toString().trim(), getHelper().getPwd());
			
			if(getFragmentManager().getBackStackEntryCount() > 0){
//				for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); i++) {
//					getFragmentManager().popBackStack();
					FragmentManager.BackStackEntry first = getFragmentManager().getBackStackEntryAt(0);
					getFragmentManager().popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
//				}
			}
			Util.showAlert(getActivity(),"Message", "Password updated successfully");
			/*FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left, R.anim.out_to_right);
	        //fragmentTransaction.addToBackStack(null);
	        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	        LoginFragment loginFragment = new LoginFragment();
	        fragmentTransaction.replace(R.id.login_root, loginFragment);
	        fragmentTransaction.commit();*/
	        
			/*startActivity(new Intent(getActivity(), MainActivity.class));
			getActivity().finish();*/
		}else{
			Util.showAlert(getActivity(),"Message", ""+abstractPasswordReg.getError());
		}
		//L.ToastMessage(AuthenticationActivity.this, "Login = "+authentication.getIsSuccess());
	}
	
	@Override
	public void onClick(View v) {
		Util.hideSoftKeyboard(getActivity(), v);

		switch (v.getId()) {
		case R.id.set_pwd_screen_btnDone:
			if(setPwdScreenEnterpwd.getText().toString() != null && setPwdScreenEnterpwd.getText().toString().length() > 0){
				if(setPwdScreenReenterpwd.getText().toString() != null && setPwdScreenReenterpwd.getText().toString().length() > 0){
					if(setPwdScreenEnterpwd.getText().toString().equals(setPwdScreenReenterpwd.getText().toString())){
						if(setPwdScreenEnterpwd.getText().toString().length()>= 8 && setPwdScreenReenterpwd.getText().toString().length() >= 8){
							if(setPwdScreenEnterpwd.getText().toString().length() <= 15 && setPwdScreenReenterpwd.getText().toString().length() <= 15)
							{
								JSONObject object = new JSONObject();
							 try {
								
								object.put("mobile", getHelper().getMobileNo());
								object.put("password", setPwdScreenEnterpwd.getText().toString());
								//object.put("isNeedLookupList", true);
								//authenticationModel.requestPwdRegistrationDetails(Constant.PWD_REGISTATION,PostData.getPasswordRegistrationPostData(enter_pwd.getText().toString()));
								passwordRegModel.requestPwdRegistrationDetails(Constant.SET_NEWPASSWORD,object);
								
							 } catch (JSONException e) {
								e.printStackTrace();
							 }
							}else{
								setPwdScreenEnterpwd.setError("Password can't have more than 15 characters");
								setPwdScreenReenterpwd.setError("Password can't have more than 15 characters");
							}
						}else{
							setPwdScreenEnterpwd.setError( "Enter a new password with minimum 8 characters (lower and upper case) & numbers");
							setPwdScreenReenterpwd.setError("Enter a new password with minimum 8 characters (lower and upper case) & numbers");
						}
					}else{
						setPwdScreenReenterpwd.setError("Password mismatch");
					}
				}else{
					setPwdScreenReenterpwd.setError("Please re-enter your password");
				}
			}else{
				setPwdScreenEnterpwd.setError( "Please enter your password");
			}
			
			break;
		
		default:
			break;
		}
	}
	
}

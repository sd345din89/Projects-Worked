package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.strobilanthes.forrestii.R;

public class ProfileT2andT3SkillFragment extends ScrollTabHolderFragment implements OnScrollListener {
	private int mPosition;
	public static final String ARG_POSITION = "position";
	private ListView mListView;
	private ArrayList<String>SecondarySkillAl = new ArrayList<String>();
	private String strPrimaryskill;
	private Typeface mFontLight,mFontDark;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.frg_cmn_list_user_t2_t3_basic_skill, null);
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		Bundle b = getArguments();
		mPosition = getArguments().getInt(ARG_POSITION);
		SecondarySkillAl = (ArrayList<String>) b.getSerializable("secondaryskill");
		strPrimaryskill = b.getString("primarySkill");
		mListView = (ListView) view.findViewById(R.id.profileT2_generallistview);
		View placeHolderView = inflater.inflate(R.layout.view_pagestrip_header_placeholder, mListView, false);
		mListView.addHeaderView(placeHolderView);
		
		return view;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

	}
	@Override
	public void adjustScroll(int scrollHeight) {
		
		if (scrollHeight == 0 && mListView.getChildCount()>= 1) {
			return;
		}
		mListView.setSelectionFromTop(1, scrollHeight);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mListView.setOnScrollListener(this);
		mListView.setAdapter(new CustomGeneralAdapter(getActivity(), SecondarySkillAl));

	
	}
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		
		if (mScrollTabHolder != null)
			mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
		
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		super.onViewCreated(view, savedInstanceState);
		

	}

	class CustomGeneralAdapter extends BaseAdapter{
		private LayoutInflater mLayoutInflater;
		ArrayList<String>Al;
		
		public CustomGeneralAdapter(Activity activity,
				ArrayList<String> mListItems) {
			// TODO Auto-generated constructor stub
			mLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.Al = mListItems;
		}

		@Override
		public int getCount() {
			
			return Al.size();
		}
	
		@Override
		public Object getItem(int position) {
			
			return position;
		}
	
		@Override
		public long getItemId(int position) {
			
			return 0;
		}
	
		@Override
		public View getView(int position, View convertView, ViewGroup paramViewGroup) {
			
			View v = convertView;
			ViewHolder holder = null;
			if (v == null) {
				v = mLayoutInflater.inflate(R.layout.frg_cmn_user_t2_t3_skill_listitem, null);
				holder = new ViewHolder();
				holder.txt_primarySkillName = (TextView)v.findViewById( R.id.edt_primarySkill);
				holder.txt_secondaryskillName = (TextView)v.findViewById(R.id.edt_secondarySkill);
//				holder.img_edtPrimarySkill = (ImageView)v.findViewById( R.id.img_skill_edit);
//				holder.img_edtSecondarySkill = (ImageView)v.findViewById( R.id.img_secondaryskill_edit);
				holder.txt_headerPrimaryskill = (TextView)v.findViewById(R.id.profile_headerprimaryskill);
				holder.txt_headerSecondarySkill = (TextView)v.findViewById(R.id.txt_header_secondary);
				holder.lyt_primarySkill = (LinearLayout)v.findViewById(R.id.profilelyt_primaryskill);
				holder.txt_addnewSkill = (Button)v.findViewById(R.id.add_newSecondarySkill);
				holder.img_deleteSecondaryskill = (ImageView)v.findViewById(R.id.img_secondaryskill_delete);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			holder.txt_primarySkillName.setTypeface(mFontLight);
			holder.txt_secondaryskillName.setTypeface(mFontLight);
			holder.txt_headerPrimaryskill.setTypeface(mFontLight);
			holder.txt_headerSecondarySkill.setTypeface(mFontLight);
			holder.txt_addnewSkill.setTypeface(mFontLight);
			
			holder.txt_addnewSkill.setVisibility(View.GONE);
			holder.img_deleteSecondaryskill.setVisibility(View.GONE);
//			holder.img_edtPrimarySkill.setVisibility(View.GONE);
//			holder.img_edtSecondarySkill .setVisibility(View.GONE);
			if(position == 0){
				holder.txt_headerPrimaryskill.setVisibility(View.VISIBLE);
				holder.txt_headerSecondarySkill.setVisibility(View.VISIBLE);
				holder.lyt_primarySkill.setVisibility(View.VISIBLE);
				
			}else{
				holder.txt_headerPrimaryskill.setVisibility(View.GONE);
				holder.txt_headerSecondarySkill.setVisibility(View.GONE);
				holder.lyt_primarySkill.setVisibility(View.GONE);
				
			}
			holder.txt_primarySkillName.setText(strPrimaryskill);
			holder.txt_secondaryskillName.setText(Al.get(position));
				return v;
		}
		class ViewHolder {
			private TextView txt_headerPrimaryskill,txt_headerSecondarySkill;
			private ImageView img_deleteSecondaryskill;
			private LinearLayout lyt_primarySkill;
			private Button txt_addnewSkill;
			private TextView txt_primarySkillName,txt_secondaryskillName;
		}
		
}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
	}
	@Override
	public void onClick(View v) {
		
	}
//	public static Fragment newInstance(int position) {
//		GeneralFragment f = new GeneralFragment();
//		Bundle b = new Bundle();
//		b.putInt(ARG_POSITION, position);
//		//b.putSerializable("secondaryskill", FriendsProfileT2Activity.TotalSkillAL);
//		f.setArguments(b);
//		return f;
//	}

}

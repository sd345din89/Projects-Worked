package com.strobilanthes.forrestii.model.settings;

import java.io.Serializable;


public class AbstractNotificationSettings implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int notifytypeId;
	private String notifyName;
	
	
	public int getNotifytypeId() {
		return notifytypeId;
	}
	public void setNotifytypeId(int notifytypeId) {
		this.notifytypeId = notifytypeId;
	}
	public String getNotifyName() {
		return notifyName;
	}
	public void setNotifyName(String notifyName) {
		this.notifyName = notifyName;
	}
	
	
	
}

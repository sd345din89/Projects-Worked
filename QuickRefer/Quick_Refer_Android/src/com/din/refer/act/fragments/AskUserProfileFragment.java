package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import a_vcard.android.util.Log;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.activities.MainActivity;
import com.strobilanthes.forrestii.activities.SignupActivity;
import com.strobilanthes.forrestii.activities.SignupActivity.SignupTickListener;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.pwd_reg.PasswordReg;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUpdateUserProfile;
import com.strobilanthes.forrestii.model.userprofile.UserProfile;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;


public class AskUserProfileFragment extends BaseFragment<DBAccess> implements SignupTickListener{

	private static final String TAG = "Forrestii";
	//
	//@InjectView(R.id.login_screen_btnFinish)   			   private Button btn_finish;
	//@InjectView(R.id.login_profile_data)       			   private LinearLayout lyt_scroll;
	//@InjectView(R.id.login_signup_fields_txt_hint)		   private TextView txt_logo_hint;
	//@InjectView(R.id.login_signup_fields_txt_logo)		   private TextView txt_logo;
	@InjectView(R.id.ask_profile_firstname_edt)     		   private EditText edtFirstName;
	@InjectView(R.id.ask_profile_lastname_edt)     			   private EditText edtLastName;
	@InjectView(R.id.ask_profile_profession_edt)  			   private AutoCompleteTextView autoCompleteProfession;
	@InjectView(R.id.ask_profile_email_edt)    				   private AutoCompleteTextView edtUpdateEmail;
	@InjectView(R.id.ask_profile_dateofbirth_edt)       	   private EditText edtUpdateDob;
//	@InjectView(R.id.login_signup_fields_txt_gender)       	   private TextView edtUpdateGender;
	@InjectView(R.id.ask_profile_password_edt)				   private EditText edtUpdatePassword;
	@InjectView(R.id.ask_profile_done_btn)				   	   private Button btn_done;
	@InjectView(R.id.ask_profile_txt_hint)				   	   private TextView askProfileTxtHint;	
	@InjectView(R.id.radio_btn_male)         				   private RadioButton radioBtnMale;
	@InjectView(R.id.radio_btn_Female)                         private RadioButton radioBtnFemale;

	@Inject UserProfile userProfileModel;
	@Inject PasswordReg passwordRegModel;
	
	private int mDay;
	private int mMonth;
	private int mYear;
	private int SkillsetId;
	private Typeface mFontLight,mFontDark;
	
	
	
	public interface AskProfileListener{
		public void onAskProfileFinished();
	}
	
	public AskProfileListener askProfileListener;
	public SignupTickListener signupTickListener;
	
	@Override
    public void onAttach(Activity activity) {
        
        try {
        	askProfileListener = (AskProfileListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement AskProfileListener");
        }
        super.onAttach(activity);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.frg_ask_userprofile, container,false);

		super.initilizeContents();

		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		fontStyles();
		
		userProfileModel.setContext(getActivity());
		passwordRegModel.setContext(getActivity());
		
		btn_done.setOnClickListener(this);
		
		askProfileListener = (SignupActivity) getActivity();
		
		ArrayList<AbstractSkillSet> abstractSkillSets = getHelper().getDBSkillset();
		if(abstractSkillSets != null){
			autoCompleteProfession.setAdapter(new ArrayAdapter<AbstractSkillSet>(getActivity(),  R.layout.simple_list_item, abstractSkillSets));
			autoCompleteProfession.setSelection(0);
			autoCompleteProfession.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,	int position, long id) {
					AbstractSkillSet abstractSkillSet = (AbstractSkillSet) parent.getItemAtPosition(position);
					abstractSkillSet.getSkillName();
					SkillsetId = abstractSkillSet.getSkillId();
					
					L.d("Selected SkillName = "+abstractSkillSet.toString());
				}
			});
			
			autoCompleteProfession.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					SkillsetId = -1;
					s.toString();
				}
			});
		}
		
//		loadforDate();
		getAccounts();
		
		Calendar cal = Calendar.getInstance();
		mDay = cal.get(Calendar.DAY_OF_MONTH);
		mMonth = cal.get(Calendar.MONTH);
		mYear = cal.get(Calendar.YEAR);
		
		edtUpdateDob.setText(""+mDay +"/"+(mMonth+1)+"/"+mYear);
		edtUpdateDob.setOnClickListener(this);
		
		edtUpdatePassword.setFilters(Util.restrictSpaceInEdit(edtUpdatePassword));
		/*edtUpdateDateDay.setText(mDay+"");
		edtUpdateDateMonth.setText(mMonth+"");
		edtUpdateDateYear.setText(mYear+"");
		
		edtUpdateDob.setOnClickListener(mDatePicker);
		edtUpdateDateDay.setOnClickListener(mDatePicker);
		edtUpdateDateMonth.setOnClickListener(mDatePicker);
		edtUpdateDateYear.setOnClickListener(mDatePicker);*/
	}
	
	//used to load the date for date of birth
	/*private void loadforDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -18);
		int last = cal.get(Calendar.YEAR);
		
		cal = Calendar.getInstance();
	    cal.add(Calendar.YEAR, -100);
	    int start = cal.get(Calendar.YEAR);
	    
	    int totalyears = last-start;
	    String[] date = new String[31];
		String[] month = new String[12];
		String[] year = new String[totalyears]; 
		
		for (int i = 0; i < totalyears ; i++) {
			year[i]= (start+i)+"";
		}
		for (int i = 0; i < 31; i++) {
			date[i] = (i+1)+"";
			if (i < 12) {
				month[i] = (i+1)+"";
			}
		}
		edtUpdateDateDay.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, date));
		edtUpdateDateMonth.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, month));
		edtUpdateDateYear.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, year));
		
		edtUpdateDateDay.setOnFocusChangeListener(setOnFocusForDOB);
		edtUpdateDateMonth.setOnFocusChangeListener(setOnFocusForDOB);
		edtUpdateDateYear.setOnFocusChangeListener(setOnFocusForDOB);
	}*/
	
	//get primary and other account details
	
	void getAccounts(){
		
		AccountManager accountManager = AccountManager.get(getActivity().getApplicationContext());
		Account accds[]= accountManager.getAccounts();
		if (accds.length <= 0) {
			return;
		} else {
			String[] emails = new String[accds.length-1];
			for (int i = 0; i < accds.length; i++) {
				Log.d(TAG, accds[i].name);
				if (i!=0) {
					emails[i-1] = accds[i].name;
				}
			}
			
			Log.d(TAG, "Length "+accds.length);
			
			edtUpdateEmail.setAdapter(new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.common_text_color_autocomplete, emails));
			edtUpdateEmail.setOnFocusChangeListener(setOnFocusForAutocomplete);
			edtUpdateEmail.setThreshold(0);
			
			Account[] local = accountManager.getAccountsByType(("com.google"));
			if (local.length >0 && local[0] != null) {
				edtUpdateEmail.setText(accountManager.getAccountsByType(("com.google"))[0].name);
			}
			
			if (Build.VERSION.SDK_INT > 13) {
				Cursor c = getActivity().getApplication().getContentResolver().query(ContactsContract.Profile.CONTENT_URI, null, null, null, null); 
				c.moveToFirst();
				int cnt = c.getColumnCount();
				for (int i = 0; i < cnt; i++) {
					edtFirstName.setText(c.getString(c.getColumnIndex("display_name")));
				}
				c.close();
			}
		}
	}
	
	//focus for drop down date picker
	OnFocusChangeListener setOnFocusForAutocomplete = new OnFocusChangeListener() {
		
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			((AutoCompleteTextView) v).showDropDown();
		}
		
	};
	
	OnClickListener mDatePicker = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			DateDialog();
		}
	};
	public void fontStyles() {

		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		edtUpdateDob.setTypeface(mFontDark);
		edtUpdatePassword.setTypeface(mFontDark);
		edtLastName.setTypeface(mFontDark);
		edtUpdateEmail.setTypeface(mFontDark);
		edtFirstName.setTypeface(mFontDark);
		autoCompleteProfession.setTypeface(mFontDark);
		radioBtnMale.setTypeface(mFontDark);
		radioBtnFemale.setTypeface(mFontDark);
		btn_done.setTypeface(mFontDark);
		askProfileTxtHint.setTypeface(mFontLight);
	}

	@Override
	public void onResume() {
		super.onResume();
		userProfileModel.addChangeListener(this);
		passwordRegModel.addChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		Util.hideSoftKeyboard(getActivity(), edtFirstName);
		userProfileModel.removeChangeListener(this);
		passwordRegModel.removeChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {

		if (event.getPropertyName().equals("updateuserprofile")) {
			AbstractUpdateUserProfile abstractUpdateUserProfile = userProfileModel
					.getUserUpdatedProfile();

			if (abstractUpdateUserProfile != null) {
				if (abstractUpdateUserProfile.getStatus() == 1) {

					getHelper().insertPageId(Constant.PAGE_MAIN_PAGE);

					// AbstractUserProfile userAbstractUserProfile=
					// getHelper().getDBUserProfile();

					getHelper().updateUserId(abstractUpdateUserProfile.getResponse().getUserProfile().getUserId());
					getHelper().insertUserProfile(abstractUpdateUserProfile.getResponse().getUserProfile());
					getHelper().insertSettingsDetails(abstractUpdateUserProfile.getResponse().getSettings().get(0));
					
					updatePwdRegistration(abstractUpdateUserProfile);
					
					/*JSONObject object = new JSONObject();
					 try {
						 object.put("mobile", getHelper().getMobileNo());
						 object.put("password", edtUpdatePassword.getText().toString().trim());
						 passwordRegModel.requestPwdRegistrationDetails(Constant.SET_NEWPASSWORD,object);
						
					 } catch (JSONException e) {
						e.printStackTrace();
					 }*/
					
					
				} else {
					Util.showAlert(getActivity(), "Message",abstractUpdateUserProfile.getError());
				}
			} else {
				Util.showAlert(getActivity(), "Message", "Request Failed.");
			}
		} else if(event.getPropertyName().equals("password_registration")){/*
			AbstractPasswordReg abstractPasswordReg = passwordRegModel.getPassRegDetails();
			if(abstractPasswordReg != null){
				updatePwdRegistration(abstractPasswordReg);
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
		*/}

	}

	private void updatePwdRegistration(AbstractUpdateUserProfile abstractUpdateUserProfile){
		if(abstractUpdateUserProfile.getStatus() == 1){
			Intent createCardIntent = new Intent(getActivity(),MainActivity.class);
			getHelper().updatePwd(edtUpdatePassword.getText().toString().trim(), "");
			startActivity(createCardIntent);
			getActivity().setResult(Activity.RESULT_OK);
			getActivity().finish();
			
		}else{
			Util.showAlert(getActivity(),"Message", ""+abstractUpdateUserProfile.getError());
		}
		//L.ToastMessage(AuthenticationActivity.this, "Login = "+authentication.getIsSuccess());
	}
	public void DateDialog() {

		
		OnDateSetListener listener = new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
//					int maxYear = mYear - 18;
//					int maxMonth = mMonth;
//					int maxDay = mDay;
//
//					int minYear = mYear - 100;
//					int minMonth = mMonth;
//					int minDay = mDay;
//
//					if (year > maxYear || monthOfYear > maxMonth && year == maxYear
//							|| dayOfMonth > maxDay && year == maxYear
//							&& monthOfYear == maxMonth) {
//						
//						L.ToastMessage(UpdateUserProfileActivity.this, "The Age should be atleast 18");
//
//					} else if (year < minYear || monthOfYear < minMonth
//							&& year == minYear || dayOfMonth < minDay
//							&& year == minYear && monthOfYear == minMonth) {
//						
//						L.ToastMessage(UpdateUserProfileActivity.this, "The Maximum Age limit is 100");
//						
//					} else {

					// view.updateDate(year, monthOfYear, dayOfMonth);
					mYear = year;
					mMonth = monthOfYear;
					mDay = dayOfMonth;
					//edtUpdateDateDay.setText(mDay+"");
					//edtUpdateDateMonth.setText(mMonth+"");
					//edtUpdateDateYear.setText(mYear+"");
					
					edtUpdateDob.setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
//					}

			}
		};

		DatePickerDialog dpDialog = new DatePickerDialog(getActivity(),
				AlertDialog.THEME_HOLO_LIGHT, listener, mYear, mMonth, mDay);
		dpDialog.show();
		//dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

	}
	
	@Override
	public boolean onBackPressed() {
		
		return super.onBackPressed();
	}


	@Override
	public void onClick(View v) {
		Util.hideSoftKeyboard(getActivity(), v);
		switch (v.getId()) {
		case R.id.ask_profile_dateofbirth_edt:
			DateDialog();
			break;
		case R.id.ask_profile_done_btn:
			Util.setTouchDelay(getActivity(), v);
			String firstName = edtFirstName.getText().toString();
			String lastName = edtLastName.getText().toString();
			String email = edtUpdateEmail.getText().toString();
			String Dob = edtUpdateDob.getText().toString();
			String profession = autoCompleteProfession.getText().toString();
			String password = edtUpdatePassword.getText().toString();
			String s[] = Dob.split("/");
			int selectedDay = Integer.parseInt(s[0]);
			int selectedMonth = Integer.parseInt(s[1]);
			int selectedYear = Integer.parseInt(s[2]);
			
			int age = Util.getAge(selectedYear,selectedMonth,selectedDay);
			L.d("age+++"+age);
			String gender = null;
			
			if(firstName.trim().length() < 3){
				//edtFirstName.requestFocus();
				edtFirstName.setFocusableInTouchMode(true);
				edtFirstName.requestFocus();
				edtFirstName.setError("First name should have at least 3 characters");
				return;
			}
			if(lastName.trim().length() < 1){
				//edtLastName.requestFocus();
				edtLastName.setFocusableInTouchMode(true);
				edtLastName.requestFocus();
				edtLastName.setError("Last name should have at least 1 character");
				Util.hideEditTextError(edtLastName);
				return;
			}
			if (SkillsetId == -1) {
				autoCompleteProfession.setFocusableInTouchMode(true);
				autoCompleteProfession.requestFocus();
				autoCompleteProfession.setError("Please select a valid skill set");
				return;
			}
			if (profession.trim().length() <= 0) {
				//autoCompleteProfession.requestFocus();
				
				autoCompleteProfession.setFocusableInTouchMode(true);
				autoCompleteProfession.requestFocus();
				autoCompleteProfession.setError("Please select your primary skill");
				return;
			}
			
			/*if (lastName.trim().length() <= 0) {
				edtLastName.setError("This field should not be empty.");
				return;
			}*/
			
			if (!(radioBtnMale.isChecked() || radioBtnFemale.isChecked())) {
				L.ToastMessage(getActivity(), "Please select Gender");
				return;
			}
			
			if(Util.isValidUserName(firstName)){
				if(Util.isValidUserName(lastName)){
					String[] i = email.split("@");
					String beforeText = i[0];
						if (Util.isValidEmail(email) && beforeText.length()<=64) {
							if ((password.length() < 8)) {
								edtUpdatePassword.setFocusableInTouchMode(true);
								edtUpdatePassword.requestFocus();
								edtUpdatePassword.setError("Password should be between 8 to 15 characters");
								return;
							}
							if ((password.length() > 16)) {
								edtUpdatePassword.setFocusableInTouchMode(true);
								edtUpdatePassword.requestFocus();
								edtUpdatePassword.setError("Password should be between 8 to 15 characters");
								return;
							}
							if(password.equals(getHelper().getMobileNo())){
								edtUpdatePassword.setFocusableInTouchMode(true);
								edtUpdatePassword.requestFocus();
								edtUpdatePassword.setError("The password should not be same as your mobile number");
								return;
							}
							
							if(age>=18){
								if(age<=100){
							
							if (radioBtnMale.isChecked()) {
								gender = "m";
							} else if(radioBtnFemale.isChecked()){
								gender = "f";
							}
			
							JSONObject jobject = new JSONObject();
							try {
								
								SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Forrestii", Context.MODE_PRIVATE);
								String latitude = sharedPreferences.getString("latitude", "0");
								String longitude = sharedPreferences.getString("longitude", "0");
								sharedPreferences.edit().putBoolean("storyActionbar", false).commit();
								sharedPreferences.edit().putBoolean("storyMenuDrawer", false).commit();
								
								JSONObject jobjSkills = new JSONObject();
								jobjSkills.put("Skills",new JSONObject().put("skillId", SkillsetId));
								jobjSkills.put("IsPrimarySkill", true);
								
								jobject.put("userProfile", new JSONObject().put("firstName", firstName).put("lastName", lastName).put("DOB", Dob).put("email", email)
										.put("gender", gender).put("mobile", getHelper().getMobileNo()).put("UserSkillSets",
										new JSONArray().put(jobjSkills)).put("password", password).put("userId", getHelper().getUserId()));
								jobject.put("latitude",Double.parseDouble(latitude));
								jobject.put("longitude",Double.parseDouble(longitude));
								jobject.put("deviceType", "android");
								jobject.put("deviceId", sharedPreferences.getString("regid", "No device id found"));
								Log.d("registration data", jobject.toString());
								
								userProfileModel.updateUserProfile(Constant.COMPLETE_REG, jobject);
								
							} catch (JSONException e) {
								e.printStackTrace();
							}
						} else {
							Util.showAlert(getActivity(), "", "The Maximum Age limit is 100");

							
						}
					} else {
//					edtUpdateDob.clearFocus();
//					edtUpdateDob.requestFocus();
//					edtUpdateDob.setFocusableInTouchMode(true);
					//edtUpdateDob.setFocusable(true);
						//edtUpdateDob.setError("The Maximum Age limit is 100.");
						Util.showAlert(getActivity(), "", "You're too young! Come back when you're 18");

					}
				}else{
					edtUpdateEmail.setFocusableInTouchMode(true);
					edtUpdateEmail.requestFocus();
					edtUpdateEmail.setError("Please enter a valid email");
//					edtUpdateDob.clearFocus();
//					edtUpdateDob.requestFocus();
//					edtUpdateDob.setFocusableInTouchMode(true);
					//edtUpdateDob.setFocusable(true);
					//edtUpdateDob.setError("You're too young! Come back when you're 18");
				}
				}else{
					edtLastName.setFocusableInTouchMode(true);
					edtLastName.requestFocus();
					edtLastName.setError("The last name should not contain any special characters or space");
					Util.hideEditTextError(edtLastName);

				}
			} else {
				edtFirstName.setFocusableInTouchMode(true);
				edtFirstName.requestFocus();
				edtFirstName.setError("The first name should not contain any special characters or space");
			}
			
			break;
		default:
			break;
		}
	}

	@Override
	public void onSignupCalled(Activity activity) {
		//L.ToastMessage(getActivity(), "Signup Called");
		L.d("OnSignup Working...");
		askProfileListener = (AskProfileListener) activity;
		
		//edtUpdateProfession.setText("Tester");
		
		askProfileListener.onAskProfileFinished();
	}

	
}

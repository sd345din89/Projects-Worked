package com.strobilanthes.forrestii.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.util.Constant;

public class SplashActivity extends BaseActivity<DBAccess> {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_splash);

		// L.d("URI Path = "+getIntent().getData().getLastPathSegment());
		//L.d("URI Path = "+getIntent().getData().getPathSegments().size());

		PackageInfo pInfo = null;
		
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		((TextView)findViewById(R.id.txt_versionname)).setText(""+pInfo.versionName);
		
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				
				Intent i = null;
				if (getHelper().getPageId() == Constant.PAGE_MAIN_PAGE) {
					
					i = new Intent(SplashActivity.this,MainActivity.class);
					startActivity(i);
					finish();
					
				}else {
					
					i = new Intent(SplashActivity.this,
							AuthenticationActivity.class);
					startActivity(i);
					finish();
					
				}
			}
		}, 3000);
	}

}
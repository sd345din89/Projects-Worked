package kra.accounts.tracker.models;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationService implements LocationListener{
	
	private Context mContext;

	private LocationManager locationManager;
	
	public Location cLocation;

	private String locProvider;
	
	private String TAG = "LocationService";
	
	private Util util;
	
	
	public LocationService(Context context){
		
		this.mContext = context;
		locationManager = (LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE);
		
		locProvider = null;
		
		util = new Util();
		
		if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			
			locProvider = LocationManager.GPS_PROVIDER;
			
		}else if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
			
			locProvider = LocationManager.NETWORK_PROVIDER;
			
		}
		
		if(locProvider != null){
			locationManager.requestLocationUpdates(locProvider, 0, 1, this);
		}
		
		
	}

	public void onLocationChanged(Location location) {
		
		//LogAMS.d(TAG, ">>>>>location="+location.getLatitude()+"|"+location.getLongitude());
		this.cLocation = location;
		//util.updateCurrentLocationData(""+location.getLatitude(), ""+location.getLongitude());
		
		
	}

	public void onProviderDisabled(String provider) {
		
		this.locProvider = null;
		
	}
	

	public void onProviderEnabled(String provider){
		
		this.locProvider = provider;
		
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}


	public void stopLocationUpdate(){
		locationManager.removeUpdates(this);
		locationManager = null;
	}
	
}

package com.strobilanthes.forrestii.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.json.JSONObject;

import a_vcard.android.provider.Contacts;
import a_vcard.android.syncml.pim.vcard.ContactStruct;
import a_vcard.android.syncml.pim.vcard.VCardComposer;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

//import com.integralblue.httpresponsecache.compat.java.io.IOException;

public class Util {

	//private static Activity context;
	// private static Util instance;
	public boolean isDeviceHasInternet;
	public HashMap<String, String> errorMap;
	public ArrayList<Bitmap> bitmapArraylist;
	public static boolean LocationEnabled;
	public File cacheDirectory;
	
	public static Toast customToast;

	private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    private static final int DAY = 24 * HOUR;
    private static final int MONTH = 12 * DAY;
    private static final int YEAR = 24 * MONTH;
    
	public static String getPhoneNumber(Context context) {
		/*
		 * try { Intent callIntent = new Intent(Intent.ACTION_CALL); //
		 * callIntent.setData(Uri.parse("tel:#456" + Uri.encode("#")));
		 * //startActivity(callIntent);
		 * 
		 * String encodedHash = Uri.encode("#"); String encodedStar =
		 * Uri.encode("*");
		 * 
		 * String ussd = encodedStar + "282" + encodedHash;
		 * startActivityForResult(new Intent("android.intent.action.CALL",
		 * Uri.parse("tel:" + ussd)), 1); Log.d("********USSD********", ussd);
		 * 
		 * } catch (ActivityNotFoundException activityException) {
		 * Log.e("helloandroid dialing example",
		 * "Call failed",activityException); }
		 */
		AccountManager am = AccountManager.get(context);
		Account[] accounts = am.getAccounts();
		String phoneNumber = null;
		for (Account ac : accounts) {

			String acname = ac.name;
			String actype = ac.type;
			if (actype.equals("com.whatsapp")) {
				phoneNumber = acname;
			}
			// Take your time to look at all available accounts
			System.out.println("Accounts : " + acname + ", " + actype
					+ phoneNumber);
			// System.out.println("Accounts : " + phoneNumber);
			
		}
		if (phoneNumber != null)
			if (!phoneNumber.matches(".*\\d.*"))
				phoneNumber = null;

		return phoneNumber;

	}


	public static void isLocationServiceON(final Context context){
		LocationEnabled = false;
		LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) && !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			  LocationEnabled = false;
			  // Build the alert dialog
			  AlertDialog.Builder builder = new AlertDialog.Builder(context,AlertDialog.THEME_HOLO_LIGHT);
			  builder.setTitle("Location Services Not Active");
			  builder.setMessage("Please enable Location Services");
			  builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialogInterface, int i) {
			    // Show location settings when the user acknowledges the alert dialog
			    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			    context.startActivity(intent);
			    }
			  });
			  builder.setNegativeButton("Cancel", null);
			  
			  Dialog alertDialog = builder.create();
			  alertDialog.setCanceledOnTouchOutside(false);
			  alertDialog.show();
		} else {
			LocationEnabled = true;
		}
	}
	public static Bitmap getRotatedBitmap(String path, Bitmap bitmap) {
		Bitmap rotatedBitmap = null;
		Matrix m = new Matrix();
		ExifInterface exif = null;
		int orientation = 1;

		try {
			if (path != null) {
				// Getting Exif information of the file
				exif = new ExifInterface(path);
			}
			if (exif != null) {
				orientation = exif.getAttributeInt(
						ExifInterface.TAG_ORIENTATION, 0);
				switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_270:
					m.preRotate(270);
					break;

				case ExifInterface.ORIENTATION_ROTATE_90:
					m.preRotate(90);
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					m.preRotate(180);
					break;

				}
				// Rotates the image according to the orientation
				rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
						bitmap.getWidth(), bitmap.getHeight(), m, true);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return rotatedBitmap;
	}

	public static String getFilePath(Uri data,Context context) {
		String path = "";

		// For non-gallery application
		path = data.getPath();

		// For gallery application
		String[] filePathColumn = { MediaStore.Images.Media.DATA };
		Cursor cursor = context.getContentResolver().query(data,
				filePathColumn, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			path = cursor.getString(columnIndex);
			cursor.close();
		}
		return path;
	}

	public static String uploadImage(int userId, String Url,
			byte[] imgByteArray) {

		String result = null;
		try {
			// Log.d(TAG,""+Url);
			URL url = new URL(Url);
			HttpURLConnection http = (HttpURLConnection) url.openConnection();
			http.setConnectTimeout(4000);
			http.setDoInput(true);
			
			if (android.os.Build.VERSION.SDK_INT < 14)
				http.setDoOutput(true);
			
			//HttpParams myParams = new BasicHttpParams();
			//myParams.setParameter("userId", userId);
			//HttpConnectionParams.setConnectionTimeout(myParams, 4000);
			//HttpProtocolParams.set
			http.setRequestMethod("POST");

			if (imgByteArray != null) { // For Upload booth image

				L.v("Byte Array Length = : " + imgByteArray.length);
				L.v("Url = : " + Url);
				L.v("UserId :" + userId);
				
				http.setRequestProperty("userid", ""+userId);
				http.setRequestProperty("Content-Length", ""+imgByteArray.length);
				http.setRequestProperty("Content-Type", "image/png");
				
				DataOutputStream dos = new DataOutputStream(http.getOutputStream());
				dos.write(imgByteArray);
				dos.flush();
				dos.close();

			}

			// convert response to string
			try {
				InputStream is = http.getInputStream();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "utf-8"));
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				result = sb.toString();
				L.v("RESULT: " + result);
			} catch (Exception e) {
				e.printStackTrace();
				L.e("log_tag", e);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return result;
	}

	public void uploadImageHttpClient() {

	}

	public static void sendSMS(String mobileNum,Context context){
		
		Intent shareIntent = new Intent(Intent.ACTION_VIEW);
		//shareIntent.setAction(android.content.Intent.ACTION_SEND_MULTIPLE);
		shareIntent.setType("vnd.android-dir/mms-sms");
		shareIntent.putExtra("address", mobileNum);

		//shareIntent.setType("image/*");
		shareIntent.putExtra("sms_body", "Hey! I am using Forrestii -- this awesome trust powered network. Join me to find trusted referrals when you need!");
		context.startActivity(shareIntent);
		
		        // prompts only sms-mms clients
		

		/*Uri smsUri = Uri.parse("tel:"+mobileNum);
		Intent intent = new Intent(Intent.ACTION_VIEW, smsUri);
		intent.putExtra("sms_body", "Hey, Iam using forrestii socializing business cards, come and join with me!!!!");
		intent.setType("vnd.android-dir/mms-sms"); 
		context.startActivity(intent);
		
		Intent sendIntent = new Intent(Intent.ACTION_VIEW);
		// sendIntent.setClassName("com.androi;d.mms",
		// "com.android.mms.ui.ComposeMessageActivity");
		//sendIntent.setPackage("com.android.mms");
		sendIntent.setType("vnd.android-dir/mms-sms");
		sendIntent.putExtra("address",mobileNum);
		sendIntent.putExtra("sms_body", "Hey, Iam using forrestii socializing business cards, come and join with me!!!!");
		//sendIntent.putExtra(Intent.EXTRA_STREAM,
		//		Uri.fromFile(new File(image1Path)));
	
		context.startActivity(sendIntent);*/
		
	}
	public String getCurrentDateAndTiming() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	public String getCurrentTime() {

		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		String currentTime = df.format(c.getTime());

		return currentTime;
	}

	public static String updatedOnTime(long updateon){
		
		int JUST_NOW_SECONDS = 5; // in seconds
		 int SECONDS_SCENARIO = 60; // in seconds
		 int MINUTES_SCENARIO = 3600; // in seconds
		 int HOURS_SCENARIO = 86400; // in seconds
		 int DAYS_SCENARIO = 604800; // in seconds
		 int WEEKS_SCENARIO = 730; // in hours
		 int MONTHS_SCENARIO = 8765; // in hours
		
		long currentTimeMillis = System.currentTimeMillis();
		//System.out.println("Given time ==> " + inTime);
		//System.out.print("System time ==> "+currentTimeMillis); //1403369181492
		
	
		long diffSecs = getSeconds(currentTimeMillis) - getSeconds(updateon);
		
		long diffHrs = getHours(currentTimeMillis) - getHours(updateon);
		
		if(diffSecs <= JUST_NOW_SECONDS){
			return "Just now";
		}else if(diffSecs > JUST_NOW_SECONDS && diffSecs < SECONDS_SCENARIO){
			return diffSecs + " seconds ago";
			
		}else if(diffSecs > SECONDS_SCENARIO && diffSecs < MINUTES_SCENARIO){
			return diffSecs/SECONDS_SCENARIO + " minute ago";
			
		}else if(diffSecs > MINUTES_SCENARIO && diffSecs < HOURS_SCENARIO){
			return diffSecs/MINUTES_SCENARIO + " hours ago";
			
		}else if(diffSecs > HOURS_SCENARIO && diffSecs < DAYS_SCENARIO){
			return diffSecs/HOURS_SCENARIO + " Days ago";
			
		}else if(diffSecs > DAYS_SCENARIO && diffHrs < WEEKS_SCENARIO){
			return diffSecs/DAYS_SCENARIO + " Weeks ago";
			
		}else if(diffHrs > WEEKS_SCENARIO && diffHrs < MONTHS_SCENARIO){
			return diffHrs/WEEKS_SCENARIO + " months ago";
			
		}else if(diffHrs > MONTHS_SCENARIO){
			return diffHrs/MONTHS_SCENARIO + " years ago";	
		}
		return null;
	}
	private static long getSeconds(long secTm){
		
		//System.out.println("Getting seconds... ==> " + secTm);
		long secOut = TimeUnit.MILLISECONDS.toSeconds(secTm);
		
		//System.out.println("Result seconds ==> " + secOut);
		return secOut;
	}
	
	private static long getHours(long secTm){
		
		//System.out.println("Getting millis... ==> " + secTm);
		long hrs = TimeUnit.MILLISECONDS.toHours(secTm);
		
		//System.out.println("Result hours ==> " + hrs);
		return hrs;
	}
	
	public static boolean isConnectedNetwork(Context context) {

		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isConnectedOrConnecting();

	}// End of network checkability

	
	public boolean isEmpty(String str) {
		if (str.trim().length() > 0) {
			return false;
		}
		return true;
	}// End of isEmpty

	public static boolean isValidEmail(String email) {
		// Email validation Pattern String
		final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("^[a-zA-Z0-9]+(\\+[a-zA-Z0-9]+)?(\\-[a-zA-Z0-9]+)?(\\_[a-zA-Z0-9]+)?(\\.[a-zA-Z0-9]+)*@[a-zA-Z]+\\.[a-zA-Z]{2,4}(\\.[a-zA-Z]{2,5}){0,1}$");

		if (EMAIL_ADDRESS_PATTERN.matcher(email.trim()).matches()) {
			return true;
		}

		return false;
	}

	public static boolean isValidUserName(String userName){
		// UserName Validation Pattern String
//		final Pattern USER_NAME_PATTERN = Pattern.compile("^[a-zA-Z][a-zA-Z0-9]+$");
		final Pattern USER_NAME_PATTERN = Pattern.compile("^[\\p{L} ]+$");
		if(USER_NAME_PATTERN.matcher(userName.trim()).matches()){
			return true;			
		}
		return false;
	}
	public static boolean isOnlyText(String userName){
		// UserName Validation Pattern String
       final Pattern USER_NAME_PATTERN = Pattern.compile("^[a-zA-Z\\s.,$;@!-_+=%/&*?'#&]+$");
		//final Pattern USER_NAME_PATTERN = Pattern.compile("^[\\p{L} ]+$");
		if(USER_NAME_PATTERN.matcher(userName.trim()).matches()){
			return true;			
		}
		return false;
	}
	
	public static boolean limitSpecialCharacters(String about){
		// UserName Validation Pattern String
		final Pattern USER_NAME_PATTERN = Pattern.compile("^[a-zA-Z0-9.,$;@!-_+=%/&*?'#&\\s]+$");
		if(USER_NAME_PATTERN.matcher(about).matches()){
			return true;			
		}
		return false;
	}
	
	public static boolean isValidPhoneNumber(String mobileNum) {
		if(mobileNum.startsWith("7") ||  mobileNum.startsWith("8")  || mobileNum.startsWith("9"))
			return true;
		else
			return false;
	}

	public static Bitmap getRoundedShapeBitmap(Bitmap bitmap) {
     	
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
	            .getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);
	
	    final int color = 0xff424242;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	    final RectF rectF = new RectF(rect);
	    final float roundPx = 120;
	    final float borderSizePx = 5;
	
	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
	
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(bitmap, rect, rect, paint);
 	    //Bitmap _bmp = Bitmap.createScaledBitmap(targetBitmap, targetWidth/2 + 15, targetHeight/2 + 15 , true);
 	    
	 // draw border
	    /*paint.setColor(border_color);
	    paint.setStyle(Paint.Style.STROKE);
	    paint.setStrokeWidth((float) borderSizePx);
	    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);*/
	    
 	    return output;
 	    
 	    //return targetBitmap;    
     }

	public static void showAlert(Context context, String title, String message) {
		if (customToast != null) {
			customToast.cancel();
		}
		 customToast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
		 customToast.show();
		 
		/*AlertDialog.Builder alertbuilder = new AlertDialog.Builder(context,
				AlertDialog.THEME_HOLO_LIGHT);
		// alertbuilder.setTitle(title);
		alertbuilder.setMessage(message);

		alertbuilder.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});

		alertbuilder.show();*/

	}
	public static void stopAlert(){
		if (customToast != null) {
			customToast.cancel();
		}
	}

	/*public static void getToastNotification(Activity activity){
		View view = activity.getLayoutInflater().inflate(R.layout.crouton_custom_view, null);
		Crouton crouton = Crouton.make(activity, view);
	    crouton.show();
	}*/
	public void trimCache(Context context) {
		try {
			File cachedir = context.getCacheDir();
			File appDir = new File(cachedir.getParent());

			if (appDir.exists()) {
				String[] children = appDir.list();

				for (String s : children) {
					if (!s.equals("lib") && !s.equals("shared_prefs")) {

						deleteDir(new File(appDir, s));
						Log.i("TAG",
								"**************** File /data/data/APP_PACKAGE/"
										+ s + " DELETED *******************");
					}
				}
			}

			if (cachedir != null && cachedir.isDirectory()) {
				deleteDir(cachedir);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		// The directory is now empty so delete it
		return dir.delete();
	}

	public boolean ifJsonValid(JSONObject jsonObject) {

		if (jsonObject == null) {
			return false;
		} else {
			return true;

		}
	}

	public static void setBackground(ImageView imageView, Bitmap roundedBitmap,Context context){
		if(roundedBitmap != null){
			if(Constant.SUPPORTS_JELLYBEAN_APILEVEL_16){
				imageView.setBackground(new BitmapDrawable(context.getResources(),roundedBitmap ));
       	    }else{
       	    	imageView.setBackgroundDrawable(new BitmapDrawable(context.getResources(), roundedBitmap));
       	    }
		}else{
			if(Constant.SUPPORTS_JELLYBEAN_APILEVEL_16){
				imageView.setBackground(context.getResources().getDrawable(R.drawable.default_user));
       	    }else{
       	    	imageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.default_user));
       	    }
		}
	}

	public static void writeToFile(String myCardId, Bitmap bitmap) {

		File file = new File(Constant.IMAGE_PATH);
		if (!file.exists())
			file.mkdirs();
		File outputFile = new File(file, myCardId + ".png");

		// Encode the file as a PNG image.
		FileOutputStream outStream;
		try {

			outStream = new FileOutputStream(outputFile);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
			/* 100 to keep full quality of the image */

			outStream.flush();
			outStream.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void deleteFolderAndAll(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
				deleteFolderAndAll(child);

		fileOrDirectory.delete();
	}

	public void writeMyPhotoFile(String myCardId, Bitmap bitmap) {

		File file = new File(Constant.IMAGE_PATH);
		if (!file.exists())
			file.mkdirs();
		File outputFile = new File(file, myCardId);

		// Encode the file as a PNG image.
		FileOutputStream outStream;
		try {

			outStream = new FileOutputStream(outputFile);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
			/* 100 to keep full quality of the image */

			outStream.flush();
			outStream.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isImgFileAlreadyExists(String myCardId) {

		File file = new File(Constant.IMAGE_PATH + myCardId + ".png");
		if (file.exists())
			return true;
		else
			return false;
	}

	
	  public boolean isMyPhotoFileAlreadyExists(String myCardId) {
		  File file =new File(Constant.IMAGE_PATH+myCardId); if(file.exists())
	     return true; else return false; 
	  }
	 

	public boolean isVcfFileAlreadyExists(String myCardId) {

		File file = new File(Constant.IMAGE_PATH + myCardId + ".vcf");
		if (file.exists())
			return true;
		else
			return false;
	}

	public Bitmap readFromFile(String myCardId) {

		String path = Constant.IMAGE_PATH + myCardId + ".png";
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;

		return BitmapFactory.decodeFile(path, options);

	}

	public Bitmap readMyPhotoFile(String myCardId) {

		String path = Constant.IMAGE_PATH + myCardId;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;

		return BitmapFactory.decodeFile(path, options);

	}


	public byte[] getByteArrayFromImage(String myCardId) {

		String path = Constant.IMAGE_PATH + myCardId + ".png";
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;

		Bitmap bmp = BitmapFactory.decodeFile(path, options);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();

		return byteArray;
	}

	public byte[] getByteArrayFromBitmap(Bitmap mBitmap) {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();

		return byteArray;
	}

	public static float getDimensPxtoDp(int size,Context context) {
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size,
				context.getResources().getDisplayMetrics());
	}

	public static Bitmap decodeScaledBitmapFromSdCard(String filePath,
			int reqSize) {

		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(filePath), null, o);

			// The new size we want to scale to
			final int REQUIRED_SIZE = reqSize;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_SIZE
					&& o.outHeight / scale / 2 >= REQUIRED_SIZE)
				scale *= 2;

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;

			return BitmapFactory.decodeStream(new FileInputStream(filePath),
					null, o2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	public static void showRoundedCornerBitmap(ImageView userImageView, Bitmap bitmap) {

		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		// final float roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		// canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		canvas.drawCircle(output.getWidth() / 2, output.getHeight() / 2,
				output.getWidth() / 2, paint);

		paint.setXfermode(new PorterDuffXfermode(
				android.graphics.PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		userImageView.setImageBitmap(output);
	}

	public static void createVcard(AbstractUserProfile userProfile,byte[] imageData) {
		try {
			/*Toast.makeText(context, "Success!!", Toast.LENGTH_SHORT)
					.show();*/
			File file = new File(Constant.IMAGE_PATH);
			 if(!file.exists())file.mkdirs();
			 File outputFile = new File(file, userProfile.getFirstName()+".vcf");
			 if(outputFile.exists()){
				 if(outputFile.delete())
					 outputFile = new File(file, userProfile.getFirstName()+".vcf");
			 }
			 OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8");
			/*OutputStreamWriter writer = new OutputStreamWriter(
					new FileOutputStream(Constant.IMAGE_PATH+ "/"
									+ ""+userProfile.getUserId()+".vcf"), "UTF-8");*/

			VCardComposer composer = new VCardComposer();

			// create a contact
			ContactStruct contact1 = new ContactStruct();
			if(userProfile.getFirstName() != null)
				contact1.name = userProfile.getFirstName();
			/*if(userProfile.getcompany_name() != null)
				contact1.company = cardDetailsList.getcompany_name();*/
			contact1.photoBytes = imageData;
			if(userProfile.getEmail() != null)
				contact1.Email = userProfile.getEmail();
			if(userProfile.getMobile() != null)
				contact1.addPhone(Contacts.Phones.TYPE_MOBILE, userProfile.getMobile(), null, true);
			// create vCard representation
			String vcardString = composer.createVCard(contact1,	VCardComposer.VERSION_VCARD30_INT);

			// write vCard to the output stream
			writer.write(vcardString);
			writer.write("\n"); // add empty lines between contacts
			writer.flush();
			// repeat for other contacts
			// ...

			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static int getAge(int year, int month, int day){
	    Calendar dob = Calendar.getInstance();
	    Calendar today = Calendar.getInstance();

	    dob.set(year, month, day); 

	    int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

	    if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
	        age--; 
	    }

	    return age;  
	}
	/*public static float getRatings(float ratingvaule){
		
		float a = ratingvaule;
		int d = (int) Math.ceil(a);
		return d;
	}*/
	
	public static void toggleSoftKeyboard(Activity activity) {
	    InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Context.INPUT_METHOD_SERVICE);
	    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 1);
	}
	
	public static void hideSoftKeyboard(Activity activity,View v) {
	    InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Context.INPUT_METHOD_SERVICE);
	    inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}
	
	public static void setTouchDelay(final Activity con,final View view){
		view.setEnabled(false);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
					con.runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							
							view.setEnabled(true);
						}
					});
			}
		}, 2000);
	}
	
	public static InputFilter[] restrictSpaceInEdit(EditText editText){
		InputFilter filter = new InputFilter() { 
	        public CharSequence filter(CharSequence source, int start, int end, 
	        		Spanned dest, int dstart, int dend) { 
	                for (int i = start; i < end; i++) { 
	                        if (!Character.isLetterOrDigit(source.charAt(i))) { 
	                                return ""; 
	                        } 
	                } 
	                return null; 
	        } 
		}; 

		return new InputFilter[]{filter}; 
	}
	
	
	public static void hideEditTextError(final EditText edtText){
	edtText.addTextChangedListener(new TextWatcher()
    {
        public void afterTextChanged(Editable edt){
            if( edtText.getText().length()>0)
            {
            	edtText.setError(null);
            }
        }

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}});
	}
	public static TextWatcher allowOnlyText(final Context con,final AutoCompleteTextView editText){
		TextWatcher textWatcher = (new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s!= null) {
					String c = s.toString();
					if (c.length()>0) {
						if (!Util.isOnlyText(c)) {
							editText.setText(c.subSequence(0, c.length()-1));
							if (c.length() > 1) 
								editText.setSelection(c.length()-1);
//							/Util.showAlert(con, "", "Only texts are allowed");
							editText.setError("Only texts are allowed"); 
						}
					}
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		return textWatcher;
	}
	public static void setMaxLength(EditText v, int maxLength){
		
		InputFilter[] fArray = new InputFilter[1];
		fArray[0] = new InputFilter.LengthFilter(maxLength);
		v.setFilters(fArray);
	}
	public static String maskString(String chars){
		
		return chars.substring(0,1)+chars.substring(1,chars.length()-2).replaceAll("[a-zA-Z0-9.,$;@!-_+=%/&*?'#&\\s]", "*")+chars.substring(chars.length()-2,chars.length()-1);
		
	}
	public static String maskEmail(String email) {
		
		String[] i = email.split("@");
		String replace = i[0].replaceAll("[a-zA-Z0-9.,$;@!-_+=%/&*?'#&\\s]", "*");
		return email.substring(0, 1)+replace + "@" +i[1];
	}

	public static String maskMobileNumber(String mobile) {
		
		String seperate = mobile.substring(1, mobile.length()-1);
		String replaceString = seperate.replaceAll("[0-9]", "*");
		return mobile.substring(0,2)+ replaceString + mobile.substring(mobile.length()-1);
	}
}// End of Util class

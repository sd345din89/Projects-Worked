package com.strobilanthes.forrestii.activities;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.callbacks.LocationChangeListener;
import com.strobilanthes.forrestii.act.fragments.ForgotPwdFragment;
import com.strobilanthes.forrestii.act.fragments.LoginFragment;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.util.L;

public class LoginActivity extends BaseActivity<DBAccess> implements OnClickListener,ConnectionCallbacks,
																					OnConnectionFailedListener,
																					LocationListener{
	
	LocationChangeListener locationChangeListener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
       
		//requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setContentView(R.layout.act_login);
		
		getSupportActionBar().setTitle("Login");
		
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		
		/*getSupportActionBar().setCustomView(R.layout.auth_actionbar);
		ImageView img_back = (ImageView)findViewById(R.id.auth_left_image);
		ImageView img_done = (ImageView)findViewById(R.id.auth_right_image);
		img_back.setOnClickListener(this);
		img_done.setOnClickListener(this);
		img_back.setImageResource(R.drawable.ic_action_previous_item);
		img_done.setImageResource(R.drawable.ic_action_accept);
		TextView text = (TextView)findViewById(R.id.auth_title_text);
		text.setText("Login");*/
		
		FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        LoginFragment fragment = new LoginFragment();
        locationChangeListener = fragment;
        fragmentTransaction.replace(R.id.login_root,fragment);
        fragmentTransaction.commit();
	           	
	} 

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getSupportMenuInflater().inflate(R.menu.actionbar_menu, menu);
		//MenuItem menuItem = menu.findItem(R.id.action_done);
		//menuItem.setVisible(true);
		//invalidateOptionsMenu();
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				
				if(getSupportFragmentManager().getBackStackEntryCount() == 0){
					finish();
				}else{
					getSupportFragmentManager().popBackStack();
				}
				
				return true;
			case R.id.action_done:
				
				int mStackCount =  getSupportFragmentManager().getBackStackEntryCount();
				
				if(mStackCount == 0){
					
					
				} else if(mStackCount == 1){
				
				} else if(mStackCount == 2){
					
				} else if(mStackCount == 3){
					//signupTickListener.onSignupCalled(this);
					
				}
				
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	@Override
	public void onLocationChanged(Location location) {
		L.d("Location Data = "+location);
		locationChangeListener.onLocationChanged(location);
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		 mLocationClient.requestLocationUpdates(
	                REQUEST,
	                this); 
	}

	@Override
	public void onDisconnected() {
		
	}

	@Override
	public void onResume(){
		super.onResume();
		
        setUpLocationClientIfNeeded();
        
        mLocationClient.connect();
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		
		if (mLocationClient != null) {
	         mLocationClient.disconnect();
	    }
	}
	private void setUpLocationClientIfNeeded() {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(
                    getApplicationContext(),
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
    }
	
	
	@Override
	public void onBackPressed() {
		if(getSupportFragmentManager().getBackStackEntryCount() == 0){
			finish();
		}else{
			getSupportFragmentManager().popBackStack();
		}
		//super.onBackPressed();
	}

	@Override
	public void onClick(View v) {
		FragmentTransaction fragmentTransaction = null;
		switch (v.getId()) {
		
			case R.id.login_screen_forgetpassword_txt:
				fragmentTransaction = getSupportFragmentManager().beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left, R.anim.out_to_right);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.replace(R.id.login_root, new ForgotPwdFragment());
		        fragmentTransaction.commit();
				break;
				
			case R.id.auth_left_image:
				if(getSupportFragmentManager().getBackStackEntryCount() == 0){
					finish();
				}else{
					getSupportFragmentManager().popBackStack();
				}
				break;
			case R.id.auth_right_image:
				int mStackCount =  getSupportFragmentManager().getBackStackEntryCount();
				if(mStackCount == 0){
					
					
					
				}else if(mStackCount == 1){
					/*fragmentTransaction = getSupportFragmentManager().beginTransaction();
			        fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left, R.anim.out_to_right);
			        fragmentTransaction.addToBackStack(null);
			        fragmentTransaction.replace(R.id.login_root, new OTPRegFragment());
			        fragmentTransaction.commit();*/
				}
				break;
			default:
				break;
		}
	}
}// End of Activity

package com.strobilanthes.forrestii.model.skillset;

import java.io.Serializable;

public class UserSkillsSet implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int userSkillSetId;
	private AbstractSkillSet skills;
	private boolean isPrimarySkill;
	
	public int getUserSkillSetId() {
		return userSkillSetId;
	}
	public void setUserSkillSetId(int userSkillSetId) {
		this.userSkillSetId = userSkillSetId;
	}
	public AbstractSkillSet getSkills() {
		return skills;
	}
	public void setSkills(AbstractSkillSet skills) {
		this.skills = skills;
	}
	public boolean isPrimarySkill() {
		return isPrimarySkill;
	}
	public void setPrimarySkill(boolean isPrimarySkill) {
		this.isPrimarySkill = isPrimarySkill;
	}
	
}
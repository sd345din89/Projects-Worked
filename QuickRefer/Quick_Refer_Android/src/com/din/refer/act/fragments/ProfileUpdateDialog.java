package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.general.AbstractCountry;
import com.strobilanthes.forrestii.model.general.AbstractCountry.Cities;
import com.strobilanthes.forrestii.model.general.AbstractCountry.Pincodes;
import com.strobilanthes.forrestii.model.general.AbstractCountry.States;
import com.strobilanthes.forrestii.model.userprofile.AbstractEditUserProfile;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.userprofile.UserProfile;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class ProfileUpdateDialog extends BaseDialogFragment<DBAccess> {
	
	@InjectView(R.id.profile_firstNameEdit)		private EditText firstnameEdit;
	@InjectView(R.id.profile_lastNameEdit) 		private EditText lastNameEdit;
	@InjectView(R.id.profile_Address_Edit)		private EditText addressEdit;
	@InjectView(R.id.profile_City_Edit)			private Spinner cityEdit;
	@InjectView(R.id.profile_State_Edit)		private Spinner stateEdit;
	@InjectView(R.id.profile_Country_Edit)		private Spinner countryEdit;
	@InjectView(R.id.profile_Pincode_Edit)		private Spinner pincodeEdit;
	@InjectView(R.id.profile_About_Edit)		private EditText aboutEdit;
	@InjectView(R.id.profile_Edit)				private TextView profileDone;
	@InjectView(R.id.profile_header_general)		private TextView headerGeneral;
	@InjectView(R.id.profile_header_about)				private TextView headerAbout;
	@InjectView(R.id.totalprofilelayout)		private LinearLayout changeLayout;
	@InjectView(R.id.totalprofilelayout1)		private LinearLayout changeLayout1;
	@InjectView(R.id.header_Layout)				private	 LinearLayout headerLayout;
	
	@Inject UserProfile userProfileModel;
	private ArrayList<AbstractCountry> country;
	private HashMap<Integer, ArrayList<States>> stateHashmap;
	private HashMap<Integer, ArrayList<Cities>> citiesHashMap;
	private HashMap<Integer, ArrayList<Pincodes>> pincodeHashMap;
	private AbstractUserProfile profile;
	private int strCountryId;
	private int strStateId;
	private int strCityId;
	private int strPincodeId;
	private Typeface mFontLight,mFontDark;

	private static UpdateProfileListner profileListner;
	private boolean defaultvalchk = true;
	
	interface UpdateProfileListner{
		void profileupdated();
	}
	
    public static ProfileUpdateDialog newInstance(AbstractUserProfile abstractUserProfile, ArrayList<AbstractCountry> country, HashMap<Integer, ArrayList<Cities>> citiesHashMap, HashMap<Integer, ArrayList<States>> stateHashmap, HashMap<Integer, ArrayList<Pincodes>> pincodeHashMap, ProfileBasicInfoFragment profileBasicInfoFragment) {
    	profileListner  = profileBasicInfoFragment;
    	ProfileUpdateDialog frag = new ProfileUpdateDialog();
    
         Bundle args = new Bundle();
         args.putSerializable("profile", abstractUserProfile);
         args.putSerializable("country", country);
         args.putSerializable("state", stateHashmap);
         args.putSerializable("city", citiesHashMap);
         args.putSerializable("pincode", pincodeHashMap);
         
         frag.setArguments(args);
         return frag;
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.frg_user_profile_basicinfo, null);
		
		return view;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		fontStyles();
		int padding = (int) getResources().getDimension(R.dimen.spacing_small_low);
		changeLayout1.setBackgroundDrawable(getResources().getDrawable(R.drawable.cardlayout));
		changeLayout.setPadding(padding, padding, padding, padding);
		headerLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.header_corner));
		
		userProfileModel.setContext(getActivity());
		Util.hideEditTextError(firstnameEdit);
		Util.hideEditTextError(lastNameEdit);
		Util.hideEditTextError(addressEdit);
		Util.hideEditTextError(aboutEdit);
		
		country = (ArrayList<AbstractCountry>) getArguments().getSerializable("country");
		stateHashmap= (HashMap<Integer, ArrayList<States>>) getArguments().getSerializable("state");
		citiesHashMap= (HashMap<Integer, ArrayList<Cities>>) getArguments().getSerializable("city");
		pincodeHashMap= (HashMap<Integer, ArrayList<Pincodes>>) getArguments().getSerializable("pincode");
		profile = (AbstractUserProfile) getArguments().getSerializable("profile");
		
		strCountryId = profile.getCountryId();
		strStateId = profile.getStateId();
		strCityId = profile.getCityId();
		strPincodeId = profile.getPincodeId();

		
		firstnameEdit.setVisibility(View.VISIBLE);
		lastNameEdit.setVisibility(View.VISIBLE);
		addressEdit.setVisibility(View.VISIBLE);
		cityEdit.setVisibility(View.VISIBLE);
		stateEdit.setVisibility(View.VISIBLE);
		countryEdit.setVisibility(View.VISIBLE);
		pincodeEdit.setVisibility(View.VISIBLE);
		aboutEdit.setVisibility(View.VISIBLE);
		profileDone.setText("Done");
		
		view.findViewById(R.id.profile_firstName).setVisibility(View.GONE);
		view.findViewById(R.id.profile_lastName).setVisibility(View.GONE);
		view.findViewById(R.id.profile_Email).setVisibility(View.GONE);
		view.findViewById(R.id.profile_Mobile).setVisibility(View.GONE);
		view.findViewById(R.id.profile_Address).setVisibility(View.GONE);
		view.findViewById(R.id.profile_City).setVisibility(View.GONE);
		view.findViewById(R.id.profile_State).setVisibility(View.GONE);
		view.findViewById(R.id.profile_Country).setVisibility(View.GONE);
		view.findViewById(R.id.profile_Pincode).setVisibility(View.GONE);
		view.findViewById(R.id.profile_About).setVisibility(View.GONE);
		
		firstnameEdit.setText(profile.getFirstName() !=null ? profile.getFirstName() : "Forrestii User");
		lastNameEdit.setText(profile.getLastName()!= null ? profile.getLastName() : "");
//		emailText.setText(profile.getEmail() != null ? profile.getEmail() : "");
//		mobileText.setText(profile.getMobile() != null ? profile.getMobile() : "");
		addressEdit.setText(profile.getAddress()!= null ? profile.getAddress() : "");
		aboutEdit.setText(profile.getAbout() != null ? profile.getAbout() : "");
		
		strCountryId = profile.getCountryId();
		
		ArrayAdapter<AbstractCountry> countryAdapter = new ArrayAdapter<AbstractCountry>(getActivity(), android.R.layout.simple_spinner_dropdown_item, country);
		countryEdit.setAdapter(countryAdapter);
		if (strCountryId != 0) {
			if (defaultvalchk) {
				for (int ccnt = 0; ccnt < country.size(); ccnt++) {
					if (strCountryId == (country.get(ccnt).getCountryId())) {
						countryEdit.setSelection(ccnt);
					}
				}
			}
		}
		
		countryEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent,View view, int position, long id) {
				AbstractCountry abstractCoun = (AbstractCountry) parent.getItemAtPosition(position);
				ArrayList<States> statary = stateHashmap.get(abstractCoun.getCountryId());
				ArrayAdapter<States> stateAdapter = new ArrayAdapter<States>(getActivity(),android.R.layout.simple_spinner_dropdown_item, statary);
				stateEdit.setAdapter(stateAdapter);
				if (defaultvalchk) {
					for (int j = 0; j < statary.size(); j++) {
						if (strStateId != 0 && statary.get(j).getStateId() == strStateId) {
							stateEdit.setSelection(j);
						}
					}
				}
				strCountryId = abstractCoun.getCountryId();
				Log.i("Address", "" + strCountryId);
			};
		
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// do nothing
			}
		});
		
		stateEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent,View view, int position, long id) {
				States states = (States) parent.getItemAtPosition(position);
				ArrayList<Cities> cityary = citiesHashMap.get(states.getStateId());
				ArrayAdapter<Cities> cityAdapter = new ArrayAdapter<Cities>(getActivity(),android.R.layout.simple_spinner_dropdown_item,cityary );
				strStateId = states.getStateId();
				Log.i("Address", "" + strStateId);
				cityEdit.setAdapter(cityAdapter);
				if (defaultvalchk) {
					for (int j = 0; j < cityary.size(); j++) {
						if (strCityId != 0 && cityary.get(j).getCityId() == strCityId) {
							cityEdit.setSelection(j);
						}
					}
				}
				strStateId = states.getStateId();
				Log.i("Address", "" + strStateId);
			};
		
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// do nothing
			}
		});
		
		cityEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent,View view, int position, long id) {
				Cities cities = (Cities) parent.getItemAtPosition(position);
				ArrayList<Pincodes> pinary = pincodeHashMap.get(cities.getCityId());
				ArrayAdapter<Pincodes> pinAdapter = new ArrayAdapter<Pincodes>(getActivity(),android.R.layout.simple_spinner_dropdown_item, pinary);
				pincodeEdit.setAdapter(pinAdapter);
				if (defaultvalchk) {
					for (int j = 0; j < pinary.size(); j++) {
						if (strPincodeId != 0 && pinary.get(j).getPincodeId() == strPincodeId) {
							pincodeEdit.setSelection(j);
						}
					}
				}
				strCityId = cities.getCityId();
				Log.i("Address", "" + strCityId);
			};
		
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// do nothing
			}
		});
		
		pincodeEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent,View view, int position, long id) {
				Pincodes pincodes = (Pincodes) parent.getItemAtPosition(position);
				strPincodeId = pincodes.getPincodeId();
				Log.i("++pin++", "" + strPincodeId);
				defaultvalchk = false;
			};
		
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// do nothing
			}
		});
		
		profileDone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Util.hideSoftKeyboard(getActivity(), v);
				String strFirstName = firstnameEdit.getText().toString();
				String strLastName = lastNameEdit.getText().toString();
				String strAddress = addressEdit.getText().toString();
//				String strDob = txt_dob.getText().toString();
				String strAbout = aboutEdit.getText().toString();
				//strAbout = strAbout.replaceAll("\\n"," ");
				//String strEmail = txt_profileEmail.getText().toString();
//				String s[] = strDob.split("/");
//				int selectedDay = Integer.parseInt(s[0]);
//				int selectedMonth = Integer.parseInt(s[1]);
//				int selectedYear = Integer.parseInt(s[2]);

				if (strFirstName.trim().length() > 0 ){
					
					if(strFirstName.trim().length()<3){
						firstnameEdit.setFocusableInTouchMode(true);
						firstnameEdit.requestFocus();
						firstnameEdit.setError("Name should be atleast 3 charectors");
						return;
					}
					if(strLastName.trim().length() < 1){
						//edtLastName.requestFocus();
						lastNameEdit.setFocusableInTouchMode(true);
						lastNameEdit.requestFocus();
						lastNameEdit.setError("Last name should have at least 1 character");
						return;
					}
					if(Util.isValidUserName(strFirstName)){
						if(Util.isValidUserName(strLastName)){
							if (strAddress.trim().length() > 0 && !Util.limitSpecialCharacters(strAddress)) {
								addressEdit.setError("Invalid special characters");
								return;
							}
							
							if (strAbout.trim().length() > 0 && !Util.limitSpecialCharacters(strAbout)) {
								aboutEdit.setError("The about should not contain any special characters");
								return;
							}
							
						//if (Util.limitSpecialCharacters(strAbout)) {
							try {
								JSONObject object = new JSONObject();
								profile = getHelper().getDBUserProfile();
								profile.setFirstName(strFirstName);
								profile.setLastName(strLastName);
								profile.setAddress(strAddress.trim());
								profile.setCountryId(strCountryId);
								profile.setStateId(strStateId);
								profile.setCityId(strCityId);
								profile.setPincodeId(strPincodeId);
								profile.setAbout(strAbout.trim());
								//L.i("about text"+profile.getAbout());
								//abstractUserProfile.setEmail(txt_profileEmail.getText().toString());
								//abstractUserProfile.setEmail(txt_profileEmail.getText().toString());
								object.put("firstName", ""+ strFirstName);
								object.put("lastName", ""+ strLastName);
								object.put("address", strAddress.trim());
								object.put("ObjCountryMaster",new JSONObject().put("countryId",strCountryId));
								object.put("ObjStateMaster",new JSONObject().put("stateId",strStateId));
								object.put("ObjCityMaster",new JSONObject().put("cityId",strCityId));
					            object.put("ObjPincode", new JSONObject().put("pincodeId", strPincodeId));
								//object.put("email", ""+ txt_profileEmail.getText().toString());
								//object.put("DOB", ""+ txt_dob.getText().toString());
								// if(address1 != null)
								// object.put("address", address1);
								object.put("About", strAbout.trim());
								object.put("userId", getHelper().getUserId());
								userProfileModel.editUserProfile(Constant.UPDATE_USER_PROFILE,object);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						} else {
							lastNameEdit.setFocusableInTouchMode(true);
							lastNameEdit.requestFocus();
							lastNameEdit.setError("The last name should not contain any special characters or space");
						}
						}else {
							firstnameEdit.setFocusableInTouchMode(true);
							firstnameEdit.requestFocus();
							firstnameEdit.setError("The first name should not contain any special characters or space");
						}
				} else {
					firstnameEdit.setFocusableInTouchMode(true);
					firstnameEdit.requestFocus();
					firstnameEdit.setError("The first name should not be empty");						
				}
					//}
//					else {
//						firstnameEdit.setError("The first name should not contain any special characters or space");
//					}
						
//				} else {
//					firstnameEdit.setError("Name should not be empty");
//				}
			}
		});
	}
	public void fontStyles() {

		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		headerGeneral.setTypeface(mFontLight);
		headerAbout.setTypeface(mFontLight);
		firstnameEdit.setTypeface(mFontDark);
		lastNameEdit.setTypeface(mFontDark);
		addressEdit.setTypeface(mFontDark);
		aboutEdit.setTypeface(mFontDark);
		profileDone.setTypeface(mFontDark);
	}

	@Override
	public void onClick(View v) {
		
	}
	
	 @Override
     public Dialog onCreateDialog(Bundle savedInstanceState) {
        
			Dialog commentsDialog = new Dialog(getActivity(),R.style.AppTheme);
			commentsDialog.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
			commentsDialog.getWindow().getAttributes().windowAnimations = R.style.comment_animation;
			//commentsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			//commentsDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
			
			commentsDialog.setCancelable(true);
			
         return commentsDialog;
     }
	 @Override
	public void onAttach(Activity activity) {
		 super.onAttach(activity);
	}

	@Override
	public void onResume(){
		super.onResume();
		userProfileModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		userProfileModel.removeChangeListener(this);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("edituserprofile")) {
			AbstractEditUserProfile profileUpdate = userProfileModel.getUserEditedProfile();
			if (profileUpdate != null) {
				if (profileUpdate.getStatus() == 1) {
				
					getHelper().insertUserProfile(profile);
					profileListner.profileupdated();
					dismiss();
//					updateUserInfoDetails(profile);
					L.ToastMessage(getActivity(), "Profile upated successfully");
				} else {
					
				}
			} else {
				Util.showAlert(getActivity(), "Message",
						"Request Failed.");
			}
		} 
	}
}

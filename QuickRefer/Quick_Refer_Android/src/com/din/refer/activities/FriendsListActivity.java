package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.custom.views.CustomWallPostListview;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.friendslist.AbstractFriendsList;
import com.strobilanthes.forrestii.model.friendslist.Friends;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.shake.AbstractShakeDetails;
import com.strobilanthes.forrestii.model.shake.AbstractShakeDetails.GeneralListData;
import com.strobilanthes.forrestii.model.skillset.UserSkillsSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class FriendsListActivity extends BaseActivity<DBAccess> implements PropertyChangeListener{
	
	private ArrayList<FriendsList>abstractT3FriendsList = null;
	private ArrayList<FriendsList>abstractDBT2Friends = null;
	private ArrayList<FriendsList>updatedT3FriendsList = null;
	private Typeface mFontLight,mFontDark;
	private String profileView;
	private CustomFriendsListAdapter adapter;
	private ShakeCustomListAdapter shakeCustomListAdapter;
	
	@InjectView(R.id.contacts_list)					private CustomWallPostListview T3contactsListview;
	@InjectView(R.id.contacts_txt_emptyview)		private TextView txt_emptyview;
	
	@Inject Friends contactsModel;
	
	private int T3RequestCode = 0;
	private AbstractShakeDetails abstractShakeDetails;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.frg_t2contactslist);
		
		contactsModel.setContext(this);
		
		/*inflate_stub = (ViewStub) findViewById(R.id.inflate_stub);
	    inflate_stub.inflate();
		inflate_stub.setVisibility(View.GONE);*/
		
		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		
		txt_emptyview.setText("No Rewards found...");
		txt_emptyview.setTypeface(mFontDark);
		
		abstractShakeDetails = (AbstractShakeDetails) getIntent().getSerializableExtra("forrestii_shake");
		abstractDBT2Friends =  getHelper().getDBT2Friends();
		
		AbstractUserProfile abstractUserProfile = getHelper().getDBUserProfile();
		
		profileView = getIntent().getStringExtra("profile_view");
	
		if(profileView.equals("T2")){
			adapter = new CustomFriendsListAdapter(this,abstractDBT2Friends);
			adapter.updateContactAL(abstractDBT2Friends);
			T3contactsListview.setAdapter(adapter);
	
		} else if(profileView.equals("T3")){
			if(abstractShakeDetails != null){
				//ArrayList<GeneralListData> friendsArrayList = abstractShakeDetails.getResponse().getGeneralList();
				
				ArrayList<FriendsList> updatedFriendsListData = new ArrayList<FriendsList>();
				
				for(GeneralListData generalList : abstractShakeDetails.getResponse().getGeneralList()){
					if(generalList.getUserProfile().getUserId() != abstractUserProfile.getUserId()){
						boolean isFriendsFound = false;
						if(abstractShakeDetails.getResponse().getFriendList() != null){
							for (FriendsList friendsListData :abstractShakeDetails.getResponse().getFriendList()) {
								if(generalList.getUserProfile().getUserId() == friendsListData.getFriendUserProfile().getUserId()){
									updatedFriendsListData.add(friendsListData);
									isFriendsFound = true;
								}
							}
						}
						
						if(!isFriendsFound){
							FriendsList friendsList = new FriendsList();
							friendsList.setIsFriend(3);
							friendsList.setFriendUserProfile(generalList.getUserProfile());
							updatedFriendsListData.add(friendsList);
						}
					}
				}
				shakeCustomListAdapter = new ShakeCustomListAdapter(this,updatedFriendsListData);
				
				T3contactsListview.setAdapter(shakeCustomListAdapter);
				
			} else {
				String userId = getIntent().getStringExtra("T2UserId");
				JSONObject postObject = new JSONObject();
				try {
					postObject.put("addedByUserProfile", new JSONObject().put("userId",getHelper().getUserId()));
					postObject.put("FriendUserProfile", new JSONObject().put("userId",userId));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				contactsModel.requestT3ContactsList(Constant.GET_CONTACTS_T3fromT2,postObject);
			}
		}
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			super.onBackPressed();
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("T3fromT2")){
			
			loadT3PropertyEvent(contactsModel.getT3ContactsList());
		}
		
	}
	
	private void loadT3PropertyEvent(AbstractFriendsList abstractFriendsList){	
		if (abstractFriendsList != null) {
			if (abstractFriendsList.getStatus() == 1) {
				
				//getHelper().updateT3FriendsList(abstractFriendsList.getFriendsListResponse());
				
				abstractT3FriendsList =  abstractFriendsList.getFriendsListResponse();
				adapter = new CustomFriendsListAdapter(this,abstractFriendsList.getFriendsListResponse());
				adapter.updateContactAL(abstractFriendsList.getFriendsListResponse());
				T3contactsListview.setAdapter(adapter);
				
			} else {
				Util.showAlert(this, "Message", ""+abstractFriendsList.getError());
			}
		} else {
			Util.showAlert(this, "Message", "Request Failed.");
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		contactsModel.addChangeListener(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		contactsModel.removeChangeListener(this);
	}
	
	class CustomFriendsListAdapter extends BaseAdapter{

		private LayoutInflater mLayoutInflater;
		private ImageLoadingListener animateFirstListener;
		ArrayList<FriendsList> abstractFriendsLists;
		private String strPrimarySkill;

		
		public CustomFriendsListAdapter(Context context,ArrayList<FriendsList> abstractT2FriendsLists) {
			this.abstractFriendsLists = abstractT2FriendsLists;
			mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.default_user)
					.showImageForEmptyUri(R.drawable.default_user)
					.showImageOnFail(R.drawable.default_user)
					.cacheInMemory(true).cacheOnDisc(true)
					.considerExifParams(true)
					.build();

			animateFirstListener = new AnimateFirstDisplayListener();
		}

		public void updateContactAL(ArrayList<FriendsList> abstractFriendsLists) {
			this.abstractFriendsLists = abstractFriendsLists;
			notifyDataSetChanged();
			
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder = null;
			if (v == null) {
				v = mLayoutInflater.inflate(R.layout.frg_t2contactlist_item, null);
				holder = new ViewHolder();
				holder.txt_userName = (TextView) v
						.findViewById(R.id.friend_list_item_txt_name);
				holder.img_usercontactsImg = (RoundedImageView) v
						.findViewById(R.id.friend_list_item_img_user);
				holder.contactsListItemTxtSkills = (TextView)v.findViewById( R.id.friend_list_item_txt_skills );
				holder.contacts_ratingBar = (RatingBar) v.findViewById(R.id.contacts_ratingBar);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			holder.txt_userName.setSelected(true);
			holder.txt_userName.setTypeface(mFontLight);
			holder.contactsListItemTxtSkills.setSelected(true);
			holder.contactsListItemTxtSkills.setTypeface(mFontLight);

			holder.txt_userName.setText(abstractFriendsLists.get(position).getFriendUserProfile().getFirstName());
			holder.contacts_ratingBar.setRating(abstractFriendsLists.get(position).getFriendUserProfile().getTotalRating());
			ArrayList<UserSkillsSet>skillset = new ArrayList<UserSkillsSet>();
			skillset = abstractFriendsLists.get(position).getFriendUserProfile().getUserSkillSets();
			
			holder.contactsListItemTxtSkills.setText("");
			
			if(skillset.size()>0){
				for (UserSkillsSet userSkillsSet:skillset) {
					if(userSkillsSet.isPrimarySkill()){
						strPrimarySkill = userSkillsSet.getSkills().getSkillName();
						holder.contactsListItemTxtSkills.setText(strPrimarySkill);
						break;
					}
				}
			}
			
			ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + abstractFriendsLists.get(position).getFriendUserProfile().getPhotoID(), holder.img_usercontactsImg, options,animateFirstListener);
			v.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(profileView.equals("T2")){
						Intent intent = new Intent(FriendsListActivity.this,FriendsProfileT2Activity.class);
						intent.putExtra("ProfileT2",abstractFriendsLists.get(position).getFriendUserProfile());
						startActivity(intent);
						
					}else if(profileView.equals("T3")){
						Intent intent = new Intent(FriendsListActivity.this,FriendsProfileT3Activity.class);
						intent.putExtra("ProfileT3",abstractFriendsLists.get(position).getFriendUserProfile());
						intent.putExtra("isFriend",abstractFriendsLists.get(position).getIsFriend());
						intent.putExtra("postion", position);
						updatedT3FriendsList = abstractFriendsLists;
						startActivityForResult(intent, T3RequestCode);
						
					}
				}
			});
			return v;
		}
		
		class ViewHolder {
			private RoundedImageView img_usercontactsImg;
			private TextView txt_userName,contactsListItemTxtSkills;
			private RatingBar contacts_ratingBar;
			private LinearLayout contacts_root_lyt;
		}
		@Override
		public int getCount() {
			return abstractFriendsLists.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}
	}
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

	@Override
	public void onLoadingComplete(String imageUri, View view,
			Bitmap loadedImage) {
		if (loadedImage != null) {
			ImageView imageView = (ImageView) view;
			boolean firstDisplay = !displayedImages.contains(imageUri);
			if (firstDisplay) {
				FadeInBitmapDisplayer.animate(imageView, 500);
				displayedImages.add(imageUri);
			}
		}
	}
}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		getSupportMenuInflater().inflate(R.menu.contact_menu, menu);
		MenuItem menuItem = menu.findItem(R.id.search);
		menuItem.setVisible(true);
		SearchView searchView = (SearchView)menuItem.getActionView();
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

         searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
         //searchView.setIconifiedByDefault(false); 
         EditText search_text = (EditText) searchView.findViewById(searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null));
         search_text.setFilters(new InputFilter[] { new InputFilter.LengthFilter(41) });
         
         SearchView.OnQueryTextListener textChangeListener  = new SearchView.OnQueryTextListener(){

				@Override
				public boolean onQueryTextSubmit(String newText) {
					// TODO Auto-generated method stub
					
					return false;
				}

				@Override
				public boolean onQueryTextChange(String newText) {
					if(profileView.equals("T2")){
						if (newText.length() > 0) {
							ArrayList<FriendsList> contactsfilterAl1 = new ArrayList<FriendsList>();
							for (int i = 0; i < abstractDBT2Friends.size(); i++) {
								if (abstractDBT2Friends.get(i).getFriendUserProfile().getFirstName()
										.toLowerCase()
										.contains(
												newText.toLowerCase())) {
	
									contactsfilterAl1.add(abstractDBT2Friends.get(i));
									
									L.d("FriendsList"+contactsfilterAl1.toString());
	
									}
								adapter.updateContactAL(contactsfilterAl1);
							}
						}else {
							adapter.updateContactAL(abstractDBT2Friends);
						}
					}else if(profileView.equals("T3")){
						if (newText.length() > 0) {
							ArrayList<FriendsList> contactsfilterAl = new ArrayList<FriendsList>();
							if(abstractT3FriendsList != null && abstractT3FriendsList.size() > 0){
								for (int i = 0; i < abstractT3FriendsList.size(); i++) {
									if (abstractT3FriendsList.get(i).getFriendUserProfile()
											.getFirstName().toLowerCase().contains(newText.toLowerCase())) {
										contactsfilterAl.add(abstractT3FriendsList.get(i));
									}
									adapter.updateContactAL(contactsfilterAl);
								}
							}
						}
						else {
							adapter.updateContactAL(abstractT3FriendsList);
						}
					}
					return false;
				}
         	
         };
            searchView.setOnQueryTextListener(textChangeListener);
            return true;
	}
	class ShakeCustomListAdapter extends BaseAdapter{

		private LayoutInflater mLayoutInflater;
		private ImageLoadingListener animateFirstListener;
		private String strPrimarySkill;
		private ArrayList<FriendsList> shakeGeneralList;
		
		public ShakeCustomListAdapter(Context context, ArrayList<FriendsList> shakeGeneralList) {
			
			this.shakeGeneralList = shakeGeneralList;
			mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.default_user)
					.showImageForEmptyUri(R.drawable.default_user)
					.showImageOnFail(R.drawable.default_user)
					.cacheInMemory(true).cacheOnDisc(true)
					.considerExifParams(true)
					.build();

			animateFirstListener = new AnimateFirstDisplayListener();
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder = null;
			if (v == null) {
				v = mLayoutInflater.inflate(R.layout.frg_t2contactlist_item, null);
				holder = new ViewHolder();
				holder.txt_userName = (TextView) v
						.findViewById(R.id.friend_list_item_txt_name);
				holder.img_usercontactsImg = (RoundedImageView) v
						.findViewById(R.id.friend_list_item_img_user);
				holder.contactsListItemTxtSkills = (TextView)v.findViewById( R.id.friend_list_item_txt_skills );
			
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			
			holder.txt_userName.setSelected(true);
			holder.txt_userName.setTypeface(mFontLight);
			
			holder.txt_userName.setText(getItem(position).getFriendUserProfile().getFirstName());

			
			ArrayList<UserSkillsSet>skillset = new ArrayList<UserSkillsSet>();
			skillset = getItem(position).getFriendUserProfile().getUserSkillSets();
			if(skillset.size()>0){
				for (UserSkillsSet userSkillsSet:skillset) {
					if(userSkillsSet.isPrimarySkill() == true){
						strPrimarySkill = userSkillsSet.getSkills().getSkillName();
						holder.contactsListItemTxtSkills.setText(strPrimarySkill);
						break;
					}
				}
			}
			
			ForrestiiApplication.getImageLoaderInstance().displayImage(
					Constant.FOLDER_PROFILE_PHOTO_NAME + getItem(position).getFriendUserProfile().getPhotoID(), holder.img_usercontactsImg, options,
					animateFirstListener);
			v.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Intent intent = new Intent(FriendsListActivity.this,FriendsProfileT3Activity.class);
					intent.putExtra("ProfileT3",getItem(position).getFriendUserProfile());
					intent.putExtra("isFriend",getItem(position).getIsFriend());
					intent.putExtra("postion", position);
					startActivity(intent);
					
				}
			});
			return v;
		}
		
		class ViewHolder {
			private RoundedImageView img_usercontactsImg;
			private TextView txt_userName,contactsListItemTxtSkills;
		}
		@Override
		public int getCount() {
			return shakeGeneralList.size();
		}

		@Override
		public FriendsList getItem(int position) {
			return shakeGeneralList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}
	}
	
	
	/*private void loadT3PropertyEvent(AbstractFriendsList abstractT3FriendsList){	
		if (abstractT3FriendsList != null) {
			if (abstractT3FriendsList.getStatus() == 1) {
				
				getHelper().updateT3FriendsList(abstractT3FriendsList.getFriendsListResponse());
				mT3FriendsList = abstractT3FriendsList.getFriendsListResponse();
				adapter = new CustomFriendsListAdapter(this,abstractT3FriendsList.getFriendsListResponse());
				T3contactsListview.setAdapter(adapter);
//				searchT3Friends();
				
			} else {
				
			}
		} else {
			Util.showAlert(this, "Message", "Request Failed.");
		}

//		T3contactsListview.setEmptyView(txt_emptyview);
	}*/
	
	/*private void searchT3Friends() {
		TextWatcher filterTextWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
					
				if(mT3FriendsList != null){
					if (contactsEdtSearch.getText().toString().length() > 0) {
						ArrayList<FriendsList> contactsfilterAl = new ArrayList<FriendsList>();
						for (int i = 0; i < mT3FriendsList.size(); i++) {
							if (mT3FriendsList.get(i).getFriendUserProfile()
									.getFirstName().toLowerCase().contains(contactsEdtSearch.getText()
									.toString().toLowerCase())) {
								contactsfilterAl.add(mT3FriendsList.get(i));
							}
							adapter.updateContactAL(contactsfilterAl);
						}
					}else {
						adapter.updateContactAL(mT3FriendsList);
					}
				}
				
			}
		};	
		contactsEdtSearch.addTextChangedListener(filterTextWatcher);
	}*/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == T3RequestCode ) {
			if (resultCode == RESULT_OK) {
				int Position = data.getExtras().getInt("position");
				if(updatedT3FriendsList !=null){
					FriendsList profile = updatedT3FriendsList.get(Position);
					profile.setIsFriend(0);
					updatedT3FriendsList.set(Position, profile);
					adapter = new CustomFriendsListAdapter(this,updatedT3FriendsList);
					adapter.updateContactAL(updatedT3FriendsList);
					T3contactsListview.setAdapter(adapter);
				}
			}
		}
	}
}

package com.strobilanthes.forrestii.model.otpvalidation;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.OtpValidationVolleyTasks;

public class OtpValidationImpl implements OtpValidation,VolleyTasksListener{

	private AbstractOtpValidation abstractOtpValidation; 
	private AbstractResendOTP abstractResendOTP;
	List<PropertyChangeListener> listener=new ArrayList<PropertyChangeListener>();
	private Activity context;
	private CustomProgressbar mCustomProgressbar;
	
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners( PropertyChangeEvent evt ){
		for ( Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator.hasNext(); ){
			PropertyChangeListener name = (PropertyChangeListener)iterator.next();
			name.propertyChange(evt);
		}
	}
	
	@Override
	public void handleResult(String method_name,JSONObject jsonObject) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		if(method_name.equals("otp_verify")){
			AbstractOtpValidation previousResult= abstractOtpValidation;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractOtpValidation = gson.fromJson(jsonObject.toString(), AbstractOtpValidation.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractOtpValidation); 
			notifyListeners(event);
		}else if(method_name.equals("resendotp")){
			AbstractResendOTP previousResult= abstractResendOTP;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractResendOTP = gson.fromJson(jsonObject.toString(), AbstractResendOTP.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractResendOTP); 
			notifyListeners(event);
		}
		
	}

	@Override
	public void handleError(String method_name,Exception e) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		AbstractOtpValidation previousResult= abstractOtpValidation;
		//abstractOtpValidation = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this,method_name, previousResult, null); 
		notifyListeners(event);
	}

	@Override
	public void requestOTPValidationDetails(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new OtpValidationVolleyTasks("otp_verify", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}


	@Override
	public void doValidation(String content) {
		
	}

	@Override
	public AbstractOtpValidation getOtpValidationDetails() {
		return abstractOtpValidation;
	}

	@Override
	public void resendOTP(String url, JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(
				new OtpValidationVolleyTasks("resendotp", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractResendOTP getResendOtpValidationDetails() {
		return abstractResendOTP;
	}

	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}
}

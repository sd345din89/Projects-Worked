package com.strobilanthes.forrestii.model.lookuplist;

import java.util.ArrayList;

import com.strobilanthes.forrestii.model.general.AbstractCountry;
import com.strobilanthes.forrestii.model.general.AbstractLocation;
import com.strobilanthes.forrestii.model.general.AbstractServerInfo;
import com.strobilanthes.forrestii.model.general.AbstractTemplate;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;

public class AbstractLookUpList {

	private String error;
	private int status;
	private LookupListResponse response;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public LookupListResponse getResponse() {
		return response;
	}

	public void setResponse(LookupListResponse response) {
		this.response = response;
	}

	public class LookupListResponse {
		
		private ArrayList<AbstractSkillSet> skills;
		private ArrayList<AbstractLocation>	Location;
		private ArrayList<AbstractServerInfo> ServerInfo;
		private ArrayList<AbstractTemplate> Template;
		private ArrayList<AbstractCountry> Country;

		public ArrayList<AbstractSkillSet> getSkills() {
			return skills;
		}

		public void setSkills(ArrayList<AbstractSkillSet> skills) {
			this.skills = skills;
		}

		public ArrayList<AbstractServerInfo> getServerInfo() {
			return ServerInfo;
		}

		public void setServerInfo(ArrayList<AbstractServerInfo> serverInfo) {
			ServerInfo = serverInfo;
		}

		public ArrayList<AbstractTemplate> getTemplate() {
			return Template;
		}

		public void setTemplate(ArrayList<AbstractTemplate> template) {
			Template = template;
		}

		public ArrayList<AbstractCountry> getCountry() {
			return Country;
		}

		public void setCountry(ArrayList<AbstractCountry> country) {
			Country = country;
		}

		public ArrayList<AbstractLocation> getLocation() {
			return Location;
		}

		public void setLocation(ArrayList<AbstractLocation> location) {
			Location = location;
		}
		
	}
}

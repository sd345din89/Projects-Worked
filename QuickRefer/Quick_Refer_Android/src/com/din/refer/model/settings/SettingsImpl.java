package com.strobilanthes.forrestii.model.settings;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.SettingsVolleyTasks;

public class SettingsImpl implements Settings, VolleyTasksListener {

	private AbstractSettings abstractSettings;
	private AbstractPassword abstractPassword;
	private AbstractChange abstractChange;
	private AbstractVerify abstractVerify;
	private AbstractRemoveMobileNo abstractRemoveMobile;
	private AbstractRemoveEmailId abstractRemoveEmail;

	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private Activity context;
	private CustomProgressbar mCustomProgressbar;


	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonObject) {
		try {
	        if ((this.mCustomProgressbar != null) && this.mCustomProgressbar.isShowing()) {
	            this.mCustomProgressbar.dismiss();
	        }
	    } catch (final IllegalArgumentException e) {
	    	e.printStackTrace();
	    } catch (final Exception e) {
	    	e.printStackTrace();
	    } finally {
	        this.mCustomProgressbar = null;
	    }  
		
		if(method_name.equals("getSettings")){
		
			
			AbstractSettings previousResult = abstractSettings;
	
			AbstractSettings aSettings = new AbstractSettings();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			aSettings = gson.fromJson(jsonObject.toString(), AbstractSettings.class);
			abstractSettings = aSettings;
	
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractSettings);
			notifyListeners(event);
		
		}else if(method_name.equals("password_update")){
			
			AbstractPassword previousResult = abstractPassword;

			AbstractPassword aPassword = new AbstractPassword();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			aPassword = gson.fromJson(jsonObject.toString(), AbstractPassword.class);



			abstractPassword = aPassword;

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractPassword);
			notifyListeners(event);
		}else if(method_name.equals("change_MobileNo")){
			AbstractChange previousResult = abstractChange;

			AbstractChange aMobileNo = new AbstractChange();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			aMobileNo = gson.fromJson(jsonObject.toString(), AbstractChange.class);



			abstractChange = aMobileNo;

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractChange);
			notifyListeners(event);
		}else if(method_name.equals("verify_MobileNo")){
			AbstractVerify previousResult = abstractVerify;

			AbstractVerify aMobileNo = new AbstractVerify();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			aMobileNo = gson.fromJson(jsonObject.toString(), AbstractVerify.class);



			abstractVerify = aMobileNo;

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractVerify);
			notifyListeners(event);
		}else if(method_name.equals("UpdateNotificationSettings") || method_name.equals("UpdatePrivacySettings")){
		
			
			AbstractSettings previousResult = abstractSettings;
	
			AbstractSettings aSettings = new AbstractSettings();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			aSettings = gson.fromJson(jsonObject.toString(), AbstractSettings.class);
			abstractSettings = aSettings;
	
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractSettings);
			notifyListeners(event);
		
		}else if(method_name.equals("change_EmailId")){
			AbstractChange previousResult = abstractChange;

			AbstractChange achangeEmailId = new AbstractChange();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			achangeEmailId = gson.fromJson(jsonObject.toString(), AbstractChange.class);



			abstractChange = achangeEmailId;

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractChange);
			notifyListeners(event);
		}else if(method_name.equals("verify_EmailId")){
			AbstractVerify previousResult = abstractVerify;

			AbstractVerify aVerifyEmailId = new AbstractVerify();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			aVerifyEmailId = gson.fromJson(jsonObject.toString(), AbstractVerify.class);



			abstractVerify = aVerifyEmailId;

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractVerify);
			notifyListeners(event);
		}else if(method_name.equals("remove_MobileNo")){
			AbstractRemoveMobileNo previousResult = abstractRemoveMobile;

			AbstractRemoveMobileNo aRemoveMobile = new AbstractRemoveMobileNo();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			aRemoveMobile = gson.fromJson(jsonObject.toString(), AbstractRemoveMobileNo.class);



			abstractRemoveMobile = aRemoveMobile;

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractRemoveMobile);
			notifyListeners(event);
		}else if(method_name.equals("remove_EmailId")){
			AbstractRemoveEmailId previousResult = abstractRemoveEmail;

			AbstractRemoveEmailId aRemoveEmail = new AbstractRemoveEmailId();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			aRemoveEmail = gson.fromJson(jsonObject.toString(), AbstractRemoveEmailId.class);



			abstractRemoveEmail = aRemoveEmail;

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractRemoveEmail);
			notifyListeners(event);
		}
		else if(method_name.equals("logout")){
			AbstractVerify previousResult = abstractVerify;

			AbstractVerify aVerifyEmailId = new AbstractVerify();
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			aVerifyEmailId = gson.fromJson(jsonObject.toString(), AbstractVerify.class);



			abstractVerify = aVerifyEmailId;

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractVerify);
			notifyListeners(event);
		}

	}

	@Override
	public void handleError(String method_name, Exception e) {
		try {
	        if ((this.mCustomProgressbar != null) && this.mCustomProgressbar.isShowing()) {
	            this.mCustomProgressbar.dismiss();
	        }
	    } catch (final IllegalArgumentException ex) {
	    	ex.printStackTrace();
	    } catch (final Exception ex) {
	    	ex.printStackTrace();
	    } finally {
	        this.mCustomProgressbar = null;
	    }  
		
		AbstractSettings previousResult = abstractSettings;
		abstractSettings = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				previousResult, null);
		notifyListeners(event);
	}

	@Override
	public void createSettings(String url, JSONObject postdata) {
		
	}

	
	@Override
	public void deleteSettings(String url, JSONObject postdata) {
		
	}

	@Override
	public AbstractSettings getSettingsDetails() {
		return abstractSettings;
	}

	
	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	@Override
	public void UpdateUserProfile(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("UpdateUserProfile", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void updatePrivacySettings(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("UpdatePrivacySettings", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	
	@Override
	public void updateNotificationSettings(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("UpdateNotificationSettings", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	
	@Override
	public void getSettings(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("getSettings", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void updatePwdDetails(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("password_update", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void changeMobileNo(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("change_MobileNo", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));	
	}

	

	@Override
	public void verifyMobileNo(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("verify_MobileNo", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	@Override
	public void changeEmailId(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("change_EmailId", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	@Override
	public void verifyEmailId(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("verify_EmailId", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	
	@Override
	public void requestLogout(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("logout", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractPassword updatePasswordDetails() {
		return abstractPassword;
	}

	@Override
	public AbstractChange changeEmailId() {
		return abstractChange;
	}

	@Override
	public AbstractVerify verifyEmailId() {
		return abstractVerify;
	}

	@Override
	public AbstractChange changeMobileNo() {
		return abstractChange;
	}

	@Override
	public AbstractVerify verifyMobileNo() {
		return abstractVerify;
	}

	
	@Override
	public AbstractVerify getLogoutDetails() {
		return abstractVerify;
	}
	@Override
	public AbstractRemoveMobileNo removeMobileNo() {
		// TODO Auto-generated method stub
		return abstractRemoveMobile;
	}

	@Override
	public AbstractRemoveEmailId removeEmailId() {
		// TODO Auto-generated method stub
		return abstractRemoveEmail;
	}
	
	@Override
	public void removeMobileNo(String url, JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("remove_MobileNo", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void removeEmailId(String url, JSONObject postdata) {
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SettingsVolleyTasks("remove_EmailId", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	
}

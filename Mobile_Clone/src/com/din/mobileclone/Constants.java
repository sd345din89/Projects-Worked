//------------------------------------------------------------------------------
//
//    (c) Copyright 2014.Dinesh Kumar M. All Rights Reserved.
//
//------------------------------------------------------------------------------

package com.din.mobileclone;

import java.io.File;
import java.util.ArrayList;

import android.os.Environment;

import com.dropbox.client2.session.Session.AccessType;

public class Constants {

	//DropBox API
	final static public String APP_KEY = "k5d1qx7f2ygzlq3";
	final static public String APP_SECRET = "yg1xe22fj95hlen";
	final static public AccessType ACCESS_TYPE = AccessType.APP_FOLDER;
	
	final static public String CONTACTS_PATH = Environment.getExternalStorageDirectory()
												.toString() + File.separator +"Test_Contacts";
	final static public String DOWNLOAD_IMAGES_PATH = Environment.getExternalStorageDirectory()
			.toString() + File.separator +"MClone_Images";
	final static public String DOWNLOAD_CONTACTS_PATH = Environment.getExternalStorageDirectory()
			.toString() + File.separator +"MClone_Contacts";
	//static public String IMAGES_PATH = null;
	
	public static ArrayList<File> VCARD = new ArrayList<File>();
	
	public static ArrayList<File> DOWNLOADED_VCARD = new ArrayList<File>();
	    
}

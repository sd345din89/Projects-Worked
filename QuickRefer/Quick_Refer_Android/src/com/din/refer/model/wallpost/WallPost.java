package com.strobilanthes.forrestii.model.wallpost;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface WallPost extends ForrestiiSubject {
	//void refreshWallPost(String url , JSONObject postdata);
	//AbstractGetWallPost getWallPostResponse();
	
	void createWallPost(String url , JSONObject postdata);
	AbstractCreateWallPost getCreateWallPostResponse();
	
	void updateWallPost(String url , JSONObject postdata);
	AbstractUpdateWallPost getUpdateWallPostResponse();
	
	void deleteWallPost(String url , JSONObject postdata);
	AbstractDelete getDeleteWallPostResponse();
	
	void hideWallPost(String url , JSONObject postdata);
	AbstractDelete getHideWallPostResponse();
	
	void replyWallPost(String url , JSONObject postdata);
	AbstractReplyWallPost getReplyWallPostResponse();
	
	void deleteReplyWallPost(String url , JSONObject postdata);
	AbstractDelete getDeleteReplyPosResponse();
	
	void updateReplyWallPost(String url , JSONObject postdata);
	AbstractDelete getUpdateReplyPostResponse();
	
	void paginateWallPost(String url , JSONObject postdata);
	AbstractPaginateGetWallPost getPaginateWallpost();
	
	void doValidation();
}

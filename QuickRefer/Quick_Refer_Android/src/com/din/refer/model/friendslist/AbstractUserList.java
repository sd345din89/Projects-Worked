package com.strobilanthes.forrestii.model.friendslist;

import java.io.Serializable;

import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

public class AbstractUserList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String error;
	private int status;
	private AbstractUserProfile response;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public AbstractUserProfile getResponse() {
		return response;
	}

	public void setResponse(AbstractUserProfile response) {
		this.response = response;
	}

}

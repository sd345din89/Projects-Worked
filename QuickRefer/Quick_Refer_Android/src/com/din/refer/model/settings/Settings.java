package com.strobilanthes.forrestii.model.settings;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Settings extends ForrestiiSubject {

	void UpdateUserProfile(String url, JSONObject postdata);

	void createSettings(String url, JSONObject postdata);

	void updatePrivacySettings(String url, JSONObject postdata);
	
	void updateNotificationSettings(String url, JSONObject postdata);

	void deleteSettings(String url, JSONObject postdata);
	void getSettings(String url, JSONObject postdata);
	// void updateNotificationSettings(String postdata);
	AbstractSettings getSettingsDetails();

	void updatePwdDetails(String url, JSONObject postdata);
	AbstractPassword updatePasswordDetails();
	
	void changeMobileNo(String url,JSONObject postdata);
	AbstractChange changeMobileNo();
	
	void verifyMobileNo(String url,JSONObject postdata);
	AbstractVerify verifyMobileNo();
	
	void changeEmailId(String url,JSONObject postdata);
	AbstractChange changeEmailId();
	
	void verifyEmailId(String url,JSONObject postdata);
	AbstractVerify verifyEmailId();
	
	void removeMobileNo(String url,JSONObject postdata);
	AbstractRemoveMobileNo removeMobileNo();
	
	void removeEmailId(String url,JSONObject postdata);
	AbstractRemoveEmailId removeEmailId();
	
	void requestLogout(String url,JSONObject postdata);
	AbstractVerify getLogoutDetails();
}

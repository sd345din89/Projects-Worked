package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.act.fragments.WallPostExpandCommentsFragment;
import com.strobilanthes.forrestii.act.fragments.WallPostExpandReferralFragment;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.referral.Referral;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.AbstractDelete;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;
import com.strobilanthes.forrestii.model.wallpost.WallPost;
import com.strobilanthes.forrestii.slidingtab.PagerSlidingTabStrip;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class WallPostExpandActivity extends BaseActivity<DBAccess> implements OnPageChangeListener,PropertyChangeListener, WallPostExpandCommentsFragment.UpdateWallpost{
	
	@InjectView(R.id.pager)                   private ViewPager mViewPager;
	@InjectView(R.id.wall_post_img_user)      private RoundedImageView wallPostImgUser;
	@InjectView(R.id.wall_post_message)       private TextView wallPostMessage;
	@InjectView(R.id.wall_subview_count)      private TextView wallSubviewCount;
	@InjectView(R.id.wall_post_txt_need)      private TextView wallPostTxtNeed;
	@InjectView(R.id.wall_subview_profession) private TextView wallSubviewProfession;
	@InjectView(R.id.wall_post_txt_postedby)  private TextView wallPostTxtPostedby;
	@InjectView(R.id.wall_post_txt_userName)  private TextView wallPostTxtUserName;
	@InjectView(R.id.wall_post_txt_time)      private TextView wallPostTxtTime;
	@InjectView(R.id.tabs)					  private PagerSlidingTabStrip pagerTabStrip;
	@InjectView(R.id.wall_post_options)		  private ImageView wallpostoptions;
	
	@Inject WallPost 				wallPostModel;
	@Inject Referral  				referralModel;
	
	private AbstractUserProfile abstractUserProfile;
	
	private PagerAdapter mPagerAdapter;
	private Typeface mFontLight,mFontDark;
	private GetWallPost wallPostCotnent;
	private int mPostId;
	private int mPostByUserId = 0;
	private String mMessage;
	private int mPosition;
	private String postUserName;
	private Object replyPostArrayList;
	private int mWallId;
	
	private String []userItems = {"It's annoying","It's spam"};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_dg_wp_expand);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		getSupportActionBar().setDisplayUseLogoEnabled(false);
		fontStyles();
		
		wallPostModel.setContext(this);
		referralModel.setContext(this);
		
		wallPostCotnent = (GetWallPost) getIntent().getSerializableExtra("WallPostContent");
		mPostId = wallPostCotnent.getWallPosts().getPostId();
		mWallId = wallPostCotnent.getWallId();
		mPostByUserId = wallPostCotnent.getWallPosts().getPostByUserProfile().getUserId();
		mMessage = wallPostCotnent.getWallPosts().getMessage();
		mPosition = getIntent().getIntExtra("position", -1);
		
		abstractUserProfile = getHelper().getDBUserProfile();
		//postUserName = wallPostCotnent.getWallPosts().getPostByUserProfile().getFirstName();

		if(abstractUserProfile.getUserId() == wallPostCotnent.getWallPosts().getPostByUserProfile().getUserId()){
			//firstName = userProfile.getFirstName();
			postUserName = "You";
			getSupportActionBar().setTitle("Your post");

		}else{
			if(wallPostCotnent.getWallPosts().getPostByUserProfile().getFirstName() == null){
				postUserName = "Forrestii User";
				getSupportActionBar().setTitle(postUserName+"'s post");

			}
			else
				postUserName = wallPostCotnent.getWallPosts().getPostByUserProfile().getFirstName();
				getSupportActionBar().setTitle(postUserName+"'s post");

		}
//		if(abstractUserProfile.getUserId() == wallPostCotnent.getWallPosts().getPostByUserProfile().getUserId()){
//		}else{
//		}
		wallPostTxtUserName.setText(postUserName);
		wallPostTxtUserName.setSelected(true);
		if(mMessage != null && mMessage.length() > 0){
			wallPostMessage.setText(wallPostCotnent.getWallPosts().getMessage().trim().toString());
		}else{
			wallPostMessage.setText("I need "+wallPostCotnent.getWallPosts().getSkill().getSkillName()+". Can anyone please refer me a good one!!");
		}
		
		if(abstractUserProfile.getUserId() == mPostByUserId){
			wallSubviewCount.setText(""+wallPostCotnent.getWallPosts().getReplies().size()+" Replies & "+wallPostCotnent.getWallPosts().getReferral().size()+" Referrals" );	
		}else{
			wallSubviewCount.setText(""+wallPostCotnent.getWallPosts().getReplies().size()+" Replies");
		}
		
		
		wallPostTxtTime.setText(Util.updatedOnTime(wallPostCotnent.getWallPosts().getUpdatedOn()));
		ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + wallPostCotnent.getWallPosts().getPostByUserProfile().getPhotoID(), wallPostImgUser,options);
		wallSubviewProfession.setSelected(true);
		wallSubviewProfession.setText(wallPostCotnent.getWallPosts().getSkill().getSkillName());
		replyPostArrayList = wallPostCotnent.getWallPosts().getReplies();
		
		mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
		mViewPager.setOffscreenPageLimit(3);
		mViewPager.setAdapter(mPagerAdapter);
		mViewPager.setOnPageChangeListener(this);
		
		pagerTabStrip.setViewPager(mViewPager);
		pagerTabStrip.setTextSize((int)Util.getDimensPxtoDp(14, this));
		 pagerTabStrip.setTypeface(mFontDark, Typeface.NORMAL);
		pagerTabStrip.setOnPageChangeListener(this);
		
		wallpostoptions.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PopupMenu popup = new PopupMenu(WallPostExpandActivity.this, v);
                /** Adding menu items to the popup menu */
			 	Menu menuItems = (Menu) popup.getMenu();
                popup.getMenuInflater().inflate(R.menu.popup,menuItems);
	                if(abstractUserProfile.getUserId() != wallPostCotnent.getWallPosts().getPostByUserProfile().getUserId()){
	                	menuItems.getItem(1).setVisible(false);
	                }else{
	                	menuItems.getItem(0).setVisible(false);
	                	menuItems.getItem(2).setVisible(false);

	                }
                /** Defining menu item click listener for the popup menu */
                popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                    	JSONObject jsonObject = null;
                    	switch (item.getItemId()) {
							case R.id.action1:
								jsonObject = new JSONObject();
								try {
									jsonObject.put("WallId",mWallId);
									wallPostModel.deleteWallPost(Constant.HIDE_WALL_POST, jsonObject);
								} catch (JSONException e) {
									e.printStackTrace();
								}	
								break;
							case R.id.action2:
								jsonObject = new JSONObject();
								try {
									jsonObject.put("postId",mPostId);
									wallPostModel.deleteWallPost(Constant.DELETE_POST, jsonObject);
								} catch (JSONException e) {
									e.printStackTrace();
								}	
								break;
							case R.id.action3:
								AlertDialog.Builder alertbuilder = new AlertDialog.Builder(WallPostExpandActivity.this,AlertDialog.THEME_HOLO_LIGHT);
								alertbuilder.setTitle("Why do you want to report this post");
								AlertDialog dialog = alertbuilder.create();
								alertbuilder.setItems(userItems , new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										if(which == 0){
											AlertDialog.Builder alertbuilder1 = new AlertDialog.Builder(WallPostExpandActivity.this,AlertDialog.THEME_HOLO_LIGHT);
											alertbuilder1.setMessage("Report and delete this post");
											AlertDialog dialog1 = alertbuilder1.create();
											alertbuilder1.setPositiveButton("Report", new DialogInterface.OnClickListener() {
												
												@Override
												public void onClick(DialogInterface dialog, int which) {
													// TODO Auto-generated method stub
													JSONObject jsonObject = new JSONObject();
													try {
														jsonObject.put("postId",mPostId);
														wallPostModel.deleteWallPost(Constant.DELETE_POST, jsonObject);
													} catch (JSONException e) {
														e.printStackTrace();
													}
												}
											});
											alertbuilder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
												
												@Override
												public void onClick(DialogInterface dialog, int which) {
													// TODO Auto-generated method stub
													
												}
											});
											alertbuilder1.show();
											
										
										}
										else if(which == 1){
											AlertDialog.Builder alertbuilder1 = new AlertDialog.Builder(WallPostExpandActivity.this,AlertDialog.THEME_HOLO_LIGHT);
											alertbuilder1.setTitle("Report and delete this post");
											AlertDialog dialog1 = alertbuilder1.create();
											alertbuilder1.setPositiveButton("Report", new DialogInterface.OnClickListener() {
												
												@Override
												public void onClick(DialogInterface dialog, int which) {
													// TODO Auto-generated method stub
													JSONObject jsonObject = new JSONObject();
													try {
														jsonObject.put("postId",mPostId);
														wallPostModel.deleteWallPost(Constant.DELETE_POST, jsonObject);
													} catch (JSONException e) {
														e.printStackTrace();
													}	
												}
											});
											alertbuilder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
												
												@Override
												public void onClick(DialogInterface dialog, int which) {
													// TODO Auto-generated method stub
													
												}
											});
											alertbuilder1.show();
										}
										
									}
								});
								
								//dialog.show();
								alertbuilder.show();
						default:
							break;
						}
                        return true;
                    }
                });
                /** Showing the popup menu */
                popup.show();				
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			super.onBackPressed();
			setResult(RESULT_OK);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	public void fontStyles() {

		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		 
		wallPostMessage.setTypeface(mFontDark);
		wallSubviewCount.setTypeface(mFontDark);
		wallPostTxtNeed.setTypeface(mFontDark);
		wallSubviewProfession.setTypeface(mFontDark);
		wallPostTxtPostedby.setTypeface(mFontDark);
		wallPostTxtUserName.setTypeface(mFontDark);
		wallPostTxtTime.setTypeface(mFontDark);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		wallPostModel.addChangeListener(this);
		referralModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		wallPostModel.removeChangeListener(this);
		referralModel.removeChangeListener(this);
	}
	@Override
	public void finish() {
		setResult(RESULT_OK);
		super.finish();
	}
	public class PagerAdapter extends FragmentPagerAdapter {

		// Tab Titles
		private String tabtitles[] = new String[] { "Comments", "Referrals"};
		Context context;
		private FragmentManager fm;

		public PagerAdapter(FragmentManager fm) {
			super(fm);
			this.fm = fm;
		}

		@Override
		public int getCount() {
			return tabtitles.length;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			Fragment fragment = (Fragment) super.instantiateItem(container,position);
			// registeredFragments.put(position, fragment);
			return fragment;
		}

		@Override
		public Object instantiateItem(View container, int position) {
			return super.instantiateItem(container, position);
		}

		private String getFragmentTag(int pos) {
			return "android:switcher:" + R.id.pager + ":" + pos;
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			
			case 0:
				return WallPostExpandCommentsFragment.newInstance(mPosition, wallPostCotnent);
			case 1:
				return WallPostExpandReferralFragment.newInstance(mPosition,wallPostCotnent);
				
			}
			return null;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// registeredFragments.remove(position);
			// super.destroyItem(container, position, object);
		}

		/*
		 * public Fragment getRegisteredFragment(int position) { return
		 * registeredFragments.get(position); }
		 */

		@Override
		public CharSequence getPageTitle(int position) {
			return tabtitles[position];
		}
	}
	
	
	
	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onPageSelected(int position) {
		 mViewPager.setCurrentItem(position);
	}
	@Override
	public void uploadComments(int position) {
		setResult(RESULT_OK);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getPropertyName().equals("DeleteWallPost")) {
			
			AbstractDelete abstractGetWallPost = wallPostModel.getDeleteWallPostResponse();
			if(abstractGetWallPost != null){
				if(abstractGetWallPost.getStatus() == 1){
					Constant.mGetWallPostArrayList.remove(mPosition);
					setResult(RESULT_OK);
					finish();
					
					if(abstractUserProfile.getUserId() != wallPostCotnent.getWallPosts().getPostByUserProfile().getUserId()){
						Util.showAlert(this, "", "Post removed from your wall");
	                }else{
	                	Util.showAlert(this, "", "Wallpost deleted successfully");
	                }
				} else {
					L.ToastMessage(this, ""+abstractGetWallPost.getError());
				}
			}else{
				Util.showAlert(this,"Message", "Request failed");
			}
			
		} else if (event.getPropertyName().equals("HideWallPost")) {
			
			AbstractDelete abstractDelete = wallPostModel.getHideWallPostResponse();
			if(abstractDelete != null){
				if(abstractDelete.getStatus() == 1){
					Constant.mGetWallPostArrayList.remove(mPosition);
					setResult(RESULT_OK);
					finish();
				} else {
					L.ToastMessage(this, ""+abstractDelete.getError());
				}
			}else{
				Util.showAlert(this,"Message", "Request failed");
			}
		}		
	}
}

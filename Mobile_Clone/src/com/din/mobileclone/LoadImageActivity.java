

//------------------------------------------------------------------------------
//
//    (c) Copyright 2014.Dinesh Kumar M. All Rights Reserved.
//
//------------------------------------------------------------------------------

package com.din.mobileclone;

import static com.din.mobileclone.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.din.mobileclone.CommonUtilities.EXTRA_MESSAGE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.din.mobileclone.R;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxUnlinkedException;
import com.dropbox.client2.session.AppKeyPair;
import com.google.android.gcm.GCMRegistrar;


public class LoadImageActivity extends SherlockFragmentActivity{

	private int count;
	private Bitmap[] thumbnails;
	private boolean[] thumbnailsselection;
	private String[] arrPath;
	private ImageAdapter imageAdapter;
	ArrayList<String> f = new ArrayList<String>();// list of file paths
	File[] listFile;
	
	// In the class declaration section:
	private DropboxAPI<AndroidAuthSession> mDBApi;
	//private final static String FILE_DIR_IMAGES = "/MobileCloneImages/";
	//private final static String FILE_DIR_CONTACTS = "/MobileCloneContacts/";
	
	final static public String ACCOUNT_PREFS_NAME = "Prefs";
	final static public String ACCESS_KEY_NAME = "ACCESS_KEY";
	final static public String ACCESS_SECRET_NAME = "ACCESS_SECRET";
	ProgressDialog dialog = null;
	
	private ListView selecting_usersetup_listview;
	private GridView imagegrid;
	// private SelectingUserSetUpCursorAdapter mUserSetupAdapter;
	SelectingUserSetupBaseAdapter selectingUserSetupBaseAdapter;

	private EditText selectusersetup_search;
	private Map<String, ContactList> treeMap;

	private Cursor cursor;
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.load_images);
		
		dialog = new ProgressDialog(this);
		
		getFromSdcard();
		
		registerReceiver(mHandleMessageReceiver,
                new IntentFilter(DISPLAY_MESSAGE_ACTION));
		
		imagegrid = (GridView) findViewById(R.id.PhoneImageGrid);
	    imageAdapter = new ImageAdapter();
	    imagegrid.setAdapter(imageAdapter);
		    
	    selecting_usersetup_listview = (ListView) findViewById(R.id.selecting_usersetup_listview);

	    imagegrid.setVisibility(View.VISIBLE);
	    selecting_usersetup_listview.setVisibility(View.GONE);
	    
		//new LoadContactsTask().execute();
	    File file = new File(Constants.CONTACTS_PATH);
	    if(file.exists()){
	    	if(file.listFiles().length > 0){
	    		if (file.isDirectory())
		        {
	    			Constants.VCARD = new ArrayList<File>();
		            listFile = file.listFiles();
		            for (int i = 0; i < listFile.length; i++)
		            {
		            	
		            	Constants.VCARD.add(listFile[i]);

		            }
		        }
	    		
	    		selectingUserSetupBaseAdapter = new SelectingUserSetupBaseAdapter(
						LoadImageActivity.this, Constants.VCARD);
				selecting_usersetup_listview
						.setAdapter(selectingUserSetupBaseAdapter);
	    	}else{
	    		new LoadContactsTask().execute();
	    	}
	    }else{
    		new LoadContactsTask().execute();
    	}
	
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Load Images");
		
		// And later in some initialization function:
		AppKeyPair appKeys = new AppKeyPair(Constants.APP_KEY, Constants.APP_KEY);
		AndroidAuthSession session = new AndroidAuthSession(appKeys);
		
		
		Log.d("Session Type", "Is Session Linked = " + session.isLinked());
		
		
		mDBApi = new DropboxAPI<AndroidAuthSession>(session);
		
		//getUploadedContents();
		
		//AccessTokenPair access = mDBApi.getSession().getAccessTokenPair();
		//Log.d("Access Token Pair", "Token Pair = " + access.key +" *** "+access.secret);
		// MyActivity below should be your activity class name
		
		//if(getKeys() == null)
			//mDBApi.getSession().startOAuth2Authentication(LoadImageActivity.this);
	}

	protected void onResume() {
	    super.onResume();
	    if(Constants.VCARD.size() > 0){
	    	 if (mDBApi.getSession().authenticationSuccessful()) {
	 	        try {
	 	            // Required to complete auth, sets the access token on the session
	 	            mDBApi.getSession().finishAuthentication();
	 	            String accessToken = mDBApi.getSession().getOAuth2AccessToken();
	 	            
	 	        } catch (IllegalStateException e) {
	 	            Log.i("DbAuthLog", "Error authenticating", e);
	 	        }
	 	    }else{
	 	    	mDBApi.getSession().startOAuth2Authentication(LoadImageActivity.this);
	 	    }
	    }
	}
	
	@Override
    protected void onDestroy() {
        try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        super.onDestroy();
    }
	 
	 private final BroadcastReceiver mHandleMessageReceiver =
	            new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            //mDisplay.append(newMessage + "\n");
            if(newMessage.equals("Wow!!! You Received Alert Message From MClone!!!!"))
            	new UploadImageAsynckTask().execute();
        }
	  };
	    
	//copied from dropbox API
	private void storeKeys(String key, String secret) {
	    // Save the access key for later
	    SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
	    Editor edit = prefs.edit();
	    edit.putString(ACCESS_KEY_NAME, key);
	    edit.putString(ACCESS_SECRET_NAME, secret);
	    edit.commit();
	}


	private String[] getKeys() {
	    SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
	    String key = prefs.getString(ACCESS_KEY_NAME, null);
	    String secret = prefs.getString(ACCESS_SECRET_NAME, null);
	    if (key != null && secret != null) {
	        Log.i("Drop box keys","Got keys");
	        String[] ret = new String[2];
	        ret[0] = key;
	        ret[1] = secret;
	        return ret;
	    } else {
	        return null;
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		
		menu.add("Contacts")
		.setIcon(R.drawable.ic_action_person)
		.setShowAsAction(
				MenuItem.SHOW_AS_ACTION_ALWAYS
						| MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		menu.add("Images")
		.setIcon(R.drawable.ic_action_picture)
		.setShowAsAction(
				MenuItem.SHOW_AS_ACTION_ALWAYS
						| MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		menu.add("Upload")
				.setIcon(R.drawable.ic_action_upload)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_ALWAYS
								| MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		//MenuInflater actionbarmenuinflater = getSupportMenuInflater();
		//actionbarmenuinflater.inflate(R.menu.action_bar_menu, menu);

		/*getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#68a339")));*/

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if (item.getItemId() == android.R.id.home) {
			finish();
		}else{
			if(item.getTitle().equals("Upload")) {
				new UploadImageAsynckTask().execute();
			}else if(item.getTitle().equals("Contacts")) {
				imagegrid.setVisibility(View.VISIBLE);
			    selecting_usersetup_listview.setVisibility(View.GONE);
			}else if(item.getTitle().equals("Images")) {
				imagegrid.setVisibility(View.GONE);
			    selecting_usersetup_listview.setVisibility(View.VISIBLE);
			}
			
		}
		return super.onOptionsItemSelected(item);

	}

	class LoadContactsTask extends AsyncTask<Void, Void, Void> {

		// ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setTitle("Please wait");
		    dialog.setMessage("Retriving Contacts and Images...");
		    dialog.setIndeterminate(false);
		    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		    dialog.setCancelable(true);
		    dialog.show();
		    
		    Constants.VCARD = new ArrayList<File>();
		}

		@Override
		protected Void doInBackground(Void... params) {

			//treeMap = getPhoneContactDetails();

			getVcardString();
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			 if (dialog != null)
				 dialog.dismiss();

			 if (mDBApi.getSession().authenticationSuccessful()) {
	 	        try {
	 	            // Required to complete auth, sets the access token on the session
	 	            mDBApi.getSession().finishAuthentication();
	 	            String accessToken = mDBApi.getSession().getOAuth2AccessToken();
	 	            
	 	        } catch (IllegalStateException e) {
	 	            Log.i("DbAuthLog", "Error authenticating", e);
	 	        }
		 	 }else{
		 	    mDBApi.getSession().startOAuth2Authentication(LoadImageActivity.this);
		 	 }
			 
			// dbAccess.insertContactList(treeMap);

			 selectingUserSetupBaseAdapter = new SelectingUserSetupBaseAdapter(
						LoadImageActivity.this, Constants.VCARD);
			 selecting_usersetup_listview
						.setAdapter(selectingUserSetupBaseAdapter);
		}

	}
	
	private void getVcardString() {
		
		cursor = getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
				null, null);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {

				get(cursor);
				Log.d("TAG",
						"Contact " + (i + 1) + "VcF String is" + Constants.VCARD.get(i));
				cursor.moveToNext();
			}

		} else {
			Log.d("TAG", "No Contacts in Your Phone");
		}
	}
	
	public void get(Cursor cursor) {

		// cursor.moveToFirst();
		String lookupKey = cursor.getString(cursor
				.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
		Uri uri = Uri.withAppendedPath(
				ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
		String displayName = cursor.getString(cursor
				.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
		
		AssetFileDescriptor fd;
		try {
			fd = this.getContentResolver().openAssetFileDescriptor(uri, "r");

			// Your Complex Code and you used function without loop so how can
			// you get all Contacts Vcard.??

			/*
			 * FileInputStream fis = fd.createInputStream(); byte[] buf = new
			 * byte[(int) fd.getDeclaredLength()]; fis.read(buf); String VCard =
			 * new String(buf); String path =
			 * Environment.getExternalStorageDirectory().toString() +
			 * File.separator + vfile; FileOutputStream out = new
			 * FileOutputStream(path); out.write(VCard.toString().getBytes());
			 * Log.d("Vcard", VCard);
			 */

			FileInputStream fis = fd.createInputStream();
			byte[] buf = new byte[(int) fd.getDeclaredLength()];
			fis.read(buf);
			String vcardstring = new String(buf);
			

			File file = new File(Constants.CONTACTS_PATH);
			file.mkdirs();
			String storage_path = Environment.getExternalStorageDirectory()
					.toString() + File.separator +"Test_Contacts/"+displayName+".txt";
			file = new File(storage_path);
			if(!file.exists())file.createNewFile();
			FileOutputStream mFileOutputStream = new FileOutputStream(file);
			mFileOutputStream.write(vcardstring.toString().getBytes());
			mFileOutputStream.flush();
			mFileOutputStream.close();
			
			Constants.VCARD.add(file);
			Log.d("Vcard",  vcardstring);

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	private Map<String, ContactList> getPhoneContactDetails() {

		// ArrayList<ContactList> contactArrayList = null;
		Map<String, ContactList> sortedMap = new TreeMap<String, ContactList>();

		ContactList contactList = new ContactList();

		ContentResolver contentResolver = getContentResolver();
		String emailId = null;
		Cursor test_cursor = contentResolver.query(RawContacts.CONTENT_URI,
				new String[] { RawContacts._ID, RawContacts.ACCOUNT_TYPE },
				/*
				 * RawContacts.ACCOUNT_TYPE + " <> 'com.anddroid.contacts.sim' "
				 * //if you don't want to Sim contacts + " AND " +
				 * RawContacts.ACCOUNT_TYPE + " <> 'com.google' "
				 */// if you don't want to google contacts also
				null, null, null);
		if (test_cursor.getCount() > 0) {
			// contactArrayList = new ArrayList<ContactList>();
			while (test_cursor.moveToNext()) {

				String rawContactId = test_cursor.getString(test_cursor
						.getColumnIndex(RawContacts._ID));
				// LogHereIAM.d("Raw contact ID= "+rawContactId);
				Cursor cursor = contentResolver.query(Data.CONTENT_URI,
						new String[] { Data._ID, Data.CONTACT_ID,
								Data.DISPLAY_NAME, Phone.NUMBER, Phone.TYPE },
						"(" + Data.CONTACT_ID + "=? AND " + Data.MIMETYPE
								+ "='" + Phone.CONTENT_ITEM_TYPE + "' AND "
								+ Data.DISPLAY_NAME + " Not Null)",
						new String[] { rawContactId }, Data.DISPLAY_NAME
								+ " ASC");

				if (cursor.getCount() > 0) {
					while (cursor.moveToNext()) {
						// LogHereIAM.d("================= ===========================");
						// if
						// (Integer.parseInt(cursor.getString(cursor.getColumnIndex(Data.HAS_PHONE_NUMBER)))
						// > 0){
						// contactName = cursor.getString(1);
						// }
						// LogHereIAM.d("Contact ID = "+contactName);
						if (cursor.getString(4).equals("" + Phone.TYPE_MOBILE)) {

							contactList.setContactId(cursor.getString(1));
							contactList.setPersonName(cursor.getString(2));
							contactList.setPhoneNum(cursor.getString(3));
							// LogHereIAM.d("Contact Name = "+displayName);
							// LogHereIAM.d("Phone Num = "+phoneNum);
						}
						// LogHereIAM.d("============================================");
					}
				}

				if (contactList.getPersonName() != null) {

					Cursor email_cursor = contentResolver.query(
							Data.CONTENT_URI,
							new String[] { Data._ID, Data.CONTACT_ID,
									Email.ADDRESS, Phone.TYPE }, "("
									+ Data.CONTACT_ID + "=? AND "
									+ Data.MIMETYPE + "='"
									+ Email.CONTENT_ITEM_TYPE + "')",
							new String[] { rawContactId }, null);

					if (email_cursor.getCount() > 0) {
						while (email_cursor.moveToNext()) {
							if (email_cursor.getString(1) != null) {
								// LogHereIAM.d("****************** Email Id ******"
								// + email_cursor.getString(2));
								// LogHereIAM.d("****************** Contact Id ******"
								// + email_cursor.getString(1));
								emailId = email_cursor.getString(2);
							}
						}
					}

					email_cursor.close();
				}
				// LogHereIAM.d("Count = "+cursor.getCount());
				cursor.close();

				if (contactList.getPersonName() != null) {
					// LogHereIAM.d("Test>>>>> " + emailId);
					/*
					 * if(emailId !=null) contactList.setPersonEmailId(emailId);
					 */
					String personName = contactList.getPersonName();
					contactList.setPersonName(contactList.getPersonName()
							+ "&&" + emailId);
					emailId = null;
					// contactArrayList.add(contactList);
					sortedMap.put(personName, contactList);

					contactList = new ContactList();
				}

			}
		}
		// Collections.sort(contactArrayList);
		return sortedMap;
	}
	
	public void getFromSdcard()
	{
		SharedPreferences preferenceManager = PreferenceManager.getDefaultSharedPreferences(LoadImageActivity.this);
		Map<String,?> keys = preferenceManager.getAll();

		for(Map.Entry<String,?> entry : keys.entrySet()){
		            Log.d("map values",entry.getKey() + ": " + 
		                                   entry.getValue().toString());            
		 }
		
		
	    File file= new File(android.os.Environment.getExternalStorageDirectory(),""+preferenceManager.getString("preferences_enter_file_name", "sdcard"));
	    
	        if (file.isDirectory())
	        {
	            listFile = file.listFiles();


	            for (int i = 0; i < listFile.length; i++)
	            {
	            	Log.d("Path", "***"+listFile[i].getAbsolutePath());
	                f.add(listFile[i].getAbsolutePath());

	            }
	        }
	}

	public class ImageAdapter extends BaseAdapter {
	    private LayoutInflater mInflater;

	    public ImageAdapter() {
	        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    }

	    public int getCount() {
	        return f.size();
	    }

	    public Object getItem(int position) {
	        return position;
	    }

	    public long getItemId(int position) {
	        return position;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder holder;
	        if (convertView == null) {
	            holder = new ViewHolder();
	            convertView = mInflater.inflate(
	                    R.layout.load_images_subview, null);
	            holder.imageview = (ImageView) convertView.findViewById(R.id.thumbImage);

	            convertView.setTag(holder);
	        }
	        else {
	            holder = (ViewHolder) convertView.getTag();
	        }


	        Bitmap myBitmap = decodeScaledBitmapFromSdCard(f.get(position),150);
	        holder.imageview.setBackgroundDrawable(new BitmapDrawable(getResources(),myBitmap));
	        return convertView;
	    }
	}
	class ViewHolder {
	    ImageView imageview;


	}
	
	public Bitmap decodeScaledBitmapFromSdCard(String filePath,
	        int reqSize) {

		try {
	        //Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(filePath),null,o);

	        //The new size we want to scale to
	        final int REQUIRED_SIZE = reqSize;

	        //Find the correct scale value. It should be the power of 2.
	        int scale=1;
	        while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
	            scale*=2;

	        //Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	        return BitmapFactory.decodeStream(new FileInputStream(filePath), null, o2);
	    } catch (FileNotFoundException e) {
	    	e.printStackTrace();
	    } catch (OutOfMemoryError e) {
	    	e.printStackTrace();
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    return null;
	}

	public static int calculateInSampleSize(
	        BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;

	    if (height > reqHeight || width > reqWidth) {

	        // Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);

	        // Choose the smallest ratio as inSampleSize value, this will guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }

	    return inSampleSize;
	}
	
	class UploadImageAsynckTask extends AsyncTask<String, Integer, Void> {
		
		@Override
		protected void onPreExecute() {
			//progressBar = new CustomProgressbar(SplashActivity.this);
			//dialog = ProgressDialog.show(LoadImageActivity.this,"Message", "Uploading Image...");
			dialog.setTitle("Please wait");
		    dialog.setMessage("Uploading Image...");
		    dialog.setIndeterminate(false);
		    dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		    dialog.setCancelable(true);
		    dialog.setMax(f.size());
		    dialog.show();
			   
		}

		@Override
		protected Void doInBackground(String... params) {
			//upLoadToDropbox(this);
		
			for (int i = 0; i < f.size(); i++) {
				File tmpFile = new File(f.get(i));

				FileInputStream fis = null;
				try {
					fis = new FileInputStream(tmpFile);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
		        try {
		            DropboxAPI.Entry newEntry = mDBApi.putFile("/"+f.get(i), fis, tmpFile.length(),"",null);
		            
		            publishProgress(i+1);
		            Log.i("DbExampleLog", "The uploaded file's rev is: " + newEntry.rev);
		        } catch (DropboxUnlinkedException e) {
		            Log.e("DbExampleLog", "User has unlinked.");
		        } catch (DropboxException e) {
		            Log.e("DbExampleLog", "Something went wrong while uploading.");
		        }
			}
			
			dialog.setMessage("Uploading Contacts...");
			  
			for (int i = 0; i < Constants.VCARD.size(); i++) {
				File tmpFile = Constants.VCARD.get(i);

				FileInputStream fis = null;
				try {
					fis = new FileInputStream(tmpFile);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				
		        try {
		            DropboxAPI.Entry newEntry = mDBApi.putFile(""+Constants.VCARD.get(i), fis, tmpFile.length(),"",null);
		            
		            publishProgress(i+1);
		            Log.i("DbExampleLog", "The uploaded file's rev is: " + newEntry.rev);
		        } catch (DropboxUnlinkedException e) {
		            Log.e("DbExampleLog", "User has unlinked.");
		        } catch (DropboxException e) {
		            Log.e("DbExampleLog", "Something went wrong while uploading.");
		        }
			}
			
			 File file = new File(Constants.CONTACTS_PATH);
			 if(file.exists()){
		    	if(file.listFiles().length > 0){
		    		if (file.isDirectory())
			        {
			            listFile = file.listFiles();
			            for (int i = 0; i < listFile.length; i++)
			            {
			            	listFile[i].delete();

			            }
			        }
		    	}
			 }
			 SharedPreferences preferenceManager = PreferenceManager.getDefaultSharedPreferences(LoadImageActivity.this);
			 boolean canDeleteContents = preferenceManager.getBoolean("preferences_enter_delete_contacts", false);
			 if(canDeleteContents){
				 file= new File(android.os.Environment.getExternalStorageDirectory(),""+preferenceManager.getString("preferences_enter_file_name", "sdcard"));
				 if(file.exists()){
			    	if(file.listFiles().length > 0){
			    		if (file.isDirectory())
				        {
				            listFile = file.listFiles();
				            for (int i = 0; i < listFile.length; i++)
				            {
				            	listFile[i].delete();
	
				            }
				        }
			    	}
				 }   
			 }
			    
			return null;
		}

		
		@Override
		protected void onProgressUpdate(Integer... progress) {
			// TODO Auto-generated method stub
			
			dialog.setProgress(progress[0]);
			 
			super.onProgressUpdate(progress);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (dialog != null)
				dialog.dismiss();
			
			if(listFile != null && listFile.length > 0){
				for (int i = 0; i < listFile.length; i++)
	            {
	            	listFile[i].delete();

	            }
			}
		}

	}
}

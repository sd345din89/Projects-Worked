package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.AbstractDelete;
import com.strobilanthes.forrestii.model.wallpost.AbstractReplyWallPost;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;
import com.strobilanthes.forrestii.model.wallpost.ReplyPost;
import com.strobilanthes.forrestii.model.wallpost.WallPost;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.Util;

public class WallPostCommentDialogFragment extends BaseDialogFragment<DBAccess> {
	
	private Typeface mFontLight,mFontDark;
    private ListView comentsList;
	private static final String ARG_POSITION = "position";
	private CommentsContactsAdapter adapter;
	
	@InjectView(R.id.comment_main_layout) private RelativeLayout commentMainLayout;
	@InjectView(R.id.commentsListview)    private ListView lv;
	@InjectView(R.id.seperatorComments1)  private View seperatorComments1;
	@InjectView(R.id.add_comments)        private LinearLayout addComments;
	@InjectView(R.id.comment_edittext)    private EditText cmt_message;
	@InjectView(R.id.comment_enter)       private ImageView img_send;

	@Inject WallPost 				wallPostModel;
	
	private int mPosition = -1;
	private int mPostId = 0;
	private int mPostByUserId = 0;
	private ArrayList<ReplyPost> replyPostArrayList;
	private  GetWallPost getWallPost;
	private String mTextComments  = null;
	private AbstractUserProfile abstractUserProfile;
	private CharSequence[] userItems = {"Copy Comments","Edit Comments","Delete Comments"};
	private CharSequence[] noneUserItems = {"Copy Comments"};
	private String mUpdatedString = null;
	private AbstractUserProfile mAbstractUserProfile = null;
	private int mReplyId = -1;
	private int mSelectedPostition = -1;
	static UpdateWallpost updateWallpost;
	private ClipboardManager myClipboard;
	private ClipData myClip;

	interface UpdateWallpost{
		void uploadComments(int position);
	}
	
    public static WallPostCommentDialogFragment newInstance(int pos, GetWallPost getWallPost,WallPostFragment wallPostFragment) {
		 WallPostCommentDialogFragment frag = new WallPostCommentDialogFragment();
		 updateWallpost = wallPostFragment;
         Bundle args = new Bundle();
         args.putInt("position", pos);
         args.putSerializable("wall_post", getWallPost);
         frag.setArguments(args);
         return frag;
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.view_dg_wp_comment, null);
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		wallPostModel.setContext(getActivity());
		img_send.setOnClickListener(this);
		
		Util.hideEditTextError(cmt_message);
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		cmt_message.setTypeface(mFontDark);
		int position = getArguments().getInt("position");
		mPosition = position;
		getWallPost = (GetWallPost) getArguments().getSerializable("wall_post");
		mPostId = getWallPost.getWallPosts().getPostId();
		mPostByUserId = getWallPost.getWallPosts().getPostByUserProfile().getUserId();
		
		abstractUserProfile = getHelper().getDBUserProfile();
		
		replyPostArrayList = getWallPost.getWallPosts().getReplies();
		
		adapter = new CommentsContactsAdapter(getActivity());
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {
				
				if(position !=0 && position != 1){
					
					final ReplyPost replyPost = (ReplyPost) parent.getItemAtPosition(position);
					
					AlertDialog.Builder alertbuilder = new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT);
					//alertbuilder.setTitle("Comments Options");
					AlertDialog dialog = alertbuilder.create();
					alertbuilder.setItems((abstractUserProfile.getUserId() == replyPost.getReplyUserProfile().getUserId())? userItems : noneUserItems, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if(which == 1){
								final EditText input = new EditText(getActivity());
								input.setMaxLines(6);
								int maxLength = 250;
								InputFilter[] FilterArray = new InputFilter[1];
								FilterArray[0] = new InputFilter.LengthFilter(maxLength);
								input.setFilters(FilterArray);								
								input.setText(replyPost.getMessage());
								LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
								                        LinearLayout.LayoutParams.MATCH_PARENT);
								input.setLayoutParams(lp);
								new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT).setMessage("Update Post")
								.setView(input).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										JSONObject jsonObject = new JSONObject();
										try {
											mSelectedPostition = position-2;
											mUpdatedString = input.getText().toString();
											if (mUpdatedString.length() > 0) {
												if (!Util.limitSpecialCharacters(mUpdatedString)) {
													Util.hideSoftKeyboard(getActivity(), input);
													Util.showAlert(getActivity(), "", "Invalid special characters");
													return;
												}
												
												mReplyId = replyPost.getReplyId();
												mAbstractUserProfile = replyPost.getReplyUserProfile();
												jsonObject.put("message",input.getText().toString());
												jsonObject.put("ReplyId",replyPost.getReplyId());
												wallPostModel.updateReplyWallPost(Constant.UPDATE_REPLY, jsonObject);
											} else {
												input.setError("Please enter your comments to post");
												return;
											}
											
										} catch (JSONException e) {
											e.printStackTrace();
										}
									}
								}).setNegativeButton("Cancel",null).show();
							} else if(which == 0) {
								 String text = replyPost.getMessage().toString();
								 if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
									    android.text.ClipboardManager clipboard = (android.text.ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
									    clipboard.setText(text);
									} else {
									    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
									    android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
									            clipboard.setPrimaryClip(clip);
									     
									}	
								Util.showAlert(getActivity(), "Message", "Copied to the clipboard");
							     
							} else if(which == 2) {
								new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT)
								.setMessage("Do you want to delete this post?").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										JSONObject jsonObject = new JSONObject();
										try {
											mSelectedPostition = position-2;
											jsonObject.put("ReplyId",replyPost.getReplyId());
											wallPostModel.deleteReplyWallPost(Constant.DELETE_REPLY, jsonObject);
										} catch (JSONException e) {
											e.printStackTrace();
										}
									}
								}).setNegativeButton("Cancel",null).setCancelable(false).show();	
							}
						}
					});
					alertbuilder.show();
				}

			}
		});
		
		/*cmt_enter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//upload and update comment
//				JSONObject jsonObject = new JSONObject();
				try {
//					int mSelectedPostition = position-2;
//					mUpdatedString = input.getText().toString();
//					String mUpdatedString = cmt_message.getText().toString().trim();
//					Object mReplyId = getWallPost.getWallPosts().getReplies().get(0);
//					Object mAbstractUserProfile = replyPost.getReplyUserProfile();
//					jsonObject.put("message",input.getText().toString());
//					jsonObject.put("ReplyId",replyPost.getReplyId());
//					wallPostModel.updateReplyWallPost(Constant.UPDATE_REPLY, jsonObject);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
	}
	

	@Override
	public boolean onBackPressed() {
		
		return super.onBackPressed();
	}

	@Override
	public void dismiss() {
//		updateWallpost.uploadComments(mPosition);
		super.dismiss();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.comment_enter:
			String cmt = cmt_message.getText().toString();
			if(cmt.length() > 0){
				
				if (cmt.trim().length() > 0 && !Util.limitSpecialCharacters(cmt)) {
					cmt_message.setError("The comments should not contain any special characters");
					return;
				}
				
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(cmt_message.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				
				JSONObject jsonObject = new JSONObject();
				try {
					 mTextComments = cmt_message.getText().toString();
					 mTextComments = mTextComments.replace("\\n", " "); 
					jsonObject.put("posts",new JSONObject().put("postId", mPostId).put("postByUserProfile", new JSONObject().put("userId", mPostByUserId)));
					jsonObject.put("ReplyUserProfile", new JSONObject().put("userId", getHelper().getUserId()));
					//jsonObject.put("postByUserProfile", new JSONObject().put("userId", mPostByUserId));
					jsonObject.put("message",mTextComments);
					wallPostModel.replyWallPost(Constant.REPLY_TO_POST, jsonObject);
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
			} else {
				cmt_message.setError("Please enter your comments to post");
			}
			
			break;

		default:
			break;
		}
	}
	
	 @Override
     public Dialog onCreateDialog(Bundle savedInstanceState) {
        
			Dialog commentsDialog = new Dialog(getActivity(),R.style.AppTheme);
			commentsDialog.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
			commentsDialog.getWindow().getAttributes().windowAnimations = R.style.comment_animation;
			//commentsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			commentsDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
			
			commentsDialog.setCancelable(true);
			
         return commentsDialog;
     }

	class CommentsContactsAdapter extends BaseAdapter {

		private final Context context;
		LayoutInflater inflater;
		
		public CommentsContactsAdapter(Context context) {
			this.context = context;
	        inflater = LayoutInflater.from(context);

		}

		@Override
		public int getCount() {
			return replyPostArrayList.size() + 2;
		}

		@Override
		public ReplyPost getItem(int position) {
			if(position > 1)
				return replyPostArrayList.get(position-2);
			else
				return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			if(position == 0) {
				
				convertView = inflater.inflate(R.layout.frg_wallpost_header_user, null);
				TextView txt_need = (TextView) convertView.findViewById(R.id.wall_post_txt_need);
				TextView txt_postedby = (TextView) convertView.findViewById(R.id.wall_post_txt_postedby);
				TextView txt_time = (TextView) convertView.findViewById(R.id.wall_post_txt_time);

				
				TextView txt_username = (TextView) convertView.findViewById(R.id.wall_post_txt_userName);
				TextView txt_profession = (TextView)convertView.findViewById(R.id.wall_subview_profession);
				txt_need.setTypeface(mFontDark);

				txt_username.setTypeface(mFontDark);
				txt_postedby.setTypeface(mFontDark);

				txt_profession.setTypeface(mFontDark);
				RoundedImageView img_user = (RoundedImageView) convertView.findViewById(R.id.wall_post_img_user);
				
				String postUserName = null;
				if(abstractUserProfile.getUserId() == getWallPost.getWallPosts().getPostByUserProfile().getUserId()){
					//firstName = userProfile.getFirstName();
					postUserName = "You";
				}else{
					if(getWallPost.getWallPosts().getPostByUserProfile().getFirstName() == null)
						postUserName = "Forrestii User";
					else
						postUserName = getWallPost.getWallPosts().getPostByUserProfile().getFirstName();
				}
				txt_time.setText(Util.updatedOnTime(getWallPost.getWallPosts().getUpdatedOn()));
				txt_username.setSelected(true);
				txt_username.setText(postUserName);
				txt_profession.setSelected(true);

				txt_profession.setText(getWallPost.getWallPosts().getSkill().getSkillName());
				ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + getWallPost.getWallPosts().getPostByUserProfile().getPhotoID(), img_user,options,animateFirstListener);

			} else if(position == 1) {
				
				convertView = inflater.inflate(R.layout.frg_wallpost_header_postcontents, null);
				TextView txt_message = (TextView)convertView.findViewById(R.id.wall_post_message);
				TextView txt_count = (TextView)convertView.findViewById(R.id.wall_subview_count);
				txt_message.setTypeface(mFontLight);
				txt_count.setTypeface(mFontDark);

				String mMessage = getWallPost.getWallPosts().getMessage().trim().toString();
				
				if(mMessage != null && mMessage.length() > 0){
					txt_message.setText(mMessage);
				}else{
					txt_message.setText("I need "+getWallPost.getWallPosts().getSkill().getSkillName()+". Can anyone please refer me a good one!!");
				}
				int comments = getWallPost.getWallPosts().getReplies().size();
				int referal = getWallPost.getWallPosts().getReferral().size();
				
				if(abstractUserProfile.getUserId() == mPostByUserId){
					txt_count.setText(""+comments+" Replies & "+referal+" Referrals" );	
				}else{
					txt_count.setText(""+comments+" Replies");
				}
					
			} else {
				
				convertView = inflater.inflate(R.layout.view_dg_comment_list_item, null);
				ImageView commentsListItemImgUser = (ImageView)convertView.findViewById( R.id.comments_list_item_img_user );
				TextView commentsListItemTxtName = (TextView)convertView.findViewById( R.id.comments_list_item_txt_name );
				TextView comments_list_item_txt_time = (TextView)convertView.findViewById( R.id.comments_list_item_txt_time );
				commentsListItemTxtName.setTypeface(mFontDark);
				comments_list_item_txt_time.setTypeface(mFontDark);
				ReplyPost replyPost = getItem(position);
				if(replyPost != null){
					ForrestiiApplication.getImageLoaderInstance().displayImage(Constant.FOLDER_PROFILE_PHOTO_NAME + replyPost.getReplyUserProfile().getPhotoID(), commentsListItemImgUser,options,animateFirstListener);
					String nameString = replyPost.getReplyUserProfile().getFirstName()+" : ";
					String messageString = replyPost.getMessage();
					String totalString = nameString+messageString;

					  Spannable nameSpan = new SpannableString(totalString);        
					  nameSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.forrestii_pink_color)), 0, nameString.length(),0);
					  nameSpan.setSpan(new RelativeSizeSpan(1.15f),  0, nameString.length(), 0);
					  commentsListItemTxtName.setTypeface(mFontLight);
					commentsListItemTxtName.setText(nameSpan);
					comments_list_item_txt_time.setText(Util.updatedOnTime(replyPost.getUpdatedOn()));
					comments_list_item_txt_time.setTypeface(mFontDark);
					commentsListItemTxtName.setTypeface(mFontLight);
					comments_list_item_txt_time.setTypeface(mFontDark);
				}
				
			}
			
			return convertView;
		}
	}

	@Override
	public void onResume(){
		super.onResume();
		wallPostModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		wallPostModel.removeChangeListener(this);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if(event.getPropertyName().equals("ReplyWallPost")){
			
			AbstractReplyWallPost abstractReplyWallPost = wallPostModel.getReplyWallPostResponse();
			
			if(abstractReplyWallPost != null){
				if(abstractReplyWallPost.getStatus() == 1){
					replyPostArrayList = getWallPost.getWallPosts().getReplies();
					ReplyPost replyPost = new ReplyPost();
					replyPost.setMessage(mTextComments);
					replyPost.setReplyId(abstractReplyWallPost.getReplyResponse().getReplyId());
					replyPost.setReplyUserProfile(getHelper().getDBUserProfile());
					replyPost.setUpdatedOn(Calendar.getInstance().getTimeInMillis());
					replyPostArrayList.add(replyPost);
					
					ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
					GetWallPost getWallPost = getWallPostList.get((mPosition));
					getWallPost.getWallPosts().setReplies(replyPostArrayList);
					getWallPostList.set((mPosition),getWallPost);
					Constant.mGetWallPostArrayList = getWallPostList;
					
					cmt_message.setText("");
					adapter.notifyDataSetChanged();
					updateWallpost.uploadComments(mPosition);
				} else {
					Util.showAlert(getActivity(),"Message", ""+abstractReplyWallPost.getError());
				}
			} else {
				Util.showAlert(getActivity(),"Message", "Request Failed");
				cmt_message.setText("");
			}
			
		} else if(event.getPropertyName().equals("DeleteReplyPost")){
			
			AbstractDelete abstractDelete = wallPostModel.getDeleteReplyPosResponse();
			if(abstractDelete != null){
				if(abstractDelete.getStatus() == 1){
					replyPostArrayList = getWallPost.getWallPosts().getReplies();
					
					replyPostArrayList.remove(mSelectedPostition);
					
					ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
					GetWallPost getWallPost = getWallPostList.get((mPosition));
					getWallPost.getWallPosts().setReplies(replyPostArrayList);
					getWallPostList.set((mPosition),getWallPost);
					Constant.mGetWallPostArrayList = getWallPostList;
					
					cmt_message.setText("");
					adapter.notifyDataSetChanged();
					updateWallpost.uploadComments(mPosition);
				}else{
					Util.showAlert(getActivity(),"Message", ""+abstractDelete.getError());
				}
			}
			
		} else if(event.getPropertyName().equals("UpdateReplyPost")){
			
			AbstractDelete abstractDelete = wallPostModel.getUpdateReplyPostResponse();
			if(abstractDelete != null){
				if(abstractDelete.getStatus() == 1){
					replyPostArrayList = getWallPost.getWallPosts().getReplies();
					ReplyPost replyPost = new ReplyPost();
					replyPost.setMessage(mUpdatedString);
					replyPost.setReplyId(mReplyId);
					replyPost.setReplyUserProfile(mAbstractUserProfile);
					replyPost.setUpdatedOn(Calendar.getInstance().getTimeInMillis());
					replyPostArrayList.set(mSelectedPostition,replyPost);
					
					ArrayList<GetWallPost> getWallPostList = Constant.mGetWallPostArrayList;
					GetWallPost getWallPost = getWallPostList.get((mPosition));
					getWallPost.getWallPosts().setReplies(replyPostArrayList);
					getWallPostList.set((mPosition),getWallPost);
					Constant.mGetWallPostArrayList = getWallPostList;
					
					//edit_comments.setText("");
					adapter.notifyDataSetChanged();
					
				} else {
					Util.showAlert(getActivity(),"Message", ""+abstractDelete.getError());
				}
			} else {
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}	
		}
	}
	
}

package com.strobilanthes.forrestii.model.coins;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;

public class CoinsImpl implements Coins{

	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private Activity context;

	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		// TODO Auto-generated method stub
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		// TODO Auto-generated method stub
		listener.remove(listenerToRemove);
	}

	@Override
	public void getMyReferral() {
		// TODO Auto-generated method stub

	}

	@Override
	public AbstractCoins abstractGetMyReferral() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}
}

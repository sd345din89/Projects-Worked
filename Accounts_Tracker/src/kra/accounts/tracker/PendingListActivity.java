package kra.accounts.tracker;

import java.util.ArrayList;
import java.util.Calendar;

import kra.accounts.tracker.adapter.PendingAdapterScreen;
import kra.accounts.tracker.database.KraFinanceDbManager;
import kra.accounts.tracker.models.Constants;
import kra.accounts.tracker.models.CustomerAccountDetails;
import kra.accounts.tracker.models.Util;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PendingListActivity extends Activity{
	private ListView pending_list;
	
	private Button pending_back_btn,pending_acc_spinner,pending_sort_spinner;
	
	private Util util;
	
	private KraFinanceDbManager dbManager;
	
	public String[] pending_acc_name;
	public String[] pending_sort_name;
	
	private RelativeLayout pending_filter_lyt;
	private boolean isFilterButtonClicked=true;
	private LinearLayout pending_filter_btn;

	

	private String accId;

	private String customerName;
	
	private int sortByDate;
	
	private int sortByName;
	
	private int sortByAmount;
	
	private int filterByPeriod;
	
	
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.pending_list_screen);
		
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		util = new Util();
		
		dbManager = new KraFinanceDbManager(this);
		
		pending_acc_name = util.getAllAccountList(dbManager);
		
		pending_sort_name = new String[]{"1 month", "3 months", "1 year", "All data"};
		
		accId =  null;
		
		customerName = null;
		
		sortByDate = 0;
		sortByName = -1;
		sortByAmount = -1;
		filterByPeriod = -1;
		
		pending_list=(ListView)findViewById(R.id.pending_list);
		pending_list.setEmptyView((TextView)findViewById(R.id.pending_empty_txt));
		
	
		
		(findViewById(R.id.pending_filter_byn)).setClickable(false);
		pending_acc_spinner=(Button)findViewById(R.id.pending_acc_spinner);
		pending_acc_spinner.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showPendingAccountAlert();
			}
		});
		pending_sort_spinner=(Button)findViewById(R.id.pending_sort_spinner);
		pending_sort_spinner.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showPendingSortAlert();
			}
		});
		
		pending_filter_lyt = (RelativeLayout)findViewById(R.id.pending_filter_lyt);
		pending_filter_lyt.setVisibility(View.GONE);
		
		pending_filter_btn = (LinearLayout)findViewById(R.id.pending_filter_btn);
		pending_filter_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(isFilterButtonClicked){
					pending_filter_lyt.setVisibility(View.VISIBLE);
					isFilterButtonClicked=false;
				}else{
					pending_filter_lyt.setVisibility(View.GONE);
					isFilterButtonClicked=true;
				}
			}
		});
		
		
		EditText pendingCustomerName = (EditText)findViewById(R.id.pending_filter_cus_name_txt);
		
		pendingCustomerName.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
				//filterByPeriod = -1;
				customerName =  s.toString();
				
				getPendingList();			
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});
		
		(findViewById(R.id.pending_name_tab_label)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				sortByDate = -1;
				sortByAmount = -1;
				//filterByPeriod = -1;
				
				if(sortByName == 0){
					
					((ImageView)findViewById(R.id.pending_customer_name_sort_indicator_img)).setBackgroundResource(R.drawable.aescending_order_img);
					sortByName = 1;
					getPendingList();
					
				}else{
					
					((ImageView)findViewById(R.id.pending_customer_name_sort_indicator_img)).setBackgroundResource(R.drawable.descending_order_img);
					sortByName = 0;
					getPendingList();
					
				}
				
			}
		});
		
		
		(findViewById(R.id.pending_due_date_tab_label)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				sortByName = -1;
				sortByAmount = -1;
				//filterByPeriod = -1;
				
				if(sortByDate ==0){
					sortByDate = 1;//android:id="@+id/pending_dueDate_sort_indicator_img"
					((ImageView)findViewById(R.id.pending_dueDate_sort_indicator_img)).setBackgroundResource(R.drawable.aescending_order_img);
					getPendingList();
				}else{
					sortByDate = 0;
					((ImageView)findViewById(R.id.pending_dueDate_sort_indicator_img)).setBackgroundResource(R.drawable.descending_order_img);
					getPendingList();
				}
			}
		});
		
		(findViewById(R.id.pending_principal_amt_tab_lyt)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sortByDate = -1;
				sortByName = -1;
				//filterByPeriod = -1;
				
				if(sortByAmount == 0){
					
					((ImageView)findViewById(R.id.pending_principal_amt_sort_indicator_img)).setBackgroundResource(R.drawable.aescending_order_img);
					sortByAmount = 1;
					getPendingList();
					
				}else{					
					((ImageView)findViewById(R.id.pending_principal_amt_sort_indicator_img)).setBackgroundResource(R.drawable.descending_order_img);
					sortByAmount = 0;
					getPendingList();

				}
				
			}
		});
		
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		getPendingList();
	}


	private void getPendingList(){
		
		ArrayList<CustomerAccountDetails> customerAccountDetails = util.getInterestPendingUserList(dbManager,sortByDate, sortByName,  sortByAmount, filterByPeriod, accId, customerName);
		
		if(customerAccountDetails != null){
			
			pending_list.setAdapter(new PendingAdapterScreen(this, customerAccountDetails, util, dbManager));
			
		}else{
			pending_list.setAdapter(null);
		}
		
	}
	
	
	
	
	
	protected void showPendingSortAlert() {
		
		AlertDialog.Builder pending_sort_Select = new AlertDialog.Builder(this);
		
		pending_sort_Select.setTitle("Select any pending due period");
		
		pending_sort_Select.setSingleChoiceItems(pending_sort_name, -1, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				
				pending_sort_spinner.setText(""+pending_sort_name[which]);
				//sortByDate = which;
				if(which != pending_sort_name.length-1){
					filterByPeriod = which;
				}else{
					
					filterByPeriod = -1;
				}
				
				
				
				getPendingList();
				
				dialog.dismiss();
				
			}
		});
		
		
		pending_sort_Select.show();
		
	}
	
	protected void showPendingAccountAlert(){
		
		AlertDialog.Builder pending_acc_Select = new AlertDialog.Builder(this);
		pending_acc_Select.setSingleChoiceItems(pending_acc_name, -1, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				
				pending_acc_spinner.setText(""+pending_acc_name[which]);
				
				filterByPeriod = -1;
				
				if(which != pending_acc_name.length-1){
					SQLiteDatabase db = null;
					accId = util.getAccountId(dbManager, db,  pending_acc_name[which]);
				}else{
					accId = null;
				}
				dialog.dismiss();
				
				getPendingList();
				
			}
		});
		
		pending_acc_Select.show();
	
	}
	
	
	

}

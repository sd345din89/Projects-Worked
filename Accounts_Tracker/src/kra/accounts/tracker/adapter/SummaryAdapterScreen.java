package kra.accounts.tracker.adapter;


import java.util.ArrayList;
import java.util.Calendar;

import kra.accounts.tracker.AccountDetailsActivity;
import kra.accounts.tracker.R;
import kra.accounts.tracker.adapter.PendingAdapterScreen.ViewHolder;
import kra.accounts.tracker.database.KraFinanceDbManager;
import kra.accounts.tracker.models.CustomerAccountDetails;
import kra.accounts.tracker.models.Util;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SummaryAdapterScreen extends BaseAdapter{
	private Context mcontext;
	private LayoutInflater mInflater;
	private ArrayList<CustomerAccountDetails> accountDetailsList;
	
	private Util util;
	
	private KraFinanceDbManager dbManager;
	
	
	public SummaryAdapterScreen(Activity activity, ArrayList<CustomerAccountDetails> detailsList, Util util, KraFinanceDbManager dbManager) {
		this.mcontext=activity;
		mInflater = (LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		accountDetailsList = detailsList;
		
		this.util = util;
		
		this.dbManager = dbManager;
		
	}

	@Override
	public int getCount() {
		
		return accountDetailsList.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return null;
	}

	@Override
	public long getItemId(int position) {
		
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder=null;
		
		if(convertView==null){
			
			holder=new ViewHolder();
			convertView=mInflater.inflate(R.layout.pending_listadapter_screen, null);
			holder.pending_adapter_lyt=(RelativeLayout)convertView.findViewById(R.id.pending_adapter_lyt);
			
			holder.amountTxt = (TextView)convertView.findViewById(R.id.principal_amt_txt);
			holder.amountTxt.setTextColor(mcontext.getResources().getColor(R.color.summary_adapter_txt_color));
			
			holder.cusNameTxt = (TextView)convertView.findViewById(R.id.pending_adapter_name_txt);
			holder.cusNameTxt.setTextColor(mcontext.getResources().getColor(R.color.summary_adapter_txt_color));
			
			holder.dueDateTxt = (TextView)convertView.findViewById(R.id.duedate_txt);
			holder.dueDateTxt.setTextColor(mcontext.getResources().getColor(R.color.summary_adapter_txt_color));
			
			holder.pendingCountTxt = (TextView)convertView.findViewById(R.id.pending_count_txt);
			
			holder.accountCountTxt = (TextView)convertView.findViewById(R.id.pending_account_count_txt);
			
			
			//holder.summaryPendingCountLyt = (ImageView)convertView.findViewById(R.id.pending_count_imgView);
		
			/*if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN){
				holder.pendingCountTxt.setBackgroundDrawable(mcontext.getResources().getDrawable(R.drawable.summary_adapter_img));
			}else{
				holder.pendingCountTxt.setBackground(mcontext.getResources().getDrawable(R.drawable.summary_adapter_img));
			}*/
			
			convertView.setTag(holder);
			
			
		}else{
			holder=(ViewHolder) convertView.getTag();
		}
			
		
		holder.pending_adapter_lyt.setBackgroundResource(R.drawable.sumamry_row_img);
		if(position % 2 == 0){
			holder.pending_adapter_lyt.setBackgroundResource(R.color.white);
		}
		
		/*holder.pending_adapter_lyt.setBackgroundResource(R.drawable.pending_row_bg);
		if(position % 2 == 0){
			holder.pending_adapter_lyt.setBackgroundResource(R.color.white);
		}*/
		
		final CustomerAccountDetails  accountDetails = accountDetailsList.get(position);
		
		if(accountDetails != null){
			holder.amountTxt.setText(""+util.formatAmount(accountDetails.getprincipalAmount()));
			
			holder.cusNameTxt.setText(""+accountDetails.getCustomerName());
			
			holder.dueDateTxt.setText(""+util.convertLongToDate(accountDetails.getinsertDueFromDate(), "MMM-yyyy"));
			
			String accountCount = util.getAccountCount(dbManager, accountDetails.getcustomerId());
			if(accountCount != null){
				holder.accountCountTxt.setVisibility(View.VISIBLE);
				holder.accountCountTxt.setText(""+accountCount);
			}else{
				holder.accountCountTxt.setVisibility(View.INVISIBLE);
			}
			
			//int dueDateInMonth = util.countDueDateInMonth(""+util.getPendingAmountCountForCustomer(dbManager, accountDetails.getcustomerId(), true));
			int dueDateInMonth = util.getNumOfMonths(util.getPendingAmountCountForCustomer(dbManager, accountDetails.getcustomerId(), false), Calendar.getInstance().getTimeInMillis());

			
			holder.pendingCountTxt.setText(""+dueDateInMonth);
			
			holder.pending_adapter_lyt.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					
					Intent intent = new Intent(mcontext, AccountDetailsActivity.class);
					intent.putExtra("CustomerId", Integer.parseInt(accountDetails.getcustomerId()));
					intent.putExtra("IsFromSummary", true);
					intent.putExtra("SortOrder", 0);
					mcontext.startActivity(intent);
					
				}
			});
			
		}
		
		
		return convertView;
	}
	class ViewHolder{
		

		RelativeLayout pending_adapter_lyt;
		
		TextView cusNameTxt;
		TextView amountTxt;
		TextView dueDateTxt;
		TextView pendingCountTxt;
		
		TextView accountCountTxt;
		
		//ImageView summaryPendingCountLyt;
	
	}

}

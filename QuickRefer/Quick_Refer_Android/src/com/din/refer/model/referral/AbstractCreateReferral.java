package com.strobilanthes.forrestii.model.referral;

public class AbstractCreateReferral {
	private int status;
	private String error;
	private CreateReferralRes response;
	

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public CreateReferralRes getResponse() {
		return response;
	}

	public void setResponse(CreateReferralRes response) {
		this.response = response;
	}

	public class CreateReferralRes{
		int ReferralId;
		
		public int getReferralId() {
			return ReferralId;
		}
		public void setReferralId(int referralId) {
			ReferralId = referralId;
		}
		
	}

}

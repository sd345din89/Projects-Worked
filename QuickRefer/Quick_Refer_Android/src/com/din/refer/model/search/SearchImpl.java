package com.strobilanthes.forrestii.model.search;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.SearchFFVolleyTasks;

public class SearchImpl implements Search,VolleyTasksListener{
	
	private AbstractSearchForrestiiFriend abstractSearchForrestiiFriend;
	
	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private Activity context;
	private CustomProgressbar mCustomProgressbar;


	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}


	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}
	
	@Override
	public void searchForrestiiFrnd(String url,JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show(); 
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new SearchFFVolleyTasks("SearchForrestiiFriend", this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public AbstractSearchForrestiiFriend getForrestiiFrnd() {
		return abstractSearchForrestiiFriend;
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonobject) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		if(method_name.equals("SearchForrestiiFriend")){
			///AbstractCreateWallPost previousResult = abstractWallPost;
			AbstractSearchForrestiiFriend previousResult = abstractSearchForrestiiFriend;
			
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractSearchForrestiiFriend = gson.fromJson(jsonobject.toString(), AbstractSearchForrestiiFriend.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this,"SearchForrestiiFriend",
					previousResult, abstractSearchForrestiiFriend);
			notifyListeners(event);
			
		}
	}

	@Override
	public void handleError(String method_name, Exception e) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		PropertyChangeEvent event = new PropertyChangeEvent(this,method_name, null, null); 
		notifyListeners(event);
	}
}

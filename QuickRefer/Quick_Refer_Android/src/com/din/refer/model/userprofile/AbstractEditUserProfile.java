package com.strobilanthes.forrestii.model.userprofile;


public class AbstractEditUserProfile{

	private String error;
	private int status;
	private String response;
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}

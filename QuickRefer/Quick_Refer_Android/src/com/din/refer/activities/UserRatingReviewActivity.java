package com.strobilanthes.forrestii.activities;


import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import roboguice.inject.InjectView;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.custom.views.CustomWallPostListview;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.ffFriendsList.GetForrestiiFriendList;
import com.strobilanthes.forrestii.model.referral.AbstractReferredQuickCard;
import com.strobilanthes.forrestii.model.viewreviews.AbstractReviewProfile;
import com.strobilanthes.forrestii.util.Util;

public class UserRatingReviewActivity extends BaseActivity<DBAccess>{

	private ReviewListAdapter reviewListAdapter = null;
	private ArrayList<AbstractReviewProfile> mAbstractViewReviews =  null;
	private Typeface mFontLight = null,mFontDark = null;
	private String reviews_view = null;
	
	@InjectView(R.id.contacts_list)					private CustomWallPostListview T3contactsListview;
	@InjectView(R.id.contacts_txt_emptyview)		private TextView txt_emptyview;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.frg_t2contactslist);
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		getActionBar().setTitle("Reviews");
		
		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		reviews_view = getIntent().getStringExtra("reviews_view");
		Bundle getObject = getIntent().getExtras();
             
		if(reviews_view.equals("UserProfileReviews")){
			

			mAbstractViewReviews = (ArrayList<AbstractReviewProfile>) getObject.getSerializable("UserDetails");

		}else if(reviews_view.equals("ForrestiiProfileReviews")){
			

			mAbstractViewReviews = (ArrayList<AbstractReviewProfile>) getObject.getSerializable("ForrestiiFriendDetails");
		}else if(reviews_view.equals("QuickcardProfileReviews")){

			mAbstractViewReviews = (ArrayList<AbstractReviewProfile>) getObject.getSerializable("QuickcardDetails");

		}else if(reviews_view.equals("UserFriendsReviews")){
			//showUserFriendsReviews();
			

			mAbstractViewReviews = (ArrayList<AbstractReviewProfile>) getObject.getSerializable("UserFriendDetails");

		}
		if(mAbstractViewReviews!=null){
				reviewListAdapter = new ReviewListAdapter(this,mAbstractViewReviews);
				T3contactsListview.setAdapter(reviewListAdapter);
		}}
	
//	public void showProfileReviews(){
//		JSONObject json = new JSONObject();
//		try {
//			json.put("userProfile",new JSONObject().put("userId", getHelper().getUserId()));
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		viewReviewsModel.viewUserProfileReviews(Constant.VIEW_REFERRALS_REVIEW, json);
//	}
//	public void showForrestiiProfileReviews(){
//		JSONObject json = new JSONObject();
//		try {
//			json.put("forrestiiFriend",new JSONObject().put("FFID", abstractforrestiiFriendList.getFfid()));
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		viewReviewsModel.viewUserProfileReviews(Constant.VIEW_REFERRALS_REVIEW, json);
//	}
//	public void showQuickcardProfileReviews(){
//		JSONObject json = new JSONObject();
//		try {
//			json.put("quickCard",new JSONObject().put("QuickCardId", abstractReferredQuickCard.getQuickCardId()));
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		viewReviewsModel.viewUserProfileReviews(Constant.VIEW_REFERRALS_REVIEW, json);
//	}
//	public void showUserFriendsReviews(){
//		JSONObject json = new JSONObject();
//		try {
//			json.put("userProfile",new JSONObject().put("userId", getHelper().getUserId()));
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		viewReviewsModel.viewUserProfileReviews(Constant.VIEW_REFERRALS_REVIEW, json);
//	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onBackPressed() {
		finish();		
		super.onBackPressed();
	}
	@Override
	public void onResume(){
		super.onResume();
		//viewReviewsModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		//viewReviewsModel.removeChangeListener(this);
	}
	class ReviewListAdapter extends BaseAdapter{

		private LayoutInflater mLayoutInflater;
		private ImageLoadingListener animateFirstListener;
		ArrayList<AbstractReviewProfile> abstractUserProfile;
		

		
		public ReviewListAdapter(Context context, ArrayList<AbstractReviewProfile> abstractUserProfile) {
			this.abstractUserProfile = abstractUserProfile;
			mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.default_user)
					.showImageForEmptyUri(R.drawable.default_user)
					.showImageOnFail(R.drawable.default_user)
					.cacheInMemory(true).cacheOnDisc(true)
					.considerExifParams(true)
					.build();

			animateFirstListener = new AnimateFirstDisplayListener();
		}

		public void updateContactAL(ArrayList<AbstractReviewProfile> abstractUserProfile) {
			this.abstractUserProfile = abstractUserProfile;
			notifyDataSetChanged();
			
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder = null;
			if (v == null) {
				v = mLayoutInflater.inflate(R.layout.act_view_reviews_ratings, null);
				holder = new ViewHolder();
				holder.reviewListItemLytLinear = (LinearLayout)v.findViewById( R.id.review_list_item_lyt_linear );
				holder.reviewListItemTxtName = (TextView)v.findViewById( R.id.review_list_item_txt_name );
				holder.reviewListItemTxtReviews = (TextView)v.findViewById( R.id.review_list_item_txt_reviews );
				holder.reviewListRatingBar = (RatingBar)v.findViewById( R.id.review_list_ratingBar );
				holder.reviewListItemTxtTime = (TextView)v.findViewById( R.id.review_list_item_txt_time );
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			holder.reviewListItemTxtName.setTypeface(mFontLight);
			holder.reviewListItemTxtReviews.setTypeface(mFontLight);
			holder.reviewListItemTxtTime.setTypeface(mFontDark);
			holder.reviewListItemTxtName.setSelected(true);
//			if(abstractUserProfile.get(position).getQuickCard()!=null){
//				holder.reviewListItemTxtName.setText(abstractUserProfile.get(position).getRatingFromUserProfile().getFirstName());
//				holder.reviewListItemTxtReviews.setText(abstractUserProfile.get(position).getReview());
//				holder.reviewListRatingBar.setRating(abstractUserProfile.get(position).getRating());
//				holder.reviewListItemTxtTime.setText(Util.updatedOnTime(abstractUserProfile.get(position).getUpdatedOn()));
//			}else if(abstractUserProfile.get(position).getForrestiiFriend()!=null){
//				holder.reviewListItemTxtName.setText(abstractUserProfile.get(position).getRatingFromUserProfile().getFirstName());
//				holder.reviewListItemTxtReviews.setText(abstractUserProfile.get(position).getReview());
//				holder.reviewListRatingBar.setRating(abstractUserProfile.get(position).getRating());
//				holder.reviewListItemTxtTime.setText(Util.updatedOnTime(abstractUserProfile.get(position).getUpdatedOn()));
//			}else{
			holder.reviewListItemTxtName.setText(abstractUserProfile.get(position).getRatingFromUserProfile().getFirstName());
			holder.reviewListItemTxtReviews.setText(abstractUserProfile.get(position).getReview());
			holder.reviewListRatingBar.setRating(abstractUserProfile.get(position).getRating());
			holder.reviewListItemTxtTime.setText(Util.updatedOnTime(abstractUserProfile.get(position).getUpdatedOn()));
			//}
			return v;
		}
		
		class ViewHolder {
			LinearLayout reviewListItemLytLinear;
			TextView reviewListItemTxtName,reviewListItemTxtReviews,reviewListItemTxtTime;
			RatingBar reviewListRatingBar;
			
		}
		@Override
		public int getCount() {
			return abstractUserProfile.size();
		}

		@Override
		public AbstractReviewProfile getItem(int position) {
			return abstractUserProfile.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}
	}
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

	@Override
	public void onLoadingComplete(String imageUri, View view,
			Bitmap loadedImage) {
		if (loadedImage != null) {
			ImageView imageView = (ImageView) view;
			boolean firstDisplay = !displayedImages.contains(imageUri);
			if (firstDisplay) {
				FadeInBitmapDisplayer.animate(imageView, 500);
				displayedImages.add(imageUri);
			}
		}
	}
}

//	@Override
//	public void propertyChange(PropertyChangeEvent event) {
//		// TODO Auto-generated method stub
//		if (event.getPropertyName().equals("UserProfileReviews")){
//			AbstractViewReviews abstractReviewList = viewReviewsModel.getReviewsRatings();
//			if (abstractReviewList != null) {
//				if (abstractReviewList.getStatus() == 1) {
//					
//					mAbstractViewReviews = abstractReviewList.getResponse();
//					if(mAbstractViewReviews!=null){
//					reviewListAdapter = new ReviewListAdapter(this,mAbstractViewReviews);
//					T3contactsListview.setAdapter(reviewListAdapter);
//					}
//				} else {
//					Util.showAlert(this, "", abstractReviewList.getError());
//				}
//			}else {
//				Util.showAlert(this, "Message", "Request Failed");
//				//finish();
//			}
//		}else if (event.getPropertyName().equals("QuickcardProfileReviews")){
//			AbstractViewReviews abstractReviewList = viewReviewsModel.getReviewsRatings();
//			if (abstractReviewList != null) {
//				if (abstractReviewList.getStatus() == 1) {
//					
//					mAbstractViewReviews = abstractReviewList.getResponse();
//					if(mAbstractViewReviews!=null){
//					reviewListAdapter = new ReviewListAdapter(this,mAbstractViewReviews);
//					T3contactsListview.setAdapter(reviewListAdapter);
//					}
//				} else {
//					Util.showAlert(this, "", abstractReviewList.getError());
//				}
//			}else {
//				Util.showAlert(this, "Message", "Request Failed");
//				//finish();
//			}
//		}else if (event.getPropertyName().equals("ForrestiiProfileReviews")){
//			AbstractViewReviews abstractReviewList = viewReviewsModel.getReviewsRatings();
//			if (abstractReviewList != null) {
//				if (abstractReviewList.getStatus() == 1) {
//					
//					mAbstractViewReviews = abstractReviewList.getResponse();
//					if(mAbstractViewReviews!=null){
//					reviewListAdapter = new ReviewListAdapter(this,mAbstractViewReviews);
//					T3contactsListview.setAdapter(reviewListAdapter);
//					}
//				} else {
//					Util.showAlert(this, "", abstractReviewList.getError());
//				}
//			}else {
//				Util.showAlert(this, "Message", "Request Failed");
//				//finish();
//			}
//		}
//	}
}

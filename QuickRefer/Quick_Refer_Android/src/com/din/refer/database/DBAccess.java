package com.strobilanthes.forrestii.database;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.strobilanthes.forrestii.model.contacts.ContactList;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.general.AbstractCountry;
import com.strobilanthes.forrestii.model.general.AbstractDashboard;
import com.strobilanthes.forrestii.model.general.AbstractLocation;
import com.strobilanthes.forrestii.model.general.AbstractServerInfo;
import com.strobilanthes.forrestii.model.general.AbstractTemplate;
import com.strobilanthes.forrestii.model.notification.NotificaitonList;
import com.strobilanthes.forrestii.model.settings.SettingsDetails;
import com.strobilanthes.forrestii.model.skillset.AbstractSkillSet;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.GetWallPost;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;

public class DBAccess extends SQLiteOpenHelper {

	private String TAG="Db-Access";
	private static final int DATABASE_VERSION = 1;
	private static String DATABASE_NAME="Forresstii";
   
	public DBAccess(Context context){
		 super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
    
	
	/*public DBAccess(Context LoginActivity, String name, CursorFactory factory,
			int version) {
		super(LoginActivity, name, factory, 1);
		// TODO Auto-generated constructor stub
	}*/
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		String  CREATE_TABLE_QUERY = null;
		
		// Tracking Screens
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS Tracking(Pageid INT);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("CountryList Table created");
		
		
		//Used
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS userMaster(userid INT(100),MobileNum CHAR(100),Password CHAR(100));";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("userMaster Table created");
		
		//UserProfile
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS UserProfile(UserProfileList BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("UserProfile Table created");
				
		//Wall Post
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS WallPost(GetWallPost BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("WallPost Table created");
			
		//Skill Set
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS SkillSet(Skills BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("Skill set Table created");
		
		//NotifyType
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS NotifyType(Notification BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("Settings Table created");
		
		//Settings
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS Settings(SettingsDetails BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("Settings Table created");
				
		//FriendsList
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS FriendsList(T2Friends BLOB,T3Friends BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("FriendsList Table created");

		CREATE_TABLE_QUERY="CREATE TABLE IF NOT EXISTS contactList (contacts BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("contactList Table created");
		
		CREATE_TABLE_QUERY="CREATE TABLE IF NOT EXISTS DashBoard (dashboard BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("Dashboard Table created");
		
		CREATE_TABLE_QUERY="CREATE TABLE IF NOT EXISTS Templates (templates BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("Templates Table created");
		
		CREATE_TABLE_QUERY="CREATE TABLE IF NOT EXISTS Serverinfo (serverInfo BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("ServerInfo Table created");
		
		CREATE_TABLE_QUERY="CREATE TABLE IF NOT EXISTS Notification (notification BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("Notification Table created");
		
		// Country
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS CountryList(Country BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("CountryList Table created");
		
		// Location
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS Location(Location BLOB);";
		db.execSQL(CREATE_TABLE_QUERY);
		L.i("Location Table created");
	}
	/*public void insertDetails(String userid,String username,String password){
		SQLiteDatabase dbase = this.getReadableDatabase();
		ContentValues cv = new ContentValues();
		cv.put("UserId", userid);
		cv.put("UserName",username);
		cv.put("Password", password);
		dbase.insert("UserDetails", null, cv);
		dbase.close();	
	}*/

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

	public void insertPageId(int pageId){
		
	    SQLiteDatabase SqlDB = getWritableDatabase();
	    
	    SqlDB.delete("Tracking", null, null);
	
		String INSERT="insert into Tracking (Pageid) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindLong(1,pageId);
		insertstatment.executeInsert();
		SqlDB.close();
		close();
	
	}

	public int getPageId(){
		
		int userId = 0 ;
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query("Tracking", new String[]{"Pageid"}, null, null, null, null, null);
		
		if(cursor!=null && cursor.moveToFirst()){
			if(cursor.getCount() > 0)
				userId=cursor.getInt(0);
		}
		cursor.close();
		db.close();
		close();
		
		return userId;
	
	}
	
	
	//========================== User Master Table ==============================================
	
	public void insertUserMaster(int userId,String username,String password){
		
	    SQLiteDatabase SqlDB = getWritableDatabase();
	    
	    SqlDB.delete("userMaster", null, null);
	
		String INSERT="insert into userMaster (userid,MobileNum,Password) values (?,?,?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindLong(1,userId);
		insertstatment.bindString(2,username);
		insertstatment.bindString(3,password);
		insertstatment.executeInsert();
		SqlDB.close();
		close();
	
	}
	
	public void updateUserId(int newUserId){
		
		SQLiteDatabase db = getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("userid", newUserId);
		db.update("userMaster", contentValues, "userid=?", new String[]{"1234"});
		//db.execSQL("UPDATE userMaster WHERE SET Password");
		db.close();
		close();
		
	}

	
	public void deleteUserMaster(){
		
	}
	
	public int getUserMasterCount(){
		int count=0;
		SQLiteDatabase db=getReadableDatabase();
		Cursor cursor=db.query("userMaster", null, null, null, null, null, null);
		
		count = cursor.getCount(); 
		
		cursor.close();
		db.close();
		close();
		
		return count;
	}
	
	
	public String getPwd(){
		String userType="";
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor=db.query("userMaster", new String[]{"Password"}, null, null, null, null, null);
		
		if(cursor!=null && cursor.moveToFirst()){
			if(cursor.getCount() > 0)
				userType=cursor.getString(0);
		}
		cursor.close();
		db.close();
		close();
		
		return userType;
	}
	public void updatePwd(String newPwd, String previouspwd){
		
		SQLiteDatabase db = getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("Password", newPwd);
		db.update("userMaster", contentValues, "Password=?", new String[]{previouspwd});
		//db.execSQL("UPDATE userMaster WHERE SET Password");
		db.close();
		close();
		
	}
	
	public String getMobileNo(){
		String userType="";
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor=db.query("userMaster", new String[]{"MobileNum"}, null, null, null, null, null);
		
		if(cursor!=null && cursor.moveToFirst()){
			if(cursor.getCount() > 0)
				userType=cursor.getString(0);
		}
		cursor.close();
		db.close();
		close();
		
		return userType;
	}

	public int getUserId(){
	
		int userId= 0;
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query("userMaster", new String[]{"userid"}, null, null, null, null, null);
		
		if(cursor!=null && cursor.moveToFirst()){
			if(cursor.getCount() > 0)
				userId = cursor.getInt(0);
		}
		cursor.close();
		db.close();
		close();
		
		return userId;
	
	}
	//=========================================================================================
	
	// ========================== Location Table ==============================================

	public void insertLocation(ArrayList<AbstractLocation> abstractLocation) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("Location", null, null);

		String INSERT = "insert into Location (Location) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(abstractLocation));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<AbstractLocation> getDBLocation() {

		ArrayList<AbstractLocation> gAbstractLocation = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("Location", new String[] { "Location" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				gAbstractLocation = (ArrayList<AbstractLocation>) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return gAbstractLocation;

	}
			
	// ========================== DashBoard Table ==============================================

	public void insertDashBoard(AbstractDashboard abstractDashboard) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("DashBoard", null, null);

		String INSERT = "insert into DashBoard (dashboard) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(abstractDashboard));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}

	
	public AbstractDashboard getDashBoard() {

		AbstractDashboard abstractDashboard = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("DashBoard", new String[] { "dashboard" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				abstractDashboard = (AbstractDashboard) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return abstractDashboard;

	}

	// ========================== Templates Table ==============================================

	public void insertTemplate(ArrayList<AbstractTemplate>getTemplateList) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("Templates", null, null);

		String INSERT = "insert into Templates (templates) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(getTemplateList));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}

	@SuppressWarnings("unchecked")
	public ArrayList<AbstractTemplate> getTemplateList() {

		ArrayList<AbstractTemplate> getTemplateList = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("Templates", new String[] { "templates" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				getTemplateList = (ArrayList<AbstractTemplate>) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return getTemplateList;

	}
	
	// ========================== ServerInfo Table ==============================================

	public void insertServerInfo(AbstractServerInfo abstractServerInfo) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("Serverinfo", null, null);

		String INSERT = "insert into Serverinfo (serverInfo) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(abstractServerInfo));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}

	public AbstractServerInfo getServerInfo() {

		AbstractServerInfo abstractServerInfos = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("Serverinfo", new String[] { "serverInfo" },
				null, null, null, null, null);
		if (cursor != null && cursor.moveToFirst()) {
			if (cursor.getCount() > 0)
				abstractServerInfos = (AbstractServerInfo) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return abstractServerInfos;
	}
		
	// ========================== WallPost Table ==============================================
	
	/*public void insertGetWallPost(ArrayList<GetWallPost> getWallPostList) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("WallPost", null, null);

		String INSERT = "insert into WallPost (GetWallPost) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(getWallPostList));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}

	@SuppressWarnings("unchecked")
	public ArrayList<GetWallPost> getDBWallPost() {

		ArrayList<GetWallPost> getWallPost = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("WallPost", new String[] { "GetWallPost" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				getWallPost = (ArrayList<GetWallPost>) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return getWallPost;

	}*/

	// ===========================================================================================
	
	// ========================== UserProfile Table ==============================================

	public void insertUserProfile(AbstractUserProfile abstractUserProfile) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("UserProfile", null, null);

		String INSERT = "insert into UserProfile (UserProfileList) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(abstractUserProfile));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}

	
	public AbstractUserProfile getDBUserProfile() {

		AbstractUserProfile abstractUserProfile = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("UserProfile", new String[] { "UserProfileList" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				abstractUserProfile = (AbstractUserProfile) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return abstractUserProfile;

	}

	// ==========================================================================================
	// ========================== Skills Set Table ==============================================

	public void insertSkillset(ArrayList<AbstractSkillSet> abstractSkillSets) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("SkillSet", null, null);

		String INSERT = "insert into SkillSet (Skills) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(abstractSkillSets));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}

	@SuppressWarnings("unchecked")
	public ArrayList<AbstractSkillSet> getDBSkillset() {

		ArrayList<AbstractSkillSet> gAbstractSkillSets = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("SkillSet", new String[] { "Skills" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				gAbstractSkillSets = (ArrayList<AbstractSkillSet>) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return gAbstractSkillSets;

	}

	// ========================================================================================
	// ========================== Settings Table ==============================================

	/*public void insertNotifyType(ArrayList<AbstractNotificationSettings> abstractNotificationSettings) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("NotifyType", null, null);

		String INSERT = "insert into NotifyType (Notification) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(abstractNotificationSettings));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}*/

	public void insertSettingsDetails(SettingsDetails settingsDetails) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("Settings", null, null);

		String INSERT = "insert into Settings (SettingsDetails) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(settingsDetails));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}
	
	@SuppressWarnings("unchecked")
	public SettingsDetails getDBSettingsDetails() {

		SettingsDetails geAbstractSettings = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("Settings", new String[] { "SettingsDetails" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				geAbstractSettings = (SettingsDetails) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return geAbstractSettings;

	}

	/*@SuppressWarnings("unchecked")
	public ArrayList<AbstractNotificationSettings> getDBNotificationSettings() {

		ArrayList<AbstractNotificationSettings> geAbstractSecutiryQASettings = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("NotifyType", new String[] { "Notification" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				geAbstractSecutiryQASettings = (ArrayList<AbstractNotificationSettings>) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return geAbstractSecutiryQASettings;

	}*/
	// ==================================================================================
	
	// ========================== FriendsList Table ==============================================

	public void insertFriendsList(ArrayList<FriendsList> abstractFriendsT2Lists ,ArrayList<FriendsList> abstractFriendsT3Lists) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("FriendsList", null, null);

		String INSERT = "insert into FriendsList (T2Friends,T3Friends) values (?,?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(abstractFriendsT2Lists));
		insertstatment.bindBlob(2, convertIntoBLOB(abstractFriendsT3Lists));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}

	/*public void updateT3FriendsList(ArrayList<FriendsList> abstractFriendsT3Lists){
		
		SQLiteDatabase db = getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("T3Friends", convertIntoBLOB(abstractFriendsT3Lists));
		db.update("FriendsList", contentValues, null, null);
		//db.execSQL("UPDATE userMaster WHERE SET Password");
		db.close();
		close();
		
	}*/
	
	/*public void updateT2FriendsList(ArrayList<FriendsList> abstractFriendsT2Lists){
		
		SQLiteDatabase db = getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("T2Friends", convertIntoBLOB(abstractFriendsT2Lists));
		db.update("FriendsList", contentValues, null, null);
		//db.execSQL("UPDATE userMaster WHERE SET Password");
		db.close();
		close();
		
	}*/

	@SuppressWarnings("unchecked")
	public ArrayList<FriendsList> getDBT2Friends() {

		ArrayList<FriendsList> geAbstractFriendsLists = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("FriendsList", new String[] { "T2Friends" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				geAbstractFriendsLists = (ArrayList<FriendsList>) getModelsObject(cursor.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return geAbstractFriendsLists;
	}

	/*@SuppressWarnings("unchecked")
	public ArrayList<FriendsList> getDBT3Friends() {

		ArrayList<FriendsList> geAbstractFriendsLists = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("FriendsList", new String[] { "T3Friends" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				geAbstractFriendsLists = (ArrayList<FriendsList>) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return geAbstractFriendsLists;

	}*/
	// ==================================================================================
	
	 public void insertCountryList(ArrayList<AbstractCountry> abstractCountryList) {

		SQLiteDatabase SqlDB = getWritableDatabase();
		SqlDB.delete("CountryList", null, null);
		
		String INSERT = "insert into CountryList (Country) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(abstractCountryList));
		insertstatment.executeInsert();
		SqlDB.close();
		close();
	}
	 
	@SuppressWarnings("unchecked")
	public ArrayList<AbstractCountry> getDBCountryList() {

		ArrayList<AbstractCountry> gAbstractCountryList = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("CountryList", new String[] { "Country" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				gAbstractCountryList = (ArrayList<AbstractCountry>) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return gAbstractCountryList;

	}
	
	/*public void insertNotificationList(ArrayList<NotificaitonList>getNotificationList) {

		SQLiteDatabase SqlDB = getWritableDatabase();

		SqlDB.delete("Notification", null, null);

		String INSERT = "insert into Notification (notification) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1, convertIntoBLOB(getNotificationList));
		insertstatment.executeInsert();
		SqlDB.close();
		close();

	}

	@SuppressWarnings("unchecked")
	public ArrayList<NotificaitonList> getNotificationList() {

		ArrayList<NotificaitonList> getNotificationList = null;
		SQLiteDatabase db = getReadableDatabase();
		
		Cursor cursor = db.query("Notification", new String[] { "notification" },
				null, null, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			if (cursor.getCount() > 0)
				getNotificationList = (ArrayList<NotificaitonList>) getModelsObject(cursor
						.getBlob(0));
		}
		cursor.close();
		db.close();
		close();

		return getNotificationList;

	}*/
	
	//========================== ContactList Table ==============================================
	
	public void insertContactList(LinkedHashMap<String, ContactList> treeMap){
		
	    SQLiteDatabase SqlDB = getWritableDatabase();
	    
	    SqlDB.delete("contactList", null, null);
		
	
		String INSERT="insert into contactList (contacts) values (?)";
		SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
		insertstatment.bindBlob(1,convertIntoBLOB(treeMap));
		insertstatment.executeInsert();
		SqlDB.close();
		close();
	
	}
	
	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String, ContactList> getContactList(){
		
		LinkedHashMap<String, ContactList> treemap =  null;
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query("contactList", new String[]{"contacts"}, null, null, null, null, null);
		
		if(cursor!=null && cursor.moveToFirst()){
			if(cursor.getCount() > 0)
				treemap = (LinkedHashMap<String, ContactList>) getModelsObject(cursor.getBlob(0));
		}
		cursor.close();
		db.close();
		close();
		
		return treemap;

	}

	// ==============================================================================================

	//========================== Logout Options =========================================

	public void logout(){
		
		Constant.mGetWallPostArrayList = new ArrayList<GetWallPost>();
		Constant.mAllNotificationArrayList =  new ArrayList<NotificaitonList>();
		
	    SQLiteDatabase SqlDB = getWritableDatabase();
	    
	    SqlDB.delete("userMaster", null, null);
	    SqlDB.delete("UserProfile", null, null);
	    SqlDB.delete("WallPost", null, null);
	    SqlDB.delete("SkillSet", null, null);
	    SqlDB.delete("NotifyType", null, null);
	    SqlDB.delete("Settings", null, null);
	    SqlDB.delete("FriendsList", null, null);
	    SqlDB.delete("contactList", null, null);
	    SqlDB.delete("DashBoard", null, null);
	    SqlDB.delete("Templates", null, null);
	    SqlDB.delete("Serverinfo", null, null); SqlDB.delete("Tracking", null, null);
	    SqlDB.delete("Notification", null, null);
	    SqlDB.delete("CountryList", null, null);
	    SqlDB.delete("Location", null, null);
	    SqlDB.delete("Tracking", null, null);
	
		SqlDB.close();
		
		close();
	
	}

	//===================================================================================
	// Common Methods
	
	public Object getModelsObject(byte[] bs) {
		
		try {
			ByteArrayInputStream bai=new ByteArrayInputStream(bs);

			ObjectInputStream ois = new ObjectInputStream(bai);
			Object details = ois.readObject();
			
			return details;
			
		} catch (StreamCorruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (NullPointerException e){
			e.printStackTrace();
		}
		return null;
		
	}
	
	public byte[] convertIntoBLOB(Object details){
		
		ByteArrayOutputStream bos=new  ByteArrayOutputStream();
		try {
			ObjectOutputStream ops = new ObjectOutputStream(bos);
			ops.writeObject(details);
			ops.close();
			bos.close();
			
			return bos.toByteArray();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}

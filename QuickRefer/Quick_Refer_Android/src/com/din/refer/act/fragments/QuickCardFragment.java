package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.friendslist.FriendsList;
import com.strobilanthes.forrestii.model.quickcard.AbstractQuickcard;
import com.strobilanthes.forrestii.model.quickcard.Quickcard;
import com.strobilanthes.forrestii.model.referral.AbstractCreateReferral;
import com.strobilanthes.forrestii.model.referral.AbstractReferredQuickCard;
import com.strobilanthes.forrestii.model.referral.Referral;
import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;
import com.strobilanthes.forrestii.model.wallpost.RefferalPost;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;


public class QuickCardFragment extends BaseFragment<DBAccess> implements OnClickListener{

	
	@InjectView(R.id.quickcard_first_name_edt)   private EditText quickcardFirstNameEdt;
	@InjectView(R.id.quickcard_last_name_edt)   private EditText quickcardLastNameEdt;

	@InjectView(R.id.quickcard_phone_edt)  private AutoCompleteTextView quickcardPhoneEdt;
	@InjectView(R.id.quickcard_email_edt)  private AutoCompleteTextView quickcardEmailEdt;
	@InjectView(R.id.quickcard_done_btn)   private Button askProfileDoneBtn;
	@InjectView(R.id.quickcard_txt_header)   private TextView quickcardTxtHeader;

	@Inject Quickcard  quickCardModel;
	@Inject Referral   referralModel;
	
	private AbstractUserProfile abstractUserProfile;
	private ArrayList<FriendsList>abstractFriendList;
	private ArrayList<String>friendList = new ArrayList<String>();
	
	private int postId;
	private int postByUserId;
	private String postedMobile;
	private int mWallPostPosition;
	private RefferalPost refferalPost = null;
	private Typeface mFontLight,mFontDark;
	private boolean isQuickCardRef = false;

	public static QuickCardFragment newInstance(int postId, int postByUserId,String mobile, int position) {
		QuickCardFragment frag = new QuickCardFragment();
         Bundle args = new Bundle();
         args.putInt("postId", postId);
         args.putInt("position", position);
         args.putInt("postByUserId", postByUserId);
         args.putSerializable("mobile", mobile);
         frag.setArguments(args);
         return frag;
    }
	 
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.frg_quickcard, null);

		
		super.initilizeContents();
		

		return view;
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		fontStyles();
		
		quickCardModel.setContext(getActivity());
		referralModel.setContext(getActivity());
		
		askProfileDoneBtn.setOnClickListener(this);
		
		Util.hideEditTextError(quickcardEmailEdt);
		Util.hideEditTextError(quickcardFirstNameEdt);
		Util.hideEditTextError(quickcardLastNameEdt);
		Util.hideEditTextError(quickcardPhoneEdt);
		quickcardFirstNameEdt.setFilters(Util.restrictSpaceInEdit(quickcardFirstNameEdt));
		quickcardLastNameEdt.setFilters(Util.restrictSpaceInEdit(quickcardLastNameEdt));

		postId = getArguments().getInt("postId",0);;
		postByUserId = getArguments().getInt("postByUserId");
		postedMobile = getArguments().getString("mobile");
		mWallPostPosition = getArguments().getInt("position");
		
		abstractUserProfile = getHelper().getDBUserProfile();
		abstractFriendList = getHelper().getDBT2Friends();
		if(abstractFriendList!=null){
			for(FriendsList friends : abstractFriendList){
				friendList.add(friends.getFriendUserProfile().getMobile());
			}
		}
	}
	
	public void fontStyles() {
		
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		quickcardFirstNameEdt.setTypeface(mFontDark);
		quickcardLastNameEdt.setTypeface(mFontDark);
		quickcardPhoneEdt.setTypeface(mFontDark);
		quickcardEmailEdt.setTypeface(mFontDark);
		askProfileDoneBtn.setTypeface(mFontDark);
		quickcardTxtHeader.setTypeface(mFontLight);

	}
 
	@Override
	public void onResume(){
		super.onResume();
		quickCardModel.addChangeListener(this);
		referralModel.addChangeListener(this);
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		Util.hideSoftKeyboard(getActivity(),quickcardFirstNameEdt);
		referralModel.removeChangeListener(this);
		quickCardModel.removeChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		
		if (event.getPropertyName().equals("createquickcard")) {
			AbstractQuickcard abstractQuickCard = quickCardModel.getQuickcardDetails();
			if(abstractQuickCard != null){
				if(abstractQuickCard.getStatus() == 1){
					Intent intent = new Intent();
					Bundle bundle = new Bundle(); 
					if(refferalPost != null)
						bundle.putSerializable("refferalPost", refferalPost);
					bundle.putInt("pos", mWallPostPosition);
					intent.putExtras(bundle);
					getActivity().setResult(88, intent);
					getActivity().finish();
				}else{
					Util.showAlert(getActivity(), "Message",abstractQuickCard.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}
		}else if (event.getPropertyName().equals("createFriendReferral")) {
			AbstractCreateReferral abstractCreateReferral = referralModel.abstractCreateReferral();
			if(abstractCreateReferral != null){
				if(abstractCreateReferral.getStatus() == 1){
					if(isQuickCardRef){
						isQuickCardRef = false;
						Intent intent = new Intent();
						Bundle bundle = new Bundle(); 
						if(refferalPost != null)
						bundle.putSerializable("refferalPost", refferalPost);
						bundle.putInt("pos", mWallPostPosition);
						intent.putExtras(bundle);
						getActivity().setResult(88, intent);
						getActivity().finish();
					}
					
				}else{
					Util.showAlert(getActivity(), "Message",abstractCreateReferral.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed");
			}
		}
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		case R.id.quickcard_done_btn:
			
			final String quickFirstName, quickmobileNo,quickEmailId,quickLastName;
			quickLastName = quickcardLastNameEdt.getText().toString();
			quickFirstName = quickcardFirstNameEdt.getText().toString();
			quickmobileNo = quickcardPhoneEdt.getText().toString().trim();
			quickEmailId =  quickcardEmailEdt.getText().toString().trim();
			//dialog.dismiss();
			String[] i = quickEmailId.split("@");
			String beforeText = i[0];
			L.d("QuickcardEmail++"+beforeText.length());
			if(quickFirstName.length()<=0 && quickmobileNo.length()<=0 && quickEmailId.length()<=0){
				quickcardFirstNameEdt.requestFocus();
				quickcardFirstNameEdt.setError("Please complete all the fields");
				//quickcardPhoneEdt.setError("Please complete all the fields");
				//quickcardEmailEdt.setError("Please complete all the fields");
				
			} else if(quickFirstName.trim().length() <= 0){
				quickcardFirstNameEdt.requestFocus();
				quickcardFirstNameEdt.setError("The first name should not be empty");
			} else if(quickFirstName.trim().length() < 3){
				quickcardFirstNameEdt.requestFocus();
				quickcardFirstNameEdt.setError("The first name must be atleast 3 characters");
        	} else if(quickLastName.trim().length() <= 0){
				quickcardLastNameEdt.requestFocus();
				quickcardLastNameEdt.setError("The last name should not be empty");
			} else if(quickLastName.trim().length() < 1){
				quickcardLastNameEdt.requestFocus();
				quickcardLastNameEdt.setError("The last name must be atleast 1 character");
        	}else if(!Util.isValidUserName(quickFirstName)){
        		quickcardFirstNameEdt.requestFocus();
        		quickcardFirstNameEdt.setError("The first name should not contain any special characters or space");
			} else if(!Util.isValidUserName(quickLastName)){
        		quickcardLastNameEdt.requestFocus();
        		quickcardLastNameEdt.setError("The last name should not contain any special characters or space");
			} else if(!Util.isValidPhoneNumber(quickmobileNo) || !PhoneNumberUtils.isGlobalPhoneNumber(quickmobileNo) || quickmobileNo.length() < 10){
				quickcardPhoneEdt.requestFocus();
				quickcardPhoneEdt.setError("Please enter a valid mobile number");
        	} else if(!Util.isValidEmail(quickEmailId) || (beforeText.length() > 64)){
        		quickcardEmailEdt.requestFocus();
        		quickcardEmailEdt.setError("Please enter valid email");
        	}  else if(quickmobileNo.contains(abstractUserProfile.getMobile().toString())){
        		L.ToastMessage(getActivity(), "That's your number! Try with another number");
        	} else if(quickmobileNo.equals(postedMobile)){
        		L.ToastMessage(getActivity(), "That's their number! Try with another number");
        	}else if(friendList.contains(quickmobileNo)){
        		new AlertDialog.Builder(getActivity() , AlertDialog.THEME_HOLO_LIGHT).setMessage("This person is in your friends list, do you want to proceed?")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						
						isQuickCardRef = true;
						JSONObject jobject = new JSONObject();
						
						refferalPost = new RefferalPost();
						refferalPost.setPick(false);
						refferalPost.setRating(null);
						refferalPost.setReferralFromUserProfile(getHelper().getDBUserProfile());
						refferalPost.setReferralId(0);
						refferalPost.setReferredQuickCard(null);
						
						try {
							if(abstractFriendList!=null){
								for(FriendsList friends : abstractFriendList){
									if(quickmobileNo.equals(friends.getFriendUserProfile().getMobile().toString().trim())){
										
										refferalPost.setReferredUserProfile(friends.getFriendUserProfile());
										jobject.put("referredUserProfile", new JSONObject().put("userId",friends.getFriendUserProfile().getUserId()));
										
										break;
									}
								}
							}
						jobject.put("referralFromUserProfile", new JSONObject().put("userId", getHelper().getUserId()));
						
						jobject.put("posts", new JSONObject().put("postId", postId).put("postByUserProfile", new JSONObject().put("userId", postByUserId)));
						
						referralModel.createFriendReferral(Constant.CREATE_FRIEND_REFERRAL, jobject);
							
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}).setNeutralButton("Cancel",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
        		//L.ToastMessage(getActivity(), "The user already exists in your friendlist");
        		
        	} else{
        	
        		JSONObject jobject = new JSONObject();
				JSONObject referredQuickCardObj = new JSONObject();
				JSONObject postObject =  new JSONObject();
				
				refferalPost = new RefferalPost();
				refferalPost.setPick(false);
				refferalPost.setRating(null);
				refferalPost.setReferralFromUserProfile(getHelper().getDBUserProfile());
				refferalPost.setReferralId(0);
				
				AbstractReferredQuickCard referredQuickCard = new AbstractReferredQuickCard();
				referredQuickCard.setFirstName(""+quickFirstName);
				referredQuickCard.setLastName(""+quickLastName);
				referredQuickCard.setMobileNumber(""+quickmobileNo);
				referredQuickCard.setEmailAddress(""+quickEmailId);
				referredQuickCard.setQuickCardId(0);
				refferalPost.setReferredQuickCard(referredQuickCard);
				
				refferalPost.setReferredUserProfile(null);
				
				try {
					
					jobject.put("referralFromUserProfile", new JSONObject().put("userId", getHelper().getUserId()));
					referredQuickCardObj.put("firstName", quickFirstName);
					referredQuickCardObj.put("lastName", quickLastName);
					referredQuickCardObj.put("mobileNumber", quickmobileNo);
					referredQuickCardObj.put("emailAddress", quickEmailId);
					jobject.put("referredQuickCard", referredQuickCardObj);
					postObject.put("postId", postId);
					postObject.put("postByUserProfile", new JSONObject().put("userId", postByUserId));
					jobject.put("posts", postObject);
					
					quickCardModel.createQuickCard(Constant.CREATE_REFERRAL_QUICKCARD, jobject);
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
        	}
	        
			break;
		case R.id.login_screen_btnLogin:
			
			
			break;
		default:
			break;
		}
	}
}

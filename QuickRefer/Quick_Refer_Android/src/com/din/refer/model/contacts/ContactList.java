package com.strobilanthes.forrestii.model.contacts;

import java.io.Serializable;

import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

public class ContactList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String personName;
	private String phoneNum;
	private String personImage;
	private String personEmailId;
	private String contactId;
	private int contactType = 0;
	private AbstractUserProfile abstractUserProfile;
	private boolean isSection = false;
	public ContactList(){
	}
	
	public String getContactId(){
		return contactId;
	}
	public void setContactId(String contactId){
		this.contactId = contactId;
	}
	
	public String getPersonName(){
		return personName;
	}
	public void setPersonName(String personName){
		this.personName=personName;
	}
	
	public String getPhoneNum(){
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum){
		this.phoneNum=phoneNum;
	}
	
	public String getPersonImage(){
		return personImage;
	}
	public void setPersonImage(String personImage){
		this.personImage=personImage;
	}
	
	public String getPersonEmailId(){
		return personEmailId;
	}
	public void setPersonEmailId(String personEmailId){
		this.personEmailId = personEmailId;
	}

	public AbstractUserProfile getAbstractUserProfile() {
		return abstractUserProfile;
	}

	public void setAbstractUserProfile(AbstractUserProfile abstractUserProfile) {
		this.abstractUserProfile = abstractUserProfile;
	}

	public boolean isSection() {
		return isSection;
	}

	public void setSection(boolean isSection) {
		this.isSection = isSection;
	}

	public int getContactType() {
		return contactType;
	}

	public void setContactType(int contactType) {
		this.contactType = contactType;
	}

	
}

package com.strobilanthes.forrestii.model.userprofile;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;
import com.strobilanthes.forrestii.model.viewreviews.AbstractViewReviews;


public interface UserProfile extends ForrestiiSubject {
	
	void editUserProfile(String url, JSONObject postdata);
	void updateUserProfile(String url, JSONObject postdata);
	void requestUserProfile(String url, JSONObject postdata);
	AbstractEditUserProfile getUserEditedProfile();
	AbstractUpdateUserProfile getUserUpdatedProfile();
	
	void viewUserProfileReviews(String url, JSONObject postdata);
	 void viewQuickCardUsersReviews(String url, JSONObject postdata);

	 void viewForrestiiFriendUsersReviews(String url, JSONObject postdata);

	 void viewUserFriendsReviews(String url, JSONObject postdata);

	 AbstractViewReviews getReviewsRatings();
}

package com.strobilanthes.forrestii.volleytasks;

import org.json.JSONObject;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.strobilanthes.forrestii.model.signup.SignupImpl;
import com.strobilanthes.forrestii.util.L;

public class SignupVolleyTasks extends JsonObjectRequest{

	//private static LoginImpl listener;
	
	public SignupVolleyTasks(String url, JSONObject jsonRequest,
			Listener<JSONObject> listener, ErrorListener errorListener) {
		super(url, jsonRequest, listener, errorListener);
	}

	public SignupVolleyTasks(int method, String url, JSONObject jsonRequest,
			Listener<JSONObject> listener, ErrorListener errorListener) {
		super(method, url, jsonRequest, listener, errorListener);
	}
	
	@Override
	protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
		return super.parseNetworkResponse(response);
	}

	@Override
	protected void deliverResponse(JSONObject response) {
		super.deliverResponse(response);
	}

	@Override
	public String getBodyContentType() {
		return super.getBodyContentType();
	}

	@Override
	public byte[] getBody() {
		return super.getBody();
	}

	public SignupVolleyTasks(final String method_name , final SignupImpl listener,String url , JSONObject postdata){
		super(Request.Method.POST, url, postdata, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				L.d("Response = "+response);
				listener.handleResult(method_name, response);
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				L.d("Error = "+error);
				listener.handleError(method_name, error);
			}
		});
		
		L.d("URL ="+url);
		L.d("Post Data ="+postdata);
	}
}	

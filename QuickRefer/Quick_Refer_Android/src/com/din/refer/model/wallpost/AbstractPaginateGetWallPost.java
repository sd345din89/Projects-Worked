package com.strobilanthes.forrestii.model.wallpost;

import java.util.ArrayList;

import com.strobilanthes.forrestii.model.general.AbstractDashboard;

public class AbstractPaginateGetWallPost{
	
	private int status;
	private String error;
	private AbstractPaginateResponse response;
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	
	public AbstractPaginateResponse getResponse() {
		return response;
	}

	public void setResponse(AbstractPaginateResponse response) {
		this.response = response;
	}


	public class AbstractPaginateResponse{
		private ArrayList<GetWallPost> Wall;
		private AbstractDashboard DashBoard;

		public ArrayList<GetWallPost> getWall() {
			return Wall;
		}

		public void setWall(ArrayList<GetWallPost> wall) {
			Wall = wall;
		}

		public AbstractDashboard getDashBoard() {
			return DashBoard;
		}

		public void setDashBoard(AbstractDashboard dashBoard) {
			DashBoard = dashBoard;
		}
	}
}

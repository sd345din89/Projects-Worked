package com.strobilanthes.forrestii.activities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.cropimage.RoundedImageView;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.ffFriendsList.GetForrestiiFriendList;
import com.strobilanthes.forrestii.model.userprofile.UserProfileImpl;
import com.strobilanthes.forrestii.model.viewreviews.AbstractReviewProfile;
import com.strobilanthes.forrestii.model.viewreviews.AbstractViewReviews;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;

public class ForrestiiFriendProfileActivity extends BaseActivity<DBAccess> implements PropertyChangeListener{
	
	@InjectView(R.id.ff_user_image)         private RoundedImageView ffUserImage;
	@InjectView(R.id.ff_txt_user_name)      private TextView ffTxtUserName;
	@InjectView(R.id.ff_txt_jobrole)        private TextView ffTxtJobrole;
	//@InjectView(R.id.ff_txt_rating_count)   private TextView ffTxtRatingCount;
	//@InjectView(R.id.ff_header_ratingBar)   private RatingBar ffHeaderRatingBar;
	//@InjectView(R.id.ff_txt_reviews)        private TextView ffTxtReviews;
//	@InjectView(R.id.lyt_ff_button)         private LinearLayout lytFfButton;
//	@InjectView(R.id.ff_btn_save)           private Button ffBtnSave;
	@InjectView(R.id.ff_header_phone)       private TextView ffHeaderPhone;
	@InjectView(R.id.ff_txt_phone)          private TextView ffTxtPhone;
	@InjectView(R.id.ff_header_email)       private TextView ffHeaderEmail;
	@InjectView(R.id.ff_txt_email)          private TextView ffTxtEmail;
	@InjectView(R.id.ff_header_website)     private TextView ffHeaderWebsite;
	@InjectView(R.id.ff_txt_website)        private TextView ffTxtWebsite;
	@InjectView(R.id.ff_header_address)     private TextView ffHeaderAddress;
	@InjectView(R.id.ff_txt_address)        private TextView ffTxtAddress;
	
	@InjectView(R.id.ff_img_email)          private ImageView ffImgEmail;
	@InjectView(R.id.ff_img_phone)          private ImageView ffImgPhone;
	@InjectView(R.id.ff_btn_review)         private Button ffBtnReview;
	@Inject UserProfileImpl userProfileModel;

	private Typeface mFontLight,mFontDark;
	private float rating;
	private ArrayList<AbstractReviewProfile> mAbstractViewReviews;
	private boolean isPick;


	private GetForrestiiFriendList abstractforrestiiFriendList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_forrestii_friend_view);
		
		
		loadFonts();
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		abstractforrestiiFriendList = (GetForrestiiFriendList)getIntent().getSerializableExtra("ForrestiiFriendProfile");
		isPick = getIntent().getBooleanExtra("isPick", false);

		showForrestiiProfileReviews();
		if(abstractforrestiiFriendList !=null){
			if(isPick){
				ffTxtUserName.setText(abstractforrestiiFriendList.getPocfirstName() + " " +abstractforrestiiFriendList.getPoclastName());
				ffTxtPhone.setText(""+(abstractforrestiiFriendList.getMobileNumber1() != 0 ? abstractforrestiiFriendList.getMobileNumber1() : "No Contacts Found"));
				ffTxtAddress.setText(abstractforrestiiFriendList.getFullAddress().length()>0?abstractforrestiiFriendList.getFullAddress():"No Address found");
				//ffTxtCompanyName.setText(abstractforrestiiFriendList.getCompanyName());
				ffTxtJobrole.setText(abstractforrestiiFriendList.getSkillMaster1().getSkillName()!=null?abstractforrestiiFriendList.getSkillMaster1().getSkillName():"No Skill Name");
				ffTxtEmail.setSelected(true);
				ffTxtEmail.setText(abstractforrestiiFriendList.getEmail().length()>0 ? abstractforrestiiFriendList.getEmail():"No Email Address");
				ffTxtWebsite.setText(abstractforrestiiFriendList.getWebsite().length()>0? abstractforrestiiFriendList.getWebsite():"No website found");
	            
				rating = getRatings(abstractforrestiiFriendList.getTotalRating());
			}else{
				ffTxtUserName.setText(abstractforrestiiFriendList.getPocfirstName() + " " +abstractforrestiiFriendList.getPoclastName());
				ffTxtPhone.setText(""+(abstractforrestiiFriendList.getMobileNumber1() != 0 ? Util.maskMobileNumber(String.valueOf(abstractforrestiiFriendList.getMobileNumber1())) : "No Contacts Found"));
				ffTxtAddress.setText(abstractforrestiiFriendList.getFullAddress().length()>0?abstractforrestiiFriendList.getFullAddress():"No Address found");
				//ffTxtCompanyName.setText(abstractforrestiiFriendList.getCompanyName());
				ffTxtJobrole.setText(abstractforrestiiFriendList.getSkillMaster1().getSkillName()!=null?abstractforrestiiFriendList.getSkillMaster1().getSkillName():"No Skill Name");
				ffTxtEmail.setSelected(true);
				ffTxtEmail.setText(abstractforrestiiFriendList.getEmail().length()>0 ? Util.maskString(abstractforrestiiFriendList.getEmail()):"No Email Address");
				ffTxtWebsite.setText(abstractforrestiiFriendList.getWebsite().length()>0? abstractforrestiiFriendList.getWebsite():"No website found");
	            
				rating = getRatings(abstractforrestiiFriendList.getTotalRating());
				ffImgEmail.setImageResource(R.color.transparent);
				ffImgPhone.setImageResource(R.color.transparent);
			}
			//ffHeaderRatingBar.setRating(rating);
			//ffTxtReviews.setText("Reviews: "+abstractforrestiiFriendList.getReviewCount());
			//ffTxtRatingCount.setText("Ratings: "+abstractforrestiiFriendList.getTotalRating());
			ffImgPhone.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(ffTxtPhone.getText().toString().length()>0){
						if(Util.isValidPhoneNumber(ffTxtPhone.getText().toString().trim()) && ffTxtPhone.getText().toString().length() >= 10){

							AlertDialog.Builder alertbuilder = new AlertDialog.Builder(v.getContext(),AlertDialog.THEME_HOLO_LIGHT);
							alertbuilder.setTitle("Message");
							alertbuilder.setMessage("Do you want to make a call");
							
							alertbuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Intent callIntent = new Intent(Intent.ACTION_CALL); // it's not ACTION_SEND
									//intent.setType("text/plain");
									
									callIntent.setData(Uri.parse("tel:"+ffTxtPhone.getText().toString())); 
									//intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
									startActivity(callIntent);
								}
							});
							alertbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
								}
							});
							
							alertbuilder.show();
							
					    	
						}else{
							Util.showAlert(ForrestiiFriendProfileActivity.this, "", "Please enter a valid mobile number");
						}
						
					}else{
						Util.showAlert(ForrestiiFriendProfileActivity.this, "", "No phone number found");
					}
					
				}
			});
			ffBtnReview.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(mAbstractViewReviews!=null && mAbstractViewReviews.size()>0){

					Intent intent = new Intent(ForrestiiFriendProfileActivity.this, UserRatingReviewActivity.class);
					Bundle forrestiiObject = new Bundle();
					forrestiiObject.putSerializable("ForrestiiFriendDetails", mAbstractViewReviews);
					forrestiiObject.putString("reviews_view", "ForrestiiProfileReviews");
					intent.putExtras(forrestiiObject);
					startActivity(intent);
				}else{
					Util.showAlert(ForrestiiFriendProfileActivity.this, "", "No reviews");
	
				}
				}
			});
			ffImgEmail.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(Util.isValidEmail(ffTxtEmail.getText().toString())){
						AlertDialog.Builder alertbuilder = new AlertDialog.Builder(v.getContext(),AlertDialog.THEME_HOLO_LIGHT);
						alertbuilder.setTitle("Message");
						alertbuilder.setMessage("Do you want to Send E-mail");
						
						alertbuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Intent emailIntent = new Intent(Intent.ACTION_SEND);
								emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{ffTxtEmail.getText().toString()});		  
								emailIntent.putExtra(Intent.EXTRA_TEXT, "message");
								emailIntent.setType("message/rfc822");
								startActivity(Intent.createChooser(emailIntent, "Choose an Email client :"));
							}
						});
						alertbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						
						alertbuilder.show();
					
				}else{
					Util.showAlert(ForrestiiFriendProfileActivity.this, "", "Email id is invalid");
		
				}
					
				}
			});
		}
	}
	public void showForrestiiProfileReviews(){
	JSONObject json = new JSONObject();
	try {
		json.put("forrestiiFriend",new JSONObject().put("FFID", abstractforrestiiFriendList.getFfid()));
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	userProfileModel.viewForrestiiFriendUsersReviews(Constant.VIEW_REFERRALS_REVIEW, json);
}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		finish();
		super.onBackPressed();
	}
	
	public float getRatings(float ratingvaule){
		
		float a = ratingvaule;
		L.d("getRatingValue++" +a);
		int d = (int) Math.ceil(a);
		L.d("SetRatingValue++" +d);

		return d;
	}
	private void loadFonts() {
		// TODO Auto-generated method stub
		mFontLight = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getAssets(),"andorid_google/Roboto-Regular.ttf");
		ffTxtUserName.setTypeface(mFontLight);
		ffTxtJobrole.setTypeface(mFontLight);
		ffHeaderAddress.setTypeface(mFontLight);
		ffTxtAddress.setTypeface(mFontLight);
		//ffTxtReviews.setTypeface(mFontLight);
		ffHeaderPhone.setTypeface(mFontLight);
		ffTxtPhone.setTypeface(mFontLight);
		ffHeaderEmail.setTypeface(mFontLight);
		ffTxtEmail.setTypeface(mFontLight);
		//ffTxtRatingCount.setTypeface(mFontLight);
		ffHeaderWebsite.setTypeface(mFontLight);
		ffTxtWebsite.setTypeface(mFontLight);
		
		

	//	ffHeaderCompanyName.setTypeface(mFontLight);
		//ffTxtCompanyName.setTypeface(mFontDark);
	}
	@Override
	protected void onResume() {
		super.onResume();
		userProfileModel.addChangeListener(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		userProfileModel.removeChangeListener(this);

	}
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		// TODO Auto-generated method stub
		 if (event.getPropertyName().equals("ForrestiiProfileReviews")){
				AbstractViewReviews abstractReviewList = userProfileModel.getReviewsRatings();
				if (abstractReviewList != null) {
					if (abstractReviewList.getStatus() == 1) {
						
						mAbstractViewReviews = abstractReviewList.getResponse();
						
					} else {
						//Util.showAlert(this, "", abstractReviewList.getError());
					}
				}else {
					Util.showAlert(this, "Message", "Request Failed");
					//finish();
				}
			}
	}

}

package com.strobilanthes.forrestii.model.coins;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Coins extends ForrestiiSubject {
	void getMyReferral();
	AbstractCoins abstractGetMyReferral();
	
}

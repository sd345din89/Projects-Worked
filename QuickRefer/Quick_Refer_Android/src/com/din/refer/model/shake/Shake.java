package com.strobilanthes.forrestii.model.shake;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Shake extends ForrestiiSubject {
	
	void reuestShakeDetails(String url,JSONObject postdata);
	AbstractShakeDetails getAbstractShakeDetails();
	
}

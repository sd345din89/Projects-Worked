package kra.accounts.tracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import kra.accounts.tracker.database.KraFinanceDbManager;
import kra.accounts.tracker.models.AccountDetails;
import kra.accounts.tracker.models.CollectionDetails;
import kra.accounts.tracker.models.LocationService;
import kra.accounts.tracker.models.TransactionDetails;
import kra.accounts.tracker.models.Util;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class CollectionActivity extends Activity{
	
	private Button collection_back_btn,collection_month_btn;
	//private LinearLayout collection_phone_lyt;
	private Dialog alertDialog;
	private TextView txt_contactNum1,txt_customerName,txt_customerAdress,txt_principal_amount,txt_principaldate,txt_accountName,txt_insertDueDate;
	private TextView txt_percentage_row1,txt_amount_row1,txt_net_amount_row1,txt_percentage_row2,txt_amount_row2,
					txt_net_amount_row2,txt_percentage_row3,txt_amount_row3,txt_net_amount_row3;

	private TextView txt_summary_percentage_row1,txt_summary_amount_row1,txt_summary_net_amount_row1,txt_summary_percentage_row2,txt_summary_amount_row2,
	txt_summary_net_amount_row2,txt_summary_percentage_row3,txt_summary_amount_row3,txt_summary_net_amount_row3;
	
	private EditText collection_comments_edit;
	
	private RadioButton radio_cash,radio_cheque;
	private int customerAccId = 0,customerId = 0;
	private Util util;
	private KraFinanceDbManager dbManager;
	CollectionDetails  collectionDetails;
	private AccountDetails getAccountDetails;
	
	private int interestRate12=12,interestRate18=18,interestRate24=24;
	private int totalMonths=0;
	private long interestDueDateAfterCollect = 0;
	private LocationService locationService;
	
//	private TableLayout summaryTable;
	private TextView txt_pendingAmount,txt_titleNoOfMonths;
	private TextView txt_summary_titletotal,txt_summary_titleNoOfMonths;
	
	private String TAG="ExcelParcer";
	private InputMethodManager inputMethodManager;
	private LinearLayout call_lyt;
	private TableLayout colleciton_details_table;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.collection_screen);
		
		util=new Util();
		dbManager=new KraFinanceDbManager(this);
		locationService=new LocationService(this);
		
		customerAccId = getIntent().getIntExtra("CustomerAccId", -1);
		customerId = getIntent().getIntExtra("CustomerId", -1);
		
		Log.d(TAG, "Customer Account Id = "+ customerAccId);
		collectionDetails =util.getCollectionDetails(dbManager, customerAccId,  getIntent().getBooleanExtra("IsFromSummary", false));
		
		if(collectionDetails.getAccountId() !=null)
			getAccountDetails = util.getAccountDetails(dbManager, Integer.parseInt(collectionDetails.getAccountId()));
		
		loadContents();
		
		refreshCollections(); 
		
		//addSummaryTable();
	}
	
	private void loadContents(){
		
		call_lyt=(LinearLayout) findViewById(R.id.collection_call_lyt);
		
		txt_pendingAmount=(TextView)findViewById(R.id.collection_pendingMonths);
		txt_titleNoOfMonths=(TextView)findViewById(R.id.collection_amount_title_for_months);
		
		txt_summary_titleNoOfMonths=(TextView)findViewById(R.id.collection_summary_amount_title_for_months);
		txt_summary_titletotal=(TextView)findViewById(R.id.collection_summary_amount_total);
		
		collection_comments_edit=(EditText) findViewById(R.id.collection_comments_edit);
		inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(collection_comments_edit.getWindowToken(), 0);
		
		txt_customerName=(TextView)findViewById(R.id.collection_customer_name);
		txt_customerAdress=(TextView)findViewById(R.id.collection_customer_address);
		txt_contactNum1=(TextView)findViewById(R.id.collection_contact_num1);
		txt_principal_amount=(TextView)findViewById(R.id.collection_principal_amount);
		txt_principaldate=(TextView)findViewById(R.id.collection_pricipaldate);
		txt_accountName=(TextView)findViewById(R.id.collection_account_name);
		txt_insertDueDate=(TextView)findViewById(R.id.collection_insert_due_from_date);
		
		txt_percentage_row1=(TextView)findViewById(R.id.collection_interest_percentage_row1);
		txt_percentage_row1.setText("12%");
		txt_amount_row1=(TextView)findViewById(R.id.collection_amount_row1);
		
		txt_net_amount_row1=(TextView)findViewById(R.id.collection_net_amount_row1);
	//	txt_net_amount_row1.setText(""+amount);
		
		txt_percentage_row2=(TextView)findViewById(R.id.collection_interest_percentage_row2);
		txt_percentage_row2.setText("18%");
		txt_amount_row2=(TextView)findViewById(R.id.collection_amount_row2);
		txt_net_amount_row2=(TextView)findViewById(R.id.collection_net_amount_row2);
	//	txt_net_amount_row2.setText(""+amount);
		
		txt_percentage_row3=(TextView)findViewById(R.id.collection_interest_percentage_row3);
		txt_percentage_row3.setText("24%");
		txt_amount_row3=(TextView)findViewById(R.id.collection_amount_row3);
		txt_net_amount_row3=(TextView)findViewById(R.id.collection_net_amount_row3);
	//	txt_net_amount_row3.setText(""+amount);
		
		
		txt_summary_percentage_row1=(TextView)findViewById(R.id.collection_summary_interest_percentage_row1);
		txt_summary_percentage_row1.setText("12%");
		txt_summary_amount_row1=(TextView)findViewById(R.id.collection_summary_amount_row1);
		
		txt_summary_net_amount_row1=(TextView)findViewById(R.id.collection_summary_net_amount_row1);
	//	txt_net_amount_row1.setText(""+amount);
		
		txt_summary_percentage_row2=(TextView)findViewById(R.id.collection_summary_interest_percentage_row2);
		txt_summary_percentage_row2.setText("18%");
		txt_summary_amount_row2=(TextView)findViewById(R.id.collection_summary_amount_row2);
		txt_summary_net_amount_row2=(TextView)findViewById(R.id.collection_summary_net_amount_row2);
	//	txt_net_amount_row2.setText(""+amount);
		
		txt_summary_percentage_row3=(TextView)findViewById(R.id.collection_summary_interest_percentage_row3);
		txt_summary_percentage_row3.setText("24%");
		txt_summary_amount_row3=(TextView)findViewById(R.id.collection_summary_amount_row3);
		txt_summary_net_amount_row3=(TextView)findViewById(R.id.collection_summary_net_amount_row3);
	//	txt_net_amount_row3.setText(""+amount);
		
		radio_cash = (RadioButton)findViewById(R.id.collection_cash_radio);
		radio_cash.setChecked(true);
		radio_cash.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(!radio_cash.isChecked()){
					radio_cash.setChecked(true);
					radio_cheque.setChecked(false);
				}
			}
		});
		radio_cheque = (RadioButton)findViewById(R.id.collection_cheque_radio);
		radio_cheque.setChecked(false);
		radio_cheque.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(!radio_cheque.isChecked()){
					radio_cash.setChecked(false);
					radio_cheque.setChecked(true);
				}
				
			}
		});
		
		
		/*collection_back_btn=(Button)findViewById(R.id.collection_back_btn);
		collection_back_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				CollectionActivity.this.finish();
			}
		});*/
		collection_month_btn=(Button)findViewById(R.id.collection_month_btn);
		collection_month_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				//long days = TimeUnit.MILLISECONDS.toDays(resultForMonths);
				
				Log.d("ExcelParcer", "Month Count" + totalMonths);
				
				if(totalMonths > 0)
					showMonthAlert(totalMonths);
				else{
					AlertDialog.Builder builder = new Builder(CollectionActivity.this);
					builder.setTitle("Message");
					builder.setMessage("No Pending Amount!");
					
					builder.setPositiveButton("OK", null);
					
					//builder.setNegativeButton("Cancel", null);
					
					builder.show();
				}
				
			}
		});
		
		
		call_lyt=(LinearLayout)findViewById(R.id.collection_call_lyt);
		call_lyt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				AlertDialog.Builder builder = new Builder(CollectionActivity.this);
				builder.setTitle("Confirm");
				builder.setMessage("Do you want to connect call?");
				
				builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						util.connectToCustomerMob(CollectionActivity.this, txt_contactNum1.getText().toString());
					
					}
				});
				
				builder.setNegativeButton("Cancel", null);
				
				builder.show();
			    
			}
		});
		
		
		((Button)findViewById(R.id.collection_colloect_btn)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(totalMonths > 0)
					showCollectionPaidAlert();
				else{
					AlertDialog.Builder builder = new Builder(CollectionActivity.this);
					builder.setTitle("Message");
					builder.setMessage("No Pending Amount!");
					
					builder.setPositiveButton("OK", null);
					
					//builder.setNegativeButton("Cancel", null);
					
					builder.show();
				}
			}
		});
		
		(findViewById(R.id.collection_principle_amt_lyt)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showPrincipalPaidAlert(""+customerAccId, ""+collectionDetails.getAccountId());
			}
		});
		colleciton_details_table=(TableLayout) findViewById(R.id.collection_details_table_lyt);
		
	}
	
	private void addCollectionDetailsTable(ArrayList<TransactionDetails> transactionList){
		LayoutInflater inflater = getLayoutInflater();
		
		if(transactionList.size() > 0){
			
			colleciton_details_table.removeAllViews();
			
			if(transactionList.size() > 10){
				//totalMonths = 10;
			}
			for(int i= 0 ; i < transactionList.size() ; i++){
				
				TableLayout.LayoutParams rowLp = new TableLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT,
				        LayoutParams.MATCH_PARENT);
				
				
				TableRow.LayoutParams cellLp = new TableRow.LayoutParams(
						LayoutParams.WRAP_CONTENT,
				        LayoutParams.WRAP_CONTENT);
			
				TableRow tableRow=new TableRow(this);
					
				View view = inflater.inflate(R.layout.collections_details_adapter_screen,null);
				TextView txt_count=(TextView)view.findViewById(R.id.collection_details_adapter_index);
				txt_count.setText(""+(i+1));
				TextView txt_months =(TextView)view.findViewById(R.id.collection_details_adapter_months);
				
				Calendar startCal=Calendar.getInstance();
				startCal.setTimeInMillis(Long.parseLong(transactionList.get(i).getdueDate()));
				//startCal.add(Calendar.MONTH, -1);
				SimpleDateFormat sdf=new SimpleDateFormat("MMMMM,yyyy",Locale.getDefault());
				String nameOfMonth = sdf.format(startCal.getTime());
				
				Log.d("ExcelParcer", "Transaction time "+transactionList.get(i).getdueDate()+"####"+"Name Of Month = "+nameOfMonth);
				
				txt_months .setText(nameOfMonth);
				TextView txt_date_time=(TextView)view.findViewById(R.id.collection_details_adapter_date_time);
				
				Calendar paidCal=Calendar.getInstance();
				paidCal.setTimeInMillis(Long.parseLong(transactionList.get(i).getpaidDate()));
				//startCal.add(Calendar.MONTH, -1);
				SimpleDateFormat paidCal_sdf=new SimpleDateFormat("MMM dd , HH:mm aa",Locale.getDefault());
				String paidDateATime = paidCal_sdf.format(paidCal.getTime());
				
				txt_date_time.setText(paidDateATime);
				
					/*LinearLayout parent=new LinearLayout(this);
					LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(
			              LayoutParams.MATCH_PARENT,
			              LayoutParams.MATCH_PARENT);
					parent.setLayoutParams(params);
					parent.setGravity(Gravity.CENTER);
					parent.setOrientation(LinearLayout.HORIZONTAL);*/
					
					/*final TextView paidDate=new TextView(this);
					LinearLayout.LayoutParams txt_params=new LinearLayout.LayoutParams(
	                          LayoutParams.WRAP_CONTENT,
	                          LayoutParams.WRAP_CONTENT);
					txt_params.setMargins(10, 5, 0, 0);
					txt_params.weight =1.0f;
					paidDate.setText("Test "+i);
					paidDate.setTypeface(null, Typeface.BOLD);
					paidDate.setId(i);
					paidDate.setLayoutParams(txt_params);
					paidDate.setTextSize(15);
					paidDate.setTextColor(getResources().getColor(R.color.collection_intrest_txt_color));
					
					parent.addView(paidDate);
					
					final TextView collectedMonth=new TextView(this);
					LinearLayout.LayoutParams expandparams=new LinearLayout.LayoutParams(
	                          LayoutParams.WRAP_CONTENT,
	                          LayoutParams.WRAP_CONTENT);
					expandparams.setMargins(10, 5, 0, 0);
					expandparams.weight =1.0f;
					collectedMonth.setText("Test "+i);
					
					collectedMonth.setLayoutParams(expandparams);
					collectedMonth.setTextSize(14);
					collectedMonth.setTextColor(getResources().getColor(R.color.collection_intrest_txt_color));
					collectedMonth.setVisibility(View.VISIBLE);
					parent.addView(collectedMonth);*/
					
				tableRow.addView(view,cellLp);
				
				colleciton_details_table.addView(tableRow,rowLp);
			}
		}else{
			colleciton_details_table.removeAllViews();
			
			for(int i= 0 ; i < 1 ; i++){
				
				TableLayout.LayoutParams rowLp = new TableLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT,
				        LayoutParams.MATCH_PARENT);
				
				
				TableRow.LayoutParams cellLp = new TableRow.LayoutParams(
						LayoutParams.WRAP_CONTENT,
				        LayoutParams.WRAP_CONTENT);
			
				TableRow tableRow=new TableRow(this);
					
				View view = inflater.inflate(R.layout.collections_details_adapter_screen,null);
				TextView txt_count=(TextView)view.findViewById(R.id.collection_details_adapter_index);
				txt_count.setText("");
				TextView txt_months =(TextView)view.findViewById(R.id.collection_details_adapter_months);
				
				txt_months .setText("No Data");
				TextView txt_date_time=(TextView)view.findViewById(R.id.collection_details_adapter_date_time);
				txt_date_time.setText("No Data");
			
				tableRow.addView(view,cellLp);
				
				colleciton_details_table.addView(tableRow,rowLp);
			}
		}
		
	}
		
	private void refreshCollections(){
		
		collectionDetails =util.getCollectionDetails(dbManager, customerAccId,  getIntent().getBooleanExtra("IsFromSummary", false));
		
		if(collectionDetails.getAddress() !=null){
			txt_customerName.setText(collectionDetails.getName());
			txt_customerAdress.setText(collectionDetails.getAddress());
			txt_contactNum1.setText(collectionDetails.getPhoneNum());
			
			//DecimalFormat formatter = new DecimalFormat("#,###");
			
			txt_principal_amount.setText(util.formatAmount(collectionDetails.getprincipalAmount()));
			
			
			Calendar calendar = Calendar.getInstance();
		    //calendar.setTimeInMillis(Long.parseLong(collectionDetails.getprincipalDate()));
			String pricipalDate= new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault()).format(Long.parseLong(collectionDetails.getprincipalDate())).toString();
			txt_principaldate.setText(""+pricipalDate);
			
			if(getAccountDetails.getaccountName() != null)
				txt_accountName.setText(getAccountDetails.getaccountName());
		//	Log.d("ExcelParcer", "Milliseconds" + resultForMonths);
			
			calendar = Calendar.getInstance();
			calendar.setTimeInMillis(Long.parseLong(collectionDetails.getinsertDueFromDate()));
			String insertDate= new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault()).format(Long.parseLong(collectionDetails.getinsertDueFromDate())).toString();
			txt_insertDueDate.setText(""+insertDate);
			
			
			calendar = Calendar.getInstance();
			long resultForMonths = 0;
			if(calendar.getTimeInMillis() > Long.parseLong(collectionDetails.getinsertDueFromDate())){
				//resultForMonths =util.getNumOfMonths(Long.parseLong(collectionDetails.getinsertDueFromDate()), calendar.getTimeInMillis());
				//calendar.setTimeInMillis(resultForMonths);
				
			    totalMonths=util.getNumOfMonths(Long.parseLong(collectionDetails.getinsertDueFromDate()), calendar.getTimeInMillis());
			    
			    if(totalMonths > 1){
			    	 txt_pendingAmount.setText(totalMonths+" Months");
			    }else{
			    	if(totalMonths == 0){
			    		txt_pendingAmount.setText("None");
			    	}else
			    		txt_pendingAmount.setText(totalMonths+" Month");
			    }
			}else{
				totalMonths = 0;
			    txt_pendingAmount.setText("None");
			}
			
			setInterestAmountTable(1,totalMonths);
			    
		 // String dueDate = util.getTransactionDueDateDetails(dbManager, customerAccId);
		//  Log.d("ExcelParcer", "Due Date = " + dueDate);
			calendar = Calendar.getInstance();
		    String dueDate = util.getTransactionDueDate(dbManager, customerAccId);
		    //Log.d("ExcelParcer", "Due Date = " + dueDate);
			  
		    int totalMonthsCollected=0;
		    if(dueDate.length() > 0){
		    	if(!dueDate.equals("0")){
		    		
				    totalMonthsCollected = util.getNumOfMonths(Long.parseLong(dueDate),Long.parseLong(collectionDetails.getinsertDueFromDate()));
				    
				    if(totalMonthsCollected == 0)
				    	totalMonthsCollected = 1;
				    
				   // String monthName = util.getNameOfMonth(Long.parseLong(dueDate),Long.parseLong(collectionDetails.getinsertDueFromDate()),totalMonthsCollected);
				    setSummaryTable(totalMonthsCollected);
				   
				    addCollectionDetailsTable(util.getTransactionDetails(dbManager,customerAccId));
				   // addCollectionDetailsTable(Long.parseLong(dueDate),Long.parseLong(collectionDetails.getinsertDueFromDate()),totalMonthsCollected);
				    
		    	}else{
		    		addCollectionDetailsTable(util.getTransactionDetails(dbManager,customerAccId));
		    		totalMonthsCollected = 0;
		    		setSummaryTable(totalMonthsCollected);
		    	}
		    }else{
		    	addCollectionDetailsTable(util.getTransactionDetails(dbManager,customerAccId));
		    	totalMonthsCollected = 0;
		    	setSummaryTable(totalMonthsCollected);
		    }
		    
		    
		    calendar = Calendar.getInstance();
		    calendar.setTimeInMillis(Long.parseLong(collectionDetails.getinsertDueFromDate()));
		    calendar.add(Calendar.MONTH, 1);
		    
		    interestDueDateAfterCollect = calendar.getTimeInMillis();
		    
		    
		}
	}
	
	@Override
	protected void onResume() {
		
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		super.onResume();
	}


	private void showCollectionPaidAlert(){
		
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("Confirm");
		builder.setMessage("Do you want to Collect Payment?");
		
		builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

				long dueDate=convertDateObjIntoLong(txt_insertDueDate.getText().toString(),"dd/MM/yyyy");
				Calendar calendar = Calendar.getInstance();
				String payMode=null;
				if(radio_cash.isChecked()){
					payMode="Cash";
				}else{
					payMode="Cheque";
				}
				Location location = locationService.cLocation;
				util.updateTransactionTable(dbManager,interestDueDateAfterCollect,customerAccId,dueDate,calendar.getTimeInMillis(),util.checkLoginStatus(dbManager),payMode,collection_comments_edit.getText().toString(),location !=null ? String.valueOf(location.getLatitude()):"",location !=null ? String.valueOf(location.getLongitude()):"", util.getDeviceUDID(CollectionActivity.this));
				
				
				collection_month_btn.setText("1 Month");
				
				refreshCollections();
				//finish();
			}
		});
		
		builder.setNegativeButton("Cancel", null);
		
		builder.show();
	} 
	
	private void showPrincipalPaidAlert(final String cusId, final String accId){
		
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("Confirm");
		builder.setMessage("Do you want update Principal Paid?");
		
		builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				util.updateUserPrincipalPaid(dbManager, cusId, accId);
				
				dialog.dismiss();
				finish();
			}
		});
		
		builder.setNegativeButton("Cancel", null);
		
		builder.show();
	}
	
	@Override
	protected void onDestroy() {
		
		locationService.stopLocationUpdate();
		
		super.onDestroy();
	}


	private Long convertDateObjIntoLong(Object dataObj, String dateFormat){
    	try {
    		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat,Locale.getDefault());
    		
    		long datetolong=sdf.parse(String.valueOf(dataObj)).getTime();
    		
    		//String pricipalDate= new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault()).format(datetolong).toString();  		
    		//Log.d("ExcelParcer", "Date Test " + pricipalDate);	
			return datetolong;
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
    	return (long) 0;
    	
	}
	
	private void setInterestAmountTable(int numMonths ,int totalMonths){
			
		 if(numMonths > 1){
	    	 txt_titleNoOfMonths.setText(numMonths+" - Months");
		 }else{
	    	txt_titleNoOfMonths.setText(numMonths+" - Month");
		 }
		 
		 int amount = Integer.parseInt(collectionDetails.getprincipalAmount())/100 * interestRate12;
		 txt_amount_row1.setText(""+util.formatAmount(String.valueOf(amount*numMonths)));
		 if(totalMonths > 0)
			 txt_net_amount_row1.setText(""+util.formatAmount(String.valueOf(amount*totalMonths)));
		 else
			 txt_net_amount_row1.setText("0");
		    
		 amount = Integer.parseInt(collectionDetails.getprincipalAmount())/100 * interestRate18;
		 txt_amount_row2.setText(""+util.formatAmount(String.valueOf(amount*numMonths)));
		
		 if(totalMonths > 0)
			 txt_net_amount_row2.setText(""+util.formatAmount(String.valueOf(amount*totalMonths)));
		 else
			 txt_net_amount_row2.setText("0");
		 
		 amount = Integer.parseInt(collectionDetails.getprincipalAmount())/100 * interestRate24;
		 txt_amount_row3.setText(""+util.formatAmount(String.valueOf(amount*numMonths)));
		
		 if(totalMonths != 0)
			 txt_net_amount_row3.setText(""+util.formatAmount(String.valueOf(amount*totalMonths)));
		 else
			 txt_net_amount_row3.setText("0");
	}
	
	
	private void setSummaryTable(int numOfMonthsCollected){
		
		 if(numOfMonthsCollected > 1){
	    	txt_summary_titleNoOfMonths.setText("Collected\n"+numOfMonthsCollected+" - Months");
		 }else{
			 if(numOfMonthsCollected != 0)
				 txt_summary_titleNoOfMonths.setText("Collected\n"+numOfMonthsCollected+" - Month");
			 else
				 txt_summary_titleNoOfMonths.setText("Collected\n"+"None");
		 }
		/* int totalMonthsCollected =numOfMonthsCollected + totalMonths;
		 if(totalMonthsCollected > 1){
	    	 txt_summary_titletotal.setText("From Inception\n"+totalMonthsCollected+"/Months"); 
		 }else{
	    	 txt_summary_titletotal.setText("From Inception\n"+totalMonthsCollected+"/Month");
		 }*/
		 
		
		Calendar calendar = Calendar.getInstance();
		
		//long resultForMonths = 0;
		int insceptionMonths = 0;
		if(calendar.getTimeInMillis() > Long.parseLong(collectionDetails.getprincipalDate())){
			
			insceptionMonths = util.getNumOfMonths(Long.parseLong(collectionDetails.getprincipalDate()),calendar.getTimeInMillis());
		
		    if(insceptionMonths > 1){
		    	 txt_summary_titletotal.setText("From Inception\n"+insceptionMonths+" - Months"); 
		    }else{
		    	if(insceptionMonths == 0)
		    		txt_summary_titletotal.setText("From Inception\nNone");
		    	else
		    		txt_summary_titletotal.setText("From Inception\n"+insceptionMonths+" - Month");
		    }
		 }else{
			//totalMonths = 0;
			 txt_summary_titletotal.setText("From Inception\nNone");
		 }
			
		 int amount = Integer.parseInt(collectionDetails.getprincipalAmount())/100 * interestRate12;
		   txt_summary_amount_row1.setText(""+util.formatAmount(String.valueOf(amount*numOfMonthsCollected)));
		   txt_summary_net_amount_row1.setText(""+util.formatAmount(String.valueOf(amount*insceptionMonths)));
		    
		   amount = Integer.parseInt(collectionDetails.getprincipalAmount())/100 * interestRate18;
		   txt_summary_amount_row2.setText(""+util.formatAmount(String.valueOf(amount*numOfMonthsCollected)));
		   txt_summary_net_amount_row2.setText(""+util.formatAmount(String.valueOf(amount*insceptionMonths)));
		    
		   amount = Integer.parseInt(collectionDetails.getprincipalAmount())/100 * interestRate24;
		   txt_summary_amount_row3.setText(""+util.formatAmount(String.valueOf(amount*numOfMonthsCollected)));
		   txt_summary_net_amount_row3.setText(""+util.formatAmount(String.valueOf(amount*insceptionMonths)));
	}

	
	protected void showMonthAlert(int month) {
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("Message");
		//builder.setMessage("choose an option from below!");
		builder.setView(MonthOptionsAlertView(month));
		alertDialog = builder.create();
		alertDialog.show();
	}

	private View MonthOptionsAlertView(int numOfMonths) {
		
		 LinearLayout msgLayout=new LinearLayout(this);
		 msgLayout.setOrientation(LinearLayout.VERTICAL);
		 msgLayout.setGravity(Gravity.CENTER);
		 LayoutParams msg_lp=new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		 msg_lp.setMargins(0, 0, 0, 5);
		 
		 TextView text = new TextView(this);  
		 text.setText("choose an option from below!");  
		 text.setLayoutParams(msg_lp);
		 text.setTextSize(16);
		 text.setTypeface(null, Typeface.BOLD);
		 text.setGravity(Gravity.CENTER);
		 text.setTextColor(Color.rgb(255, 255, 255));
		 msgLayout.addView(text);

		 ScrollView sv = new ScrollView(this);
		 sv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
         sv.setVerticalScrollBarEnabled(true);
         sv.setSmoothScrollingEnabled(true);
         sv.setFillViewport(true);
		 LinearLayout optionsLayout=new LinearLayout(this);
		 optionsLayout.setOrientation(LinearLayout.VERTICAL);
		 optionsLayout.setGravity(Gravity.CENTER);
		 optionsLayout.setPadding(10, 5, 10, 5);
		 optionsLayout.addView(msgLayout);
		 LayoutParams lp=new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);;
		 lp.setMargins(0, 0, 0, 3);
		 
		 for(int i= 1 ;i <= numOfMonths;i++){
			 sv.removeAllViews();
			 Button month_btn1=new Button(this);
			 if(i > 1)
				 month_btn1.setText(i+" Months");
			 else
				 month_btn1.setText(i+" Month");
			 
			 month_btn1.setTextColor(Color.BLACK);
			 month_btn1.setMinimumWidth(150);
			 month_btn1.setLayoutParams(lp);
			 month_btn1.setId(i);
			 month_btn1.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					collection_month_btn.setText(v.getId()+" Month");
					
					Calendar calendar = Calendar.getInstance();
				    calendar.setTimeInMillis(Long.parseLong(collectionDetails.getinsertDueFromDate()));
				    calendar.add(Calendar.MONTH, v.getId());
				  
				    interestDueDateAfterCollect = calendar.getTimeInMillis();
				    
				    Log.d("ExcelParcer", "intial "+v.getId()+" months "+interestDueDateAfterCollect);
				    
				    //String pricipalDate= new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault()).format(calendar.getTime()).toString();
				    
					//txt_insertDueDate.setText(pricipalDate);
					
					setInterestAmountTable(v.getId() ,totalMonths);
					
					alertDialog.dismiss();
				}
			});
			 optionsLayout.addView(month_btn1);
			 sv.addView(optionsLayout);
		 }
		 
		 return sv;
	}

	
	class CustomDetailsAdapter extends BaseAdapter{
		public CustomDetailsAdapter() {
			
		}
		@Override
		public int getCount() {
			return 10;
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return null;
		}
		
	}
}

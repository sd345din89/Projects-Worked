package com.strobilanthes.forrestii;


import com.google.inject.AbstractModule;
import com.strobilanthes.forrestii.model.friendslist.Friends;
import com.strobilanthes.forrestii.model.friendslist.FriendsListImpl;
import com.strobilanthes.forrestii.model.invite.InviteRequest;
import com.strobilanthes.forrestii.model.invite.InviteRequestImpl;
import com.strobilanthes.forrestii.model.login.Login;
import com.strobilanthes.forrestii.model.login.LoginImpl;
import com.strobilanthes.forrestii.model.logout.Logout;
import com.strobilanthes.forrestii.model.logout.LogoutImpl;
import com.strobilanthes.forrestii.model.notification.Notification;
import com.strobilanthes.forrestii.model.notification.NotificationImpl;
import com.strobilanthes.forrestii.model.otpvalidation.OtpValidation;
import com.strobilanthes.forrestii.model.otpvalidation.OtpValidationImpl;
import com.strobilanthes.forrestii.model.pwd_reg.PasswordReg;
import com.strobilanthes.forrestii.model.pwd_reg.PasswordRegImpl;
import com.strobilanthes.forrestii.model.quickcard.Quickcard;
import com.strobilanthes.forrestii.model.quickcard.QuickcardImpl;
import com.strobilanthes.forrestii.model.referral.Referral;
import com.strobilanthes.forrestii.model.referral.ReferralImpl;
import com.strobilanthes.forrestii.model.search.Search;
import com.strobilanthes.forrestii.model.search.SearchImpl;
import com.strobilanthes.forrestii.model.settings.Settings;
import com.strobilanthes.forrestii.model.settings.SettingsImpl;
import com.strobilanthes.forrestii.model.shake.Shake;
import com.strobilanthes.forrestii.model.shake.ShakeImpl;
import com.strobilanthes.forrestii.model.signup.Signup;
import com.strobilanthes.forrestii.model.signup.SignupImpl;
import com.strobilanthes.forrestii.model.userprofile.UserProfile;
import com.strobilanthes.forrestii.model.userprofile.UserProfileImpl;
import com.strobilanthes.forrestii.model.wallpost.WallPost;
import com.strobilanthes.forrestii.model.wallpost.WallPostImpl;

public class ForrestiiModules extends AbstractModule{
	
	@Override
	protected void configure() {
		
		bind(Login.class).to(LoginImpl.class);
		bind(Signup.class).to(SignupImpl.class);
		bind(OtpValidation.class).to(OtpValidationImpl.class);
		bind(PasswordReg.class).to(PasswordRegImpl.class);
		bind(Logout.class).to(LogoutImpl.class);
		bind(WallPost.class).to(WallPostImpl.class);
		bind(Friends.class).to(FriendsListImpl.class);
		bind(Settings.class).to(SettingsImpl.class);
		bind(Referral.class).to(ReferralImpl.class);
		//bind(Ratings.class).to(RatingsImpl.class);
		bind(Notification.class).to(NotificationImpl.class);
		bind(Quickcard.class).to(QuickcardImpl.class);
		bind(UserProfile.class).to(UserProfileImpl.class);
		bind(InviteRequest.class).to(InviteRequestImpl.class);
		bind(Search.class).to(SearchImpl.class);
		bind(Shake.class).to(ShakeImpl.class);
		
	}
}

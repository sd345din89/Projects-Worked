package com.strobilanthes.forrestii.util;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class L {
	
	private static final String TAG = "Forresstii App";
	
	static String className;
	static String methodName;
	static int lineNumber;
	
	private static String createLog( String log ) {

		StringBuffer buffer = new StringBuffer();
		buffer.append("[");
		buffer.append(methodName);
		buffer.append(":");
		buffer.append(lineNumber);
		buffer.append("]");
		buffer.append(log);

		return buffer.toString();
	}
	
	private static void getMethodNames(StackTraceElement[] sElements){
		className = sElements[1].getFileName();
		methodName = sElements[1].getMethodName();
		lineNumber = sElements[1].getLineNumber();
	}
	
	public static void d(String data){
		if(!Constant.LOG_FORRESSTII)
			return;
		getMethodNames(new Throwable().getStackTrace());
		Log.d(className, createLog(data));
	}
	
	public static void v(String data){
		if(!Constant.LOG_FORRESSTII)
			return;
		getMethodNames(new Throwable().getStackTrace());
		Log.v(className, createLog(data));
	}
	
	public static void i(String data){
		if(!Constant.LOG_FORRESSTII)
			return;
		getMethodNames(new Throwable().getStackTrace());
		Log.i(className, createLog(data));
	}
	
	public static void e(String data, Exception e){
		if(!Constant.LOG_FORRESSTII)
			return;
		getMethodNames(new Throwable().getStackTrace());
		Log.e(className, createLog(data));
	}
	
	public static void ToastMessage(Context context,String message){
		Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
	}
}

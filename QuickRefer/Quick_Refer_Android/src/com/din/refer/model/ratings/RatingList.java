package com.strobilanthes.forrestii.model.ratings;

import java.io.Serializable;

import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

public class RatingList implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int ratingId;
	private AbstractUserProfile ratingFromUserProfile;
	private float rating;
	private String review;
	private long updatedOn;
	//private AbstractQuickcard quickCard;
	
	public int getRatingId() {
		return ratingId;
	}
	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}
	public AbstractUserProfile getRatingFromUserProfile() {
		return ratingFromUserProfile;
	}
	public void setRatingFromUserProfile(AbstractUserProfile ratingFromUserProfile) {
		this.ratingFromUserProfile = ratingFromUserProfile;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public long getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}
	
}

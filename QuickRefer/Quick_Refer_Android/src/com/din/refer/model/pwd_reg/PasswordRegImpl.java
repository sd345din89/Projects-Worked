package com.strobilanthes.forrestii.model.pwd_reg;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.PasswordRegVolleyTasks;


public class PasswordRegImpl implements PasswordReg,VolleyTasksListener{

	private AbstractPasswordReg abstractPasswordReg; 
	private AbstractForgetPassword abstractForgetPassword; 
	private AbstractForgotPwdVerify abstractForgotPwdVerify;
	List<PropertyChangeListener> listener=new ArrayList<PropertyChangeListener>();
	private Activity context;
	private CustomProgressbar mCustomProgressbar;
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners( PropertyChangeEvent evt ){
		for ( Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator.hasNext(); ){
			PropertyChangeListener name = (PropertyChangeListener)iterator.next();
			name.propertyChange(evt);
		}
	}
	
	@Override
	public void handleResult(String method_name,JSONObject jsonObject) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		if(method_name.equals("password_registration")){
			AbstractPasswordReg previousResult= abstractPasswordReg;
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractPasswordReg = gson.fromJson(jsonObject.toString(), AbstractPasswordReg.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractPasswordReg); 
			notifyListeners(event);
		}else if(method_name.equals("forget_password")){
			AbstractForgetPassword previousResult= abstractForgetPassword;
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractForgetPassword = gson.fromJson(jsonObject.toString(), AbstractForgetPassword.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractPasswordReg); 
			notifyListeners(event);
		}else if(method_name.equals("verify_forget_password")){
			AbstractForgotPwdVerify previousResult= abstractForgotPwdVerify;
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();
			abstractForgotPwdVerify = gson.fromJson(jsonObject.toString(), AbstractForgotPwdVerify.class);
			
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name, previousResult, abstractPasswordReg); 
			notifyListeners(event);
		}
	}

	@Override
	public void handleError(String method_name,Exception e) {
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		AbstractPasswordReg previousResult= abstractPasswordReg;
		abstractPasswordReg = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this,method_name, previousResult, null); 
		notifyListeners(event);
	}

	
	@Override
	public void requestPwdRegistrationDetails(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new PasswordRegVolleyTasks("password_registration", this, url,
						postdata).setRetryPolicy(new DefaultRetryPolicy(
						Constant.SOCKET_TIMEOUT,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void requestForgetPassword(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new PasswordRegVolleyTasks("forget_password", this, url,
						postdata).setRetryPolicy(new DefaultRetryPolicy(
						Constant.SOCKET_TIMEOUT,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void requestVerifyForgetPassword(String url, JSONObject postdata) {
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new PasswordRegVolleyTasks("verify_forget_password", this, url,
						postdata).setRetryPolicy(new DefaultRetryPolicy(
						Constant.SOCKET_TIMEOUT,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void doValidation(String content) {
		
	}

	@Override
	public AbstractPasswordReg getPassRegDetails() {
		return abstractPasswordReg;
	}

	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	
	@Override
	public AbstractForgetPassword getForgetPasswordDetails() {
		return abstractForgetPassword;
	}

	
	@Override
	public AbstractForgotPwdVerify getVerifyForgetPasswordDetails() {
		return abstractForgotPwdVerify;
	}
}

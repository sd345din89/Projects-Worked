package com.strobilanthes.forrestii.model.search;

import java.util.ArrayList;

public class AbstractSearch {
	private int status;
	private searchCardRes response;
	private String error;
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public searchCardRes getResponse() {
		return response;
	}
	public void setResponse(searchCardRes response) {
		this.response = response;
	}

	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

	public class searchCardRes{
		ArrayList<getT2andT3Friends>T2;
		ArrayList<getT2andT3Friends>T3;
		
		public ArrayList<getT2andT3Friends> getT2() {
			return T2;
		}
		public void setT2(ArrayList<getT2andT3Friends> t2) {
			T2 = t2;
		}
		public ArrayList<getT2andT3Friends> getT3() {
			return T3;
		}
		public void setT3(ArrayList<getT2andT3Friends> t3) {
			T3 = t3;
		}

	}
	
	public class getT2andT3Friends{
		private FriendUserProfile friendUserProfile;
		private int id;
		
		public FriendUserProfile getFriendUserProfile() {
			return friendUserProfile;
		}
		public void setFriendUserProfile(FriendUserProfile friendUserProfile) {
			this.friendUserProfile = friendUserProfile;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
	
		
	}
	public class FriendUserProfile{
		private String userId;
		private String firstName;
		private int mobile;
		private ArrayList<UserCardInfo>userCardInfo;
		
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public int getMobile() {
			return mobile;
		}
		public void setMobile(int mobile) {
			this.mobile = mobile;
		}
		public ArrayList<UserCardInfo> getUserCardInfo() {
			return userCardInfo;
		}
		public void setUserCardInfo(ArrayList<UserCardInfo> userCardInfo) {
			this.userCardInfo = userCardInfo;
		}
	
		
	}
	
	public class UserCardInfo{
		private String cardName;

		public String getCardName() {
			return cardName;
		}

		public void setCardName(String cardName) {
			this.cardName = cardName;
		}
		}
	

}

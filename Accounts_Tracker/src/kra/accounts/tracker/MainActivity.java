package kra.accounts.tracker;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import kra.accounts.tracker.database.KraFinanceDbManager;
import kra.accounts.tracker.models.Constants;
import kra.accounts.tracker.models.ExcelParcer;
import kra.accounts.tracker.models.Util;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private KraFinanceDbManager dbManager;
	
	private Util util;
	
	private ExcelParcer excelParcer;

	private TextView pendingAmtIndicatorTxt;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        
        dbManager = new KraFinanceDbManager(this);
        
        util = new Util();

        String loginUser = getIntent().getStringExtra("LoginUser");
        
        ((TextView)findViewById(R.id.home_screen_user_name)).setText("Logged in as : "+loginUser);
        
        pendingAmtIndicatorTxt = (TextView)findViewById(R.id.pending_indicator_txt);
        pendingAmtIndicatorTxt.setVisibility(View.INVISIBLE);
        
        
        (findViewById(R.id.import_lyt)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new ExcelFileParcerTask().execute();
			}
		});
        
        
        (findViewById(R.id.change_user_btn)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				util.deleteLoginUSer(dbManager);
				Intent intent=new Intent(MainActivity.this,LoginActivity.class);
				startActivity(intent);
				finish();
			}
		});
        
        (findViewById(R.id.pending_lyt)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(MainActivity.this,PendingListActivity.class);
				startActivity(intent);
			}
		});
        (findViewById(R.id.summary_lyt)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(MainActivity.this,SummaryListActivity.class);
				startActivity(intent);
			}
		});
        
        
        (findViewById(R.id.export_lyt)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new ExportExcelTask().execute();
			}
		});
        
    }
    
    


    @Override
	protected void onResume() {
		super.onResume();
		 int pendingCount = util.getPendingAmountCount(dbManager);
	    	
    	if(pendingCount > 0){
    		pendingAmtIndicatorTxt.setVisibility(View.VISIBLE);

    		pendingAmtIndicatorTxt.setText(""+pendingCount);
    	}else{
    		pendingAmtIndicatorTxt.setVisibility(View.INVISIBLE);
    	}
    	
	}




	@Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    
    private class ExcelFileParcerTask extends AsyncTask<Void, Void, String>{
    	
    	private ProgressDialog progressDialog;
    	private Util util;
    	
		@Override
		protected void onPreExecute() {
			//super.onPreExecute();
			util = new Util();
			progressDialog = util.showProgressView(MainActivity.this, "Importing file...");
			
		}

		@Override
		protected String doInBackground(Void... params) {
			
			try {
				Log.d("READ_XL", "Start XL Parsing");
				if(Constants.KRA_FINANCE_DATA_EXCEL_FILE.exists()){
					FileInputStream is = new FileInputStream(Constants.KRA_FINANCE_DATA_EXCEL_FILE);
					HSSFWorkbook workbook = new HSSFWorkbook(is);
					
					
					new ExcelParcer(workbook, dbManager, util);
					
				}else{
					return "N";
				}

			} catch (IOException e) {
				e.printStackTrace();
				return "E";
			}
			return "S";
		}

		@Override
		protected void onPostExecute(String result) {
			//super.onPostExecute(result);
			
			if(progressDialog != null){
				progressDialog.dismiss();
			}
			
			if(result.equalsIgnoreCase("S")){
				Log.d("READ_XL", "End XL Parsing");
				 
				updateImportedFileName();
				
				Toast.makeText(MainActivity.this, "Excel Parsed successfully", Toast.LENGTH_SHORT).show();
				
			}else{
				
				if(result.equalsIgnoreCase("E"))
					Toast.makeText(MainActivity.this, "Can't Parse Excel File.", Toast.LENGTH_SHORT).show();
				else
					Toast.makeText(MainActivity.this, "File not exist!", Toast.LENGTH_SHORT).show();
				
			}
		}
    	
    }
    
    private class ExportExcelTask extends AsyncTask<Void, Void, Boolean>{

    	private ProgressDialog dialog;
    	
		@Override
		protected void onPreExecute() {
			//super.onPreExecute();
			dialog = util.showProgressView(MainActivity.this, "Exporting file...");
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			Boolean res = new ExcelParcer(dbManager, MainActivity.this).exportExcelData();
			
			return res;
		}
		

		@Override
		protected void onPostExecute(Boolean result) {
			//super.onPostExecute(result);
			if(dialog != null)
				dialog.dismiss();
			
			
			if(result)
				Toast.makeText(MainActivity.this, "File Exported Successfully.", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(MainActivity.this, "Problem Occur when Export file.", Toast.LENGTH_SHORT).show();
			
		}

    	
    }
    
    private void updateImportedFileName(){
    	
    	File sdcard = Environment.getExternalStorageDirectory();
    	File to = new File(sdcard,"KRAfinance_Imported.xls");
    	
    	Constants.KRA_FINANCE_DATA_EXCEL_FILE.renameTo(to);
    	
    	int pendingCount = util.getPendingAmountCount(dbManager);
    	
    	if(pendingCount >= 0){
    		pendingAmtIndicatorTxt.setVisibility(View.VISIBLE);
    		pendingAmtIndicatorTxt.setText(""+pendingCount);
    	}
    	

    }
    
}

package com.strobilanthes.forrestii.act.fragments;

import android.widget.AbsListView;

import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.slidingtab.ScrollTabHolder;

public abstract class ScrollTabHolderFragment extends BaseFragment<DBAccess> implements ScrollTabHolder {

	protected ScrollTabHolder mScrollTabHolder;

	public void setScrollTabHolder(ScrollTabHolder scrollTabHolder) {
		mScrollTabHolder = scrollTabHolder;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount, int pagePosition) {
		// nothing
	}

}
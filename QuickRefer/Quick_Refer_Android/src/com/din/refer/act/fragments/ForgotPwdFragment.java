package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.inject.Inject;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;
import com.strobilanthes.forrestii.model.pwd_reg.AbstractForgetPassword;
import com.strobilanthes.forrestii.model.pwd_reg.AbstractForgotPwdVerify;
import com.strobilanthes.forrestii.model.pwd_reg.PasswordReg;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.L;
import com.strobilanthes.forrestii.util.Util;


public class ForgotPwdFragment extends BaseFragment<DBAccess> {
	
	@InjectView(R.id.forgot_pwd_screen_countrycode)			private EditText edtCountryCode;
	@InjectView(R.id.forgot_pwd_screen_mobileno)			private EditText edtMobileNo;
	@InjectView(R.id.forgot_pwd_screen_otpcode)				private EditText edtOTPCode;
	@InjectView(R.id.forgot_pwd_screen_resend_otpcode)		private TextView txt_resendOTP;
	@InjectView(R.id.forgot_pwd_screen_otp_header_lyt)  	private LinearLayout forgotScreenOtpHeaderLyt;
	@InjectView(R.id.forgot_pwd_screen_btnDone)				private Button btn_done;
	
	@InjectView(R.id.forgot_pwd_screen_header)				private TextView forgotTxtHeader;

	@Inject PasswordReg passwordRegModel;
	private Typeface mFontLight,mFontDark;
	private String countryCodeId = "1";
	private String[] countryCodes;
	private String mobileno;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.frg_forgot_pwd, null);
		super.initilizeContents();

		return view;
	}
	
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		fontStyles();
		passwordRegModel.setContext(getActivity());
		btn_done.setOnClickListener(this);
		txt_resendOTP.setOnClickListener(this);
		String mob = getArguments().getString("mob");
		edtMobileNo.setText(mob != null ? mob : "");
		
		countryCodes = getResources().getStringArray(R.array.CountryCodes);
		
		//edtCountryCode.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.frg_countrycode_list_item,android.R.id.text1, countryCodes));
//		edtCountryCode.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view,
//					int position, long id) {
//				//String countryId = (String)parent.getItemAtPosition(position);
//				
//				//countryCodeId = countryId.split(",")[0];
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//				
//			}
//		});
		//edtCountryCode.setSelection(0);
		edtCountryCode.setText(countryCodes[0]);
		edtCountryCode.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loadCountryCode();
			}
		});
String total="A verification code has been sent to your mobile number if you have not yet received it CLICK HERE to resend it.";
		
		SpannableString ss1=  new SpannableString(total);
		    ss1.setSpan(new MyClickableSpan(total), 88, 98, 0);
		    ss1.setSpan(new RelativeSizeSpan(1.10f), 88, 98, 0);
		    txt_resendOTP.setText(ss1);
		    txt_resendOTP.setMovementMethod(LinkMovementMethod.getInstance());
	}
	public void loadCountryCode() {

	    
		   // final CharSequence[] items = { "John", "Michael", "Vincent", "Dalisay" };

		    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		    builder.setTitle("Choose your country code");
		    builder.setItems(countryCodes, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int position) {

		            // will toast your selection
		            //showToast("Name: " + items[item]);
		        	edtCountryCode.setText(countryCodes[position]);
		            dialog.dismiss();

		        }
		    }).show();
		}
	public void fontStyles() {

		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		forgotTxtHeader.setTypeface(mFontLight);
//		edtCountryCode.setTypeface(mFontDark);
		edtMobileNo.setTypeface(mFontDark);
		edtOTPCode.setTypeface(mFontDark);
		txt_resendOTP.setTypeface(mFontDark);
		btn_done.setTypeface(mFontDark);
	}
	@Override
	public boolean onBackPressed() {
		return super.onBackPressed();
	}


	@Override
	public void onResume(){
		super.onResume();
		passwordRegModel.addChangeListener(this);
		
	}
	
	@Override
	public void onPause(){ 
		super.onPause();
		passwordRegModel.removeChangeListener(this);
		Util.hideSoftKeyboard(getActivity(), edtMobileNo);
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if(event.getPropertyName().equals("forget_password")){
			AbstractForgetPassword abstractForgetPassword = passwordRegModel.getForgetPasswordDetails();
			if(abstractForgetPassword != null){
				if(abstractForgetPassword.getStatus() == 1){
					
					getHelper().insertUserMaster(1234, edtMobileNo.getText().toString().trim(),"");
					
					forgotScreenOtpHeaderLyt.setVisibility(View.VISIBLE);
					
					btn_done.setText("Verify Code");
					
					edtOTPCode.setText(""+abstractForgetPassword.getResponse().getOTPCode());
					
					L.ToastMessage(getActivity(), "Verification code has been sent to your mobile");
					
				}else{
					Util.showAlert(getActivity(),"Message", ""+abstractForgetPassword.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
		}else if(event.getPropertyName().equals("verify_forget_password")){
			AbstractForgotPwdVerify abstractForgotPwdVerify = passwordRegModel.getVerifyForgetPasswordDetails();
			if(abstractForgotPwdVerify != null){
				if(abstractForgotPwdVerify.getStatus() == 1){
					
					updateOTPLogin();
					//L.ToastMessage(AuthenticationActivity.this, "OTP number sent to your mobile");
				}else{
					Util.showAlert(getActivity(),"Message", ""+ abstractForgotPwdVerify.getError());
				}
			}else{
				Util.showAlert(getActivity(),"Message", "Request Failed.");
			}
		} 
	}

	private void updateOTPLogin(){
		
		FragmentTransaction fragmentTransaction =  getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,R.anim.in_from_left, R.anim.out_to_right);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.login_root, new SetPasswordFragment());
        fragmentTransaction.commit();
        
		//getHelper().insertPageId(Constant.PAGE_PASS_REG_);
		//getHelper().updateUserId(authentication.getUserId().toString());
        //L.ToastMessage(AuthenticationActivity.this, "Login = "+authentication.getIsSuccess());
        
	}
	
	@Override
	public void onClick(View v) {
		mobileno = edtMobileNo.getText().toString();
		switch (v.getId()) {
		case R.id.forgot_pwd_screen_btnDone:
			Util.hideSoftKeyboard(getActivity(), v);
			if(mobileno.length() <= 0){
				edtMobileNo.requestFocus();

				edtMobileNo.setError("Please enter your mobile number");
			} else {
				// sign up
				if (Util.isConnectedNetwork(getActivity())) {
						if(Util.isValidPhoneNumber(mobileno)&&PhoneNumberUtils.isGlobalPhoneNumber(mobileno) && mobileno.length() >= 10){
							
							if(forgotScreenOtpHeaderLyt.getVisibility() == View.VISIBLE){
								if(edtOTPCode.getText().length() > 0){
									try {
										 JSONObject object = new JSONObject();
										 object.put("OTPCode", edtOTPCode.getText().toString().trim());
										 object.put("Mobile", mobileno);
										 passwordRegModel.requestVerifyForgetPassword(Constant.VERIFY_FORGET_PASSWORD,object);
										 //queue.add(jsObjRequest);
									} catch (JSONException e) {
										e.printStackTrace();
									}
								} else {
									edtOTPCode.requestFocus();

									edtOTPCode.setError("Please enter verification code");
								}
								
							} else {
								try {
									 JSONObject object = new JSONObject();
									 object.put("mobile", mobileno);
									 object.put("ObjCountryMaster",new JSONObject().put("countryId",countryCodeId));
									 passwordRegModel.requestForgetPassword(Constant.FORGET_PASSWORD,object);
									 //queue.add(jsObjRequest);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							
						}else{
							edtMobileNo.requestFocus();

							edtMobileNo.setError("Please enter valid mobile number");
						} 
					//}
				} else {
					Util.showAlert(getActivity(),"Message","Please double check your internet connection.");
				}
			}
			break;
		case R.id.forgot_pwd_screen_resend_otpcode:
//			if(mobileno.length() <= 0){
//				edtMobileNo.setError("Please enter your mobile number.");
//			} else {
//				// sign up
//				if (Util.isConnectedNetwork(getActivity())) {
//					if(Util.isValidPhoneNumber(mobileno)&&PhoneNumberUtils.isGlobalPhoneNumber(mobileno) && mobileno.length() >= 10){
//						L.ToastMessage(getActivity(), "Resending OTP....");
//						edtOTPCode.setText("");
//						try {
//							JSONObject object = new JSONObject();
//							object.put("mobile", mobileno);
//							passwordRegModel.requestForgetPassword(Constant.FORGET_PASSWORD,object);
//						} catch (JSONException e) {
//							e.printStackTrace();
//						}
//					}
//				} else {
//					Util.showAlert(getActivity(),"Message","Please double check your internet connection.");
//				}
//			}
			break;
		default:
			break;
		}
	}
	 class MyClickableSpan extends ClickableSpan{     
		   String clicked;
		   public MyClickableSpan(String string)  
		   {
		    super();
		    clicked =string;
		   }
		   public void onClick(View tv) 
		   {
			   if(mobileno.length() <= 0){
					edtMobileNo.setError("Please enter your mobile number.");
				} else {
					// sign up
					if (Util.isConnectedNetwork(getActivity())) {
						if(Util.isValidPhoneNumber(mobileno)&&PhoneNumberUtils.isGlobalPhoneNumber(mobileno) && mobileno.length() >= 10){
							L.ToastMessage(getActivity(), "Resending OTP....");
							edtOTPCode.setText("");
							try {
								JSONObject object = new JSONObject();
								object.put("mobile", mobileno);
								passwordRegModel.requestForgetPassword(Constant.FORGET_PASSWORD,object);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					} else {
						Util.showAlert(getActivity(),"Message","Please double check your internet connection.");
					}
				}
		   }
		   public void updateDrawState(TextPaint ds)
		   {
		     ds.setColor(getResources().getColor(R.color.forrestii_pink_color));//set text color 
		     ds.setUnderlineText(true); // set to false to remove underline
		   }
		  } 
}

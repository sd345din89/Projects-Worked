package com.strobilanthes.forrestii.model.ratings;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Singleton;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;

@Singleton
public class RatingsImpl implements Ratings, VolleyTasksListener {
	
	private AbstractRatings abstractRatings;
	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}

	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}

	public void handleResult(String method_name, JSONObject jsonObject) {
		
		AbstractRatings previousResult = abstractRatings;
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.serializeNulls();
		Gson gson = gsonBuilder.create();	
		abstractRatings = gson.fromJson(jsonObject.toString(), AbstractRatings.class);
		
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				previousResult, abstractRatings);
		
		notifyListeners(event);
	}
	public void handleError(String method_name, Exception e) {
		AbstractRatings previousResult = abstractRatings;
		abstractRatings = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				previousResult, null);
		notifyListeners(event);
	}

	@Override
	public void ratingCard(String url, JSONObject postdata) {
		
		
		
	}

	@Override
	public AbstractRatings getRatingResponse() {
		
		return abstractRatings;
	}

	@Override
	public void setContext(Activity context) {
		
		
	}

	@Override
	public Activity getContext() {
		
		return null;
	}
	

}

package com.strobilanthes.forrestii.model.settings;

import java.io.Serializable;

public class SettingsDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	private int settingId;
	private boolean friendReqReceived;
	private boolean	 receiveReferralCard;
	private boolean sendReferralCard;
	
	public int getSettingId() {
		return settingId;
	}

	public void setSettingId(int settingId) {
		this.settingId = settingId;
	}

	public boolean isFriendReqReceived() {
		return friendReqReceived;
	}

	public void setFriendReqReceived(boolean friendReqReceived) {
		this.friendReqReceived = friendReqReceived;
	}

	public boolean isReceiveReferralCard() {
		return receiveReferralCard;
	}

	public void setReceiveReferralCard(boolean receiveReferralCard) {
		this.receiveReferralCard = receiveReferralCard;
	}

	public boolean isSendReferralCard() {
		return sendReferralCard;
	}

	public void setSendReferralCard(boolean sendReferralCard) {
		this.sendReferralCard = sendReferralCard;
	}

}

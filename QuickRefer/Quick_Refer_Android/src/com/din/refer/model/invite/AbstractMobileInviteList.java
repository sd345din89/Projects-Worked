package com.strobilanthes.forrestii.model.invite;

import java.util.ArrayList;

import com.strobilanthes.forrestii.model.userprofile.AbstractUserProfile;

public class AbstractMobileInviteList {
	private int status;
	private String error;
	private AbstractInviteMobileResponse response;

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public AbstractInviteMobileResponse getResponse() {
		return response;
	}
	
	public void setResponse(AbstractInviteMobileResponse response) {
		this.response = response;
	}
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

	//response
	public class AbstractInviteMobileResponse{
		
		private ArrayList<AbstractUserProfile>lstAddUser;
		
		private ArrayList<String>InviteList;
		
		private ArrayList<AbstractUserProfile>lstFriendUserList;
		
		public ArrayList<AbstractUserProfile> getLstAddUser() {
			return lstAddUser;
		}
		public void setLstAddUser(ArrayList<AbstractUserProfile> lstAddUser) {
			this.lstAddUser = lstAddUser;
		}
		public ArrayList<String> getInviteList() {
			return InviteList;
		}
		public void setInviteList(ArrayList<String> inviteList) {
			InviteList = inviteList;
		}
		public ArrayList<AbstractUserProfile> getLstFriendUserList() {
			return lstFriendUserList;
		}
		public void setLstFriendUserList(ArrayList<AbstractUserProfile> lstFriendUserList) {
			this.lstFriendUserList = lstFriendUserList;
		}

	}
}
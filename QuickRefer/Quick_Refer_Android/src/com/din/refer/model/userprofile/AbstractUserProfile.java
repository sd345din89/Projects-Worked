package com.strobilanthes.forrestii.model.userprofile;

import java.io.Serializable;
import java.util.ArrayList;

import com.strobilanthes.forrestii.model.skillset.UserSkillsSet;


public class AbstractUserProfile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String error;
	private int status;
	
	private int userId;
	private String firstName;
	private String lastName;
	private String gender;
	private String address;
	private int countryId;
	private int stateId;
	private int cityId;
	private int pincodeId;
	private String mobile;
	private String dob;
	private String phone;
	private String email;
	private String photoID;
	private float totalRating;
	private String cardName;
	private int totalCoin;
	private boolean profileCompleted;
	private int profileCompletedStatus;
	private String about;
	private String pendingEMail;
	private String pendingMobileNo;
	private int contactCount;
	private ArrayList<UserCardInfo>	 userCardInfo;
	private ArrayList<UserSkillsSet> userSkillSets;
	
	public ArrayList<UserCardInfo> getUserCardInfo() {
		return userCardInfo;
	}
	public void setUserCardInfo(ArrayList<UserCardInfo> userCardInfo) {
		this.userCardInfo = userCardInfo;
	}
	public ArrayList<UserSkillsSet> getUserSkillSets() {
		return userSkillSets;
	}
	public void setUserSkillSets(ArrayList<UserSkillsSet> userSkillSets) {
		this.userSkillSets = userSkillSets;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhotoID() {
		return photoID;
	}
	public void setPhotoID(String photoID) {
		this.photoID = photoID;
	}
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {	
		this.status = status;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public float getTotalRating() {
		return totalRating;
	}
	public void setTotalRating(float totalRating) {
		this.totalRating = totalRating;
	}
	public String getCardName() {
		return cardName;
	}
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	public int getTotalCoin() {
		return totalCoin;
	}
	public void setTotalCoin(int totalCoin) {
		this.totalCoin = totalCoin;
	}
	public boolean isProfileCompleted() {
		return profileCompleted;
	}
	public void setProfileCompleted(boolean profileCompleted) {
		this.profileCompleted = profileCompleted;
	}
	public int getProfileCompletedStatus() {
		return profileCompletedStatus;
	}
	public void setProfileCompletedStatus(int profileCompletedStatus) {
		this.profileCompletedStatus = profileCompletedStatus;
	}
	
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	public String getPendingEMail() {
		return pendingEMail;
	}
	public void setPendingEMail(String pendingEMail) {
		this.pendingEMail = pendingEMail;
	}

	public String getPendingMobileNo() {
		return pendingMobileNo;
	}
	public void setPendingMobileNo(String pendingMobileNo) {
		this.pendingMobileNo = pendingMobileNo;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public int getPincodeId() {
		return pincodeId;
	}
	public void setPincodeId(int pincodeId) {
		this.pincodeId = pincodeId;
	}
	public int getContactCount() {
		return contactCount;
	}
	public void setContactCount(int contactCount) {
		this.contactCount = contactCount;
	}
	public class UserCardInfo implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private String cardName;
		private TemplateMaster templateMaster;
		
		public String getCardName() {
			return cardName;
		}

		public void setCardName(String cardName) {
			this.cardName = cardName;
		}

		public TemplateMaster getTemplateMaster() {
			return templateMaster;
		}

		public void setTemplateMaster(TemplateMaster templateMaster) {
			this.templateMaster = templateMaster;
		}

		public class TemplateMaster implements Serializable{
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			private int templateId;
			private String templateName;
			public int getTemplateId() {
				return templateId;
			}
			public void setTemplateId(int templateId) {
				this.templateId = templateId;
			}
			public String getTemplateName() {
				return templateName;
			}
			public void setTemplateName(String templateName) {
				this.templateName = templateName;
			}
			
		}
	}

}

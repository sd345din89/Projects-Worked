package com.strobilanthes.forrestii.model.logout;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface Logout extends ForrestiiSubject{
	void requestLogoutDetails(String url,JSONObject postdata);
	AbstractLogout getAuthDetails();
	
}

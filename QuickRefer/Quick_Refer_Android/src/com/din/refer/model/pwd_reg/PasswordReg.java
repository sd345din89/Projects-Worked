package com.strobilanthes.forrestii.model.pwd_reg;

import org.json.JSONObject;

import com.strobilanthes.forrestii.model.general.ForrestiiSubject;

public interface PasswordReg extends ForrestiiSubject{
	
	void requestPwdRegistrationDetails(String url,JSONObject postdata);
	void doValidation(String content);
	AbstractPasswordReg getPassRegDetails();
	
	void requestForgetPassword(String url,JSONObject postdata);
	AbstractForgetPassword getForgetPasswordDetails();
	
	void requestVerifyForgetPassword(String url,JSONObject postdata);
	AbstractForgotPwdVerify getVerifyForgetPasswordDetails();
}

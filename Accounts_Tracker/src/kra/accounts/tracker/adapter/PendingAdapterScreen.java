package kra.accounts.tracker.adapter;


import java.util.ArrayList;
import java.util.Calendar;

import org.w3c.dom.Text;

import kra.accounts.tracker.AccountDetailsActivity;
import kra.accounts.tracker.R;
import kra.accounts.tracker.database.KraFinanceDbManager;
import kra.accounts.tracker.models.Constants;
import kra.accounts.tracker.models.CustomerAccountDetails;
import kra.accounts.tracker.models.Util;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PendingAdapterScreen extends BaseAdapter{
	private Context mcontext;
	
	private LayoutInflater mInflater;
	
	private ArrayList<CustomerAccountDetails> customerAccountDetails;
	
	private Util util;
	
	private KraFinanceDbManager dbManager;
	
	public PendingAdapterScreen(Activity activity, ArrayList<CustomerAccountDetails> customerAccountDetails, Util util, KraFinanceDbManager dbManager) {
		
		this.mcontext=activity;
		
		this.customerAccountDetails = customerAccountDetails;
		
		mInflater = (LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.dbManager = dbManager;
		
		this.util = util;
		
	}

	@Override
	public int getCount() {
		return customerAccountDetails.size();
	}

	@Override
	public Object getItem(int arg0) {
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder=null;
		
		if(convertView==null){
			holder=new ViewHolder();
			convertView=mInflater.inflate(R.layout.pending_listadapter_screen, null);
			holder.pending_adapter_lyt=(RelativeLayout)convertView.findViewById(R.id.pending_adapter_lyt);
			
			holder.amountTxt = (TextView)convertView.findViewById(R.id.principal_amt_txt);
			holder.cusNameTxt = (TextView)convertView.findViewById(R.id.pending_adapter_name_txt);
			holder.dueDateTxt = (TextView)convertView.findViewById(R.id.duedate_txt);
			holder.pendingCountTxt = (TextView)convertView.findViewById(R.id.pending_count_txt);
			
			holder.accountCountTxt = (TextView)convertView.findViewById(R.id.pending_account_count_txt);

			
			convertView.setTag(holder);
			
			
		}else{
			holder=(ViewHolder) convertView.getTag();
		}
			
		holder.pending_adapter_lyt.setBackgroundResource(R.drawable.pending_row_bg);
		if(position % 2 == 0){
			holder.pending_adapter_lyt.setBackgroundResource(R.color.white);
		}
		
		final CustomerAccountDetails accountDetails = customerAccountDetails.get(position);
		
		if(accountDetails != null){
			
			holder.amountTxt.setText(""+util.formatAmount(accountDetails.getprincipalAmount()));
			holder.cusNameTxt.setText(""+accountDetails.getCustomerName());
			holder.dueDateTxt.setText(""+util.convertLongToDate(accountDetails.getinsertDueFromDate(), "MMM-yyyy"));
			
			
			
			//int dueDateInMonth = util.countDueDateInMonth(""+util.getPendingAmountCountForCustomer(dbManager, accountDetails.getcustomerId(), false));
			int dueDateInMonth = util.getNumOfMonths(util.getPendingAmountCountForCustomer(dbManager, accountDetails.getcustomerId(), false), Calendar.getInstance().getTimeInMillis());
			if(Constants.DEBUG_LOG)Log.d("CHECK", position+">>dueDateInMonth="+dueDateInMonth);
			
			
			holder.pendingCountTxt.setText(""+dueDateInMonth);
			
			
			
			String accountCount = util.getAccountCount(dbManager, accountDetails.getcustomerId());
			if(accountCount != null){
				holder.accountCountTxt.setVisibility(View.VISIBLE);
				holder.accountCountTxt.setText(""+accountCount);
			}else{
				holder.accountCountTxt.setVisibility(View.INVISIBLE);
			}
			
			holder.pending_adapter_lyt.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					Intent intent = new Intent(mcontext, AccountDetailsActivity.class);
					intent.putExtra("CustomerId", Integer.parseInt(accountDetails.getcustomerId()));
					intent.putExtra("SortOrder", 0);
					mcontext.startActivity(intent);
					
				}
			});
		}
		
		
		return convertView;
	}
	
	class ViewHolder{
		
		RelativeLayout pending_adapter_lyt;
		
		TextView cusNameTxt;
		TextView amountTxt;
		TextView dueDateTxt;
		TextView pendingCountTxt;
		
		TextView accountCountTxt;
	}
}

//------------------------------------------------------------------------------
//
//    (c) Copyright 2014.Dinesh Kumar M. All Rights Reserved.
//
//------------------------------------------------------------------------------

package com.din.mobileclone;

import static com.din.mobileclone.CommonUtilities.SENDER_ID;
import static com.din.mobileclone.CommonUtilities.SERVER_URL;

import java.io.IOException;
import java.lang.annotation.Documented;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Profile;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuInflater;
import com.din.mobileclone.R;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class MainActivity extends SherlockFragmentActivity implements
																ConnectionCallbacks,
																OnConnectionFailedListener,
																LocationListener{

	public static ArrayList<HashMap<String, String>> result;
	AsyncTask<Void, Void, Void> mRegisterTask;
	 
	Location mLocation = null;
	
    private LocationClient mLocationClient;
   
    // These settings are the same as the settings for the map. They will in fact give you updates
    // at the maximal rates currently possible.
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(5000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.parent_layout);
		
		checkNotNull(SERVER_URL, "SERVER_URL");
        checkNotNull(SENDER_ID, "SENDER_ID");
        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(this);
        //setContentView(R.layout.main);
      
        final String regId = GCMRegistrar.getRegistrationId(this);
        if (regId.equals("")) {
            // Automatically registers application on startup.
            GCMRegistrar.register(this, SENDER_ID);
        } else {
            // Device is already registered on GCM, check server.
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                // Skips registration.
               // mDisplay.append(getString(R.string.already_registered) + "\n");
            } else {
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                final Context context = this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        boolean registered =
                                ServerUtilities.register(context, regId);
                        if (!registered) {
                            GCMRegistrar.unregister(context);
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);
            }
        }
        
		//getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("MClone");
		
	}

	 @Override
	    protected void onResume() {
	        super.onResume();
	        setUpLocationClientIfNeeded();
	        mLocationClient.connect();
	    }

	    @Override
	    public void onPause() {
	        super.onPause();
	        if (mLocationClient != null) {
	            mLocationClient.disconnect();
	        }
	    }

	@Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
			
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
        super.onDestroy();
    }
	 
	private void checkNotNull(Object reference, String name) {
        if (reference == null) {
            throw new NullPointerException(
                    getString(R.string.error_config, name));
        }
    }
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		
		MenuInflater actionbarmenuinflater = getSupportMenuInflater();
		actionbarmenuinflater.inflate(R.menu.action_bar_menu, menu);
		
		/*getSupportActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#68a339")));*/
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);

		switch (item.getItemId()) {
		case R.id.menu_settings:
			startActivity(new Intent(MainActivity.this, UsersSettingsActivity.class));
			
			/*AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
			Account[] list = manager.getAccounts();
			for (int i = 0; i < list.length; i++) {
				Log.d("Account Information", "Contact Name = "+list[i].name);
				Log.d("Account Information", "Contact Type = "+list[i].type);
				
				//Log.d("Accounts", "Contact Type = "+manager.getUserData(list[i], "key"));
			}
			
			TelephonyManager tMgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
			String mPhoneNumber = tMgr.getSimSerialNumber();
			Log.d("Phone Number", "Num = "+mPhoneNumber);
			
			final String[] SELF_PROJECTION = new String[] { Phone._ID,
	                Phone.DISPLAY_NAME,Phone.DISPLAY_NAME};
			Cursor cursor = getContentResolver().query(
	                Profile.CONTENT_URI, SELF_PROJECTION, null, null, null);
			cursor.moveToFirst();
			do {
				cursor.getColumnNames();
			} while (cursor.moveToNext());
			for (int i = 0; i < cursor.getCount(); i++) {
				Log.d("Email", "Num = "+cursor.getString(i));
				Log.d("Name", "Num = "+cursor.getString(i));
			}
			Log.d("Email", "Num = "+cursor.getString(0));
			Log.d("Name", "Num = "+cursor.getString(1));
			OwnerInfo ownerInfo = new OwnerInfo(this);*/
			
			break;

		case R.id.menu_trackmobile:
			//setMobileDataEnabled(MainActivity.this,true);
			//EnableInternet(this);
			/*try {
		        Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", "reboot" });
		        proc.waitFor();
		    } catch (Exception ex) {
		        Log.i("Log", "Could not reboot", ex);
		    }*/
			startActivity(new Intent(MainActivity.this, MsgAfterCallActivity.class));
			break;

		case R.id.menu_displayimages:
			startActivity(new Intent(MainActivity.this, LoadImageActivity.class));
			break;
	
		case R.id.menu_downloadcontents_todropbox:
			startActivity(new Intent(MainActivity.this, CloudDownloadActivity.class));
			break;
		case R.id.menu_hereiam:
			//DisableInternet(this);
			startActivity(new Intent(MainActivity.this, MyLocationDemoActivity.class));
			//getLocation(this);
			break;
		}

		return true;

	}

	
    private void setUpLocationClientIfNeeded() {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(
                    getApplicationContext(),
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
            
        }
    }

   
    @Override
    public void onLocationChanged(Location location) {
    	//PreferenceManager preferenceManager = getSharedPreferences(name, mode)
    	
    	
    	SharedPreferences preferenceManager = PreferenceManager.getDefaultSharedPreferences(this);
    	
    	if(location != null){
       	 	Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(this, Locale.getDefault());
            try {
				addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            if(addresses != null){
            	 String address = addresses.get(0).getAddressLine(0);
 	            String city = addresses.get(0).getAddressLine(1);
 	            String country = addresses.get(0).getAddressLine(2);
 	            
 	            if(address != null && city!= null && country != null)
 	            	preferenceManager.edit().putString("cur_location","Address: "+address+"\n"+"City: "+city+"\n"+"Country: "+country).apply();
 	            else
 	            	preferenceManager.edit().putString("cur_location", "Latitude : "+location.getLatitude()+"\n"+"Longitude : "+location.getLongitude()).apply();
            }else{
            	preferenceManager.edit().putString("cur_location", "Latitude : "+location.getLatitude()+"\n"+"Longitude : "+location.getLongitude()).apply();
            }
           
       }else{
    	   preferenceManager.edit().putString("cur_location", "No Location Data Found").apply();
       }
    	
    	Toast.makeText(this, "Current Location = "+preferenceManager.getString("cur_location", "Test Data"), Toast.LENGTH_SHORT).show();
    	
    }

   
    @Override
    public void onConnected(Bundle connectionHint) {
        mLocationClient.requestLocationUpdates(
                REQUEST,
                this);  // LocationListener
    }

    @Override
    public void onDisconnected() {
        // Do nothing
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Do nothing
    }
  
	public  void EnableInternet(Context mycontext)
    {
        try {
            Log.i("Reached Enable", "I am here");
            setMobileDataEnabled(mycontext,true);
        } catch (NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public  void DisableInternet(Context mycontext)
    {
        try {
            Log.i("Reached Disable", "I am here");
            setMobileDataEnabled(mycontext,false);
        } catch (NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    private void setMobileDataEnabled(Context context , boolean enabled) throws NoSuchFieldException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final Class conmanClass = Class.forName(conman.getClass().getName());

        final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
        iConnectivityManagerField.setAccessible(true);
        final Object iConnectivityManager = iConnectivityManagerField.get(conman);
        final Class iConnectivityManagerClass =  Class.forName(iConnectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
     }

	

}

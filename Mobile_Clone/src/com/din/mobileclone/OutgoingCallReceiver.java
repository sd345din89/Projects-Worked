//------------------------------------------------------------------------------
//
//    (c) Copyright 2014.Dinesh Kumar M. All Rights Reserved.
//
//------------------------------------------------------------------------------
package com.din.mobileclone;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;
 
public class OutgoingCallReceiver extends BroadcastReceiver {
	// private static final String INTENT_PHONE_NUMBER = "android.intent.extra.PHONE_NUMBER";
	
		SharedPreferences preferenceManager;
		
	    @Override
	    public void onReceive(Context context, Intent intent) {
	            Bundle bundle = intent.getExtras();

	            if(null == bundle)
	                    return;


	            String phonenumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

	            Log.i("OutgoingCallReceiver",phonenumber);
	            Log.i("OutgoingCallReceiver",bundle.toString());

	            String info = "Detect Calls sample application\nOutgoing number: " + phonenumber;

	            preferenceManager = PreferenceManager.getDefaultSharedPreferences(context);
	            
	            sendSMS(context,"This is the number some one calling : "+phonenumber+"\n"+""+preferenceManager.getString("cur_location", "No Location Found"));
	            
	    }
	    
	    private void sendSMS(Context context, String textMessage){
	    	String SENT = "SMS_SENT";
	    	String DELIVERED = "SMS_DELIVERED";
	    
	    	
	    	boolean canSendMessage = preferenceManager.getBoolean("preferences_enable_msg_after_call", false);
	    	
	    	if(canSendMessage){
	    		String sentNumber = ""+preferenceManager.getString("preferences_enter_mobile_num", "mobilenum");
		    	
	    		Toast.makeText(context, "Detect Calls sample application\nOutgoing number", Toast.LENGTH_LONG).show();
	    		 
		    	PendingIntent sendPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
		    	PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);
		    	
		    	
		    	SmsManager smsManager = SmsManager.getDefault();
		    	Log.d("Send SMS", "Number = "+sentNumber);
		    	Log.d("Send Message", "Msg = "+textMessage);
		    	smsManager.sendTextMessage(sentNumber, null, textMessage, sendPI, deliveredPI);
	    	}
	    	
	    }
}


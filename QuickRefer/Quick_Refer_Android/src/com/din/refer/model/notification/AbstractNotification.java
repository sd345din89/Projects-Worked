package com.strobilanthes.forrestii.model.notification;

import java.util.ArrayList;

public class AbstractNotification {
	
	private int status;
	private String error;
	private ArrayList<NotificaitonList> response;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ArrayList<NotificaitonList> getResponse() {
		return response;
	}

	public void setResponse(ArrayList<NotificaitonList> response) {
		this.response = response;
	}
}

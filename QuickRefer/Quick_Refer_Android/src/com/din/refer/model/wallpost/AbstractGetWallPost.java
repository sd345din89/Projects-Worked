package com.strobilanthes.forrestii.model.wallpost;

import java.util.ArrayList;

import com.strobilanthes.forrestii.model.general.AbstractDashboard;

public class AbstractGetWallPost{
	
	private int status;
	private String error;
	private GetWallPostResponse response;
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public GetWallPostResponse getResponse() {
		return response;
	}

	public void setResponse(GetWallPostResponse response) {
		this.response = response;
	}

	public class GetWallPostResponse{
		private ArrayList<GetWallPost> Wall;
		//private ArrayList<GetWallPost> Wall;
		private AbstractDashboard DashBoard;
		public ArrayList<GetWallPost> getWallPost() {
			return Wall;
		}

		public void setWallPost(ArrayList<GetWallPost> wall) {
			Wall = wall;
		}
		
		public AbstractDashboard getDashBoard() {
			return DashBoard;
		}

		public void setDashBoard(AbstractDashboard dashBoard) {
			DashBoard = dashBoard;
		}

	}
	
}

package com.strobilanthes.forrestii.model.referral;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.strobilanthes.forrestii.ForrestiiApplication;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;
import com.strobilanthes.forrestii.model.ratings.AbstractRatings;
import com.strobilanthes.forrestii.util.Constant;
import com.strobilanthes.forrestii.util.CustomProgressbar;
import com.strobilanthes.forrestii.volleytasks.RatingsVolleyTasks;
import com.strobilanthes.forrestii.volleytasks.ReferralVolleyTasks;

public class ReferralImpl implements Referral,VolleyTasksListener{

	private List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private String createFriendReferral = "createFriendReferral";
	private String pickACard = "pickACard";
	private String RatingACard= "RatingACard";
	private String unPickACard= "unPickACard";

	
	private AbstractCreateReferral abstractCreateReferral;
	private AbstractRatings abstractRatings;
	private AbstractPickandUnPickCard abstractPickandUnpickCard;
	private Activity context;
	private CustomProgressbar mCustomProgressbar;
	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		listener.add(newListener);
	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		listener.remove(listenerToRemove);
	}
	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}
	@Override
	public void handleResult(String method_name, JSONObject jsonObject) {
		
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		if(method_name.equals(createFriendReferral)){
			AbstractCreateReferral previousResult = abstractCreateReferral();


			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractCreateReferral = gson.fromJson(jsonObject.toString(), AbstractCreateReferral.class);


			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractCreateReferral);
			
			notifyListeners(event);
			
		}else if(method_name.equals(pickACard)){
			AbstractPickandUnPickCard previousResult = abstractPickandUnpickCard;


			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractPickandUnpickCard = gson.fromJson(jsonObject.toString(), AbstractPickandUnPickCard.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractPickandUnpickCard);
			
			notifyListeners(event);
		}else if(method_name.equals(RatingACard)){
			
			AbstractRatings previousResult = abstractRatings;
			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractRatings = gson.fromJson(jsonObject.toString(), AbstractRatings.class);
			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractRatings);
			
			notifyListeners(event);
		}else if(method_name.equals(unPickACard)){
			
			AbstractPickandUnPickCard previousResult = abstractPickandUnpickCard;


			GsonBuilder gsonBuilder = new GsonBuilder();
			gsonBuilder.serializeNulls();
			Gson gson = gsonBuilder.create();	
			abstractPickandUnpickCard = gson.fromJson(jsonObject.toString(), AbstractPickandUnPickCard.class);

			PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
					previousResult, abstractPickandUnpickCard);
			
			notifyListeners(event);
		}
		
	}
	@Override
	public void handleError(String method_name, Exception e) {
		
		if(mCustomProgressbar != null){
			mCustomProgressbar.dismiss();
		}
		
		abstractPickandUnpickCard = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				null, null);
		notifyListeners(event);
	}
	
	@Override
	public AbstractPickandUnPickCard getPickCardResponse() {
		return abstractPickandUnpickCard;
	}
	

	@Override
	public void createFriendReferral(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new ReferralVolleyTasks(createFriendReferral, this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void pickACard(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new ReferralVolleyTasks(pickACard, this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void ratingCard(String url, JSONObject postdata) {
		
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new RatingsVolleyTasks(RatingACard, this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	@Override
	public void unPickACard(String url, JSONObject postdata) {
		// TODO Auto-generated method stub
		mCustomProgressbar = new CustomProgressbar(getContext());
		mCustomProgressbar.show();
		
		ForrestiiApplication.getInstance().addToRequestQueue(
				new RatingsVolleyTasks(unPickACard, this, url, postdata)
						.setRetryPolicy(new DefaultRetryPolicy(
								Constant.SOCKET_TIMEOUT,
								DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
								DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	
	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

	@Override
	public AbstractCreateReferral abstractCreateReferral() {
		return abstractCreateReferral;
	}

	@Override
	public AbstractRatings getRatingResponse() {
		return abstractRatings;
	}

	

	@Override
	public AbstractPickandUnPickCard getUnPickCardResponse() {
		// TODO Auto-generated method stub
		return abstractPickandUnpickCard;
	}
	
}

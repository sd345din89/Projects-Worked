package com.strobilanthes.forrestii.model.lookuplist;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Singleton;
import com.strobilanthes.forrestii.model.general.VolleyTasksListener;

@Singleton
public class LookUpListImpl implements LookUp, VolleyTasksListener {
	private AbstractLookUpList abstractQuickCard;
	List<PropertyChangeListener> listener = new ArrayList<PropertyChangeListener>();
	private Activity context;

	@Override
	public void addChangeListener(PropertyChangeListener newListener) {
		// TODO Auto-generated method stub
		listener.add(newListener);

	}

	@Override
	public void removeChangeListener(PropertyChangeListener listenerToRemove) {
		// TODO Auto-generated method stub
		listener.remove(listenerToRemove);

	}

	protected void notifyListeners(PropertyChangeEvent evt) {
		for (Iterator<PropertyChangeListener> iterator = listener.iterator(); iterator
				.hasNext();) {
			PropertyChangeListener name = (PropertyChangeListener) iterator
					.next();
			name.propertyChange(evt);
		}
	}

	@Override
	public AbstractLookUpList getQuickcardDetails() {
		return abstractQuickCard;
	}

	@Override
	public void handleResult(String method_name, JSONObject jsonobject) {
		if(method_name.equals("createquickcard")){
		AbstractLookUpList previousResult = abstractQuickCard;

		AbstractLookUpList aQuickCard = new AbstractLookUpList();
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.serializeNulls();
		Gson gson = gsonBuilder.create();	
		aQuickCard = gson.fromJson(jsonobject.toString(), AbstractLookUpList.class);



		abstractQuickCard = aQuickCard;

		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				previousResult, abstractQuickCard);
		notifyListeners(event);
	}
	}

	@Override
	public void handleError(String method_name, Exception e) {
		AbstractLookUpList previousResult = abstractQuickCard;
		abstractQuickCard = null;
		PropertyChangeEvent event = new PropertyChangeEvent(this, method_name,
				previousResult, null);
		notifyListeners(event);
	}

	@Override
	public void createQuickCard(String url, JSONObject postdata) {
//		ForrestiiApplication
//				.getInstance()
//				.addToRequestQueue(
//						new QuickcardVolleyTasks("createquickcard", this, url,
//								postdata)
//								.setRetryPolicy(new DefaultRetryPolicy(
//										Constant.SOCKET_TIMEOUT,
//										DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//										DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}

	@Override
	public void updateQuickCard(String url, JSONObject postdata) {
//		ForrestiiApplication
//				.getInstance()
//				.addToRequestQueue(
//						new QuickcardVolleyTasks("updatequickcard", this, url,
//								postdata)
//								.setRetryPolicy(new DefaultRetryPolicy(
//										Constant.SOCKET_TIMEOUT,
//										DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//										DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));
	}
	
	@Override
	public void setContext(Activity context) {
		this.context = context;
	}

	@Override
	public Activity getContext() {
		return context;
	}

}

package kra.accounts.tracker.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class KraFinanceDbManager extends SQLiteOpenHelper{
	
	private String TAG="KRA_Finance_DataBase";
	private static final int DATABASE_VRSION = 2;
	private static String DATABASE_NAME="KRA_Finance.db";

	public KraFinanceDbManager(Context context) {
		
		super(context, DATABASE_NAME, null, DATABASE_VRSION);
		
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		try{
			
			String CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS LoginUser(userName CHAR(20), " +
					"password CHAR(20));";
			
			db.execSQL(CREATE_TABLE_QUERY);
			Log.i(TAG, "LoginUser Table created");
			
			/*Customer Master
			 ================
			customerId 	- integer
			name 		- Char(50)
			address 	- Char(250)
			ph1			-char(15)
			ph2			-char(15)*/

			CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS CustomerMaster(customerId INTEGER primary key autoincrement, " +
																					"name CHAR(50)," +
																					"address CHAR(250),"+
																					"ContactNumber CHAR(15));";
			db.execSQL(CREATE_TABLE_QUERY);
			Log.i(TAG, "CustomerMaster Table created");
			
			
			/*Account Master
			==============
			accountId	- integer
			accountName 	- char(5)
			desc		- Char(250)*/

			
			CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS AccountMaster (accountId INTEGER primary key autoincrement, " +
																			"accountName CHAR(10)," +
																			"desc CHAR(50));";
			db.execSQL(CREATE_TABLE_QUERY);
			Log.i(TAG, "AccountMaster Table created");
			
			
			/*CustomerAccount
			===============
			custAccountId	-integer
			customerId	-integer
			accountId	-integer
			principalAmt	-float
			principalDate	-dateTime
			interestPaidUptoDate - DateTime
			security - y/n 	-bit
			secDesc 	- char(500)
			isPrincipalPaid - bit*/
			
			CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS CustomerAccount(custAccountId INTEGER primary key autoincrement, " +
																			"customerId INTEGER," +
																			"accountId INTEGER," +
																			"principalAmt REAL," +
																			"principalDate INTEGER," +
																			"interestPaidUptoDate INTEGER," +
																			"security CHAR(1)," +
																			"secDesc CHAR(500)," +
																			"isPrincipalPaid CHAR(1));";
			//db.execSQL(CREATE_TABLE_QUERY);
			Log.i(TAG, "CustomerAccount Table created");
			
			
			/*Transcations
			============
			transactionId 	-integer
			custAccountId	-integer
			dueDate		-dateTime
			paidDate	-DAteTime
			collectedby	-char(50)
			paymentMode	- Char(25)
			comments	- Char(100)
			lat		-float
			lon		-float*/

			
			CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS Transcations(transactionId INTEGER primary key autoincrement," +
																		"custAccountId INTEGER," +
																		"dueDate INTEGER," +
																		"paidDate INTEGER," +
																		"collectedby CHAR(50)," +
																		"paymentMode CHAR(25)," +
																		"comments CHAR(100)," +
																		"lat REAL," +
																		"lon REAL," +
																		"deviceId CHAR(25));";
			
			
			
			
			
			
			db.execSQL(CREATE_TABLE_QUERY);
			Log.i(TAG, "Transcations Table created");
			
			
			CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS CustomerAccount(customerAccId INTEGER primary key autoincrement," +
					"customerId INTEGER,"+
					"security CHAR(1)," +
					"principalDate INTEGER," +
					"partyName CHAR(50),"+
					"principalAmt INTEGER," +
					"interestPaidUptoDate INTEGER," +
					"accountId CHAR(10)," +
					"ContactNumber CHAR(15)," +
					"isPrincipalPaid CHAR(1));";
			
			db.execSQL(CREATE_TABLE_QUERY);
			Log.i(TAG, "PartyList Table created");
			
		}catch (Exception e) {
		}
		
	}
	
	public void dropTable(){
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		String DROP_TABLE_QUERY = "DROP TABLE IF EXISTS CustomerMaster ;";
		db.execSQL(DROP_TABLE_QUERY);
		Log.i(TAG, "CustomerMaster Table Dropped");
		
		DROP_TABLE_QUERY = "DROP TABLE IF EXISTS AccountMaster ;";
		db.execSQL(DROP_TABLE_QUERY);
		Log.i(TAG, "AccountMaster Table Dropped");
		
		DROP_TABLE_QUERY = "DROP TABLE IF EXISTS CustomerAccount ;";
		db.execSQL(DROP_TABLE_QUERY);
		Log.i(TAG, "CustomerAccount Table Dropped");
		
		DROP_TABLE_QUERY = "DROP TABLE IF EXISTS Transcations ;";
		db.execSQL(DROP_TABLE_QUERY);
		Log.i(TAG, "Transcations Table Dropped");
		
		
		
		
	}
	
	public void reCreateDataBaseTables(){
		
		

		/*
		 * Reset database table when import new file
		 */
		
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		String DROP_TABLE_QUERY = "DROP TABLE IF EXISTS CustomerMaster ;";
		db.execSQL(DROP_TABLE_QUERY);
		Log.i(TAG, "CustomerMaster Table Dropped");
		
		DROP_TABLE_QUERY = "DROP TABLE IF EXISTS AccountMaster ;";
		db.execSQL(DROP_TABLE_QUERY);
		Log.i(TAG, "AccountMaster Table Dropped");
		
		DROP_TABLE_QUERY = "DROP TABLE IF EXISTS CustomerAccount ;";
		db.execSQL(DROP_TABLE_QUERY);
		Log.i(TAG, "CustomerAccount Table Dropped");
		
		DROP_TABLE_QUERY = "DROP TABLE IF EXISTS Transcations ;";
		db.execSQL(DROP_TABLE_QUERY);
		Log.i(TAG, "Transcations Table Dropped");
		
		

		String CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS CustomerMaster (customerId INTEGER primary key autoincrement, " +
																				"name CHAR(50)," +
																				"address CHAR(250)," +
																				"ph1 CHAR(15)," +
																				"ph2 CHAR(15));";
		db.execSQL(CREATE_TABLE_QUERY);
		Log.i(TAG, "CustomerMaster Table created");
	

		
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS AccountMaster (accountId INTEGER primary key autoincrement, " +
																		"accountName CHAR(5),);";
		db.execSQL(CREATE_TABLE_QUERY);
		Log.i(TAG, "AccountMaster Table created");
		
		
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS CustomerAccount(custAccountId INTEGER primary key autoincrement, " +
																		"customerId INTEGER," +
																		"accountId INTEGER," +
																		"principalAmt REAL," +
																		"principalDate INTEGER," +
																		"interestPaidUptoDate INTEGER," +
																		"security CHAR(1)," +
																		"secDesc CHAR(500)," +
																		"isPrincipalPaid CHAR(1));";
		db.execSQL(CREATE_TABLE_QUERY);
		Log.i(TAG, "CustomerAccount Table created");
		
		
		

		
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS Transcations(transactionId INTEGER primary key autoincrement," +
																	"custAccountId INTEGER," +
																	"dueDate INTEGER," +
																	"paidDate INTEGER," +
																	"collectedby CHAR(50)," +
																	"paymentMode CHAR(25)," +
																	"comments CHAR(100)," +
																	"lat REAL," +
																	"lon REAL," +
																	"deviceId CHAR(25));";
		
		
		
		db.execSQL(CREATE_TABLE_QUERY);
		
		Log.i(TAG, "Transcations Table created");
		
		
		CREATE_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS PartyList(customerId INTEGER primary key," +
								"security CHAR(1)," +
								"principalDate INTEGER," +
								"partyName CHAR(50)," +
								"primaryAmount INTEGER," +
								"duesFrom INTEGER," +
								"inBook CHAR(10)," +
								"ContactNumber CHAR(15));";
		
		
		
		
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try{
			String ALTER_TABLE_QUERY = "ALTER TABLE Transcations  ADD COLUMN deviceId CHAR(25);";
			
			db.execSQL(ALTER_TABLE_QUERY);
		}catch (Exception e) {
		}
	}
	
	

}

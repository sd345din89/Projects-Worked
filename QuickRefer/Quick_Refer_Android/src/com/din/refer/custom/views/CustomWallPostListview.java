package com.strobilanthes.forrestii.custom.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.pulltorefresh.widget.RefreshableListView;

public class CustomWallPostListview extends RefreshableListView implements OnScrollListener{

	private static final float DEFAULT_PARALLAX_FACTOR = 1.9F;
	private float parallaxFactor = DEFAULT_PARALLAX_FACTOR;
	private ParallaxedView parallaxedView;
	private boolean isCircular;
	private OnScrollListener listener = null;

	public static final String TAG = CustomWallPostListview.class.getSimpleName();
	
	private View loadingView;
	
	// A flag to prevent loading header or footer more than once
	private boolean loadingViewVisible = false;
	
	public CustomWallPostListview(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		//init(context, attrs);
	}

	public CustomWallPostListview(Context context, AttributeSet attrs) {
		super(context, attrs);
		//init(context, attrs);
		addPullDownRefreshFeature(context);
	}

	private void addPullDownRefreshFeature(final Context context) {

		setContentView(R.layout.view_pull_to_refresh);
		mListHeaderView.setBackgroundColor(0xffe0e0e0);
		setOnHeaderViewChangedListener(new OnHeaderViewChangedListener() {

			@Override
			public void onViewChanged(View v, boolean canUpdate) {
				TextView tv = (TextView) v.findViewById(R.id.refresh_text);
				ImageView img = (ImageView) v.findViewById(R.id.refresh_icon);
				Animation anim;
				if (canUpdate) {
					anim = AnimationUtils.loadAnimation(context,
							R.anim.rotate_up);
					tv.setText(R.string.refresh_release);
				} else {
					tv.setText(R.string.refresh_pull_down);
					anim = AnimationUtils.loadAnimation(context,
							R.anim.rotate_down);
				}
				img.startAnimation(anim);
			}

			@Override
			public void onViewUpdating(View v) {
				TextView tv = (TextView) v.findViewById(R.id.refresh_text);
				ImageView img = (ImageView) v.findViewById(R.id.refresh_icon);
				ProgressBar pb = (ProgressBar) v.findViewById(R.id.refresh_loading);
				pb.setVisibility(View.VISIBLE);
				tv.setText(R.string.loading);
				img.clearAnimation();
				img.setVisibility(View.INVISIBLE);
			}

			@Override
			public void onViewUpdateFinish(View v) {
				TextView tv = (TextView) v.findViewById(R.id.refresh_text);
				ImageView img = (ImageView) v.findViewById(R.id.refresh_icon);
				ProgressBar pb = (ProgressBar) v
						.findViewById(R.id.refresh_loading);

				tv.setText(R.string.refresh_pull_down);
				pb.setVisibility(View.INVISIBLE);
				tv.setVisibility(View.VISIBLE);
				img.setVisibility(View.VISIBLE);
			}

		});
	}

	protected void init(Context context, AttributeSet attrs) {
		//TypedArray typeArray = context.obtainStyledAttributes(attrs, R.styleable.ParallaxScroll);
		//this.parallaxFactor = typeArray.getFloat(R.styleable.ParallaxScroll_parallax_factor, DEFAULT_PARALLAX_FACTOR);
		//this.isCircular = typeArray.getBoolean(R.styleable.ParallaxScroll_circular_parallax, DEFAULT_IS_CIRCULAR);
		//typeArray.recycle();
		super.setOnScrollListener(this);
	}

	/*@Override
	public void setOnScrollListener(OnScrollListener l) {
		this.listener = l;
	}*/
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
	}

	public void addParallaxedHeaderView(View v) {
		super.addHeaderView(v);
		addParallaxedView(v);
	}

	public void addParallaxedHeaderView(View v, Object data, boolean isSelectable) {
		super.addHeaderView(v, data, isSelectable);
		addParallaxedView(v);
	}

	private void addParallaxedView(View v) {
		this.parallaxedView = new ParallaxedListView(v);
	}

	protected void parallaxScroll() {
		if (isCircular)
			circularParallax();
		else
			headerParallax();
	}

	private void circularParallax() {
		if (getChildCount() > 0) {
			int top = -getChildAt(0).getTop();
			float factor = parallaxFactor;
			fillParallaxedViews();
			parallaxedView.setOffset((float)top / factor);
		}
	}

	private void headerParallax() {
		if (parallaxedView != null) {
			if (getChildCount() > 0) {
				//int top = -getChildAt(0).getTop();
				//float factor = parallaxFactor;
				//parallaxedView.setOffset((float)top);
				
				/*final ViewGroup viewGroup = (ViewGroup) getChildAt(0);
				int scrolly = -viewGroup.getTop() + getFirstVisiblePosition() * viewGroup.getHeight();
				final ImageView imageView = (ImageView) viewGroup.findViewById(R.id.wall_post_user_image);
				final TextView textView = (TextView) viewGroup.findViewById(R.id.wall_post_username);
				
				if(scrolly >= 125 && scrolly <= 300){
					//int mOffsetBetweenItems = 120;
				
					final int currentX = 0;
			        final int currentY = 0;
			        final int imageViewWidth = imageView.getWidth();
			        final int imageViewHeight = imageView.getHeight();
			        TranslateAnimation animation = new TranslateAnimation(currentX, -imageView.getLeft()+10,currentY, -imageView.getTop()+10);
			        animation.setDuration(200);
			        animation.setFillAfter(false);    
			        animation.setAnimationListener(
			            new AnimationListener() {

			                @Override
			                public void onAnimationStart(Animation animation) {}

			                @Override
			                public void onAnimationRepeat(Animation animation) {}

			                @Override
			                public void onAnimationEnd(Animation animation) {
			                	imageView.clearAnimation();
			                	android.widget.RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(imageView.getWidth(), imageView.getHeight());
			                	lp.setMargins(10, 10, 0, 0);
			                	imageView.setLayoutParams(lp);
			                	//imageView.layout(newleft, newTop, newleft + imageView.getMeasuredWidth(), newTop + imageView.getMeasuredHeight() );
			                }
			            }
			        );
			        imageView.startAnimation(animation);
			        
			        TranslateAnimation animation1 = new TranslateAnimation(currentX, (-textView.getLeft()+20+imageViewWidth),currentY, -textView.getTop()+imageViewHeight/2);
			        animation1.setDuration(200);
			        animation1.setFillAfter(false);    
			        animation1.setAnimationListener(
			            new AnimationListener() {

			                @Override
			                public void onAnimationStart(Animation animation) {}

			                @Override
			                public void onAnimationRepeat(Animation animation) {}

			                @Override
			                public void onAnimationEnd(Animation animation) {
			                	textView.clearAnimation();
			                	android.widget.RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(textView.getWidth(), textView.getHeight());
			                	lp.setMargins(imageViewWidth+20, imageViewHeight/2, 0, 0);
			                	textView.setLayoutParams(lp);
			                	//imageView.layout(newleft, newTop, newleft + imageView.getMeasuredWidth(), newTop + imageView.getMeasuredHeight() );
			                }
			            }
			        );
			        textView.startAnimation(animation1);
					
				}*/
				//Log.d("Get Scroll Top", ""+scrolly);
				//Log.d("Get Height View", ""+viewGroup.getHeight());
			}
		}
	}

	private void fillParallaxedViews() {
		if (parallaxedView == null || parallaxedView.is(getChildAt(0)) == false) {
			if (parallaxedView != null) {
				parallaxedView.setOffset(0);
				parallaxedView.setView(getChildAt(0));
			} else {
				parallaxedView = new ParallaxedListView(getChildAt(0));
			}
		}
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		parallaxScroll();
		if (this.listener != null)
			this.listener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if (this.listener != null)
			this.listener.onScrollStateChanged(view, scrollState);
	}
	
	public class ParallaxedListView extends ParallaxedView {

		public ParallaxedListView(View view) {
			super(view);
		}

		@Override
		protected void translatePreICS(View view, float offset) {
			TranslateAnimation ta = new TranslateAnimation(0, 0, offset, offset);
			ta.setDuration(0);
			ta.setFillAfter(true);
			view.setAnimation(ta);
			ta.start();
		}
	}
	
	public void setLoadingView(View loadingView) {
			this.loadingView = loadingView;
	}

    private void addLoadingView(ListView listView, View loadingView) {
    	if (listView == null || loadingView == null) {
    		return;
    	}
    	// Avoid overlapping the header or footer
    	if (!loadingViewVisible) {
				// Add loading view to list view footer when scroll down to load
			listView.addFooterView(loadingView);
			loadingViewVisible = true;
    	}
    }

    private void removeLoadingView(ListView listView, View loadingView) {
    	if (listView == null || loadingView == null) {
    		return;
    	}
    	// Remove header or footer depending on the loading mode
    	if (loadingViewVisible) {
	    	
			listView.removeFooterView(loadingView);
			
	    	loadingViewVisible = false;
    	}
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
    	// Force the list view to accept its own type of adapter
    	/*if (!(adapter instanceof InfiniteScrollListAdapter)) {
    		throw new IllegalArgumentException(InfiniteScrollListAdapter.class.getSimpleName() + " expected");
    	}
    	// Pass information to adaptor
    	InfiniteScrollListAdapter infiniteListAdapter = (InfiniteScrollListAdapter) adapter;
    	infiniteListAdapter.setInfiniteListPageListener(this);
		this.setOnScrollListener(adapter);*/
		// Workaround to keep spaces for header and footer
		View dummy = new View(getContext());
		addLoadingView(CustomWallPostListview.super, dummy);
    	super.setAdapter(adapter);
    	removeLoadingView(CustomWallPostListview.super, dummy);
    }

	public void endOfList() {
		// Remove loading view when there is no more to load
		removeLoadingView(this, loadingView);
	}

	public void hasMore() {
		// Display loading view when there might be more to load
		addLoadingView(CustomWallPostListview.this, loadingView);
	}
}

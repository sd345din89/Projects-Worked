package com.strobilanthes.forrestii.act.fragments;

import java.beans.PropertyChangeEvent;
import java.io.IOException;
import java.io.InputStream;

import roboguice.inject.InjectView;
import android.annotation.SuppressLint;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.strobilanthes.forrestii.R;
import com.strobilanthes.forrestii.database.DBAccess;

@SuppressLint("SetJavaScriptEnabled")
public class SettingsTermsConditionFragment extends BaseFragment<DBAccess> {

	@InjectView(R.id.sett_txt_terms) private TextView settTxtTerms;
	
	   private Typeface mFontLight,mFontDark;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.frg_settings_terms_condition, container, false);

		

		return view;
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		
		
		super.onCreateOptionsMenu(menu, inflater);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		return super.onOptionsItemSelected(item);
	}

	private void fontStyles() {
		mFontLight = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Light.ttf");
		mFontDark = Typeface.createFromAsset(getActivity().getAssets(),"andorid_google/Roboto-Regular.ttf");
		
	
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		fontStyles();
		AssetManager assetManager = getActivity().getAssets();
		 // To load text file
        InputStream input;
        try {
            input = assetManager.open("terms.txt");
 
             int size = input.available();
             byte[] buffer = new byte[size];
             input.read(buffer);
             input.close();
 
             // byte buffer into a string
             String text = new String(buffer);
             
             settTxtTerms.setText(text);
             settTxtTerms.setTypeface(mFontLight);
             
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
 
	}



	public boolean onBackPressed() {
		return super.onBackPressed();
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		

	}
	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onClick(View v) {
		
	}
}
